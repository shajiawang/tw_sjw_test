<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
       <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts/dist/echarts.min.js"></script>
       <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts-gl/dist/echarts-gl.min.js"></script>
       <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts-stat/dist/ecStat.min.js"></script>
       <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts/dist/extension/dataTool.min.js"></script>
       <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts/map/js/china.js"></script>
       <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts/map/js/world.js"></script>
       <script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=xfhhaTThl11qYVrqLZii6w8qE5ggnhrY&__ec_v__=20190126"></script>
       <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts/dist/extension/bmap.min.js"></script>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>儲值統計</a> >> 2019年度
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					   <div id="container" style="height: 800px"></div>
						<?php 
							foreach ($this->tplVar['status']['depositfrom'] as $key => $value) : 						
								$deposit .= "'".$value['name']."',";
							endforeach;
						?>
					   <script type="text/javascript">
							var dom = document.getElementById("container");
							var myChart = echarts.init(dom);
							var app = {};
							option = null;
							app.title = '折柱混合';

							option = {
								tooltip: {
									trigger: 'axis',
									axisPointer: {
										type: 'cross',
										crossStyle: {
											color: '#999'
										}
									}
								},
								toolbox: {
									feature: {
										dataView: {show: true, readOnly: false},
										magicType: {show: true, type: ['line', 'bar']},
										restore: {show: true},
										saveAsImage: {show: true}
									}
								},
								legend: {
									data:[<?php echo substr($deposit, 0, -1); ?>]
								},
								xAxis: [
									{
										type: 'category',
										data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
										axisPointer: {
											type: 'shadow'
										}
									}
								],
								yAxis: [
									{
										type: 'value',
										name: '金額',
										min: 0,
										max: <?php echo str_pad(1,strlen($this->tplVar['status']['alltotal']),'0',STR_PAD_RIGHT);?>,
										interval: <?php echo round((str_pad(1,strlen($this->tplVar['status']['alltotal']),'0',STR_PAD_RIGHT))/10);?>,
										axisLabel: {
											formatter: '{value} 元'
										}
									}
								],
								series: [
									<?php foreach ($this->tplVar['status']['depositfrom'] as $key => $value) : ?>
									{
										name:'<?php echo $value['name']; ?>',
										type:'bar',
										<?php 
											foreach ($this->tplVar["table"]['record'] as $key2 => $value2) : 
											if ($value['drid'] == $value2['drid']){
										?>										
										data:[<?php echo substr($value2['total'], 0, -1);?>]
										<?php
											}
											endforeach;
										?>	
									},
									<?php endforeach;?>	
								]
							};
							;
							if (option && typeof option === "object") {
								myChart.setOption(option, true);
							}
					   </script>

				</li>

				
			</ul>
		</div>
	</body>
</html>