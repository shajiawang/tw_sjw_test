<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="productid">商品編號：</label></td>
					<td><input name="productid" type="text" /></td>
				</tr>			
				<tr>
					<td><label for="name">必殺商品名稱：</label></td>
					<td><input name="name" type="text" class="required"/></td>
				</tr>
				<!--tr>
					<td><label for="link">連結網址：</label></td>
					<td><input name="link" type="text" size="60" value=""/></td>
				</tr>
				<tr>
					<td><label for="thumbnail">商品Banner：</label></td>
					<td><input name="thumbnail" type="file"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail2">商品Banner2：</label></td>
					<td><input name="thumbnail2" type="file"/></td>
				</tr>				
				<tr>
					<td><label for="thumbnail_url">商品Banner(外部圖片網址)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60"/></td>
				</tr-->
				<tr>
					<td><label for="ontime">顯示時間：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start"/></td>
				</tr>
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop"/></td>
				</tr>	
				<tr>
					<td><label for="description">敘述：</label></td>
					<td><textarea name="description" class="description"></textarea></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="1"/></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>