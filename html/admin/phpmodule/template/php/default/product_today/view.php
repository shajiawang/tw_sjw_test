<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name">商品名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th>商品名稱</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_ontime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_ontime=desc">顯示時間<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_ontime"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=">顯示時間<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=asc">顯示時間<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_offtime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_offtime=desc">下架時間<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_offtime"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=">下架時間<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=asc">下架時間<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_seq"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_seq=desc">排序<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_seq"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=">排序<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=asc">排序<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) { ?>
							<?php 
							foreach($this->tplVar["table"]['record'] as $rk => $rv) { 
							?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/todayid=<?php echo $rv["todayid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/todayid=<?php echo $rv["todayid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td>
								<?php /*<td class="column" name="todayid"><?php echo $rv['todayid']; ?></td>*/?>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="ontime"><?php echo substr($rv['ontime'],0,16); ?></td>
								<td class="column" name="offtime"><?php echo substr($rv['offtime'],0,16); ?></td>
								<td class="column" name="seq"><?php echo $rv['seq']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>