<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		});
		</script>
		
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="productid">商品編號：</label></td>
					<td><input name="productid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['productid']; ?>"/></td>
				</tr>			
				<tr>
					<td><label for="name">必殺商品名稱：</label></td>
					<td><input name="name" type="text" data-validate="required" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				
				<!--tr>
					<td><label for="link">連結網址：</label></td>
					<td><input name="link" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['link']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail">商品Banner：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/product/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail2">商品Banner2：</label></td>
					<td>
						<input name="thumbnail2" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail2'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/product/'. $this->tplVar['table']['record'][0]['thumbnail2']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail_url">商品Banner(外部圖片網址)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/></td>
				</tr-->
				<tr>
					<td><label for="ontime">顯示時間：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start" value="<?php echo $this->tplVar['table']['record'][0]['ontime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>" data-org="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">敘述：</label></td>
					<td><textarea name="description" class="description"><?php echo $this->tplVar['table']['record'][0]['description']; ?></textarea></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
			
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="todayid" value="<?php echo $this->tplVar["status"]["get"]["todayid"] ;?>">
				<input type="hidden" name="oldthumbnail" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail'] ;?>">
				<input type="hidden" name="oldptid" value="<?php echo $this->tplVar['table']['record'][0]['ptid'] ;?>">
				<input type="hidden" name="oldthumbnail2" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail2'] ;?>">
				<input type="hidden" name="oldpftid" value="<?php echo $this->tplVar['table']['record'][0]['pftid'] ;?>">				
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>