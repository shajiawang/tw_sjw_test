<?php
    $arrDef=array();
	$arrDef['status']['order']='待付款';
	$arrDef['status']['bid']='得標發送';
	$arrDef['status']['deposit']='已儲值';
	$arrDef['status']['cancel']='取消儲值';
	$arrDef['status']['writeoff']='已銷帳';
	$arrDef['status']['error']='儲值錯誤';
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				
				<li class="search-field">
				<!-- li class="search-field">
					<span class="label" for="search_datetime">儲值時間：</span>
					<span class="label" for="search_countryid">國別編號：</span>
					<input type="text" name="search_countryid" size="20" value="<?php echo $this->io->input['get']['search_countryid']; ?>"/>
				</li -->
				<li class="search-field">
					<span class="label" for="search_datetime">時間：</span>
					<input name="startdatetime" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['startdatetime']; ?>" /> ~
					<input name="enddatetime" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['enddatetime']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_depository_rule_status">儲值方式：</span>
					<select name="deposit_rule_status"><option value=""></option>
						<option value="ALL" <?php echo ( $this->io->input['get']["deposit_rule_status"]=='ALL'?"selected":""); ?> >全部</option>
					<?php
						foreach ($this->tplVar["drid_option"] as $key => $value) {
					?>
						<option value="<?php echo $value['drid']?>" <?php echo ( $this->io->input['get']["deposit_rule_status"]==$value['drid']?"selected":""); ?> ><?php echo $value['name']?></option>
					<?php
						}
					?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_depository_history_status">儲值狀態：</span>
					<select name="deposit_history_status"><option value=""></option>
						 <option value="ALL" <?php echo ( $this->io->input['get']["deposit_history_status"]=='ALL'?"selected":""); ?> >全部</option>
					   <option value="order" <?php echo ( $this->io->input['get']["deposit_history_status"]=='order'?"selected":""); ?> >待付款</option>
					    <option value="bid"  <?php echo ( $this->io->input['get']["deposit_history_status"]=='bid'?"selected":""); ?> >得標發送</option>
						 <option value="writeoff"  <?php echo ( $this->io->input['get']["deposit_history_status"]=='writeoff'?"selected":""); ?> >已銷帳</option>
						   <option value="deposit"  <?php echo ( $this->io->input['get']["deposit_history_status"]=='deposit'?"selected":""); ?>  >已儲值</option>
						   <option value="cancel"  <?php echo ( $this->io->input['get']["deposit_history_status"]=='cancel'?"selected":""); ?> >取消儲值</option>
						   <option value="error"  <?php echo ( $this->io->input['get']["deposit_history_status"]=='error'?"selected":""); ?> >儲值錯誤</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_dhid">儲值記錄編號：</span>
						<input type="text" name="dhid" value="<?php echo $this->io->input['get']['dhid']; ?>" />
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_depository_history_status">含測試人員：</span>
					<select name="tester"><option value=""></option>
					   <option value="0" <?php echo ( $this->io->input['get']["tester"]=='0'?"selected":""); ?> >是</option>
					   <option value="1" <?php echo ( $this->io->input['get']["tester"]=='1'?"selected":""); ?> >否</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<?php if ($rv['status'] != 'cancel') { ?>
								<th></th>
								<?php } ?>
								<!--<th></th>-->
								<!--<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_dhid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_dhid=desc">儲值帳目編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_dhid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_dhid=">儲值帳目編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_dhid=asc">儲值帳目編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>-->
								<th>會員編號</th>
								<th>會員帳號</th>
								<th>暱稱</th>
								<!--th>加入日期</th -->
								<th>儲值單號</th>
								<th>儲值記錄編號</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">儲值時間<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">儲值時間<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">儲值時間<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>儲值方式</th>
								<th>儲值狀態</th>
								<th>台幣儲值金額</th>
								<th>外幣幣別</th>
								<th>外幣儲值金額</th>
								<th>稅額</th>
								<th>獲得殺價幣</th>
								<th>修改者</th>
								<th>來源位置</th> 
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) :?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<?php if ($rv['status'] != 'cancel') { ?>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?dhid=<?php echo $rv["dhid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<?php } else { ?>
								<td class="column"></td>
								<?php } ?>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="login_name"><?php echo $rv['login_name']; ?></td>
								<td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<!-- td class="column" name="login_insertt"><?php echo $rv['login_insertt']; ?></td -->
								<td class="column" name="depositid"><?php echo $rv['depositid']; ?></td>
								<td class="column" name="dhid"><?php echo $rv['dhid']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="behav"><?php echo $rv['behav']; ?></td>
								<td class="column" name="status"><?php echo $arrDef['status'][$rv['status']]; ?></td>
								<td class="column" name="amount"><?php echo intval($rv['amount'] * 100) / 100; ?></td>
								<td class="column" name="ori_currency"><?php echo $rv['ori_currency']; ?></td>
								<td class="column" name="ori_amount"><?php echo intval($rv['ori_amount'] * 100) / 100; ?></td>
								<td class="column" name="tax"><?php 
								if ( $rv['drid']==12 || $rv['drid']==13) {
									echo round(intval($rv['spoints_by_deposit'] * 5) / 100);
								}else if($rv['drid']==8){
									echo round(intval($rv['spoints_by_deposit'] * 8) / 100);
								}else{
									echo 0;
								} ?>
								</td>
								<td class="column" name="spoints_by_deposit"><?php echo round(intval($rv['spoints_by_deposit'] * 100) / 100); ?></td>
								<td class="column" name="modifiername"><?php echo $rv['modifiername']; ?></td>
								<td class="column" name="src_ip"><?php echo $rv['src_ip']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
					<!--div style="float:left;color: #4b6d23;">儲值金額總共 <?php echo intval($this->tplVar["Total"]["TotalAmount"] * 100 * 37) / 100; ?> 元 / 儲值筆數共 <?php IF($this->tplVar["Total"]["TotalCount"]  > 0 ){ echo $this->tplVar["Total"]["TotalCount"] * 37 + 16687; }else{ echo 0; } ?> 筆</div-->
					<div style="float:left;color: #4b6d23;">儲值金額總共 <?php echo round($this->tplVar["Total"]["TotalAmount"]); ?> 元 / 儲值筆數共 <?php IF($this->tplVar["Total"]["TotalCount"]  > 0 ){ echo $this->tplVar["Total"]["TotalCount"]; }else{ echo 0; } ?> 筆</div>
				
				<!-- Page Start -->
				 <?php echo ("<!-- ".$this->tplVar["block"]["page"]." -->"); ?> 
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>
