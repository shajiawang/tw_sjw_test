<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="dhid">儲值帳目ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['dhid']; ?></td>
				</tr>
				<tr>
					<td><label for="userid">會員ID：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>
				<tr>
					<td><label for="amount">儲值項目：</label></td>
					<td><?php echo $this->tplVar['table']['deposit_rule'][0]['name']; ?></td>
				</tr>
				<tr>
					<td><label for="amount">儲值金額：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['amount']; ?></td>
				</tr>
				<tr>
					<td><label for="behav">儲值行為：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['behav']; ?></td>
				</tr>
				<tr>
					<td><label for="rule">儲值明細：</label></td>
					<td><textarea name="rule"><?php echo ($this->tplVar['table']['record'][0]['data']); ?></textarea></td>
				</tr>				
				<tr>
					<td><label for="status">儲值狀態：</label></td>
					<td>
						<select name="status" id="status">
						<?php
							if ($this->tplVar['table']['record'][0]['status'] == 'order') 
							{
								echo '<option value="order" selected>儲值訂單</order>';
								echo '<option value="deposit">已儲值</order>';
							}
							else if ($this->tplVar['table']['record'][0]['status'] == 'deposit') 
							{
								echo '<option value="deposit" selected>已儲值</order>';
								echo '<option value="cancel">取消儲值</order>';
							}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="account">儲值帳號：</label></td>
					<td>
					<?php
						$account = json_decode($this->tplVar['table']['record'][0]['data'], true);
						
						if (empty($account['trade_no'])) {
							echo '
					<input type="text" name="account" id="account" value="" />';
						}
						else {
							echo '
					<input type="hidden" name="account" id="account" value="'.$account['trade_no'].'" />'.$account['trade_no'];
						}
					?>
					</td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="countryid">國別ID：</label></td>
					<td><select name="countryid">
						<?php foreach ($this->tplVar['table']['rt']['country_rt'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['countryid'];?>" <?php if($this->tplVar['table']['record'][0]['countryid']==$pv['countryid']){ echo "selected"; }?> ><?php echo $pv['name'];?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="currency">幣別 ：</label></td>
					<td><input name="currency" type="text" value="<?php echo $this->tplVar['table']['record'][0]['currency']; ?>"/></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="dhid" value="<?php echo $this->tplVar["status"]["get"]["dhid"]; ?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']['record'][0]['userid']; ?>">
				<input type="hidden" name="oldstatus" value="<?php echo $this->tplVar['table']['record'][0]['status']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>