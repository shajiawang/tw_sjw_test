<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<?php //echo json_encode($this->tplVar['table']['deposit_rule_item']); ?>
	<script>
		$(document).ready(function() {
			var dr = '<option value="999">其它 (須手動加券)</option>';
			var driJson = <?php echo json_encode($this->tplVar['table']['deposit_rule_item']); ?>;
            var driArray = Object.keys(driJson).map(function(_) { return driJson[_]; });            //json轉陣列
			for (var i = 0; i < driArray[0].length; i++)
			{
				dr += '<option value="' + driArray[0][i]['driid'] + '">' + Math.round(driArray[0][i]['amount']) + '</option>';
			}
			// $('#other').prop('disabled', true);
			$('#driid').html(dr);
			
			$('#drid').change(function(e) {
				
				var drid = $('#drid option:selected').attr('id');
				var dr = '<option value="999">其它 (須手動加券)</option>';
				for (var i = 0; i < driArray[drid].length; i++)
				{
					dr += '<option value="' + driArray[drid][i]['driid'] + '">' + Math.round(driArray[drid][i]['amount']) + '</option>';
				}
				$('#other').prop('disabled', false);			

				$('#driid').html(dr);
				oc();
				

				switch($('#drid').val()) {
					case "8":
						$('#other').prop('disabled', false);
						$('#tax').prop('disabled', false);
						$('#num').prop('disabled', false);
						$('#ratio').prop('disabled', false);
						$('#total').prop('disabled', false);
						$('#ori_amount').prop('disabled', true);
						$('#ratio').val(1.08);
						$("#ori_currency").val("");
						break;

					case "12":
					case "13":
						$('#other').prop('disabled', false);
						$('#tax').prop('disabled', false);
						$('#num').prop('disabled', false);
						$('#ratio').prop('disabled', false);
						$('#total').prop('disabled', false);
						$('#ori_amount').prop('disabled', true);
						$('#ratio').val(1.05);
						$("#ori_currency").val("");
						break;

					case "17":
						$('#other').prop('disabled', true);
						$('#tax').prop('disabled', true);
						$('#ratio').prop('disabled', true);
						$('#total').prop('disabled', true);
						$('#num').prop('disabled', false);
						$('#ori_amount').prop('disabled', false);
						$('#ratio').val("");
						$("#ori_currency").val("MYR");
						break;

					case "20":
						$('#other').prop('disabled', true);
						$('#tax').prop('disabled', true);
						$('#ratio').prop('disabled', true);
						$('#total').prop('disabled', true);
						$('#num').prop('disabled', false);
						$('#ori_amount').prop('disabled', false);
						$('#ratio').val("");
						$("#ori_currency").val("RMB");
						break;

					case "23":
						$('#other').prop('disabled', true);
						$('#tax').prop('disabled', true);
						$('#ratio').prop('disabled', true);
						$('#total').prop('disabled', true);
						$('#num').prop('disabled', false);
						$('#ori_amount').prop('disabled', false);
						$('#ratio').val("");
						$("#ori_currency").val("HKD");
						break;

					default:
						break;
				}		
			});

			$('#driid').change(function(e) {
				var driid = $('#driid').val();
				if (driid == 999){
					if ($('#drid').val()=="17" || $('#drid').val()=="20" || $('#drid').val()=="23") {
						$('#ori_amount').prop('disabled', false);
						$('#num').prop('disabled', false);
					}else{
						$('#other').prop('disabled', false);
						$('#tax').prop('disabled', false);
						$('#num').prop('disabled', false);
						$('#ratio').prop('disabled', false);
						$('#total').prop('disabled', false);
					}
					oc();
				}else{
					$('#other').prop('disabled', true);
					$('#tax').prop('disabled', true);
					$('#num').prop('disabled', true);
					$('#ratio').prop('disabled', true);
					$('#total').prop('disabled', true);
					$('#ori_amount').prop('disabled', true);
					
					$('#other').val(0);
					$('#tax').val(0);
					$('#num').val(0);
					$('#total').val(0);
				}		
			});
			
			$('#ratio').keyup(function(e) {
				oc();
			});
			
			$('#other').keyup(function(e) {
				oc();
			});
			
			function oc(){
				var drid = $('#drid').val();
				var other = $('#other').val();
				var ratio = $('#ratio').val();
				
				if (drid==8 || drid==12 || drid==13){
					num = other/ratio;
					tax = other - num;
					$('#tax').val(Math.round(tax));
					$('#num').val(Math.round(num));
					$('#total').val(Math.round(num));
				}else{
					$('#total').val($('#num').val());
				}
			}
			
		});
	</script>
	
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>

		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">會員ID：</label></td>
					<td><input name="userid" type="text" value="" /></td>
				</tr>
				<tr>
					<td><label for="drid">儲值項目：</label></td>
					<td>
						<select name="drid" id="drid" >
						<?php foreach ($this->tplVar['table']['deposit_rule'] as $pk => $pv) : ?>
							<option id="<?php echo $pk;?>" value="<?php echo $pv['drid'];?>" ><?php echo $pv['name'];?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="driid">儲值金額：</label></td>
					<td>
						<select name="driid" id="driid"></select>
					</td>
				</tr>
				<tr>
					<td><label for="behav">儲值輸入金額：</label></td>
					<td><input name="other" id="other" type="number" value="0" /></td>
				</tr>
				<tr>
					<td><label for="behav">稅比例：</label></td>
					<td>
						<input name="ratio" id="ratio" type="text" value="1.08" />
					</td>
				</tr>				

				<tr>
					<td><label for="behav">發票稅金額：</label></td>
					<td>
						<input name="tax" id="tax" type="number" value="0" />
					</td>
				</tr>

				<tr>
					<td><label for="behav">加點數量：</label></td>
					<td>
						<input name="num" id="num" type="number" value="0" />
					</td>
				</tr>	

				<tr>
					<td><label for="ori_amount">外幣儲值金額：</label></td>
					<td>
						<input name="ori_amount" id="ori_amount" type="number" value="0" disabled="disabled" />
					</td>
				</tr>				

				<tr>
					<td><label for="behav">儲值行為：</label></td>
					<td>
						<input name="behav" type="text" value="user_deposit"/>
					</td>
				</tr>
				<tr>
					<td><label for="behav">儲值狀態：</label></td>
					<td>
						<select name="status" id="status">
							<option value="deposit">已儲值</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="behav">儲值帳號：</label></td>
					<td><input name="account" type="text" value=""/></td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="countryid">國別ID：</label></td>
					<td><select name="countryid" disabled="disabled">
						<?php foreach ($this->tplVar['table']['rt']['country_rt'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['countryid'];?>" ><?php echo $pv['name'];?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="currency">幣別 ：</label></td>
					<td><input name="currency" type="text" value="NTD" readonly="readonly"/></td>
				</tr>
				<tr>
					<td><label for="ori_currency">外國幣別 ：</label></td>
					<td><input name="ori_currency" id="ori_currency" type="text" value="" readonly="readonly"/></td>
				</tr>
				<tr>
					<td><label for="is_other">開發票：</label></td>
					<td>
						<select name="is_other">
							<option value="N" selected >是</option>
							<option value="Y" >否</option>
						</select>
					</td>
				</tr>	
				<!-- tr>
					<td><label for="is_other">特殊收取金(不開發票)：</label></td>
					<td>
						<select name="is_other">
							<option value="N" selected >否</option>
							<option value="Y" >是</option>
						</select>
					</td>
				</tr -->				
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="total" id="total" value="0">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>