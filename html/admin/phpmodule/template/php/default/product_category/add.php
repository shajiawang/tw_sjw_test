<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
			$('select[name=layer]').change(function(){
				if ($(this).val() == 2) {
					$('select[name=node] option')
					.filter(':eq(0)').prop('disabled', true).end()
					.filter(':gt(0)').prop('disabled', false).end()
					.filter(':eq(1)').prop('selected', true)
				}
				else {
					$('select[name=node] option')
					.filter(':eq(0)').prop('disabled', false).prop('selected', true).end()
					.filter(':gt(0)').prop('disabled', true)
				}
			}).change();
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">競標商品分類管理</a>>><a>新增</a>
		</div>
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="layer">分類層級：</label></td>
					<td>
						<select name="layer">
							<option value="1" selected>1</option>
							<option value="2">2</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="node">上層分類：</label></td>
					<td>
						<select name="node">
							<option value="0">無上層</option>
						<?php foreach($this->tplVar['table']['rt']['product_category'] as $pck => $pcv) : ?>
							<option value="<?php echo $pcv['pcid'] ?>"><?php echo $pcv['name'] ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="ptype">商品分類類型：</label></td>
					<td>
						<select name="ptype">
							<option value="0" selected>殺戮分類</option>
							<option value="1">圓夢分類</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="name">商品分類名稱：</label></td>
					<td><input name="name" type="text"/></td>
				</tr>
				<tr>
					<td><label for="description">商品分類敘述：</label></td>
					<td><input name="description" type="text"/></td>
				</tr>
				<tr>
					<td><label for="seq">商品分類排序：</label></td>
					<td><input name="seq" type="text"/></td>
				</tr>
				<tr>
					<td><label for="seq">商品分類啟用：</label></td>
					<td>
						<select name="switch">
							<option value="Y">啟用</option>
							<option value="N">停用</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>