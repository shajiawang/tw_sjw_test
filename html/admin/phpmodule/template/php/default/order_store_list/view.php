<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<!--<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_channelid">頻道：</span>
					<select name="search_channelid"><option value="" > </option>
						<?php foreach ($this->tplVar['table']['rt']['channel_rt'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['channelid']; ?>" <?php echo ($this->io->input['get']['search_channelid']==$cv['channelid']) ? "selected" : "";?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_name">公告標題：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_description">公告內容：</span>
					<input type="text" name="search_description" size="20" value="<?php echo $this->io->input['get']['search_description']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>-->
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<!--<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>	-->					
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_evrid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_evrid=desc">訂單ID<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_evrid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_evrid=">訂單ID<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_evrid=asc">訂單ID<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>訂單名稱</th>
								<th>會員暱稱</th>
								<th>商家名稱</th>
								<th>商品名稱</th>
								<th>商品單價</th>
								<th>商品數量</th>
								<th>商品總金額</th>
								<th>備註</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_commit_time"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_commit_time=desc">訂單完成日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_commit_time"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_commit_time=">訂單完成日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_commit_time=asc">訂單完成日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="evrid"><?php echo $rv['evrid']; ?></td>
								<td class="column" name="prod_name"><?php echo $rv['prod_name']; ?></td>
								<td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<td class="column" name="companyname"><?php echo $rv['companyname']; ?></td>
								<td class="column" name="pname"><?php echo $rv['pname']; ?></td>
								<td class="column" name="unit_price"><?php echo (int)$rv['unit_price']; ?></td>
								<td class="column" name="tx_quantity"><?php echo (int)$rv['tx_quantity']; ?></td>
								<td class="column" name="total_bonus"><?php echo (int)$rv['total_bonus']; ?></td>
								<td class="column" name="memo"><?php echo nl2br($rv['memo']); ?></td>
								<td class="column" name="commit_time"><?php echo $rv['commit_time']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>