<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="drid">儲值規則ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['drid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">名稱：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="channelid">敘述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="mark">幣別符號：</label></td>
					<td><input name="mark" type="text" value="<?php echo $this->tplVar['table']['record'][0]['mark']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="type">儲值類型：</label></td>
					<td>
						<select name="type">
							<option value="">請選擇</option>
							<option value="1"   <?php echo $this->tplVar['table']['record'][0]['type']=="1"?" selected":""; ?>>信用卡</option>
							<option value="2"   <?php echo $this->tplVar['table']['record'][0]['type']=="2"?" selected":""; ?>>匯款</option>
							<option value="3"   <?php echo $this->tplVar['table']['record'][0]['type']=="3"?" selected":""; ?>>QR CODE</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="remark">付款說明事項：</label></td>
					<td><textarea name="remark" id="ckeditor_remark" class="ckeditor"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['remark']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="switch">啟用：</label></td>
					<td>
						<select name="switch">
							<option value="Y"<?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>啟用</option>
							<option value="N"<?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>停用</option>
						</select>
					</td>
				</tr>
			</table>
			
			<?php /*<table class="form-table">
				<tr>
					<td><label for="channelid">頻道：</label></td>
					<td>
						<select name="channelid">
						<?php foreach ($this->tplVar['table']['rt']['channel_rt'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['channelid']; ?>" ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
			</table>*/?>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="drid" value="<?php echo $this->tplVar["status"]["get"]["drid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>