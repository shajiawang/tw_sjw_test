<!DOCTYPE html>
<?php
if ($this->tplVar['table']['record'][0]['plid'] == 11) {
	$worning_text = "*新手限定類別不提供更改！";
}else{
	$worning_text = "";
}

?>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<script type="text/javascript">
		$(document).ready(function() {
			<?php if ($this->tplVar['table']['record'][0]['kind'] == "any" ){ ?>
			$('#mvalue').prop( "disabled", true );
			$('#svalue').prop( "disabled", true );
			<?php } else if ($this->tplVar['table']['record'][0]['kind'] == "big" ){ ?>
			$('#mvalue').prop( "disabled", true );
			$('#svalue').prop( "disabled", false );
			<?php } else if ($this->tplVar['table']['record'][0]['kind'] == "small" ){ ?>
			$('#mvalue').prop( "disabled", false );
			$('#svalue').prop( "disabled", true );
			<?php } ?>
			
			$('#kind').change(function() {
				if( $(this).val() == 'any') {
					$('#mvalue').prop( "disabled", true );
					$('#svalue').prop( "disabled", true );
				} else if( $(this).val() == 'big') {      
					$('#mvalue').prop( "disabled", true );
					$('#svalue').prop( "disabled", false );
				} else if( $(this).val() == 'small') {
					$('#mvalue').prop( "disabled", false );
					$('#svalue').prop( "disabled", true );
				}
			});

			// 新手限定不能動
			<?php if ($this->tplVar['table']['record'][0]['plid'] == 11) { ?>
				 $(":input").prop( "disabled", true );
			<?php } ?>

		});		
	</script>	
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="plid">限定編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['plid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">限定名稱：</label></td>
					<td><input name="name" type="text" maxlength="5" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/>*名稱最多輸入5個字</td>
				</tr>
				<tr>
					<td><label for="description">限定描述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="kind">選擇得標限定次數：</label></td>
					<td><select name="kind" id="kind">
							<option value="any" <?php echo $this->tplVar['table']['record'][0]['kind']=="any"?" selected":""; ?> >不限</option>
							<option value="big" <?php echo $this->tplVar['table']['record'][0]['kind']=="big"?" selected":""; ?> >大於</option>
							<option value="small" <?php echo $this->tplVar['table']['record'][0]['kind']=="small"?" selected":""; ?> >小於</option>
					</select></td>
				</tr>	
				<tr>
					<td><label for="seq">限定得標限定次數：</label></td>
					<td><input name="mvalue" id="mvalue" type="text" value="<?php echo $this->tplVar['table']['record'][0]['mvalue']; ?>"/>次以內</td>
				</tr>
				<tr>
					<td><label for="seq">限定得標限定次數：</label></td>
					<td><input name="svalue" id="svalue" type="text" value="<?php echo $this->tplVar['table']['record'][0]['svalue']; ?>"/>次以上</td>
				</tr>
				<tr>
					<td><label for="limit_group">下標合併計算</label></td>
					<td>
						<select name="limit_group" id="limit_group">
							<option value="0" <?php echo $this->tplVar['table']['record'][0]['limit_group']=="0"?" selected":""; ?> >不計算</option>
							<option value="1" <?php echo $this->tplVar['table']['record'][0]['limit_group']=="1"?" selected":""; ?> >新手</option>
							<option value="2" <?php echo $this->tplVar['table']['record'][0]['limit_group']=="2"?" selected":""; ?> >老手</option>
							<option value="3" <?php echo $this->tplVar['table']['record'][0]['limit_group']=="3"?" selected":""; ?> >其他</option>
						</select>
					</td>
				</tr>			
				<tr>
					<td><label for="thumbnail">類別圖片：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/product/limit/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="colorid">類別顏色：</label></td>
					<td><select name="colorid" style="background-color:#<?php echo $this->tplVar['table']['record'][0]['colorcode']; ?>" onchange="this.style.backgroundColor=this.options[this.selectedIndex].style.backgroundColor">
						<option value="0">請選擇</option>
					<?php foreach ($this->tplVar['table']['rt']['color'] as $pck => $pcv) : ?>
						<option style="background-color:#<?php echo $pcv['colorcode']; ?>" value="<?php echo $pcv['cid']; ?>" <?php echo $this->tplVar['table']['record'][0]['colorid']==$pcv['cid']?" selected":""; ?> ><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select></td>
				</tr>
				<tr>
					<td><label for="used">商品分類啓用：</label></td>
					<td><select name="used">
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['used']=="N"?" selected":""; ?>>關閉</option>
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['used']=="Y"?" selected":""; ?> >啓用</option>
					</select></td>
				</tr>					
				<tr>
					<td><label for="seq">限定排序：</label></td>
					<td><input name="seq" id="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>			
			</table>
			
			<div class="functions">
				<input type="hidden" name="oldthumbnail" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail'];?>">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="plid" value="<?php echo $this->tplVar["status"]["get"]["plid"] ;?>">
				<div class="button submit">
				<?php if ($this->tplVar['table']['record'][0]['plid'] != 11) { ?>
					<input type="submit" value="送出" class="submit">
				<?php }?>
				</div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div style="color: red;"><?php echo $worning_text; ?></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>