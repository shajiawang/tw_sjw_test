<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#mvalue').prop( "disabled", true );
			$('#svalue').prop( "disabled", true );
			
			$('#kind').change(function() {
				if( $(this).val() == 'any') {
					$('#mvalue').prop( "disabled", true );
					$('#svalue').prop( "disabled", true );
				} else if( $(this).val() == 'big') {      
					$('#mvalue').prop( "disabled", true );
					$('#svalue').prop( "disabled", false );
				} else if( $(this).val() == 'small') {
					$('#mvalue').prop( "disabled", false );
					$('#svalue').prop( "disabled", true );
				}
			});	
		});		
	</script>	
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">限定名稱：</label></td>
					<td><input name="name" id="name" maxlength="5" type="text"/>*名稱最多輸入5個字</td>
				</tr>
				<tr>
					<td><label for="description">限定描述：</label></td>
					<td><input name="description" id="description" type="text" value=""/></td>
				</tr>
				<tr>
					<td><label for="kind">選擇得標限定次數：</label></td>
					<td><select name="kind" id="kind">
							<option value="any" selected >不限</option>
							<option value="big" >大於</option>
							<option value="small" >小於</option>
					</select></td>
				</tr>	
				<tr>
					<td><label for="seq">限定得標限定次數：</label></td>
					<td><input name="mvalue" id="mvalue" type="text" value="0"/>次以內</td>
				</tr>
				<tr>
					<td><label for="seq">限定得標限定次數：</label></td>
					<td><input name="svalue" id="svalue" type="text" value="0" />次以上</td>
				</tr>
				<tr>
					<td><label for="limit_group">下標合併計算</label></td>
					<td>
						<select name="limit_group" id="limit_group">
							<option value="0">不計算</option>
							<option value="1">新手</option>
							<option value="2">老手</option>
							<option value="3">其他</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail">類別圖片：</label></td>
					<td><input name="thumbnail" type="file"/></td>
				</tr>	
				<tr>
					<td><label for="colorid">類別顏色：</label></td>
					<td><select name="colorid" onchange="this.style.backgroundColor=this.options[this.selectedIndex].style.backgroundColor" >
						<option value="0">請選擇</option>
					<?php foreach ($this->tplVar['table']['rt']['color'] as $pck => $pcv) : ?>
						<option style="background-color:#<?php echo $pcv['colorcode']; ?>" value="<?php echo $pcv['cid']; ?>" ><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select></td>
				</tr>
				<tr>
					<td><label for="used">商品分類啓用：</label></td>
					<td><select name="used">
							<option value="N" >關閉</option>
							<option value="Y" selected >啓用</option>
					</select></td>
				</tr>	
				<tr>
					<td><label for="seq">限定排序：</label></td>
					<td><input name="seq" id="seq" type="text" value="0"/></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>