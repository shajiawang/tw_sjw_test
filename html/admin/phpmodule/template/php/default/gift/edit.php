<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="giid">禮券帳目ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['giid']; ?></td>
				</tr>
				<tr>
					<td><label for="userid">會員ID：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>
				<tr>
					<td><label for="amount">禮券數量：</label></td>
					<td><input name="amount" type="text" value="<?php echo $this->tplVar['table']['record'][0]['amount']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="behav">禮券行為：</label></td>
					<td><input name="behav" type="text" value="<?php echo $this->tplVar['table']['record'][0]['behav']; ?>"/></td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="countryid">國別ID：</label></td>
					<td><select name="countryid">
						<?php foreach ($this->tplVar['table']['rt']['country_rt'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['countryid'];?>" <?php if($this->tplVar['table']['record'][0]['countryid']==$pv['countryid']){ echo "selected"; }?> ><?php echo $pv['name'];?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="giid" value="<?php echo $this->tplVar["status"]["get"]["giid"]; ?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']['record'][0]['userid']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>