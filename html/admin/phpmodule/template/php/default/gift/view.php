<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_userid">會員ID：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_countryid">國別ID：</span>
					<input type="text" name="search_countryid" size="20" value="<?php echo $this->io->input['get']['search_countryid']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_giid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_giid=desc">禮券帳目ID<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_giid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_giid=">禮券帳目ID<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_giid=asc">禮券帳目ID<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>會員ID</th>
								<th>國別ID</th>
								<th>禮券數量</th>
								<th>禮券行為</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/giid=<?php echo $rv["giid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/giid=<?php echo $rv["giid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="giid"><?php echo $rv['giid']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="countryid"><?php echo $rv['countryid']; ?></td>
								<td class="column" name="amount"><?php echo $rv['amount']; ?></td>
								<td class="column" name="behav"><?php echo $rv['behav']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>