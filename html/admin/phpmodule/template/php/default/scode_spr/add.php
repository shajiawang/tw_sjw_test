<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label"><?php echo $this->tplVar["page_name"]; ?>：新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">顯示名稱：<br>(顯示於儲值頁面)</label></td>
					<td><input name="name" type="text"/></td>
				</tr>
				
				<?php if($this->tplVar["status"]['get']['t']=='c'){ ?>
				<tr>
					<td><label for="amount">充值滿額數量：</label></td>
					<td><input name="amount" type="text" value="1"/></td>
				</tr>
				<?php }else{ ?>
				<input type="hidden" name="amount" value="1">
				<?php } ?>
				
				<tr>
					<td><label for="num">贈送組數：</label></td>
					<td><input name="num" type="text" value="1"/></td>
				</tr>
				
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="0"/></td>
				</tr>
				
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="spid">活動主檔ID：</label></td>
					<td><select name="spid">
						<?php foreach ($this->tplVar['table']['rt']['promote'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['spid'];?>" ><?php echo $pv['name'];?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				
				<?php if ($this->tplVar["status"]['get']['t'] == 'c') { ?>
				<tr>
					<td><label for="description">儲值項目：</label></td>
					<td>
					<select name="driid" id="driid">
						<?php foreach ($this->tplVar['table']['rt']['deposit_rule_item'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['driid']; ?>" ><?php echo $pcv['name'] .' -- '. $pcv['dr_name'] ; ?></option>
						<?php endforeach; ?>
					</select>
					</td>
				</tr>
				<?php } ?>					
				
			</table>
			
			<div class="functions">
				<input type="hidden" name="behav" value="<?php echo $this->tplVar["status"]['get']['t']; ?>">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>