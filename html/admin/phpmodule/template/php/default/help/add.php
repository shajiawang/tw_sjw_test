<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="0"/></td>
				</tr>			
				<tr>
					<td><label for="hcid">分類：</label></td>
					<td>
						<select name="hcid">
						<?php foreach($this->tplVar['table']['rt']['help_category'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['hcid']; ?>" <?php if($this->tplVar['table']['record'][0]['hcid']==$cv['hcid']){ echo 'selected'; } ?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>			
				<tr>
					<td><label for="name">問答標題：</label></td>
					<td><input name="name" type="text" maxlength="100" /></td>
				</tr>
				<tr>
					<td><label for="description">問答內容：</label></td>
					<td><textarea name="description" id="ckeditor_description"></textarea></td>
				</tr>

			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>