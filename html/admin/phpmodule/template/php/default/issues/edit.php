<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="fcid">分類：</label></td>
					<td>
						<select name="category">
						<?php
							foreach($this->tplVar["table"]["rt"]['user_issues_category'] as $key => $value) {
								if ($value['uicid'] == $this->tplVar['table']['record'][0]['uicid']) {
									echo '<option value="'.$value['uicid'].'" selected>'.$value['name'].'</option>';
								} else {
									echo '<option value="'.$value['uicid'].'" >'.$value['name'].'</option>';
								}
							}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="rpfid">結案狀態：</label></td>
					<td>
						<?php if($this->tplVar['table']['record'][0]['status']=='3'){ ?>
						<input type="checkbox" name="isclose" value="Y" checked />
						<?php }else{ ?>
						<input type="checkbox" name="isclose" value="Y" />
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td><label for="seq">顯示：</label></td>
					<td>
						<select name="switch">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>顯示</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>不顯示</option>
						</select>
					</td>
				</tr>
				
				<tr>
					<td><label for="name">留言標題：</label></td>
					<td><input name="title" type="text" data-validate="required" value="<?php echo $this->tplVar['table']['record'][0]['title']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">留言內容：</label></td>
					<td><textarea name="content" class="description" cols="40"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['content']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail">主圖一：</label></td>
					<td>
						<?php if (!empty($this->tplVar['table']['record'][0]['ispid'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/issues/'. $this->tplVar['table']['record'][0]['ispid']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail2">主圖二：</label></td>
					<td>
						<?php if (!empty($this->tplVar['table']['record'][0]['ispid2'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/issues/'. $this->tplVar['table']['record'][0]['ispid2']; ?>" target="_blank">預覽</a>  
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail3">主圖三：</label></td>
					<td>
						<?php if (!empty($this->tplVar['table']['record'][0]['ispid3'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/issues/'. $this->tplVar['table']['record'][0]['ispid3']; ?>" target="_blank">預覽</a> 
						<?php endif; ?>
					</td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="uiid" value="<?php echo $this->tplVar["status"]["get"]["uiid"];?>">
				<input type="hidden" name="uirid" value="<?php echo $this->tplVar['table']['record'][0]["uirid"];?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>