<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">留言管理</a>>>
			<a>回覆</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_uid">會員ID：</span>
					<input type="text" name="search_uid" size="20" value="<?php echo $this->io->input['get']['search_uid']; ?>"/>
				</li>
				<li class="search-field">
					<?php 
					$search_stat = empty($this->tplVar['status']['search']["search_stat"]) ? $this->io->input['get']['search_stat'] : $this->tplVar['status']['search']["search_stat"]; 
					?>
					<span class="label" for="search_stat">回覆狀態：</span>
					<select name="search_stat">
						<option value="-1" <?php echo ($search_stat=='-1')?'selected':'';?> >All</option>
						<option value="1" <?php echo ($search_stat=='1')?'selected':'';?> >客服未回</option>
						<option value="2" <?php echo ($search_stat=='2')?'selected':'';?> >用戶未讀</option>
						<option value="3" <?php echo ($search_stat=='3')?'selected':'';?> >已結案</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_category">分類：</span>
					<select name="search_category">
						<option value="">All</option>
						<?php
							if (is_array($this->tplVar["table"]["rt"]["issues_category"])) {
								foreach ($this->tplVar["table"]["rt"]["issues_category"] as $key => $value) {
									if ($this->io->input['get']['search_category'] == $value['uicid']) {
										echo '<option value="'.$value['uicid'].'" selected>'.$value['name'].'</option>';
									}
									else {
										echo '<option value="'.$value['uicid'].'">'.$value['name'].'</option>';
									}
								}
							}
						?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_keyword">關鍵字：</span>
					<input type="text" name="search_keyword" size="20" value="<?php echo $this->io->input['get']['search_keyword']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_name">主題：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<!--div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div-->						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>刪除</th>
								<th>回覆狀態</th>
								<th>分類</th>
								<th>會員</th>
								<th>提問主題</th>
								<?php /*<th>內容</th>*/?>
								<th>附加訊息</th>
								
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if (is_array($this->tplVar["table"]['record'])) {
									foreach($this->tplVar["table"]['record'] as $rk => $rv) {
										$TypeName = ""; 
										if (is_array($this->tplVar["table"]["rt"]["issues_category"])) {
											foreach ($this->tplVar["table"]["rt"]["issues_category"] as $key => $value) {
												if ($rv['uicid'] == $value['uicid']) {
													$TypeName .= $value['name']." ";
												}
											}
										}
							?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/uiid=<?php echo $rv["uiid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/uiid=<?php echo $rv["uiid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td>
								<td class="column" name="status">
									<a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/reply/uiid=<?php echo $rv["uiid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Reply'>
									<?php if ($rv['status'] == '3'){ ?>
										已結案
									<?php } elseif ($rv['status'] == '2'){ ?>
										用戶未讀
									<?php } else { ?>
										客服未回(或新提問)
									<?php } ?>
									</a>
								</td>								
								<td class="column" name="TypeName"><?php echo $TypeName; ?></td>
								
								<td class="column" name="userid"><?php echo $rv['userid']; ?>
								</td>
								<?php /*<td class="column" name="name"><?php echo $rv['title']; ?></td>*/?>
								<td class="column" name="description"><?php echo mb_substr($rv['content'], 0, 30, 'utf-8')."..."; ?></td>
								<td class="column" name="info"><?php echo empty($rv['info'])?'':mb_substr($rv['info'], 0, 20, 'utf-8')."..."; ?></td>
								
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>