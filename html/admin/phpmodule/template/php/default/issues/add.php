<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="rcid">回覆方式：</label></td>
					<td>
						<select name="msgtype">
						<?php foreach($this->tplVar['table']['rt']['user_issues_category'] as $key => $value) : ?>
							<option value="<?php echo $value['uicid']; ?>"><?php echo $value['name']; ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="name">主題：</label></td>
					<td><input name="name" type="text" class="required"/></td>
				</tr>
				<tr>
					<td><label for="description">敘述：</label></td>
					<td><textarea name="description" class="description" cols="40"></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail_url">主圖(外部圖片網址)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60"/></td>
				</tr>
				<tr>
					<td><label for="url">連結網址：</label></td>
					<td><input name="url" type="text" size="60"/></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="0"/></td>
				</tr>
				<tr>
					<td><label for="seq">顯示：</label></td>
					<td>
						<select name="display">
							<option value="Y">顯示</option>
							<option value="N" checked>不顯示</option>
						</select>
					</td>
				</tr>
			</table>
			<table class="form-table">
				<tr>
					<td><label for="description">客服種類：</label></td>
					<td>
						<ul class="relation-list">
						<?php foreach($this->tplVar['table']['rt']['response_category'] as $rck => $rcv) : ?>
							<li>
								<input class="relation-item" type="checkbox" name="rcid[]" id="rcid_<?php echo $rck; ?>" value="<?php echo $rcv['rcid']; ?>"/>
								<span class="relation-item-name" for="rcid_<?php echo $rck; ?>"><?php echo $rcv['name']; ?></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>