<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<script type="text/javascript">
		function set_reply(i) {
			var rc = $("#r"+i).text();
			var rp = $("#p"+i).data('p');
			
			$("#uirid").val(i);
			$("#oldrpfid").val(rp);
			$("#replycontent").val(rc);
			$("#replycontent").focus();
		}
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">會員提問資料</div>
					</div>
				</li>
				<li class="body">
					<table>
						<tr>
							<td width="15%"><label for="fcid">分類：</label></td>
							<td width="20%">
								<?php
									foreach($this->tplVar["table"]["rt"]['user_issues_category'] as $key => $value) {
										if ($value['uicid'] == $this->tplVar['table']['record'][0]['uicid']) {
											echo $value['name'];
										}
									}
								?>
							</td>
							<td width="15%"><label for="name">會員：</label></td>
							<td >
							<?php echo $this->tplVar['table']['record'][0]['userid']; ?> 
							(<?php echo $this->tplVar['table']['record'][0]['nickname']; ?>)
							</td>
						</tr>
					</table>
					<table>	
						<tr>
							<td width="15%"><label for="description">提問內容：</label></td>
							<td><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['content']); ?></td>
						</tr>
					</table>
					<table>	
						<tr>
							<td width="15%"><label for="info">附加訊息：</label></td>
							<td><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['info']); ?></td>
						</tr>
					</table>
					<table>
						<tr>
							<td width="15%"><label for="thumbnail">主圖一：</label></td>
							<td width="20%">
								<?php if (!empty($this->tplVar['table']['record'][0]['ispid'])) : ?>
								<a href="//<?php echo $this->config->domain_name .'/site/images/site/issues/'. $this->tplVar['table']['record'][0]['ispid']; ?>" target="_blank">預覽</a>
								<?php endif; ?>
							</td>
						
							<td width="15%"><label for="thumbnail2">主圖二：</label></td>
							<td width="20%">
								<?php if (!empty($this->tplVar['table']['record'][0]['ispid2'])) : ?>
								<a href="//<?php echo $this->config->domain_name .'/site/images/site/issues/'. $this->tplVar['table']['record'][0]['ispid2']; ?>" target="_blank">預覽</a>  
								<?php endif; ?>
							</td>
						
							<td width="15%"><label for="thumbnail3">主圖三：</label></td>
							<td >
								<?php if (!empty($this->tplVar['table']['record'][0]['ispid3'])) : ?>
								<a href="//<?php echo $this->config->domain_name .'/site/images/site/issues/'. $this->tplVar['table']['record'][0]['ispid3']; ?>" target="_blank">預覽</a> 
								<?php endif; ?>
							</td>
						</tr>
					</table>
					<table>
						<tr>
							<td width="15%"><label for="seq">顯示：</label></td>
							<td width="20%">
								<?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" 顯示":"不顯示"; ?>
							</td>
						
							<td width="15%"><label for="seq">更新時間：</label></td>
							<td width="20%">
								<?php echo $this->tplVar['table']['record'][0]['modifyt']; ?>
							</td>
						
							<td width="15%"><label for="seq">結案狀態：</label></td>
							<td>
								<?php if($this->tplVar['table']['record'][0]['status']=='3'){ ?>
								結案( <?php echo $this->tplVar['table']['record'][0]['closetime']; ?> )
								<?php } ?>
							</td>
						</tr>
					</table>
				</li>
			</ul>
			
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">回覆留言串</div>
					</div>
				</li>
			</ul>
			<ul class="table">
				<li class="body">
					<table>
						<tr>
							<td width="10%"><label for="userid">UserID：</label></td>
							<td width="20%"><label for="insertt">時間(回/讀)：</label></td>
							<td width="60%"><label for="reply_content">回覆內容：</label></td>
							<td width="10%"><label for="rpfid">附圖：</label></td>
						</tr>
						<?php 
						if(!empty($this->tplVar['table']["user_issues_reply"]) ){
						$user_issues_reply = $this->tplVar['table']["user_issues_reply"];
						foreach($user_issues_reply as $k => $v) {
						?>
						<tr>
							<td><?php echo empty($v['userid'])? $v['adminid'] : $v['userid']; ?></td>
							<td>回覆:<?php echo $v['insertt']; ?>
								<?php if(!empty($v['readtime'])){ echo '<br>讀取:'. $v['readtime']; }?>
							</td>
							<td>
								<?php if($v['userid']=='0'){ ?>
								<a class="icon-edit icons" href="javascript:;" onclick="set_reply(<?php echo $v["uirid"];?>);" title='Edit'>E</a>
								<?php } ?>
								<span id="r<?php echo $v["uirid"];?>"><?php echo htmlspecialchars_decode($v['reply_content']); ?></span>
								
							</td>
							<td>
								<?php if (!empty($v['rpfid'])) : ?>
								<a href="//<?php echo $this->config->domain_name .'/site/images/site/issues/'. $v['rpfid']; ?>" target="_blank">
								<span id="p<?php echo $v["uirid"];?>" data-p="<?php echo $v['rpfid'];?>">預覽</span></a>
								<?php endif; ?>
							</td>
						</tr>
						<?php } } ?>
					</table>
				</li>
			</ul>
		</div>
		
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/replyupdate">
			<div class="form-label">回覆資料</div>
			<table class="form-table">
				<tr>
					<td><label for="replycontent">回覆內容：</label></td>
					<td><textarea id="replycontent" name="replycontent" class="description" cols="40"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['reply_content']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="rpfid">附圖：</label></td>
					<td>
						<input name="rpfid" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['rpfid'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/issues/'. $this->tplVar['table']['record'][0]['rpfid']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
			</table>			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="uiid" value="<?php echo $this->tplVar["status"]["get"]["uiid"];?>">
				<input type="hidden" id="uirid" name="uirid" value="">
				<input type="hidden" id="oldrpfid" name="oldrpfid" value="">
				
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>

