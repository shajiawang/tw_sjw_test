<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<meta charset="UTF-8">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/images.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-main.css" type="text/css">
	    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
<!--
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">商家帳號管理</a>>><a>查詢</a>
		</div>
-->
        <?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form">
			<div style="font-weight: bold;">企業資料</div>
			<table class="form-table">
				<tr>
					<td><label for="enterpriseid">企業ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['enterpriseid']; ?></td>
				</tr>
				<tr>
					<td><label for="loginname">登入帳號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['loginname']; ?></td>
				</tr>
				<tr>
					<td><label for="exchange_store">兌換中心</label></td>
					<td><?php echo $this->tplVar['table']['rt']['exchange_store'][0]['name']; ?></td>
				</tr>
			</table>
			<table style="clear:both;">&nbsp;</table>
			<div style="font-weight: bold;">企業基本資料</div>
			<table class="form-table">
				<tr>
					<td><label for="companyname">公司名稱</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_profile'][0]['companyname']; ?></td>
				</tr>
				<tr>
					<td><label for="name">負責人姓名</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_profile'][0]['name']; ?></td>
				</tr>
				<tr>
					<td><label for="uniform">統一編號</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_profile'][0]['uniform']; ?></td>
				</tr>
				<tr>
					<td><label for="phone">聯絡電話</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_profile'][0]['phone']; ?></td>
				</tr>
				<tr>
					<td><label for="fax">公司傳真</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_profile'][0]['fax']; ?></td>
				</tr>
				<tr>
					<td><label for="country">國家</label></td>
					<td><?php echo $this->tplVar['table']['rt']['country'][0]['name']; ?></td>
				</tr>
				<tr>
					<td><label for="channel">經銷商</label></td>
					<td><?php echo $this->tplVar['table']['rt']['channel'][0]['name']; ?></td>
				</tr>
				<tr>
					<td><label for="address">公司地址</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_profile'][0]['area']; ?>&nbsp;<?php echo $this->tplVar['table']['rt']['enterprise_profile'][0]['address']; ?></td>
				</tr>
			</table>
			<table style="clear:both;">&nbsp;</table>
			<div style="font-weight: bold;">企業帳務資料</div>
			<table class="form-table">
				<tr>
					<td><label for="account_contactor">帳務負責人</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['account_contactor']; ?></td>
				</tr>
				<tr>
					<td><label for="account_title">職務</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['account_title']; ?></td>
				</tr>
				<tr>
					<td><label for="account_phone">手機號碼</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['account_phone']; ?></td>
				</tr>
				<tr>
					<td><label for="execute_contactor">實際營運人</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['execute_contactor']; ?></td>
				</tr>
				<tr>
					<td><label for="execute_title">職務</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['execute_title']; ?></td>
				</tr>
				<tr>
					<td><label for="execute_phone">手機號碼</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['execute_phone']; ?></td>
				</tr>
				<tr>
					<td><label for="accountname">撥款帳戶</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['accountname']; ?></td>
				</tr>
				<tr>
					<td><label for="accountnumber">撥款帳號</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['accountnumber']; ?></td>
				</tr>
				<tr>
					<td><label for="bankname">銀行名稱</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['bankname']; ?></td>
				</tr>
				<tr>
					<td><label for="branchname">分行名稱</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_account'][0]['branchname']; ?></td>
				</tr>
			</table>
			<table style="clear:both;">&nbsp;</table>
			<div style="font-weight: bold;">企業商店資料</div>
			<table class="form-table">
				<tr>
					<td><label for="marketingname">對外行銷名稱</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_shop'][0]['marketingname']; ?></td>
				</tr>
				<tr>
					<td><label for="url">網址</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_shop'][0]['url']; ?></td>
				</tr>
				<tr>
					<td><label for="businesstime">營業時間</label></td>
					<td><?php echo date("H:i", strtotime($this->tplVar['table']['rt']['enterprise_shop'][0]['businessontime'])); ?> ~ <?php echo date("H:i", strtotime($this->tplVar['table']['rt']['enterprise_shop'][0]['businessofftime'])); ?></td>
				</tr>
				<tr>
					<td><label for="turnover">月平均營業額</label></td>
					<td><?php echo $this->tplVar['status']['turnoverArr'][$this->tplVar['table']['rt']['enterprise_shop'][0]['turnover']]; ?></td>
				</tr>
				<tr>
					<td><label for="transaction">單筆平均交易額</label></td>
					<td><?php echo $this->tplVar['status']['transactionArr'][$this->tplVar['table']['rt']['enterprise_shop'][0]['transaction']]; ?></td>
				</tr>
				<tr>
					<td><label for="trade">行業型態</label></td>
					<td><?php echo $this->tplVar['status']['tradeArr'][$this->tplVar['table']['rt']['enterprise_shop'][0]['trade']]; ?></td>
				</tr>
				<tr>
					<td><label for="marketingmodel">行銷模式</label></td>
					<td><?php echo $this->tplVar['status']['marketingmodelArr'][$this->tplVar['table']['rt']['enterprise_shop'][0]['marketingmodel']]; ?></td>
				</tr>
				<tr>
					<td><label for="business_area">營業面積</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_shop'][0]['business_area']; ?></td>
				</tr>
				<tr>
					<td><label for="employees">員工人數</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_shop'][0]['employees']; ?></td>
				</tr>
				<tr>
					<td><label for="franchise_store">分店/加盟店</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_shop'][0]['franchise_store']; ?></td>
				</tr>
				<tr>
					<td><label for="seniority">營業年資</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_shop'][0]['seniority']; ?></td>
				</tr>
				<tr>
					<td><label for="source">商品來源</label></td>
					<td><?php echo $this->tplVar['status']['sourceArr'][$this->tplVar['table']['rt']['enterprise_shop'][0]['source']]; ?></td>
				</tr>
				<tr>
					<td><label for="card_demand">國外信用卡需求</label></td>
					<td><?php echo $this->tplVar['status']['card_demandArr'][$this->tplVar['table']['rt']['enterprise_shop'][0]['card_demand']]; ?></td>
				</tr>
				<tr>
					<td><label for="operation_overview">營運概況</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_shop'][0]['operation_overview']; ?></td>
				</tr>
				<tr>
					<td><label for="contact_email">聯絡信箱</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_shop'][0]['contact_email']; ?></td>
				</tr>
				<tr>
					<td><label for="service_email">客服信箱</label></td>
					<td><?php echo $this->tplVar['table']['rt']['enterprise_shop'][0]['service_email']; ?></td>
				</tr>
			</table>
			<div class="functions">
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">回上一頁</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>