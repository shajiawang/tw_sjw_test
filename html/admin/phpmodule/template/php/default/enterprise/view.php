<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<script type="text/javascript">
		//Email驗證
		function to_verified(id){
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
				{
					type: "verified",
					uid: id,
					location_url: '<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])); ?>'
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						alert(json.msg);
						location.reload();
					}
				}
			);
		};
		</script>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label">帳號：</span>
					<input type="text" name="search_loginname" size="20" value="<?php echo $this->io->input['get']['search_loginname']; ?>"/>
				</li>
				<!--<li class="search-field">
					<span class="label">手機驗證：</span>
					<select name="search_verified"><option value="">All</option>
						<option value="N" <?php echo ($this->io->input['get']['search_verified']=='N') ? 'selected' : ''; ?> >No</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_verified']=='Y') ? 'selected' : ''; ?> >Yes</option>
					</select>
				</li>-->
				<li class="search-field">
					<span class="label">公司名稱：</span>
					<input type="text" name="search_companyname" size="20" value="<?php echo $this->io->input['get']['search_companyname']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">負責人姓名：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">統一編號：</span>
					<input type="text" name="search_uniform" size="20" value="<?php echo $this->io->input['get']['search_uniform']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">Email：</span>
					<input type="text" name="search_email" size="20" value="<?php echo $this->io->input['get']['search_email']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
			<form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/file/?location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" enctype="multipart/form-data">
			<ul>
				<li class="search-field">
					<span class="label" for="file_name">選取上傳CSV檔：</span>
					<input type="file" name="file_name" />
				</li>
				<li class="button">
					<button>匯入CSV</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
			<!--
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php /*echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; */?>/add">新增</a>
						</div>						
					</div>
				</li>
            -->
				<li class="body">
					<table>
						<thead>
							<tr>
								<?php /*<th>修改</th>
								<th>刪除</th>*/?>
								<?php $base_href = $this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"]; ?>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_userid"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_enterpriseid=desc">enterpriseid<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_userid"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_enterpriseid=">enterpriseid<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_enterpriseid=asc">enterpriseid<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>公司名稱</th>
								<th>負責人姓名</th>
								<th>帳號</th>
								<!--th>Email驗證</th-->
								<th>統編</th>
								<th>公司電話</th>
								<th>營業地址</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<?php /*<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/userid=<?php echo $rv["userid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/enterpriseid=<?php echo $rv["enterpriseid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>*/?>
								<td class="column" name="enterpriseid"><?php echo $rv['enterpriseid']; ?></td>
								<td class="column" name="companyname"><a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/viewdetail/?enterpriseid=<?php echo $rv["enterpriseid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ?>"><?php echo $rv['companyname']; ?></a></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="loginname"><?php echo $rv['loginname']; ?></td>
								<!--td class="column" name="verified">
								<?php if(!empty($rv['verified']) && $rv['verified']=='Y'){ ?>已驗證
								<?php }else{ ?>
									No <input class="table_td_button" type="button" value="驗證" onclick="to_verified('<?php echo $rv['enterpriseid']; ?>')">
								<?php } ?>
								</td-->
								<td class="column" name="uniform"><?php echo $rv['uniform']; ?></td>
								<td class="column" name="phone"><?php echo $rv['phone']; ?></td>
								<td class="column" name="address"><?php echo $rv['area']." ".$rv['address']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>