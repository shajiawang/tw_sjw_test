<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="langid">銀行ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['bkid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">銀行名稱：</label></td>
					<td><input name="bankname" type="text" value="<?php echo $this->tplVar['table']['record'][0]['bankname']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="bankid">銀行代號：</label></td>
					<td><input name="bankid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['bankid']; ?>"/></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="bkid" value="<?php echo $this->tplVar["status"]["get"]["bkid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>