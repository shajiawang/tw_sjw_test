<?php
    $arrDesc['behav']['feedback']='回饋';
	$arrDesc['behav']['prod_buy']='商品購買';
	$arrDesc['behav']['prod_sell']='中標商品折算';
	$arrDesc['behav']['sajabonus']='鯊魚點兌換';
	$arrDesc['behav']['process_fee']='中標處理費';
	$arrDesc['behav']['user_saja']='下標手續費';
	$arrDesc['behav']['bid_refund']='流標退款';
	$arrDesc['behav']['user_deposit']='使用者儲值';
	$arrDesc['behav']['bid_by_saja']='中標送殺價幣';
	$arrDesc['behav']['gift']='(儲值/活動)贈送';
	$arrDesc['behav']['system']='系統轉入';
	$arrDesc['behav']['system_testv']='系統測試';
	$arrDesc['behav']['other_system_use']='第三方抵扣';
	$arrDesc['behav']['others']='其他';
	
	$arrDesc['remark']['0']='';
	$arrDesc['remark']['1']='老殺友回娘家';
	$arrDesc['remark']['2']='記者會活動';
	$arrDesc['remark']['3']='重機交車派對';
	$arrDesc['remark']['6']='手機驗證送';
	$arrDesc['remark']['8']='推薦送';
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>會員殺價幣清單</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_behav">殺幣類型：</span>
					<select name="behav" id="behav">
						<option value="">全部</option>
						<?php foreach($arrDesc['behav'] as $k=>$v) {
							      $selected=($k==$this->io->input['get']['behav']?"selected":"");
							      echo "<option value='${k}' ${selected} >${v}</option>";
						      }  
					    ?>
						<!-- option value="feedback" <?php echo ($this->io->input['get']['behav'] == 'feedback') ? "selected" : "";?> >回饋</option>
						<option value="prod_buy" <?php echo ($this->io->input['get']['behav'] == 'prod_buy') ? "selected" : "";?> >商品購買</option>
						<option value="prod_sell" <?php echo ($this->io->input['get']['behav'] == 'prod_sell') ? "selected" : "";?> >中標商品折算</option>
						<option value="sajabonus" <?php echo ($this->io->input['get']['behav'] == 'sajabonus') ? "selected" : "";?> >鯊魚點兌換</option>
						<option value="process_fee" <?php echo ($this->io->input['get']['behav'] == 'process_fee') ? "selected" : "";?> >中標處理費</option>
						<option value="user_saja" <?php echo ($this->io->input['get']['behav'] == 'user_saja') ? "selected" : "";?> >下標手續費</option>
						<option value="bid_refund" <?php echo ($this->io->input['get']['behav'] == 'bid_refund') ? "selected" : "";?> >流標退款</option>
						<option value="user_deposit" <?php echo ($this->io->input['get']['behav'] == 'user_deposit') ? "selected" : "";?> >使用者儲值</option>
						<option value="bid_by_saja" <?php echo ($this->io->input['get']['behav'] == 'bid_by_saja') ? "selected" : "";?> >中標送殺價幣</option>
						<option value="gift" <?php echo ($this->io->input['get']['behav'] == 'gift') ? "selected" : "";?> >(儲值)贈送</option>
						<option value="system" <?php echo ($this->io->input['get']['behav'] == 'system') ? "selected" : "";?> >系統轉入</option>
						<option value="system_test" <?php echo ($this->io->input['get']['behav'] == 'system_test') ? "selected" : "";?> >系統測試</option>
						<option value="other_system_use" <?php echo ($this->io->input['get']['behav'] == 'other_system_use') ? "selected" : "";?> >第三方抵扣</option>
						<option value="others" <?php echo ($this->io->input['get']['behav'] == 'others') ? "selected" : "";?> >其他</option -->
					</select>
				</li>				
				<li class="search-field">
					<span class="label" for="search_joindate">新增日期：</span>
					<input name="joindatefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindatefrom']; ?>" /> ~ 
					<input name="joindateto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindateto']; ?>"/>
				</li>
				<input name="userid" type="hidden" value="<?php echo $this->io->input['get']['userid']; ?>" />
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/">回上一頁</a>
						</div>	
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export_spoint/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>殺幣來源/用途</th>
								<th>金額</th>
								<th>新增時間</th>
								<th>活動註記</th>	
								<th>備註說明</th>								
							</tr>
						</thead>
						<tbody>
							<?php
								if (is_array($this->tplVar["table"]['record'])) {
									foreach($this->tplVar["table"]['record'] as $rk => $rv) {
							?>
							<tr>
								<td class="column" name="behav"><?php echo $arrDesc['behav'][$rv['behav']]; ?></td>
								<td class="column" name="amount"><?php echo $rv['amount']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="remark"><?php echo $arrDesc['remark'][$rv['remark']]; ?></td>
								<td class="column" name="memo"><?php echo $rv['memo']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>