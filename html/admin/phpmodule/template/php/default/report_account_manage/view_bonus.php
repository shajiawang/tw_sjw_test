<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>會員鯊魚點清單</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_behav">鯊魚點類型：</span>
					<select name="behav" id="behav">
						<option value="">全部</option>
						<option value="ibon_exchange" <?php echo ($this->io->input['get']['behav'] == 'ibon_exchange') ? "selected" : "";?> >ibon 兌換</option>
						<option value="order_refund" <?php echo ($this->io->input['get']['behav'] == 'order_refund') ? "selected" : "";?> >缺貨退費</option>
						<option value="user_exchange" <?php echo ($this->io->input['get']['behav'] == 'user_exchange') ? "selected" : "";?> >兌換商品</option>
						<option value="product_close" <?php echo ($this->io->input['get']['behav'] == 'product_close') ? "selected" : "";?> >結標轉鯊魚點</option>
						<option value="user_qrcode_tx" <?php echo ($this->io->input['get']['behav'] == 'user_qrcode_tx') ? "selected" : "";?> >Qrcode 兌換</option>
						<option value="other_system_exchange" <?php echo ($this->io->input['get']['behav'] == 'other_system_exchange') ? "selected" : "";?> >第三方兌換使用</option>
						<option value="system" <?php echo ($this->io->input['get']['behav'] == 'system') ? "selected" : "";?> >系統/手動加入</option>
						<option value="system_test" <?php echo ($this->io->input['get']['behav'] == 'system_test') ? "selected" : "";?> >測試用</option>
						<option value="order_close" <?php echo ($this->io->input['get']['behav'] == 'order_close') ? "selected" : "";?> >得標獲鯊魚點</option>
					</select>
				</li>				
				<li class="search-field">
					<span class="label" for="search_joindate">新增日期：</span>
					<input name="joindatefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindatefrom']; ?>" /> ~ 
					<input name="joindateto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindateto']; ?>"/>
				</li>
				<input name="userid" type="hidden" value="<?php echo $this->io->input['get']['userid']; ?>" />
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/">回上一頁</a>
						</div>
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export_bonus/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>							
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>鯊魚點類型</th>
								<th>點數</th>
								<th>新增時間</th>
								<th>註記</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if (is_array($this->tplVar["table"]['record'])) {
									foreach($this->tplVar["table"]['record'] as $rk => $rv) {
							?>
							<tr>
								<td class="column" name="behav"><?php echo $this->tplVar['status']['behav'][$rv['behav']]; ?></td>
								<td class="column" name="amount"><?php echo $rv['amount']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="memo"><?php echo $rv['memo']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>