<!DOCTYPE html>
<?php
    $arrSelectItem['search_giftspoint']['ALL']='';
	$arrSelectItem['search_giftspoint']['Y']='';
	$arrSelectItem['search_giftspoint']['N']='';
?>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>會員帳戶管理</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="10" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>	
                <li class="search-field">
					<span class="label" for="search_name">會員帳號：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
                <li class="search-field">
					<span class="label" for="search_gift_spoint">獲贈殺幣：</span>
					<select name="search_gift_spoint" id="search_gift_spoint">
					  <?php
					    $arrSelectItem['search_gift_spoint'][$this->io->input['get']['search_gift_spoint']] ="selected";   
					  ?>
					  <option value="ALL" <?php echo $arrSelectItem['search_gift_spoint']["ALL"]; ?> >不限</option>	
					  <option value="Y" <?php echo $arrSelectItem['search_gift_spoint']["Y"]; ?> >有</option>	
					  <option value="N" <?php echo $arrSelectItem['search_gift_spoint']["N"]; ?> >沒有</option>	
					</select>
				</li>	
                <li class="search-field">
					<span class="label" for="search_switch">是否停權：</span>
					<select name="switch" id="switch">
					    <option value="Y">否</option>
						<option value="N">是</option>
					</select>
				</li>                
				<li class="search-field">
					<span class="label" for="search_tester">是否加入測試人員：</span>
					<select name="tester" id="tester">
						<?php foreach ($this->tplVar['status']['tester'] as $key => $value) : ?>
						<option value="<?php echo $key; ?>" <?php echo ($this->io->input['get']['tester'] == $key) ? "selected" : "";?> ><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>會員編號</th>
								<th>帳號</th>
								<th>暱稱</th>
								<th>停權</th>
								<!-- th>小額免密額度</th -->
								<th>活動獲贈殺幣</th>
								<th>舊會員轉入殺幣</th>
								<th>儲值購得殺幣</th>
								<th>獲得殺幣總額</th>
								<th>流標退點總額</th>
								<th>下標費用總額</th>
								<th>殺幣餘額</th>
								<th>結帳總額</th>
								<th>凍結殺幣</th>
								<th>(鯊魚點)結標轉入</th>
								<th>得標獲點</th>
								<th>系統轉入</th>
								<th>已使用</th>
								<th>餘額</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if (is_array($this->tplVar["table"]['record'])) {
									foreach($this->tplVar["table"]['record'] as $rk => $rv) {
							?>
							<tr>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="nickname"><?php echo urldecode($rv['nickname']); ?></td>
							    <td class="column" name="switch"><?php echo $rv['switch']=='Y'?"否":"是"; ?></td>
								<!-- td class="column" name="bonus_noexpw"><?php echo round($rv['bonus_noexpw']); ?></td -->
								<td class="column" name="gift_input_total"><?php if ($rv['gift_input_total'] == 0) { echo '0'; } else { echo rtrim((int)$rv['gift_input_total'],'.'); } ?></td>
								<td class="column" name="system_input_total"><?php if ($rv['system_input_total'] == 0) { echo '0'; } else { echo rtrim((int)$rv['system_input_total'],'.'); } ?></td>
								<td class="column" name="spoints_by_deposit"><?php if ($rv['spoints_by_deposit'] == 0) { echo '0'; } else { echo rtrim((int)$rv['spoints_by_deposit'],'.'); } ?></td>
								<td class="column" name="input_total"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_spoint/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['input_total'] == 0) { echo '0'; } else { echo rtrim((int)$rv['input_total'],'.'); } ?></a></td>
                                <td class="column" name="bid_refund"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_spoint/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['bid_refund'] == 0) { echo '0'; } else { echo rtrim(abs((int)$rv['bid_refund']),'.'); } ?></a></td>
								<td class="column" name="total_bid_process_amount"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_spoint/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['total_bid_process_amount'] == 0) { echo '0'; } else { echo rtrim(abs((int)$rv['total_bid_process_amount']),'.'); } ?></a></td>
								<td class="column" name="spoint_total"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_spoint/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['spoint_total'] == 0) { echo '0'; } else { echo rtrim((int)$rv['spoint_total'],'.'); } ?></a></td>
								<!--td class="column" name="total_process_amount"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_spoint/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['total_process_amount'] == 0) { echo '0'; } else { echo rtrim(abs((int)$rv['total_process_amount']),'.'); } ?></a></td-->
								<td class="column" name="bid_price_total_all"><?php if ($rv['bid_price_total_all'] == 0) { echo '0'; } else { echo rtrim(abs((int)$rv['bid_price_total_all']),'.'); } ?></td>

								<td class="column" name="freeze"><?php if ($rv['freeze'] == 0) { echo '0'; } else { echo rtrim((int)$rv['freeze'],'.'); } ?></td>								
								<td class="column" name="bonus_input_total"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_bonus/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['bonus_input_total'] == 0) { echo '0'; } else { echo rtrim((int)$rv['bonus_input_total'],'.'); } ?></a></td>
                                <!-- 得標獲鯊魚點  -->								
								<td class="column" name="bonus_input_total"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_bonus/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['bonus_input_total'] == 0) { echo '0'; } else { echo rtrim((int)$rv['bonus_input_total2'],'.'); } ?></a></td>	
								<!-- 系統手動轉入鯊魚點  -->	
								<td class="column" name="bonus_input_total"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_bonus/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['bonus_input_total'] == 0) { echo '0'; } else { echo rtrim((int)$rv['bonus_input_total3'],'.'); } ?></a></td>	
     							<td class="column" name="bonus_overage"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_bonus/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['bonus_overage'] == 0) { echo '0'; } else { echo rtrim((int)$rv['bonus_overage'],'.'); } ?></a></td>
								<td class="column" name="bonus_total"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/view_bonus/?userid=<?php echo $rv['userid']; ?>" ><?php if ($rv['bonus_total'] == 0) { echo '0'; } else { echo rtrim((int)$rv['bonus_total'],'.'); } ?></a></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>