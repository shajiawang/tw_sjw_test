<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="deid">活動編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['deid']; ?></td>
				</tr>
				<tr>
					<td><label for="denum">檔期編碼：</label></td>
					<td><input name="denum" type="text" value="<?php echo $this->tplVar['table']['record'][0]['denum']; ?>" onKeyUp="if(this.value.length>4){this.value=this.value.substr(0,4)};this.value=this.value.replace(/[^\d]/g,'');" /></td>
				</tr>				
				<tr>
					<td><label for="dename">活動名稱：</label></td>
					<td><input name="dename" type="text" value="<?php echo $this->tplVar['table']['record'][0]['dename']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="year">年度：</label></td>
					<td><select name="year">
							<option value="2019" <?php echo $this->tplVar['table']['record'][0]['year']=="2019"?" selected":""; ?>>2019</option>
							<option value="2020" <?php echo $this->tplVar['table']['record'][0]['year']=="2020"?" selected":""; ?>>2020</option>
							<option value="2021" <?php echo $this->tplVar['table']['record'][0]['year']=="2021"?" selected":""; ?>>2021</option>
							<option value="2022" <?php echo $this->tplVar['table']['record'][0]['year']=="2022"?" selected":""; ?>>2022</option>
							<option value="2023" <?php echo $this->tplVar['table']['record'][0]['year']=="2023"?" selected":""; ?>>2023</option>
							<option value="2024" <?php echo $this->tplVar['table']['record'][0]['year']=="2024"?" selected":""; ?>>2024</option>
							<option value="2025" <?php echo $this->tplVar['table']['record'][0]['year']=="2025"?" selected":""; ?>>2025</option>
						</select></td>
				</tr>				
				<tr>
					<td><label for="ontime">開始時間：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start" value="<?php echo $this->tplVar['table']['record'][0]['ontime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="offtime">結束時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>"/></td>
				</tr>				
				<tr>
					<td><label for="isdefault">是否為預設：</label></td>
					<td>
						<select name="isdefault">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['isdefault']=="Y"?" selected":""; ?>>是</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['isdefault']=="N"?" selected":""; ?>>否</option>
						</select>						
					</td>
				</tr>
				<tr>
					<td><label for="url">導入URL：</label></td>
					<td><input name="url" type="text" value="<?php echo $this->tplVar['table']['record'][0]['url']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="tplfile">上傳檔案：</label></td>
					<td><input name="tplfile" type="text" value="<?php echo $this->tplVar['table']['record'][0]['tplfile']; ?>" /></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="deid" value="<?php echo $this->tplVar["status"]["get"]["deid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>