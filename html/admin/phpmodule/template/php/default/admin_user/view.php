<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
<!--	    <div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label">管理者帳號：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">Email：</span>
					<input type="text" name="search_email" size="20" value="<?php echo $this->io->input['get']['search_email']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>刪除</th>
								<?php $base_href = $this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"]; ?>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_userid"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_userid=desc">userid<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_userid"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_userid=">userid<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_userid=asc">userid<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_name"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_name=desc">帳號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_name"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_name=">帳號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_name=asc">帳號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>管理權限</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_email"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_email=desc">Email<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_email"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_email=">Email<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_email=asc">Email<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/userid=<?php echo $rv["userid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/userid=<?php echo $rv["userid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="permission"><?php if ($rv['department'] == 'S') { echo "管理員"; } else { echo "一般"; } ?></td>
								<td class="column" name="email"><?php echo $rv['email']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>