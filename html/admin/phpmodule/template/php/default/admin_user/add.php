<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<meta charset="UTF-8">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/images.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-main.css" type="text/css">
	    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">管理者帳號：</label></td>
					<td><input name="name" type="text" /></td>
				</tr>
				<tr>
					<td><label for="passwd">管理者密碼：</label></td>
					<td><input name="passwd" type="text" /></td>
				</tr>
				<tr>
					<td><label for="email">Email：</label></td>
					<td><input name="email" type="text" /></td>
				</tr>
				<tr>
					<td><label for="department">部門：</label></td>
					<td>
						<select name="department">
							<option value="S" >系統管理員</option>
							<option value="A" >管理部</option>
							<option value="B" >工程部</option>
							<option value="C" >行銷部</option>
							<option value="D" >財會部</option>
							<option value="E" >客服部</option>
							<option value="F" >總務部</option>
							<option value="G" >業務部</option>
							<option value="H" >人事部</option>
						</select>
					</td>
				</tr>				
				
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>