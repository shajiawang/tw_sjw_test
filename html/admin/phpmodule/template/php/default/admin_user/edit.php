<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<meta charset="UTF-8">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/images.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-main.css" type="text/css">
	    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			
			<table class="form-table">
				<tr>
					<td><label for="userid">管理者ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">管理者帳號：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="passwd">新密碼：</label></td>
					<td><input name="passwd" type="text" value=""/></td>
				</tr>
				<tr>
					<td><label for="email">Email：</label></td>
					<td><input name="email" type="text" value="<?php echo $this->tplVar['table']['record'][0]['email']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="department">部門：</label></td>
					<td>
						<select name="department">
							<option value="S" <?php echo $this->tplVar['table']['record'][0]['department']=="S"?" selected":""; ?>>系統管理員</option>
							<option value="A" <?php echo $this->tplVar['table']['record'][0]['department']=="A"?" selected":""; ?>>管理部</option>
							<option value="B" <?php echo $this->tplVar['table']['record'][0]['department']=="B"?" selected":""; ?>>工程部</option>
							<option value="C" <?php echo $this->tplVar['table']['record'][0]['department']=="C"?" selected":""; ?>>行銷部</option>
							<option value="D" <?php echo $this->tplVar['table']['record'][0]['department']=="D"?" selected":""; ?>>財會部</option>
							<option value="E" <?php echo $this->tplVar['table']['record'][0]['department']=="E"?" selected":""; ?>>客服部</option>
							<option value="F" <?php echo $this->tplVar['table']['record'][0]['department']=="F"?" selected":""; ?>>總務部</option>
							<option value="G" <?php echo $this->tplVar['table']['record'][0]['department']=="G"?" selected":""; ?>>業務部</option>
							<option value="H" <?php echo $this->tplVar['table']['record'][0]['department']=="H"?" selected":""; ?>>人事部</option>
						</select>
					</td>
				</tr>					
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']['record'][0]['userid']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>