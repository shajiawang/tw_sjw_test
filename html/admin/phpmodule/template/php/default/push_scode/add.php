<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		<script>
			function changeshow(){
				if ($('#sendto').val()==''){
					$('#select_force_show').attr('disabled',false);
				}else{
					$('#select_force_show').attr('disabled',true);
				}
			}
			$(function (){
				$("#groupset").select2({
			        language: 'zh-TW',
			        width: '100%',
			        // 最多字元限制
			        maximumInputLength: 10,
			        // 最少字元才觸發尋找, 0 不指定
			        minimumInputLength: 0,
			        // 當找不到可以使用輸入的文字
			        tags: true,
		    	});
			});
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>

		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="groupset">推播分組：</label></td>
					<td><select name="groupset" id='groupset'>
						<?php foreach ($this->tplVar["table"]["rt"]["groupsets"] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['groupset']; ?>" <?php if ( $pcv['groupset']== $this->tplVar['table']['record'][0]['groupset']) echo "selected" ?> ><?php echo $pcv['groupset'] ; ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="title">推播標題：</label></td>
					<td><input name="title" type="text" data-validate="required" value=""/> 商品名稱 請勿輸入 單引號' 雙引號" 右斜線\ (系統儲存時將會自動過濾移除)</td>
				</tr>
				<tr>
					<td><label for="body">推播內容：</label></td>
					<td>
						<select name="body[]">
							<option value="">無</option>
							<option value="{nickname}"    >使用者名稱</option>
							<option value="{winner}"      >得標者名稱</option>
							<option value="{productname}" >商品名稱</option>
						</select>

						<input type="text" name="body[]" value="" style="width: 250px;">

						<select name="body[]">
							<option value="">無</option>
							<option value="{nickname}"    >使用者名稱</option>
							<option value="{winner}"      >得標者名稱</option>
							<option value="{productname}" >商品名稱</option>
						</select>

						<input type="text" name="body[]" value="" style="width: 250px;">

						<select name="body[]">
							<option value="">無</option>
							<option value="{nickname}"    >使用者名稱</option>
							<option value="{winner}"      >得標者名稱</option>
							<option value="{productname}" >商品名稱</option>
						</select>
						<br>
						推播內容 請勿輸入 分號 " ； "
					</td>
				</tr>
				<tr id="tr_action">
					<td><label for="action">推送路徑(APP)：</label></td>
					<td>
						<select name="action" id="select_action">
							<option value="0">不做事</option>
							<option value="1">開啟web(內開)</option>
							<option value="2">開啟web(外連)</option>
							<option value="3">開大圖</option>
							<option value="4">跳頁面</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="page">跳轉頁面</label></td>
					<td><select name="page" id="page">
							<option value="1">商品頁</option>
							<option value="2">購買殺價幣</option>
							<option value="3">直播頁</option>
							<option value="4">首頁</option>
							<option value="5">殺價紀錄</option>
							<option value="6">商城兌換清單</option>
							<option value="7">邀請好友</option>
							<option value="8">圓夢商品頁</option>
							<option value="9">殺友專區</option>
							<option value="10">我的帳號</option>
							<option value="11">殺戮戰場首頁</option>
							<option value="12">圓夢商品首頁</option>
							<option value="13">鯊魚商城首頁</option>
							<option value="14">殺價幣</option>
							<option value="15">殺價劵</option>
							<option value="16">票劵/卡片</option>
							<option value="17">新手教學</option>
							<option value="18">最新得標</option>
							<option value="19">王者秘笈</option>
							<option value="20">掃瞄支付頁(使用者掃店家)</option>
							<option value="21">鯊魚點使用明細</option>
							<option value="22">超級殺價劵</option>
							<option value="23">關於殺價王</option>
						</select>
					</td>
				</tr>
				<tr id="tr_productid">
					<td><label for="productid">產品編號：</label></td>
					<td>
						<input type="text" name="productid" id="productid" >
						<!-- select name="productid" id="productid">
						<?php foreach ($this->tplVar["table"]["rt"]["valid_product"] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['productid']; ?>" ><?php echo $pcv['productid'] .' -- '. $pcv['name'] ; ?></option>
						<?php endforeach; ?>
					    </select -->
					</td>
				</tr>
				<tr id="tr_url_title">
					<td><label for="url_title">webview標題：</label></td>
					<td>
						<input type="text" name="url_title">
					</td>
				</tr>
				<tr id="tr_url">
					<td><label for="url">webview連結：</label></td>
					<td>
						<input type="text" name="url">
					</td>
				</tr>
				<tr id="tr_url">
					<td><label for="sendto">發送對象(userid)：</label></td>
					<td>
						<input type="text" name="sendto" id ='sendto' onkeyup="changeshow();">
					</td>
				</tr>
				<tr id="tr_force_show">
					<td><label for="force_show">通知中心顯示：(此欄群發時才有效)</label></td>
					<td>
						<select name="force_show" id="select_force_show">
							<option value="Y">強制顯示</option>
							<option value="N">強制不顯示</option>
						</select>
					</td>
				</tr>
				<tr id="tr_url">
					<td><label for="sendtime">發送時間：</label></td>
					<td>
						<input name="sendtime" type="text" class="datetime" autocomplete="off">
					</td>
				</tr>
			</table>

			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input name="edit_item" id="edit_item" type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>

		</form>
		<div class="dialogs"></div>
	</body>
</html>