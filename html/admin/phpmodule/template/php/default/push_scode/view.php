<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_pushid">推播編號：</span>
					<input type="text" name="search_pushid" size="20" value="<?php echo $this->io->input['get']['search_pushid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_title">推播標題：</span>
					<input type="text" name="search_title" size="20" value="<?php echo $this->io->input['get']['search_title']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_used">是否已發送：</span>
					<select name="search_used">
						<option value="">All</option>
						<option value="N" <?php echo ($this->io->input['get']['search_used']=='N')?'selected':''; ?> >否</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_used']=='Y')?'selected':''; ?> >是</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th>推播編號</th>
								<th>推播群</th>
								<th>推播標題</th>
								<th>推播路徑(APP)</th>
								<th>推播標題(URL)</th>
								<th>URL路徑</th>
								<th>發送人</th>
								<th>發送對象</th>
								<th>強制顯示</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_sendtime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_sendtime=desc">定時發送時間<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_sendtime"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_sendtime=">定時發送時間<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_sendtime=asc">定時發送時間<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>是否發送</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) { ?>
							<?php
							foreach($this->tplVar["table"]['record'] as $rk => $rv) {
							?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/pushid=<?php echo $rv["pushid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<td class="column" name="pushid"><?php echo $rv['pushid']; ?></td>
								<td class="column" name="groupset"><?php echo $rv['groupset']; ?></td>
								<td class="column" name="title"><?php echo $rv['title']; ?></td>
								<td class="column" name="action"><?php echo $rv['action']; ?></td>
								<td class="column" name="url_title"><?php echo $rv['url_title']; ?></td>
								<td class="column" name="url"><?php echo $rv['url']; ?></td>
								<td class="column" name="sender"><?php echo $rv['sender']; ?></td>
								<td class="column" name="sendto"><?php echo $rv['sendto']; ?></td>
								<td class="column" name="force_show"><?php echo (!(empty($rv['sendto'])))?" ":$rv['force_show']; ?></td>
								<td class="column" name="sendtime"><?php echo $rv['sendtime']; ?></td>
								<td class="column" name="pushed"><?php echo $rv['pushed']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>

				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>
