<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
			$('select[name=layer]').change(function(){
				if ($(this).val() == 2) {
					$('select[name=node] option')
						<?php for($j=0; $j<$this->tplVar['table']['rt']['product_category_level_all']+1; $j++){ ?>		
						<?php if (($j+1) != 2){?>	
						.filter('option[rel="<?php echo $j+1;?>"]').prop('disabled', true).end()
						<?php }else{ ?>
						.filter('option[rel="<?php echo $j+1;?>"]').prop('disabled', false).end()
						<?php } ?>
						<?php } ?>
					<?php if (!empty($this->tplVar['table']['record'][0]['node'])) : ?>
					.filter('[value=<?php echo $this->tplVar['table']['record'][0]['node'] ?>]').prop('selected', true)
					<?php else : ?>
					.filter(':eq(1)').prop('selected', true)
					<?php endif; ?>
				<?php for($i=2; $i<$this->tplVar['table']['rt']['product_category_level_all']+1; $i++){ ?>						
				} else if ($(this).val() == <?php echo $i+1;?>){
					$('select[name=node] option')
					<?php for($j=0; $j<$this->tplVar['table']['rt']['product_category_level_all']+1; $j++){ ?>						
					<?php if (($j+1) != ($i+1)){?>	
					.filter('option[rel="<?php echo $j+1;?>"]').prop('disabled', true).end()
					<?php }else{ ?>
					.filter('option[rel="<?php echo $j+1;?>"]').prop('disabled', false).end()
					<?php } ?>
					<?php } ?>
					<?php if (!empty($this->tplVar['table']['record'][0]['node'])) : ?>
					.filter('[value=<?php echo $this->tplVar['table']['record'][0]['node'] ?>]').prop('selected', true)
					<?php else : ?>
					.filter('option[rel="3"]').prop('selected', true)
					<?php endif; ?>
				<?php } ?>		
				} else {
					$('select[name=node] option')
					.filter('option[rel="1"]').prop('disabled', false).prop('selected', true).end()
					.filter('option[rel="2"]').prop('disabled', true).end()
					.filter('option[rel="3"]').prop('disabled', true).end()
				}
			}).val(<?php echo $this->tplVar['table']['record'][0]['layer']; ?>).change();
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="epcid">分類ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['epcid']; ?></td>
				</tr>
				
				<tr>
					<td><label for="layer">分類層級：</label></td>
					<td>
						<select name="layer">
							<?php for($i=1; $i<$this->tplVar['table']['rt']['product_category_level_all']+2; $i++){ ?>
							<option value="<?php echo $i; ?>" <?php if($this->tplVar['table']['record'][0]['layer']==$i){ echo 'selected'; } ?>><?php echo $i; ?></option>
							<?php } ?>						
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="node">上層分類：</label></td>
					<td>
						<select name="node">
							<option rel="1" value="0">無上層</option>
							
							<?php for($i=0; $i<$this->tplVar['table']['rt']['product_category_level_all']; $i++){ ?>

							<?php foreach($this->tplVar['table']['rt']['product_category_level'.($i+1)] as $pck => $pcv) : ?>
							<option rel="<?php echo $i+2; ?>" value="<?php echo $pcv['epcid'] ?>"><?php echo $pcv['name'] ?></option>
							<?php endforeach; ?>

							<?php } ?>
						</select>
					</td>
				</tr>
				
				<tr>
					<td><label for="name">名稱：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">敘述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail">主圖：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/product/category/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>				
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">啟用：</label></td>
					<td>
						<select name="switch">
							<option value="Y"<?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>啟用</option>
							<option value="N"<?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>停用</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="display">是否顯示：</label></td>
					<td>
						<select name="display">
							<option value="Y"<?php echo $this->tplVar['table']['record'][0]['display']=="Y"?" selected":""; ?>>顯示</option>
							<option value="N"<?php echo $this->tplVar['table']['record'][0]['display']=="N"?" selected":""; ?>>不顯示</option>
						</select>
					</td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="channelid">頻道：</label></td>
					<td>
						<select name="channelid">
						<?php foreach ($this->tplVar['table']['rt']['channel_rt'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['channelid']; ?>" <?php if($this->tplVar['table']['record'][0]['channelid']==$cv['channelid']){ echo 'selected'; } ?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="epcid" value="<?php echo $this->tplVar["status"]["get"]["epcid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>