<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
			$(function(){
				$('select[name=layer]').change(function(){					
					if ($(this).val() == 2) {
						$('select[name=node] option')
						<?php for($j=0; $j<$this->tplVar['table']['rt']['product_category_level_all']+1; $j++){ ?>		
						<?php if (($j+1) != 2){?>	
						.filter('option[rel="<?php echo $j+1;?>"]').prop('disabled', true).end()
						<?php }else{ ?>
						.filter('option[rel="<?php echo $j+1;?>"]').prop('disabled', false).end()
						<?php } ?>
						<?php } ?>
						.filter(':eq(1)').prop('selected', true)
					<?php for($i=2; $i<$this->tplVar['table']['rt']['product_category_level_all']+1; $i++){ ?>						
					} else if ($(this).val() == <?php echo $i+1;?>) {
						$('select[name=node] option')
						<?php for($j=0; $j<$this->tplVar['table']['rt']['product_category_level_all']+1; $j++){ ?>						
						<?php if (($j+1) != ($i+1)){?>	
						.filter('option[rel="<?php echo $j+1;?>"]').prop('disabled', true).end()
						<?php }else{ ?>
						.filter('option[rel="<?php echo $j+1;?>"]').prop('disabled', false).end()
						<?php } ?>
						<?php } ?>
						.filter('option[rel="<?php echo $i+1;?>"]').prop('selected', true)
					<?php } ?>			
					} else {
						$('select[name=node] option')
						.filter('option[rel="1"]').prop('disabled', false).prop('selected', true).end()
						.filter('option[rel="2"]').prop('disabled', true).end()
						.filter('option[rel="3"]').prop('disabled', true).end()
						.filter('option[rel="4"]').prop('disabled', true).end()
					}
				}).change();
			})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="layer">分類層級：</label></td>
					<td>
						<select name="layer">
							<?php for($i=1; $i<$this->tplVar['table']['rt']['product_category_level_all']+2; $i++){ ?>
							<option value="<?php echo $i; ?>" <?php if (($i) == 1){?>selected<?php } ?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="node">上層分類：</label></td>
					<td>
						<select name="node">
							<option rel="1" value="0">無上層</option>
							<?php for($i=0; $i<$this->tplVar['table']['rt']['product_category_level_all']; $i++){ ?>

							<?php foreach($this->tplVar['table']['rt']['product_category_level'.($i+1)] as $pck => $pcv) : ?>
							<option rel="<?php echo $i+2; ?>" value="<?php echo $pcv['epcid'] ?>"><?php echo $pcv['name'] ?></option>
							<?php endforeach; ?>
							
							<?php } ?>
						</select>
					</td>
				</tr>
				
				<tr>
					<td><label for="name">名稱：</label></td>
					<td><input name="name" type="text"/></td>
				</tr>
				<tr>
					<td><label for="description">敘述：</label></td>
					<td><input name="description" type="text"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail">主圖：</label></td>
					<td><input name="thumbnail" type="file"/></td>
				</tr>				
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="1"/></td>
				</tr>
				<tr>
					<td><label for="display">是否顯示：</label></td>
					<td>
						<select name="display">
							<option value="Y">顯示</option>
							<option value="N">不顯示</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="switch">啟用：</label></td>
					<td>
						<select name="switch">
							<option value="Y">啟用</option>
							<option value="N">停用</option>
						</select>
					</td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="channelid">頻道：</label></td>
					<td>
						<select name="channelid">
						<?php 
							foreach ($this->tplVar['table']['rt']['channel_rt'] as $ck => $cv) :
								$selected = "";
								if ($cv['channelid'] == "0") {
								 	$selected = "selected='selected'";
								} 
						?>
							<option value="<?php echo $cv['channelid']; ?>" <?php echo $selected;?>><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>