<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		function tosend(){
			alert('確認新增 !!');
			$("#form-add").submit();
		}
		function changeshow(){
			if ($('#userid').val()==''){
				$('#select_force_show').attr('disabled',false);
			}else{
				$('#select_force_show').attr('disabled',true);
			}
		}
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增贈送超殺券</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><input name="userid" id="userid" type="text" style="width: 300px;"/>
					<br>多筆請用「,」(逗號)分隔,限一千筆內!</td>
				</tr>			
				<tr>
					<td><label for="scode">超殺券組數：</label></td>
					<td><input name="scode" id="scode" type="number" value="0" maxlength="6" /></td>
				</tr>
				<tr>
					<td><label for="behav">超殺券行為：</label></td>
					<td>
						<select id="behav" name="behav">
						<option value="a">下標</option>
						<option value="b">分享</option>
						<option value="c">儲值</option>
						<option value="d" selected>系統</option>
						<option value="e">序號</option>
						<option value="f">得標</option>
						</select>					
					</td>
				</tr>
				<tr>
					<td><label for="spid">活動編號：</label></td>
					<td>
						<select id="spid" name="spid">
							<option value="0">無相關活動</option>
							<?php foreach($this->tplVar['table']['record'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['spid']; ?>"><?php echo $cv['name']; ?></option>
							<?php endforeach; ?>
						</select>					
					</td>
				</tr>	
				<tr>
					<td><label for="offtime">使用期限：</label></td>
					<td><?php $off_time = date('Y-m-d H:i:00',strtotime('+3 day')); ?>
					<input name="offtime" id="offtime" type="text" value="<?php echo $off_time; ?>" class="datetime time-stop"/></td>
				</tr>
                <tr>
					<td><label for="memo">備註說明：</label></td>
					<td><input name="memo" id="memo" type="text" size="64" maxlength="255" /></td>
				</tr>				
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="push_title">推播標題：</label></td>
					<td><input name="push_title" type="text" data-validate="required" value="" style="width: 300px;"/> 商品名稱 請勿輸入 單引號' 雙引號" 右斜線\ (系統儲存時將會自動過濾移除)</td>
				</tr>
				<tr>
					<td><label for="push_body">推播內容：</label></td>
					<td>
						<input type="text" name="push_body" data-validate="required" value="" style="width: 600px;">
					</td>
				</tr>
				<tr id="tr_productid">
					<td><label for="productid">產品編號：</label></td>
					<td>
						<input type="text" name="productid" id="productid" value="" >
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="button" value="送出" onclick="tosend();" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>