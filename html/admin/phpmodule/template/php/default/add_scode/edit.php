<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-edit" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
			
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>
				<tr>
					<td><label for="productid">商品編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['productid']; ?></td>
				</tr>					
				<tr>
					<td><label for="amount">殺價券數量：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['amount']; ?></td>
				</tr>
				<tr>
					<td><label for="remainder">殺價券剩餘數量：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['remainder']; ?></td>
				</tr>				
				<tr>
					<td><label for="behav">殺價幣行為：</label></td>
					<td>
						<select id="behav" name="behav" disabled="disabled">
							<option value="a" <?php echo $this->tplVar['table']['record'][0]['behav']=="a"?" selected":""; ?>>下標</option>
							<option value="b" <?php echo $this->tplVar['table']['record'][0]['behav']=="b"?" selected":""; ?>>分享</option>
							<option value="c" <?php echo $this->tplVar['table']['record'][0]['behav']=="c"?" selected":""; ?>>儲值</option>
							<option value="d" <?php echo $this->tplVar['table']['record'][0]['behav']=="d"?" selected":""; ?>>系統</option>
							<option value="e" <?php echo $this->tplVar['table']['record'][0]['behav']=="e"?" selected":""; ?>>序號</option>
							<option value="f" <?php echo $this->tplVar['table']['record'][0]['behav']=="f"?" selected":""; ?>>得標</option>
						</select>					
					</td>
				</tr>
				<tr>
					<td><label for="spid">活動編號：</label></td>
					<td>
						<select id="spid" name="spid" disabled="disabled">
							<option value="0">無相關活動</option>
							<?php foreach($this->tplVar['table']["rt"]["promote"] as $ck => $cv) : ?>
							<option value="<?php echo $cv['spid']; ?>" <?php if($this->tplVar['table']['record'][0]['spid']==$cv['spid']){ echo 'selected'; } ?> ><?php echo $cv['name']; ?></option>
							<?php endforeach; ?>
						</select>					
					</td>
				</tr>				
				<tr>
					<td><label for="offtime">使用期限：</label></td>
					<td><input name="offtime" id="offtime" type="text" class="datetime time-stop" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>" data-org="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>"/></td>
				</tr>				
				<tr>
					<td><label for="switch">顯示：</label></td>
					<td>
						<select name="switch" readonly="readonly">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>關閉</option>
						</select>						
					</td>
				</tr>
                <tr>
					<td><label for="memo">備註/說明：</label></td>
					<td><input name="memo" id="memo" type="text" value="<?php echo $this->tplVar['table']['record'][0]['memo']; ?>" size="64" /></td>
				</tr>				
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="spointid" value="<?php echo $this->tplVar["status"]["get"]["spointid"] ;?>">
				<input type="hidden" name="scodeid" value="<?php echo $this->tplVar['table']['record'][0]["scodeid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">回上頁</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>