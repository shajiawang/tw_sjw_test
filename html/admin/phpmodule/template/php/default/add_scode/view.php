<!DOCTYPE html>
<?php
   if(empty($this->io->input['get']['search_behav']))
	  $this->io->input['get']['search_behav']='d';
?>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_btime">開始時間：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">結束時間：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_behav">使用狀態：</span>
					<select id="search_behav" name="search_behav" >
						<option value="a" <?php echo $this->io->input['get']['search_behav']=="a"?" selected":""; ?>>下標</option>
						<option value="b" <?php echo $this->io->input['get']['search_behav']=="b"?" selected":""; ?>>分享</option>
						<option value="c" <?php echo $this->io->input['get']['search_behav']=="c"?" selected":""; ?>>儲值</option>
						<option value="d" <?php echo $this->io->input['get']['search_behav']=="d"?" selected":""; ?>>系統</option>
						<option value="e" <?php echo $this->io->input['get']['search_behav']=="e"?" selected":""; ?>>序號</option>
						<option value="f" <?php echo $this->io->input['get']['search_behav']=="f"?" selected":""; ?>>得標</option>
					</select>
				</li>					
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">一般送券</a>
						</div>
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/addonline/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">在線會員送券</a>
						</div>	
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_scodeid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_scodeid=desc">殺價券編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_scodeid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_scodeid=">殺價券編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_scodeid=asc">殺價券編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>會員編號</th>
								<th>總組數</th>
								<th>剩餘組數</th>
								<th>使用狀態</th>
								<th>活動編號</th>
								<th>備註說明</th>
								<th>到期時間</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?scodeid=<?php echo $rv["scodeid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="scodeid"><?php echo $rv['scodeid']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="amount" style="text-align:right;"><?php echo number_format($rv['amount']); ?></td>
								<td class="column" name="remainder" style="text-align:right;"><?php echo number_format($rv['remainder']); ?></td>
								<td class="column" name="behav"><?php echo $this->tplVar['status']['behav'][$rv['behav']];  ?></td>
								<td class="column" name="spid"><?php echo $rv['spid']; ?></td>
								<td class="column" name="memo">
								<?php if(!empty($rv['memo'])){ echo $rv['memo'] ."<br>"; } ?>
								<?php if(!empty($rv['productid'])){ echo $rv['productid'] .' '; echo (!empty($rv['pdname']))?$rv['pdname']:''; } ?>
								</td>
								<td class="column" name="offtime"><?php echo $rv['offtime']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>