<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="appid">編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['id']; ?></td>
				</tr>
				<tr>
					<td><label for="name">項目名稱：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="ckeditor_description">內容：</label></td>
					<td><textarea name="description" id="ckeditor_description" class="description"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail">圖片：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/product/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="used">使用狀態：</label></td>
					<td>
						<select name="used">
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['used']=="N"?" selected":""; ?> >否</option>
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['used']=="Y"?" selected":""; ?> >是</option>
						</select>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="id" value="<?php echo $this->tplVar["status"]["get"]["id"] ;?>">
				<input type="hidden" name="oldthumbnail" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail'];?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>