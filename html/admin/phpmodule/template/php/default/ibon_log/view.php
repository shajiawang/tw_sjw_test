<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_no">檔期編號：</span>
					<select name="search_no">
						<option value="">All</option>
					<?php foreach ($this->tplVar['table']['rt']['prdate'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['no']; ?>" <?php if ($this->tplVar['status']["search"]["search_no"] == $pcv['no']) {echo "selected";} ?> ><?php echo $pcv['date_start']. '<=>' .$pcv['date_end']; ?></option>
					<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_uid">會員編號：</span>
					<input type="text" name="search_uid" size="20" value="<?php echo $this->io->input['get']['search_uid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_account">帳號：</span>
					<input type="text" name="search_account" size="20" value="<?php echo $this->io->input['get']['search_account']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_code">電文代碼：</span>
					<input type="text" name="search_code" size="20" value="<?php echo $this->io->input['get']['search_code']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_btime">開始時間：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">截止時間：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_state">狀態：</span>
					<select name="search_state">
						<option value="">All</option>
						<option value="1" <?php if ($this->tplVar['status']["search"]["search_state"] == 1) {echo "selected";} ?> >1</option>
						<option value="2" <?php if ($this->tplVar['status']["search"]["search_state"] == 2) {echo "selected";} ?> >2</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>電文代碼</th>
								<th>電文單號</th>
								<th>會員編號</th>
								<th>帳號</th>
								<th>ibon點數</th>
								<th>狀態</th>
								<th>日期</th>
								<th>兌換檔次</th> 
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="i_BUSINESS"><?php echo $rv['i_BUSINESS']; ?></td>
								<td class="column" name="i_DETAIL_NUM"><?php echo $rv['i_DETAIL_NUM']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="account_id"><?php echo $rv['account_id']; ?></td>
								<td class="column" name="ibon_bonus"><?php echo $rv['ibon_bonus']; ?></td>
								<td class="column" name="state"><?php echo $rv['state']; ?></td>
								<td class="column" name="adate"><?php echo $rv['adate']; ?></td>
								<td class="column" name="prdate_no"><?php echo $rv['prdate_no']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>