<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	    <script type="text/javascript">
	    	
			$(function(){
				$('select[name="category"] option[value="<?php echo $this->tplVar['table']['record'][0]['category']; ?>"]').prop('selected', true);
			});
		</script>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="epid">商品編號：</label></td>
					<td><input name="epid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['epid']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="code1">序號1：</label></td>
					<td><input name="code1" type="text" value="<?php echo $this->tplVar['table']['record'][0]['code1']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="codeformat1">序號格式1：</label></td>
					<td><input name="codeformat1" type="text" value="<?php echo $this->tplVar['table']['record'][0]['codeformat1']; ?>"/>
						條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
					</td>
				</tr>
				<tr>
					<td><label for="code2">序號2：</label></td>
					<td><input name="code2" type="text" value="<?php echo $this->tplVar['table']['record'][0]['code2']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="codeformat2">序號格式2：</label></td>
					<td><input name="codeformat2" type="text" value="<?php echo $this->tplVar['table']['record'][0]['codeformat2']; ?>"/>
						條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
					</td>
				</tr>
				<tr>
					<td><label for="code3">序號3：</label></td>
					<td><input name="code3" type="text" value="<?php echo $this->tplVar['table']['record'][0]['code3']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="codeformat3">序號格式3：</label></td>
					<td><input name="codeformat3" type="text" value="<?php echo $this->tplVar['table']['record'][0]['codeformat3']; ?>"/>
						條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
					</td>
				</tr>
				<tr>
					<td><label for="code4">序號4：</label></td>
					<td><input name="code4" type="text" value="<?php echo $this->tplVar['table']['record'][0]['code4']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="codeformat4">序號格式4：</label></td>
					<td><input name="codeformat4" type="text" value="<?php echo $this->tplVar['table']['record'][0]['codeformat4']; ?>"/>
						條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
					</td>
				</tr>
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><input name="userid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['userid']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="orderid">訂單編號：</label></td>
					<td><input name="orderid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['orderid']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="purchaseid">進貨單號：</label></td>
					<td><input name="purchaseid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['purchaseid']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="used">使用狀態：</label></td>
					<td>
						<select name="used">
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['used']=="N"?" selected":""; ?> >否</option>
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['used']=="Y"?" selected":""; ?> >是</option>
						</select>
					</td>
				</tr>				
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="cid" value="<?php echo $this->tplVar["status"]["get"]["cid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>