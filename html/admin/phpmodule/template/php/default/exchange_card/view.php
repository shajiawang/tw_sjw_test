<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">商品編號：</span>
					<input type="text" name="search_epid" size="20" value="<?php echo $this->io->input['get']['search_epid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">訂單編號：</span>
					<input type="text" name="search_orderid" size="20" value="<?php echo $this->io->input['get']['search_orderid']; ?>"/>
				</li>				
				<li class="search-field">
					<span class="label">序號1：</span>
					<input type="text" name="search_code1" size="20" value="<?php echo $this->io->input['get']['search_code1']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">序號2：</span>
					<input type="text" name="search_code2" size="20" value="<?php echo $this->io->input['get']['search_code2']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_used">是否使用：</span>
					<select name="search_used">
						<option value="">All</option>
						<option value="N" <?php echo ($this->io->input['get']['search_used']=='N')?'selected':''; ?> >未使用</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_used']=='Y')?'selected':''; ?> >已使用</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_starttime">訂單開始：</span>
					<input type="text" name="search_starttime" size="20" value="<?php echo $this->io->input['get']['search_starttime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_endtime">訂單截止：</span>
					<input type="text" name="search_endtime" size="20" value="<?php echo $this->io->input['get']['search_endtime']; ?>" class="datetime time-stop"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_startinsertt">資料建立開始：</span>
					<input type="text" name="search_startinsertt" size="20" value="<?php echo $this->io->input['get']['search_startinsertt']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_endinsertt">資料建立截止：</span>
					<input type="text" name="search_endinsertt" size="20" value="<?php echo $this->io->input['get']['search_endinsertt']; ?>" class="datetime time-stop"/>
				</li>
				<li class="button">
					<button>搜尋</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
			<form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/file/?location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" enctype="multipart/form-data">
			<ul>
				<li class="search-field">
					<span class="label" for="file_name">選取上傳CSV檔：</span>
					<input type="file" name="file_name" />
				</li>
				<li class="button">
					<button>匯入CSV</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="functions">
							<div class="button">
								<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
							</div>
							<div class="button">
								<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
							</div>						
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>刪除</th>
								<?php $base_href = $this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"]; ?>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_cid"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_cid=desc">cid<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_cid"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_cid=">cid<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_cid=asc">cid<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>商品編號</th>
								<th>序號1</th>
								<th>序號2</th>
								<th>序號3</th>
								<th>序號4</th>
								<th>會員編號</th>
								<th>訂單編號</th>
								<th>使用狀態</th>
								<th>訂單日期</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/cid=<?php echo $rv["cid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/cid=<?php echo $rv["cid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="cid"><?php echo $rv['cid']; ?></td>
								<td class="column" name="epid"><?php echo $rv['epid']; ?></td>
								<td class="column" name="code1"><?php echo $rv['code1']; ?></td>
								<td class="column" name="code2"><?php echo $rv['code2']; ?></td>
								<td class="column" name="code3"><?php echo $rv['code3']; ?></td>
								<td class="column" name="code4"><?php echo $rv['code4']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="orderid"><?php echo $rv['orderid']; ?></td>
								<td class="column" name="used"><?php echo $rv['used']; ?></td>
								<?php
								if(empty($rv['order_time'])){
									?>
									<td class="column" name="order_time"><?php echo '0000-00-00 00:00:00'; ?></td>
								<?php
								}else{
									?>
									<td class="column" name="order_time"><?php echo $rv['order_time']; ?></td>
									<?php
								}
								?>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>