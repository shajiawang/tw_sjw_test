<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
			$(function(){
				<?php for($i=0; $i<($this->tplVar['table']['rt']['product_category_level']-1); $i++){ ?>
				$('.relation-list.layer<?php echo $i+1;?> .relation-item')
				.on('change', function(){
					var $this = $(this),
						node = $this.val(),
						checked = $this.prop('checked');

					if (checked) {
						$('.relation-list.layer<?php echo $i+2;?> li:has(.relation-item[rel='+node+'])').show();
					} else {
						$('.relation-list.layer<?php echo $i+2;?> li:has(.relation-item[rel='+node+'])').hide()
						.find('.relation-item[rel='+node+']').prop('checked', false);
					}				
				}).change();
				<?php } ?>

				$('#add_item').on('click', function(e){
					// prod_name=$("#name").val();
					// if(!prod_name || prod_name=='') {
						// alert('請填寫內容 !!');
						// return false;
					// }
					ontime = Date.parse(new Date($('#ontime').val().replace(/-/g, '/')));
					if(isNaN(ontime)) {
						alert('請填寫開始時間!');
						return false;
					}
					offtime = Date.parse(new Date($('#offtime').val().replace(/-/g, '/')));
					if(isNaN(offtime)) {
						alert('請填寫到期時間!');
						return false;
					}
					if(ontime>=offtime) {
						alert('開始時間不可大於到期時間!');
						return false;
					}
					$('#form-add').submit();
				});
				$("#ul_pcid").find("li[ptype='0']").hide();
			});
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<!--tr>
					<td><label for="name">內容：</label></td>
					<td><input name="name" id="name" type="text" class="required"/></td>
				</tr-->
				<tr>
					<td><label for="url_app">鏈結內容(APP)：</label></td>
					<td><input name="url_app" id="url_app" type="text" class="required"/></td>
				</tr>
				<tr>
					<td><label for="url_web">鏈結內容(WEB)：</label></td>
					<td><input name="url_web" id="url_web" type="text" class="required"/></td>
				</tr>
				<tr>
					<td><label for="qty">筆數：</label></td>
					<td>
						<select name="qty">
							<option value="1" >1筆</option>
							<option value="10" >10筆</option>
							<option value="100" >100筆</option>
							<option value="500" >500筆</option>
							<option value="1000" >1000筆</option>
							<option value="3000" >3000筆</option>
							<option value="5000" >5000筆</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="ontime">開始時間：</label></td>
					<td><input name="ontime" id="ontime" type="text" class="datetime time-start"/></td>
				</tr>
				<tr>
					<td><label for="offtime">到期時間：</label></td>
					<td><input name="offtime" id="offtime" type="text" class="datetime time-stop"/></td>
				</tr>
				<tr>
					<td><label for="amount">價值：</label></td>
					<td><input name="amount" id="amount" type="number" value="0" class="required"/></td>
				</tr>
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><input name="userid" id="userid" type="text" value="0" class="required"/></td>
				</tr>
				<tr>
					<td><label for="epid">商品編號：</label></td>
					<td><input name="epid" id="epid" type="text" value="0" class="required"/></td>
				</tr>
				<tr>
					<td><label for="epid">商品分類編號：</label></td>
					<td><input name="epcid" id="epcid" type="text" value="0" class="required"/></td>
				</tr>
			</table> 
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input name="add_item" id="add_item" type="button" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div> 
	</body>
</html>