<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
			$(function(){
				$('#add_item').on('click', function(e){
					// prod_name=$("#name").val();
					// if(!prod_name || prod_name=='') {
						// alert('請填寫內容 !!');
						// return false;
					// }
					ontime = Date.parse(new Date($('#ontime').val().replace(/-/g, '/')));
					if(isNaN(ontime)) {
						alert('請填寫開始時間!');
						return false;
					}
					offtime = Date.parse(new Date($('#offtime').val().replace(/-/g, '/')));
					if(isNaN(offtime)) {
						alert('請填寫到期時間!');
						return false;
					}
					if(ontime>=offtime) {
						alert('開始時間不可大於到期時間!');
						return false;
					}
					$('#form-add').submit();
				});
				$("#ul_pcid").find("li[ptype='0']").hide();
			});
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">新增資料</div>
			<table class="form-table" style="width:50%">
				<tr>
					<td><label for="qrid">二維碼編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['qrid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">二維碼內容：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['name']; ?></td>
				</tr>
				<tr>
					<td><label for="url_app">鏈結內容(APP)：</label></td>
					<td><input name="url_app" id="url_app" type="text" value="<?php echo $this->tplVar['table']['record'][0]['url_app']; ?>" class="required"/></td>
				</tr>
				<tr>
					<td><label for="url_web">鏈結內容(WEB)：</label></td>
					<td><input name="url_web" id="url_web" type="text" value="<?php echo $this->tplVar['table']['record'][0]['url_web']; ?>" class="required"/></td>
				</tr>
				<tr>
					<td><label for="ontime">開始時間：</label></td>
					<td><input name="ontime" id="ontime" type="text" value="<?php echo $this->tplVar['table']['record'][0]['ontime']; ?>" class="datetime time-start"/></td>
				</tr>
				<tr>
					<td><label for="offtime">到期時間：</label></td>
					<td><input name="offtime" id="offtime" type="text" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>" class="datetime time-stop"/></td>
				</tr>
				<tr>
					<td><label for="amount">價值：</label></td>
					<td><input name="amount" id="amount" type="number" value="<?php echo $this->tplVar['table']['record'][0]['amount']; ?>" class="required"/></td>
				</tr>
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><input name="userid" id="userid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['userid']; ?>" class="required"/></td>
				</tr>
				<tr>
					<td><label for="epid">商品編號：</label></td>
					<td><input name="epid" id="epid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['epid']; ?>" class="required"/></td>
				</tr>
				<tr>
					<td><label for="epid">商品分類編號：</label></td>
					<td><input name="epcid" id="epcid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['epcid']; ?>" class="required"/></td>
				</tr>
				<tr>
					<td><label for="used">使用狀態：</label></td>
					<td>
						<select name="used">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['used']=="Y" ? "selected" : ""; ?> >已使用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['used']=="N" ? "selected" : ""; ?> >未使用</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="used_time">使用時間：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['used_time']; ?></td>
				</tr>
				<tr>
					<td><label for="orderid">訂單編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['orderid']; ?></td>
				</tr>
				<tr>
					<td><label for="remark">批次：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['remark']; ?></td>
				</tr>
			</table> 
			<?php $url = '{"mtype":"app","act_type":78,"code":"'.$this->tplVar['table']['record'][0]['name'].'"}'; ?>
			<table class="form-table">
				<tr>
					<td><label for="qrid">二維碼：</label></td>
					<td><img id="codeNum" class="img-fluid" src='https://www.saja.com.tw/site/phpqrcode/?data=<?php echo $url;?>' alt=""></td>
				</tr>
				<tr>
					<td><label for="used_time">二維碼內容：</label></td>
					<td><?php echo '{"mtype":"app","act_type":78,"code":"'.$this->tplVar['table']['record'][0]['name'].'"}'; ?></td>
				</tr>
			</table> 
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="qrid" value="<?php echo $this->tplVar["status"]["get"]["qrid"] ;?>"> 
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div> 
	</body>
</html>