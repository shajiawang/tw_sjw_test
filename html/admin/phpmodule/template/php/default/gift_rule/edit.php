<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">禮券回饋規則管理</a>>><a>編輯</a>
		</div>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="grid">規則ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['grid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">規則名稱：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">規則敘述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="type">規則類型：</label></td>
					<td>
						<select name="type">
							<option value="gift"<?php echo $this->tplVar['table']['record'][0]['type']=="gift"?" selected":""; ?>>廠商禮券</option>
							<option value="sgift"<?php echo $this->tplVar['table']['record'][0]['type']=="sgift"?" selected":""; ?>>殺價禮券</option>
							<option value="bonus"<?php echo $this->tplVar['table']['record'][0]['type']=="bonus"?" selected":""; ?>>鯊魚點積分</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="starttime">規則開始時間：</label></td>
					<td><input name="starttime" type="text" class="datetime" value="<?php echo $this->tplVar['table']['record'][0]['starttime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="stoptime">規則結束時間：</label></td>
					<td><input name="stoptime" type="text" class="datetime" value="<?php echo $this->tplVar['table']['record'][0]['stoptime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">規則排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">規則啟用：</label></td>
					<td>
						<select name="switch">
							<option value="Y"<?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>啟用</option>
							<option value="N"<?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>停用</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="grid" value="<?php echo $this->tplVar["status"]["get"]["grid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>