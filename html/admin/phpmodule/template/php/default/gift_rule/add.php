<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">禮券回饋規則管理</a>>><a>新增</a>
		</div>
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">規則名稱：</label></td>
					<td><input name="name" type="text"/></td>
				</tr>
				<tr>
					<td><label for="description">規則敘述：</label></td>
					<td><input name="description" type="text"/></td>
				</tr>
				<tr>
					<td><label for="type">規則類型：</label></td>
					<td>
						<select name="type">
							<option value="gift">廠商禮券</option>
							<option value="sgift">殺價禮券</option>
							<option value="bonus">鯊魚點積分</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="starttime">規則開始時間：</label></td>
					<td><input name="starttime" type="text" class="datetime"/></td>
				</tr>
				<tr>
					<td><label for="stoptime">規則結束時間：</label></td>
					<td><input name="stoptime" type="text" class="datetime"/></td>
				</tr>
				<tr>
					<td><label for="seq">規則排序：</label></td>
					<td><input name="seq" type="text"/></td>
				</tr>
				<tr>
					<td><label for="seq">規則啟用：</label></td>
					<td>
						<select name="switch">
							<option value="Y">啟用</option>
							<option value="N">停用</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>