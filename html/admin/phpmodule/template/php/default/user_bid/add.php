<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<meta charset="UTF-8">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/images.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/main.css" type="text/css">
	    <style type="text/css"></style>
	    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">推薦管理</a>>><a>編輯</a>
		</div>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="intro_by">推薦人會員ID：</label></td>
					<td><input name="intro_by" type="text" value=""/></td>
				</tr>
				<tr>
					<td><label for="userid">新人會員ID：</label></td>
					<td><input name="userid" type="text" value=""/></td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]['get']['fun']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]['get']['fun']; ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>