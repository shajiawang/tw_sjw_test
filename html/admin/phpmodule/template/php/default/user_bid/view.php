<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<style type="text/css">
		.message {
			color: #29736c; width:300px; background-color: #ecf8f7; border: 1px solid #005d54; border-radius: 4px; display: none;
		}
		.message header {
			padding: 4px 0; margin: 3px; background-color: #8dc8c3; border-radius: 4px; position: relative;
		}
		.message header h2 {
			font-weight: bold; font-size: 16px; line-height: 20px; margin: 0 12px;			
		}
		.message header .button-close {
			text-decoration: none; color: #ecf8f7; float: right; margin-right: -6px;
		}
		.message .entry {
			padding: 3px 0;
		}
		.message .entry p {
			margin: 0 6px; padding: 10px 0;
		}
		.message.center {
			position: fixed; left: 50%; top: 50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%);		  -moz-transform: translate(-50%, -50%);
		}
		.msg{
			color: #29736c;
		}
		</style>
	</head>
	
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a>出價得標查詢</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				
				<li class="search-field">
					<span class="label" for="search_userid">會員ID：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				
				<li class="search-field">
					<span class="label" for="search_product">商品編碼：</span>
					<input type="text" name="search_product" size="20" value="<?php echo $this->io->input['get']['search_product']; ?>"/>
				</li>
				
				<li class="button">
					<button>搜尋</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<?php /*<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add">新增</a>*/?>
						</div>					
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								
								<?php /*
								$base_href = $this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"]; 
								`shid`, `userid`, `nickname`,`productid`, `ptype`,`shdid`, `price`, `pgpid`, `switch`,`insertt`
								*/?>
								
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_userid"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_userid=desc">會員<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_userid"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_userid=">會員<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_userid=asc">會員<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								
								<th>商品編碼</th>
								<th>商品類型</th>
								<th>下標金額</th>
								<th>得標訊息</th>
								<th>Switch</th>
								
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_insertt=desc">下標日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_insertt=">下標日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_insertt=asc">下標日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								
							</tr>
						</thead>
						
						<tbody>
							<?php if (is_array($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="userid">
									<?php echo $rv['userid'];?> 
									<br><?php echo $rv['nickname']; ?>
								</td>
								
								<td class="column" name="productid">(<?php echo $rv['productid']; ?>) 
								<?php echo empty($rv['productname'])?'':$rv['productname']; ?>
								</td><?php //shdid ?>
								
								<td class="column" name="ptype">
								<?php if(!empty($rv['ptype']) && $rv['ptype']=='1'){ ?>圓夢商品
								<?php }else{ ?>殺價商品<?php } ?> 
								</td>
								
								<td class="column" name="price"><?php echo ceil($rv['price']); ?></td>
								<td class="column" name="pgpid"><?php if(!empty($rv['pgpid'])){
								echo '總下標額: '. ceil($rv['total']) .', 結帳: '. $rv['complete']; 
								} ?></td>
								
								<td class="column" name="switch">
								<?php if(!empty($rv['switch']) && $rv['switch']=='Y'){ ?>Yes
								<?php }else{ ?>No<?php } ?> 
								</td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
		
		<article class="message center" id="msg-dialog">
			<header>
				<h2>
					<span class="msg" id="msg-title">訊息</span>
					<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
				</h2>
			</header>
			<div class="" >
				<p id="msg">Message content</p>
			</div>
		</article>
		
	</body>
</html>

<script type="text/javascript">

$(function(){
	//訊息窗
	$('#msg-close').on('click', function(){
		$('#msg-dialog').hide();
	});
});		

</script>