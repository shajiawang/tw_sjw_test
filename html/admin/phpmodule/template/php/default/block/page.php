<?php if (!empty($this->tplVar['page']['item'])) :?>
<ul>
	<li class="totalPage">資料共<?php echo $this->tplVar['page']['total']; ?>筆</li>
	<li class="splitter"></li>
	<li><a href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $this->tplVar['page']['firstpage'] ?>">最前頁</a></li>
	<li class="splitter"></li>
	<li><a href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $this->tplVar['page']['previouspage'] ?>">上一頁</a></li>
	<li class="splitter"></li>
	<li>&nbsp;[&nbsp;</li>
	<li>
        <?php foreach ($this->tplVar['page']['item'] as $item) : ?>
        <a class="pageNum" href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $item['p']; ?>"><?php echo $item['p']; ?></a>
        <?php endforeach; ?>
	</li>
	<li>&nbsp;]&nbsp;</li>
	<li class="splitter"></li>
	<li><a href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $this->tplVar['page']['nextpage'] ?>">下一頁</a></li>
	<li class="splitter"></li>
	<li><a href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $this->tplVar['page']['lastpage'] ?>">最後頁</a></li>
</ul>
<?php endif; ?>


<script>
    $(function(){
        var $active_page = '<?php echo $_GET['p']; ?>' || 1;
        var $page_index = $active_page - 1;
        var $pages = $('.footer ul li').find('.pageNum');
        $pages.removeClass('active');
        $pages.eq($page_index).addClass('active');
    });
    
</script>