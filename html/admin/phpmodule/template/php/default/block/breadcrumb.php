<div class="breadcrumb header-style">
<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>

<?php
switch($this->io->input['get']['fun']) {
	case 'push_msg':
		$name = "一般推播管理";
		break;
	case 'push_msg_manual':
		$name = "推播排程管理";
		break;
	case 'push_msg_hand':
		$name = "即時推播管理";
		break;
	case 'saja_sys':
		$name = "系統訊息管理";
		break;
	case 'app_version':
		$name = "app版本管理";
		break;	
	case 'bank':
		$name = "銀行管理";
		break;	
	case 'oscode':
		$name = "限定殺價券活動管理";
		break;
	case 'scode':
		$name = "殺價券活動管理";
		break;
	case 'scode_spr':
		$name = "殺價券活動類別";
		break;
	case 'exchange_mall':
		$name = "兌換中心";
		break;
	case 'exchange_store':
		$name = "兌換店家";
		break;
	case 'exchange_product_category':
		$name = "兌換商品分類";
		break;
	case 'exchange_product':
		$name = "兌換商品";
		break;
	case 'order':
		$name = "訂單管理";
		break;
	case 'stock':
		$name = "庫存管理";
		break;
	case 'promote_rule':
		$name = "活動規則";
		break;
	case 'promote_rule_item':
		$name = "活動規則項目";
		break;
	case 'exchange_bonus_history':
		$name = "兌換鯊魚點記錄";
		break;
	case 'exchange_gift_history':
		$name = "兌換禮券記錄";
		break;
	case 'return_history':
		$name = "退貨記錄";
		break;
	case 'channel':
		$name = "經銷商管理";
		break;
	case 'country':
		$name = "國別管理";
		break;
	case 'language':
		$name = "語系管理";
		break;
	case 'bonus':
		$name = "鯊魚點使用明細";
		break;
	case 'deposit':
		$name = "儲值帳戶";
		break;
	case 'deposit_rule':
		$name = "儲值規則";
		break;
	case 'deposit_rule_item':
		$name = "儲值規則項目";
		break;
	case 'gift':
		$name = "禮券帳戶";
		break;
	case 'user':
		$name = "會員帳號";
		break;
	case 'user_profile':
		$name = "會員詳細資料";
		break;
	case 'user_sms_auth':
		$name = "會員手機驗證記錄";
		break;
	case 'news':
		$name = "會員公告";
		break;
	case 'issues_category':
		$name = "客服訊息分類";
		break;
	case 'issues':
		$name = "客服訊息";
		break;
	case 'faq_category':
		$name = "新手教學分類";
		break;
	case 'faq':
		$name = "新手教學問答";
		break;
	case 'tech_category':
		$name = "殺價密技分類";
		break;
	case 'tech':
		$name = "殺價密技問答";
		break;
	case 'admin_user':
		$name = "管理員帳號";
		break;
	case 'product_today':
		$name = "主題商品";
		break;
	case 'ad_list':
		$name = "廣告管理";
		break;
	case 'ad_category':
		$name = "廣告主題版位";
		break;
	case 'response':
		$name = "自動回覆";
		break;
	case 'response_category':
		$name = "自動回覆分類";
		break;
	case 'member_bonus':
		$name = "會員鯊魚點點數";
		break;
	case 'user_extrainfo':
		$name = "會員其他資訊";
		break;
	case 'enterprise':
		$name = "商家帳號";
		break;
	case 'enterprise_bonus':
		$name = "商家鯊魚點查詢";
		break;
	case 'order_store':
		$name = "商家訂單";
		break;
	case 'order_store_list':
		$name = "商家訂單查詢";
		break;
	case 'loan':
		$name = "貸款類別查詢";
		break;
	case 'telecompany':
		$name = "電信業者查詢";
		break;
	case 'telpaytype':
		$name = "電信業務查詢";
		break;	
	case 'product_limited':
		$name = "限定商品類別管理";
		break;	
	case 'color':
		$name = "色彩使用管理";
		break;
	case 'report_product_statistics':
		$name = "下標商品統計";
		break;	
	case 'report_exchange_statistics':
		$name = "兌換商品統計";
		break;
	case 'exchange_card':
		$name = "兌換商品卡管理";
		break;	
	case 'exchange_lifepay':
		$name = "兌換生活繳費管理";
		break;	
	case 'report_account_device':
		$name = "會員使用裝置統計";
		break;	
	case 'report_activity_statistics':
		$name = "活動來源統計";
		break;	
	case 'help_category':
		$name = "幫助中心分類";
		break;
	case 'help':
		$name = "幫助中心";
		break;
	case 'deposit_times':
		$name = "首次儲值送倍數";
		break;	
	case 'dream_income':
		$name = "活動來源管理";
		break;	
	case 'dream_event':
		$name = "圓夢活動管理";
		break;
	case 'dream_code':
		$name = "圓夢編號管理";
		break;
	case 'user_card':
		$name = "會員信用卡資訊";
		break;	
	case 'admin_log':
		$name = "後台操作紀錄";
		break;	
	case 'add_oscode':
		$name = "手動加殺價卷";
		break;	
	case 'member_asset':
		$name = "會員資產";
		break;
	case 'ad_popups_list':
		$name = "首頁彈窗管理";
		break;
	case 'add_spoint':
		$name = "手動加殺價幣";
		break;
	case 'add_scode':
		$name = "手動加超級殺價卷";
		break;		
	case 'user_ibon':
		$name = "會員ibon兌換資訊";
		break;
	case 'ibon_prdate':
		$name = "ibon檔期管理";
		break;
	case 'ibon_prlist':
		$name = "ibon商品管理";
		break;
	case 'ibon_record':
		$name = "ibon交易清單";
		break;
	case 'ibon_log':
		$name = "ibon操作管理";
		break;
	case 'size':
		$name = "尺寸使用管理";
		break;
	case 'report_accounting_statistics':
		$name = "財務分析統計";
		break;
	case 'qrcode':
		$name = "商品二維碼管理";
		break;
	case 'rebate':
		$name = "購物金管理";
		break;
}
?>
<a href="<?php echo $this->config->default_main.'/'.$this->io->input['get']['fun']; ?>"><?php echo $name; ?></a>>>

<?php
switch($this->tplVar['status']['get']['act']) {
	case 'add':
		$act = "新增".$name;
		break;
	case 'edit':
		$act = "編輯".$name;
		break;
	default:
		$act = $name."列表";
		break;
}
?>
<a href="<?php echo $this->config->default_main.'/'.$this->io->input['get']['fun'].'/'.$this->tplVar['status']['get']['act']; ?>"><?php echo $act; ?></a>
</div>