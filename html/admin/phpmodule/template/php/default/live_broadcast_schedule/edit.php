<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	    <script type="text/javascript">
	    	//區域
			function livebroadcast(id) {
				$('select[name="lbid"] option').hide();
				$('select[name="lbid"]').val("");
				$('select[name="lbid"] option[rel=""]').show();
				$('select[name="lbid"] option[rel='+id+']').show();
			}
	        	
			$(function(){
				$('select[name="lbuserid"] option[value="<?php echo $this->tplVar['table']['record'][0]['lbuserid']; ?>"]').prop('selected', true);
				$('select[name="lbid"] option').hide();
				$('select[name="lbid"] option[rel=""]').show();
				$('select[name="lbid"] option[rel="<?php echo $this->tplVar['table']['record'][0]['lbuserid']; ?>"]').show();
				$('select[name="lbid"] option[value="<?php echo $this->tplVar['table']['record'][0]['lbid']; ?>"]').prop('selected', true);
			});
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">直播時間表</a>>>
			<a>編輯</a>	
		</div>			
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="lbuserid">直播主：</label></td>
					<td>
						<select name="lbuserid" onchange="livebroadcast(this.value);">
							<option value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['live_broadcast_user'])) {
									foreach ($this->tplVar['table']['rt']['live_broadcast_user'] as $key => $value) {
										echo '<option value="'.$value['lbuserid'].'">'.$value['lbname'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="lbid">直播間：</label></td>
					<td>
						<select name="lbid">
							<option rel="" value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['live_broadcast'])) {
									foreach ($this->tplVar['table']['rt']['live_broadcast'] as $key => $value) {
										echo '<option rel="'.$value['lbuserid'].'" value="'.$value['lbid'].'">'.$value['name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="ontime">開始時間：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start" value="<?php echo $this->tplVar['table']['record'][0]['ontime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="offtime">結束時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>" data-org="<?php echo date("Y-m-d H:i", strtotime("+1 year")); ?>"/></td>
				</tr>				
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="lbsid" value="<?php echo $this->tplVar["status"]["get"]["lbsid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>