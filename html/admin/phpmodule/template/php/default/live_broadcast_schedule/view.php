<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">直播時間表</a>>>
			<a>列表</a>	
		</div>		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_lbuserid">直播主：</span>
					<select name="search_lbuserid">
						<option value="" >請選擇</option>
						<?php foreach ($this->tplVar['table']['rt']['live_broadcast_user'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['lbuserid']; ?>" <?php echo ($this->io->input['get']['search_lbuserid']==$cv['lbuserid']) ? "selected" : "";?> ><?php echo $cv['lbname']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>			
				<li class="search-field">
					<span class="label" for="search_lbid">直播間：</span>
					<select name="search_lbid">
						<option value="" >請選擇</option>
						<?php foreach ($this->tplVar['table']['rt']['live_broadcast'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['lbid']; ?>" <?php echo ($this->io->input['get']['search_lbid']==$cv['lbid']) ? "selected" : "";?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>				
				<li class="search-field">
					<span class="label" for="search_btime">開始時間：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">結束時間：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th>直播時間表ID</th>
								<th>直播主名稱</th>
								<th style="max-width:200px;">直播間名稱</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_ontime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_ontime=desc">開始時間<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_ontime"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=">開始時間<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=asc">開始時間<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_offtime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_offtime=desc">結束時間<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_offtime"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=">結束時間<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=asc">結束時間<span class="sort init"></span></a>
								<?php endif; ?>
								</th>								
								<th>啟用</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/lbsid=<?php echo $rv["lbsid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/lbsid=<?php echo $rv["lbsid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="lbuserid"><?php echo $rv['lbsid']; ?></td>
								<td class="column" name="lbname"><?php echo $rv['lbname']; ?></td>
								<td class="column" name="phone"><?php echo $rv['name']; ?></td>
								<td class="column" name="ontime"><?php echo $rv['ontime']; ?></td>
								<td class="column" name="offtime"><?php echo $rv['offtime']; ?></td>								
								<td class="column" name="switch"><?php echo $rv['switch']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>