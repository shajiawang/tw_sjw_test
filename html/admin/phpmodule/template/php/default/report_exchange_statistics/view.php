<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_epid">兌換商品代碼：</span>
					<input type="text" name="epid" value="<?php echo $this->io->input['get']['epid']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_epname">兌換商品名稱：</span>
					<input type="text" name="epname" value="<?php echo $this->io->input['get']['epname']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_ontime">兌換商品上架日期：</span>
					<input name="ontimefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['ontimefrom']; ?>" /> ~ 
					<input name="ontimeto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['ontimeto']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_offtime">兌換商品下架日期：</span>
					<input name="offtimefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['offtimefrom']; ?>" /> ~ 
					<input name="offtimeto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['offtimeto']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_total_price">兌換點數：</span>
					<input type="text" name="total_price" size="20" value="<?php echo $this->io->input['get']['total_price']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_cost_price">商品成本：</span>
					<input type="text" name="cost_price" size="20" value="<?php echo $this->io->input['get']['cost_price']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_depositfrom">兌換商品分類：</span>
					<select name="epcid" id="epcid">
						<option value="">全部</option>
						<?php 
							foreach ($this->tplVar['epcArr'][1] as $key1 => $value1) { 
								echo '
						<option value="'.$value1['epcid'].'" '.(($this->io->input['get']['epcid'] == $value1['epcid']) ? "selected" : "").' >'.$value1['name'].'</option>';
								
								foreach ($this->tplVar['epcArr'][2][$value1['epcid']] as $key2 => $value2) {
									echo '
						<option value="'.$value2['epcid'].'" '.(($this->io->input['get']['epcid'] == $value2['epcid']) ? "selected" : "").' >&nbsp;&nbsp;&nbsp;&nbsp;'.$value2['name'].'</option>';
								}
							}
						?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_exchange_count">兌換次數：</span>
					<input type="text" name="exchange_count" size="20" value="<?php echo $this->io->input['get']['exchange_count']; ?>" />
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
            </ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/productpic/" >當年度報表</a>						
						</div>							
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>兌換商品代碼</th>
								<th>兌換商品名稱</th>
								<th>兌換商品上架日期</th>
								<th>兌換商品下架日期</th>
								<th>兌換點數</th>
								<th>商品成本</th>
								<th>兌換商品分類</th>
								<th>兌換次數</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="epid"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/epview/?epid=<?php echo $rv['epid']; ?>" ><?php echo $rv['epid']; ?></a></td>
								<td class="column" name="epname"><?php echo $rv['epname']; ?></td>
								<td class="column" name="ontime"><?php echo $rv['ontime']; ?></td>
								<td class="column" name="offtime"><?php echo $rv['offtime']; ?></td>
								<td class="column" name="total_price"><?php echo $rv['total_price']; ?></td>
								<td class="column" name="cost_price"><?php echo $rv['cost_price']; ?></td>
								<td class="column" name="epcname"><?php echo $rv['epcname']; ?></td>
								<td class="column" name="exchange_count"><?php echo $rv['exchange_count']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>