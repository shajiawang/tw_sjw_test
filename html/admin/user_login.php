<?php
require_once '/var/www/html/admin/phpmodule/include/config.ini.php';
require_once '/var/www/html/admin/libs/admin.inc.php';
require_once '/var/www/html/admin/libs/helpers.php';

session_start();

$io = new intputOutput();
//print_R($io->input['post']);

// Check Variable Start
if (empty($io->input['post']["name"]) ) {
	jsAlertMsg('登錄帳號錯誤!!');
}
if (empty($io->input['post']["passwd"]) ) {
	jsAlertMsg('登錄密碼錯誤!!');
}
// Check Variable End
require_once "saja/mysql.ini.php";

$model = new mysql($config["db"][0]);
$model->connect();
$db_user = $config["db"][0]["dbname"];
//print_R($db_user);exit;
require_once "saja/convertString.ini.php";
$str = new convertString();

##############################################################################################################################################
// Table Start

$query ="
SELECT u.*
FROM `{$db_user}`.`{$config['default_prefix']}admin_user` u
WHERE 
	u.prefixid = '".$config['default_prefix_id']."' 
	AND u.name = '".$io->input['post']["name"]."' 
	AND u.switch = 'Y' 
";
$table = $model->getQueryRecord($query);

if (empty($table['table']['record'])) {
	jsAlertMsg('登錄帳號不存在!!');
	die();
}
$user = $table['table']['record'][0];
	
$passwd = $io->input['post']['passwd']; //$this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key);
if ($user['passwd'] !== $passwd ) {
	jsAlertMsg('登錄密碼錯誤!!');
}

// push_notify(1705,"登入title","登入bodybody");
// Table End 
##############################################################################################################################################

$_SESSION['admin_user'] = $user;
$_SESSION['user'] = $user;  
error_log("Admin user : ".$user['userid']);

/*if (!empty($io->input['get']['location_url'])) {
	header("location:".urldecode(base64_decode($io->input['get']['location_url'])));
}
else {*/
	header("location:".$config['default_main'] .'?back=future');
//}
