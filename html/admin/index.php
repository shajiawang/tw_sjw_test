<?php
session_start();

// error_log("Request URI :".$_SERVER['REQUEST_URI']);

if( (isset($_GET['back']) && $_GET['back']=="future")) {
    // error_log("111111");
	$_SESSION['back']="future"; 
	setcookie("back", "future", time()+3600*24*30);
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/admin/');
	exit;
}
else if( $_GET['back']=="" && $_SESSION['back']=="future" && $_COOKIE['back']=="future" ) {
	if($_GET){
		$arr = explode('/',key($_GET));
		error_log(json_encode($arr));
		foreach($arr as $ak => $av){
			if($av != ''){
				$path_arr_key[] = $ak ; 
				$path_arr_val[] = $av ; 
			}
		}
		//echo count($path_arr_key); exit ;
		if(count($path_arr_key) == '1'){
			$_GET["fun"] =  $path_arr_val[0] ; 
			$_GET["act"] =  'view' ; 
			if (isset($path_arr_val[2])) {
				$_GET[$path_arr_val[2]] =  $_GET[key($_GET)] ; 
			}
		}
		else{
			$_GET["fun"] =  $path_arr_val[0] ; 
			$_GET["act"] =  $path_arr_val[1] ; 
			if (isset($path_arr_val[2])) {
				$_GET[$path_arr_val[2]] =  $_GET[key($_GET)] ; 
			}
		}
	}
	//IP ADDRESS
	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    // $temp_ip = split(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
	    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
	    $ip = $temp_ip[0];
		// error_log("8888888");
	} else {
	    // error_log("9999999");
	    $ip = $_SERVER['REMOTE_ADDR'];
	}
	
	if (empty($_SESSION['admin_user'])) {
		//alert('請先登入管理者帳號!!');
		echo "
			<html>
			<head>
			<script type=\"text/javascript\">
			<!--
				top.location.href = '/admin/login.php' ; 
			-->	
			</script>
			</head>
			</html>
		";
		exit ; 
	}
	else if (( $_SESSION['admin_user']['userid'] != '1' 
	        && $_SESSION['admin_user']['userid'] != '2' 
			&& $_SESSION['admin_user']['userid'] != '3' 
			&& $_SESSION['admin_user']['userid'] != '4'	
			&& $_SESSION['admin_user']['userid'] != '5' 
			&& $_SESSION['admin_user']['userid'] != '6' 
			&& $_SESSION['admin_user']['userid'] != '7' 
			&& $_SESSION['admin_user']['userid'] != '8' 
			&& $_SESSION['admin_user']['userid'] != '9' 
			&& $_SESSION['admin_user']['userid'] != '10'
			&& $_SESSION['admin_user']['userid'] != '11' 
			&& $_SESSION['admin_user']['userid'] != '12' 			
			&& $_SESSION['admin_user']['userid'] != '26'
			&& $_SESSION['admin_user']['userid'] != '27'
			&& $_SESSION['admin_user']['userid'] != '29'
			&& $_SESSION['admin_user']['userid'] != '32'
			&& $_SESSION['admin_user']['userid'] != '36'
			&& $_SESSION['admin_user']['userid'] != '41'
			&& $_SESSION['admin_user']['userid'] != '46'
			&& $_SESSION['admin_user']['userid'] != '48'
			&& $_SESSION['admin_user']['userid'] != '51'
			) 
	         
			 ) {
		//alert('您無權限登入!!');
		echo "
			<html>
			<head>
			<script type=\"text/javascript\">
			<!--
				alert('您無權限登入!!');
				top.location.href = '/admin/login.php' ; 
			-->	
			</script>
			</head>
			</html>
		";
		exit ; 
	}
	else {
		require_once '/var/www/html/admin/phpmodule/include/config.ini.php';
		//ini_set('display_errors',1);
		/*
		ini_set('session.save_handler' , 'memcache');
		ini_set('session.save_path' , 'tcp://'.$config['memcache']['server_list']['session']['ip'].':'.$config['memcache']['server_list']['session']['port'].'');
		ini_set('session.gc_maxlifetime' , '2592000');
		*/
		ob_start('ob_gzhandler');
		//session_start();
		
		require_once $config['path_class']."/admin.ini.php";
		$obj = new initAdmin(); 
	}
	
}
else {
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/site/index.htm');
	exit;
}
