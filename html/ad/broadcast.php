<!DOCTYPE html>
<html prefix="og:http://ogp.me/ns# fb:http://ogp.me/ns/fb#" >
<!-- html xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" -->
<head>
<meta charset="utf-8">
<title>閃殺QRcode　好康放送</title>
<meta property="og:title" content="" />
<meta property="og:type" content="article" />
<meta property="og:image" content="" />
<meta property="og:url" content="http://bid.shajiawang.com/kuso/product/broadcast/" />
<meta property="og:description" content="" />
<meta name="mobileoptimized" content="0" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<meta name="screen-orientation" content="portrait" />
<meta name="browsermode" content="application" />
<meta property="wb:webmaster" content="1a4d5f9e7b901534" />
<meta property="qc:admins" content="2405036366630121171676375731457" />
<!-- SEO: -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="歡迎來到殺價王，全球首創殺價式拍賣行銷平臺" /> 
<meta name="keywords" content="殺價王, 殺價式拍賣, 競標網站" /> 
<!-- /SEO -->         
<link rel="stylesheet" href="/kuso/static/themes/saja.min.css" />
<link rel="stylesheet" href="/kuso/static/themes/jquery.mobile.icons.min.css" />
<script src="/kuso/static/js/jquery-2.1.0.min.js"></script>
<link rel="stylesheet" href="/kuso/static/css/jquery.mobile.structure-1.4.3.min.css" />
<script src="/kuso/static/js/jquery.mobile-1.4.3.min.js"></script>
<script src="/kuso/static/js/jquery.timers-1.2.js"></script>
<link rel="stylesheet" href="/kuso/static/css/share.css" />
<script src="/kuso/static/js/share.js"></script>
<!-- jquery cycle2 begin -->
<script src="/kuso/static/js/jquery.cycle2.min.js"></script>
<script src="/kuso/static/js/jquery.cycle2.caption2.min.js"></script>
<script src="/kuso/static/js/jquery.cycle2.center.min.js"></script>
<!-- jquery cycl2e end-->
<script src="/kuso/static/js/jquery.cookie.js"></script>
<!�V[if IE]>
<script src="/kuso/javascript/html5.js"></script>
<![endif]->
</head>
<body>
<div id="fb-root"></div>
<div data-role="page" id="kuso_qrcode_broadcast">
	<div data-role="content" class="ui-content2">
		<div class="article" align="center"　style="background-color:#3B3B3B" >
		<ul>
		  <li>
			  <div  id="rotate_imgs" data-cycle-center-horz=true
									 data-cycle-center-vert=true
									 data-cycle-caption="#alt-caption"
									 data-cycle-caption-template="{{alt}}">
			  </div>
			  <div id="alt-caption" align="center" ></div>
		  </li>
	      <!-- div id="adjustbar" align="center" >
			  <label for="broadcast_timeout">每組廣告顯示秒數 : </label>
			  <input type="range" name="broadcast_timeout" id="broadcast_timeout" value="" min="10" max="60" >
			  <input type="button" id="btn_adjust" value="調整秒數"> 
		  </div -->
		  <li><div data-type="horizontal"><label for="clock">現在時間 : </label><label id="clock" /></div></li>
		  <li><div data-type="horizontal"><label for="latlng">地理位置 : (Latitude,Longitude)=</label><label id="latlng" /></div></li>
        </ul>
 		</div><!-- /article -->
	</div><!--data-role="content"-->
</div><!--data-role="page"-->
</body>
<script>
		    var imgTotal=0;
		    $(document).on('pageshow',
			    function (event){
					if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(
							   geoSuccess
							  ,geoError
							  ,{enableAcuracy:false, maximumAge:0,timeout:600000}
						);
					} else {
					   alert("您的瀏覽器不支援地理定位功能!!");
					}
				});
		
		    $(document).ready(function() {
			    $(this).stopTime('chktime');
				var t = new Date();
				$('#clock').text(padding0(t.getHours())+":"+padding0(t.getMinutes())+":"+padding0(t.getSeconds()));
			    $(this).everyTime('1s','chktime',function() {  
				    t = new Date();
					$('#clock').text(padding0(t.getHours())+":"+padding0(t.getMinutes())+":"+padding0(t.getSeconds()));
					if(t.getMinutes()% 5 ==0 && t.getSeconds()==0) { 
					   // 每5分的00秒更新一次
					   location.reload();
					} 
					t=null;
				});
			});
				
            function padding0(num) {
                     if(num<10) return "0"+num;
					 else  return ""+num;
            }			
		    
			function geoSuccess(position) {
				  // $('#lat').text(position.coords.latitude);
				  // $('#lng').text(position.coords.longitude);
				  $('#latlng').text('('+position.coords.latitude+','+position.coords.longitude+')');
				  $.post('/kuso/product/getBroadcastInfo/',
				         {lat:position.coords.latitude, 
						  lng:position.coords.longitude},
						  function(data){
						       var arrJSON=$.parseJSON(data);
							   for(imgTotal=0;imgTotal<arrJSON.length;++imgTotal) {
							       var img = $("<img id='img_"+imgTotal+"'>");
								   img.attr('src', arrJSON[imgTotal].thumbnail_url);
								   img.attr('alt', arrJSON[imgTotal].name);
								   img.attr('data-cycle-timeout', parseInt(arrJSON[imgTotal].broadcast_timeout)*1000);
								   img.appendTo('#rotate_imgs');
								}
								$('#rotate_imgs').cycle({ 
									fx:    'scrollHorz', 
									speed:  1000      // 進入
								});								
   						  });
			}
	
			function geoError(error) {
			  var errorTypes={
					0:"不明原因錯誤",
					1:"使用者拒絕提供位置資訊",
					2:"無法取得位置資訊",
					3:"位置查詢逾時"
				   };
				  alert(errorTypes[error.code]);
				  // alert("code=" + error.code + " " + error.message); //開發測試時用
			 }
			 		 
			 
		</script>
</html>