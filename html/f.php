<?php
date_default_timezone_set("Asia/Taipei");
	
function base64UrlEncode($str) {
	 $find = array('+', '/');
     $replace = array('-', '_');
     return str_replace($find, $replace, base64_encode($str));
}

$access_key = '4985285b-fb2a-4dbe-bce5-856a4a763452';
$secret_key = 'a99e6466-ab99-4020-a65d-6e25261e49aa';

$jwtToken = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxOTM2IiwiaWF0IjoxNTU0ODgwODI2LCJpc3MiOiJYSUFiS0hWeTF2bm1JOThrTXR1aTFZdmQxT3FHMlFUZCJ9.9Ns4fQfSmaU2iwunVHzoDogkfLUEi8tPIc4FqSTWkDQ';
$method = 'POST';
$host = 'apitest.kgibank.com';
$endpoint = 'https://api.fintechspace.com.tw/bill/v1/ebill/fiscQuery?jwt='.$jwtToken;
$content_type = 'application/json';
$canonical_uri = '/v1/ebill/fiscQuery';

$amz_date = gmdate("D, d M Y H:i:s +0000");
$amz_date = "Mon, 15 Apr 2019 03:50:29 +0000";
$txDateTime=date("YmdHis");
$txDateTime="20190410152611";


$request_params =  '{';
    $request_params .=  '"uuid": "173133-731785",';
    $request_params .=  '"txDateTime": "'.$txDateTime.'",';
    $request_params .=  '"merchant": "53754652-001",';
    $request_params .=  '"uniqueId": "11111122222",';
    $request_params .=  '"feeRefId": "00067051",';
    $request_params .=  '"param": {}';
    $request_params .=  '}';
	
echo ("<br>[aws.php request_params] : ".$request_params);

$content_md5 = base64_encode(md5(utf8_encode($request_params),true));
echo("<br>[aws.php content_md5] : ".$content_md5);

$canonical_headers = $method.'\n'. 
                     $content_md5.'\n'.
                     $content_type.'\n'.
                     $amz_date .'\n'.
                     $canonical_uri;

echo("<br>[aws.php canonical_headers] : ".$canonical_headers);                     

$signature = base64_encode(hash_hmac('sha1', utf8_encode($canonical_headers), utf8_encode($secret_key),true));
echo("<br>[aws.php signature]: ".$signature); 

$headers = array();
$headers['Content-Type']=$content_type;
$headers['Date']=$amz_date;
$headers['Content-MD5']=$content_md5;
$headers['Authorization']="AWS ".$access_key.":".$signature;
echo("<br>[aws.php headers] : ".json_encode($headers));

/*
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$endpoint);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
curl_setopt($ch, CURLOPT_POSTFIELDS, $request_params); 
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
$response = curl_exec($ch);

curl_close($ch);

echo "<br>response : ".$response;
*/
?>