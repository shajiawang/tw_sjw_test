<?php
//裝載模板文件
require_once "/var/www/html/site/lib/config.php";
require_once "/var/www/lib/saja/mysql.ini.php";
//require_once "/var/www/html/site/lib/wechat.config.php";
require_once "/var/www/html/site/lib/wechat.class.php";

$model = new mysql($config["db"][0]);
$model->connect();
$accessToken = weixin_access_token($config, $model);

$wechatObj = new Wechat();					//实例化微信类
$wechatObj->deleteMenu($accessToken);	//删除菜单
$wechatObj->createMenu($accessToken);	//创建菜单

echo "Finish!";
?>