<?php
/**
 * Script para la generaci鏮 de CAPTCHAS
 *
 * @author  Jose Rodriguez <jose.rodriguez@exec.cl>
 * @license GPLv3
 * @link    http://code.google.com/p/cool-php-captcha
 * @package captcha
 * @version 0.3
 *
 */

session_start();

include_once 'simple_captcha.php';
$captcha = new SimpleCaptcha();

// OPTIONAL Change configuration...
//$captcha->wordsFile = 'words/es.php'; //是否讀取字典檔 若否則改為null 
//$captcha->session_var = 'secretword'; //可自訂session的變數名稱 預設為captcha
//$captcha->imageFormat = 'png'; //圖片格式
$captcha->lineWidth = 2; //干擾線寬度
//$captcha->scale = 3; $captcha->blur = true; //是否帶有模糊效果及其比例
//$captcha->resourcesPath = "/var/cool-php-captcha/resources"; //附件的路徑

// OPTIONAL Simple autodetect language example //根據語系的不同切換字典檔 
/*
if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    $langs = array('en', 'es');
    $lang  = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    if (in_array($lang, $langs)) {
        $captcha->wordsFile = "words/$lang.php";
    }
}
*/

// Image generation
$captcha->CreateImage();
?>
