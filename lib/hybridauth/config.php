<?php
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

$config = array(
	"base_url" => "http://howard.sajar.com.tw/site/user/sso_login", 

	"providers" => array ( 
		// openid providers
		"OpenID" => array (
			"enabled" => false
		),

		"Yahoo" => array ( 
			"enabled" => true,
			"keys"    => array ( "key" => "dj0yJmk9UXVYeFkycGFjQnY1JmQ9WVdrOVZ6aDNPSFpoTTJNbWNHbzlNakV4TkRZM05qWTJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD1mMA--", "secret" => "14cd53cd45d6c7f4950c86abd53a6c125f64ac79" ),
		),

		"AOL"  => array ( 
			"enabled" => false 
		),

		"Google" => array ( 
			"enabled" => false,
			"keys"    => array ( "id" => "", "secret" => "" ), 
		),

		"Facebook" => array ( 
			"enabled" => true,
			"keys"    => array ( "id" => "241362542699671", "secret" => "7308a809c267dd4f71fda35b68518523" ), 
			"scope"   => "email"
		),

		"Twitter" => array ( 
			"enabled" => true,
			"keys"    => array ( "key" => "8Q1cG2HoEESpldzo1POBQ", "secret" => "9nBB1smgwGTyAcXLKJp7KnZEaurjYqhFnTNe2YxUrA" ) 
		),

		// windows live
		"Live" => array ( 
			"enabled" => false,
			"keys"    => array ( "id" => "", "secret" => "" ) 
		),

		"MySpace" => array ( 
			"enabled" => false,
			"keys"    => array ( "key" => "", "secret" => "" ) 
		),

		"LinkedIn" => array ( 
			"enabled" => false,
			"keys"    => array ( "key" => "", "secret" => "" ) 
		),

		"Foursquare" => array (
			"enabled" => false,
			"keys"    => array ( "id" => "", "secret" => "" ) 
		),
	),

	// if you want to enable logging, set 'debug_mode' to false  then provide a writable file by the web server on "debug_file"
	"debug_mode" => false,

	"debug_file" => "",
);
