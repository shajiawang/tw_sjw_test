<?php
include_once "openid/openid.php";

class walletOpenID extends LightOpenID
{
	public $default_identity = array(
				"yahoo"=>"https://me.yahoo.com",
				"google"=>"https://www.google.com/accounts/o8/id"
				);

	public $default_required = array(
				"namePerson", 
				"namePerson/friendly", 
				"namePerson/first", 
				"namePerson/last", 
				"contact/email", 
				"pref/language"
				);

	function __construct()
	{
		parent::__construct();
	}

	public function getYahooAuthUrl($returnUrl = "")
	{
		$returnUrl != "" && ($this->returnUrl = $returnUrl);
		$this->required = $this->default_required;
		$this->identity = $this->default_identity["yahoo"];
		return $this->authUrl();
	}

	public function getGoogleAuthUrl($returnUrl = "")
	{
		$returnUrl != "" && ($this->returnUrl = $returnUrl);
		$this->required = $this->default_required;
		$this->identity = $this->default_identity["google"];
		return $this->authUrl();
	}

	public function getIdentity()
	{
		return trim($this->identity);
	}

	public function getName()
	{
		if (is_array($attr = $this->getAttributes()))
		{
			if (isset($attr['namePerson'])) {
				return trim($attr['namePerson']);
			} else {
				return trim($attr['namePerson/last'].$attr['namePerson/first']);
			}
		}
		return "";
	}
	
	public function getEmail()
	{
		if (is_array($attr = $this->getAttributes()))
		{
			if (isset($attr['contact/email'])) {
				return $attr['contact/email'];
			}
		}
		return "";
	}
	
	public function getOtid()
	{
		if (is_array($this->data))
		{
			if (isset($this->data["openid"])) {
				return $this->data["openid"];
			}
		}
		return "";
	}
}

?>
