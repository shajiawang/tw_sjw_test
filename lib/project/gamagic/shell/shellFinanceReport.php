<?php
include_once "snsplus/gamagic/include/test-config.ini.php"; 
include_once "snsplus/mysql.ini.php";
include_once "snsplus/convertString.ini.php";
include_once "snsplus/user.ini.php";

$obj->config = (object)$config; 
$obj->model[0] = new mysql($config['db'][0]);
$obj->model[0]->connect();
$obj->model[1] = new mysql($config['db'][1]);
$obj->model[1]->connect();
$obj->string = new convertString();

$query = "
	SELECT th.insertt, th.thid, th.uid, o.amount, ol.pid FROM `sns_exchange`.`sns_transaction_history` th 
	LEFT OUTER JOIN `sns_product`.`sns_order` o ON o.order_id = th.order_id 
	LEFT OUTER JOIN `sns_product`.`sns_order_list` ol ON ol.order_id = o.order_id 
	WHERE 1 
	AND o.order_id IS NOT NULL 
	AND ol.order_list_id IS NOT NULL 
";
$history = $obj->model[1]->getQueryRecord($query);

$query = "SELECT * FROM `sns_product`.`sns_product` WHERE 1";
$products = $obj->model[1]->getQueryRecord($query);
foreach ($products['table']['record'] as $k => &$v) {
	$products[$v['pid']] = $v;
}
unset($products['table']);

$query = "SELECT * FROM `sns_game`.`sns_game` WHERE 1";
$games = $obj->model[0]->getQueryRecord($query);
foreach ($games['table']['record'] as $k => &$v) {
	$games[$v['game_id']] = $v;
}
unset($games['table']);

foreach ($history['table']['record'] as &$v) {
	$v['name'] = $games[$products[$v['pid']]['game_id']]['name'];
	$v['item'] = $products[$v['pid']]['name'];
	unset($v['pid']);
}
print_r($history);
