<?php
/**
 * wallet Exchange object
 *
 * @author Albert
 * @version $Id:walletExchange.php, v1.0 2010-11-21 Albert $
 * @package wallet
 * @copyright 2011(C)Snsplus
 */
class walletExchange{

	public $params;
	public $obj;
	public $dbname;
	public $default_prefix;
	public $model;	
	
	public function init($obj,$params = ''){

		if(empty($this->model)){
			$this->model = new mysql($obj->config->db[1]);
			$this->model->connect();
		}
		$this->params = $params;
		$this->obj = $obj;
		$this->dbname = $this->obj->config->db[12]["dbname"];
		$this->default_prefix = $this->obj->config->default_prefix;
	}
	
	/* 加點程序 */
	public function doExchangeHistory($obj,$params = ''){
		self::init($obj,$params);
		
		// 訂單是否存在
		$orderCt = self::doCheckExchangeHistory();
		if (!empty($orderCt)) { 
			return self::returnParams('0','order_already_exists');
			die();
		}
			
		self::setExchangeHistoryRecord();
		self::setTransactionHistoryRecord();
		
	}
	/*
	* Albert 2012-08-07
	* 確認該訂單是否已 rollback 過
	* input : array order_id
	* output : boolean
	*/
	public function checkOrderRollback($obj,$params){
		self::init($obj,$params);
		$checkRollback = self::doGetRollbackTransactionByOrder();
		
		return empty($checkRollback['thid'])?false:true;
	}
	
	public function getExchangeHistoryByOrder($obj,$params){
		self::init($obj,$params);
		$exchangeHistroy = self::doCheckExchangeHistory();
		
		return $exchangeHistroy;
	}
	/**/
	public function getTransactionByOrder($obj,$params){
		self::init($obj,$params);
		
		$transaction = self::doGetTransactionByOrder();
		
		return $transaction;
	}	
	public function getSuccessTransactionByOrder($obj,$params){
		self::init($obj,$params);
		$transaction = self::doGetSuccessTransactionByOrder();
		return $transaction;
	}		
	/* 取得轉點資訊 */
	public function getTransaction($obj,$params = ''){
		self::init($obj,$params);
		return self::doGetTransaction();
	}
	/* 轉點完成 */
	public function completeTransaciton($obj,$params = ''){
		self::init($obj,$params);
		self::doCompleteTransaction();
		
		self::sendSms($obj,array('status'=>'success'));
	}

	/* 倒回程序 */
	public function doRollbackHistory($obj,$params){
		
		self::init($obj,$params);
		
		//if(!empty($this->params['thid']) && !empty($this->params["userid"]) && !empty($this->params['minus_amount'])){
		$rh = self::doGetRollbackHistory();
		if($rh == false){
			self::setRollbackHistoryRecord();
			self::doRollbackTransaction();
			self::sendSms($obj,array('status'=>'rollback'));
			return $this->params['rhid'];
		}else{
			return false;
		}
	}
	public function checkTransaction($obj,$params){
		self::init($obj,$params);
		
		return self::doCheckTransaction();
	}
	
	/* Albert 2012-08-17
	*  發送sms
	*/
	
	public function testObj($obj,$params){
		self::init($obj,$params);
		
		return $this->obj;
	}
	/*
	* Albert 2012-08-17
	* 
	*/
	private function checkUserCellphone(){
		$query = "SELECT cellphone 
					FROM `".$this->obj->config->db[0]["dbname"]."`.`".$this->obj->config->default_prefix."user`
					WHERE userid = '".$this->params["userid"]."'";
					
		$result["table"] = "";
		$modelP = new mysql($this->obj->config->db[0]);
		$modelP->connect();		
        $result = $modelP->getQueryRecord($query,$obj);
		
		return $result["table"]["record"][0]['cellphone'];
					
			
	}	
	public function sendSms($obj,$params){
		//self::init($obj,$params);
		$cellphone = $this->checkUserCellphone();
		if($obj->config->sms['switch'] == true && !empty($cellphone)){
			$smsApi = $obj->config->sms['api'];//"http://smexpress.mitake.com.tw/SmSendTCC.asp";
			$smsAcc = $obj->config->sms['acc'];
			$smspw = $obj->config->sms['pw'];
			
			switch($params['status']){
				case 'success':
					if($obj->config->sms['send_success'] == true){
						$url = $smsApi.'?username='.$smsAcc.'&password='.$smspw.'&dstaddr='.$cellphone.'&smbody='.urlencode(iconv('utf8','TIS-620',sprintf($obj->config->sms['success_exchange_sms'], date('d/m/Y-H:i'))));
						$this->_curl($url);
					}
				break;
				case 'rollback':
					if($obj->config->sms['send_fail'] == true){
						$url = $smsApi.'?username='.$smsAcc.'&password='.$smspw.'&dstaddr='.$cellphone.'&smbody='.urlencode(iconv('utf8','TIS-620',sprintf($obj->config->sms['fail_exchange_sms'], date('d/m/Y-H:i'))));
						$this->_curl($url);
					}
				break;
			}		
		}
		return $url;

	}
	
	public function getExchangeList(){
		self::init($obj,$params);
		
		
	}
	
	public function checkTransactionSig($params,$secret){
	
		foreach($params as $paramsKey => $paramsValue){
			if($params != 'sig')
				$sigString .= $paramsKey."=".$paramsValue;
		}
		$sig = md5($sigString.$secret);
		
		return ($sig == $params['sig'])?true:false;
	}
	
	private function doGetSuccessTransactionByOrder(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->default_prefix."transaction_history` WHERE order_id = '".$this->params['order_id']."' AND `status` = 'Y' AND prefixid = '".$this->obj->config->default_prefix_id."'";
		$result = $this->model->getQueryRecord($query);
		$transaction = $result['table']['record'][0];
		return $transaction;
	}		
	
	private function doGetRollbackTransactionByOrder(){
		$query = "SELECT thid FROM `".$this->dbname."`.`".$this->default_prefix."transaction_history` th WHERE th.order_id = '".$this->params['order_id']."' AND th.status = 'R' AND prefixid = '".$this->obj->config->default_prefix_id."'";
		$result = $this->model->getQueryRecord($query);
		

		$rollback = $result['table']['record'][0];
		
		return $rollback;		
	}
	
	private function doGetTransactionByOrder(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->default_prefix."transaction_history` WHERE order_id = '".$this->params['order_id']."' AND `status` = 'N' AND prefixid = '".$this->obj->config->default_prefix_id."'";
	
		$result = $this->model->getQueryRecord($query);
		

		$transaction = $result['table']['record'][0];
		
		return $transaction;
	}	
	
	private function doGetExchangeList(){
		$query = "SELECT ehid FROM `".$this->dbname."`.`".$this->default_prefix."exchange_history` WHERE order_id = '".$this->params["order_id"]."'";
		
		$result = $this->model->getQueryRecord($query);
		
		return $result['table']['record'][0];
	}
	
	private function doGetRollbackHistory(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->default_prefix."rollback_history` WHERE thid = '".$this->params["thid"]."' AND uid = '".$this->params["uid"]."' AND prefixid = '".$this->obj->config->default_prefix_id."'";
		
		$result = $this->model->getQueryRecord($query);
		return empty($result['table']['record'][0]['rhid'])?false:$result['table']['record'][0];			
	}
	
	private function createUniqkey(){
		$key = md5(uniqid($this->default_prefix));
		return $key;
	}		
	private function returnParams($result,$message){
		$return_params = array("result" => $result,"message"=>$message);
		return json_encode($return_params);
	}		

	private function doCheckExchangeHistory(){
		$query = "SELECT ehid FROM `".$this->dbname."`.`".$this->default_prefix."exchange_history` WHERE order_id = '".$this->params["order_id"]."'";
		
		$result = $this->model->getQueryRecord($query);
		
		return $result['table']['record'];
	}
	
	private function doCheckTransaction(){
		$query = "SELECT thid, order_id FROM `".$this->dbname."`.`".$this->default_prefix."transaction_history` WHERE thid = '".$this->params['transaction_id']."' AND `status` = 'N' AND prefixid = '".$this->obj->config->default_prefix_id."'";
		$result = $this->model->getQueryRecord($query);
		return empty($result['table']['record'][0]['thid'])?false:$result['table']['record'][0];	
	}
	
	private function doCompleteTransaction(){
	
		$query = "UPDATE `".$this->dbname."`.`".$this->obj->config->default_prefix."transaction_history` SET `status` = 'Y',  modifyt = NOW() WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND thid = '".$this->params['thid']."'";
		
		$result = $this->model->query($query);
	}
	
	private function doRollbackTransaction(){
	
		$query = "UPDATE `".$this->dbname."`.`".$this->obj->config->default_prefix."transaction_history` SET `status` = 'R',  modifyt = NOW() WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND thid = '".$this->params['thid']."'";
		
		$result = $this->model->query($query);		
	}
	
	private function doGetTransaction(){
		
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."transaction_history` th WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND th.thid = '".$this->params['thid']."' AND th.status = 'N'";
		
        $result = $this->model->getQueryRecord($query);
		
		return $result;
	}	
	
	private function setExchangeHistoryRecord(){
		
		$ehid = self::createUniqkey();
		
		$query ="INSERT INTO 
				`".$this->dbname."`.`".$this->default_prefix."exchange_history`(
					`prefixid`, 
					`ehid`, 
					`uid`, 
					`order_id`, 
					`insertt`,
					`modifyt`
				)
				VALUES(
					'".$this->obj->config->default_prefix_id."',
					'".$ehid."' , 
					'".$this->params["userid"]."' , 
					'".$this->params["order_id"]."' , 
					now(),
					now()
				)
				";
		$this->model->query($query);
				
		$this->params['ehid'] = $ehid;
		
		return true;
	}
	
	private function setTransactionHistoryRecord(){
		
		$thid = self::createUniqkey();
		
		$query ="INSERT INTO 
				`".$this->dbname."`.`".$this->default_prefix."transaction_history`(
					`prefixid`, 
					`thid`, 
					`uid`, 
					`order_id`,
					`status`,
					`insertt`,
					`modifyt`
				)
				VALUES(
					'".$this->obj->config->default_prefix_id."',
					'".$thid."' , 
					'".$this->params["userid"]."' ,
					'".$this->params['order_id']."',
					'N' , 
					now(),
					now()
				)
				";
		$this->model->query($query);
				
		$this->params['thid'] = $thid;
		
		return true;
	}
	
	private function setRollbackHistoryRecord(){
	
		$rhid = self::createUniqkey();
		
		$query ="INSERT INTO 
				`".$this->dbname."`.`".$this->default_prefix."rollback_history`(
					`prefixid`, 
					`rhid`, 
					`uid`, 
					`thid`,
					`amount`,
					`insertt`,
					`modifyt`
				)
				VALUES(
					'".$this->obj->config->default_prefix_id."',
					'".$rhid."' , 
					'".$this->params["userid"]."' ,
					'".$this->params['thid']."',
					'".$this->params['minus_amount']."' , 
					now(),
					now()
				)
				";
		$this->model->query($query);
				
		$this->params['rhid'] = $rhid;
		
		return true;
	}
	
	private function _curl($url){
		$ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url);
        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 15 );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec ( $ch );
        curl_close ( $ch );
	}
}

?>