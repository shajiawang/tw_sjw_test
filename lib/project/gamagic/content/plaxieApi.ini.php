<?PHP
/**
 * plaxie Api object
 *
 * @author Albert
 * @version $Id:plaxieApi.ini.php, v1.0 2012-03-19 Albert $
 * @package wallet
 * @copyright 2012(C)Snsplus
 */
class plaxieApi{

	//public $params;
	public $api_url;
	

	public function init($params){
		$this->api_url = $params['api_url'].'?'.http_build_query($params['api_params']);
	}

	public function infoApi($params){
		self::init($params);
		self::_curl();
	}
	
	public function paymentApi($params){
		self::init($params);
		self::_curl();
	}
	
	public function loginApi($params){
		self::init($params);
		self::_curl();
	}
	
	private function _curl(){
		$ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $this->api_url);
        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 15 );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec ( $ch );
        curl_close ( $ch );
	}
}
?>