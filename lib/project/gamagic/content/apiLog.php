<?php
/**
 * wallet Product object
 *
 * @author Albert
 * @version $Id:walletProduct.php, v1.0 2010-11-21 Albert $
 * @package wallet
 * @copyright 2011(C)Snsplus
 */
class apiLog{

    public $params;
    public $obj;
    public $dbname;
    public $default_prefix;
    public $model;	

    public function init($obj,$params=''){
        if(empty($this->model)){
            $this->model = new mysql($obj->config->db[0]);
            $this->model->connect();
        }
        $this->params = $params;
        $this->obj = $obj;
        $this->dbname = $this->obj->config->db[0]["dbname"];
        $this->default_prefix = $this->obj->config->default_prefix;
        
    }
    
    public function setLog($obj,$params){
        self::init($obj,$params);
        
        self::doSetLog();
    }
    
    private function doSetLog(){
        $log_id = self::createUniqkey();
        
        $query ="INSERT INTO 
                `".$this->dbname."`.`".$this->default_prefix."api_log`(
                    `prefixid`, 
                    `log_id`,
                    `order_id`,
                    `gsid`,
                    `request_url`, 
                    `request_params`, 
                    `response_content`, 
                    `insertt`
                )
                VALUES(
                    '".$this->obj->config->default_prefix_id."',
                    '".$log_id."',
                    '".$this->params['order_id']."',
                    '".$this->params["gsid"]."' , 
                    '".$this->params["request_url"]."' , 
                    '".$this->params['request_params']."',
                    '".$this->params["response_content"]."', 
                    now()
                )
                ";
        $this->model->query($query);
                
        $this->params['log_id'] = $log_id;
        
        return true;
    }
    private function createUniqkey(){

        $key = md5(uniqid($this->default_prefix));
        return $key;
    }	
}
?>