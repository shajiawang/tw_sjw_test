<?php
class Game_top {
	public $mc;
	public $obj;
	public $cachetime;

	function __construct(&$obj) {
		if (empty ($obj)) {
			die();
		}
		$this->obj = $obj;
		if(empty($obj->model)){
			$this->model = new mysql($obj->config->db[0]);
		} else {
			$this->model = $obj->model;
		}
		if(empty($obj->mc)){
			$this->mv = mc();
		} else {
			$this->mc = $obj->mc;
		}

		$this->model->connect();
		$this->cachetime = 86400;
		$this->mc->connectionMemcache($obj->config->memcache["server_list"]["ap1"]["ip"],$obj->config->memcache["server_list"]["ap1"]["port"]);
	}

        public function getskybar($game_id) {
                $key = 'sns_game_skybar';
                $skybar = $this->mc->readMemcache($key);
                if (empty ($skybar)) {
                        $query = "
                                SELECT * from `".$this->obj->config->db[10]["dbname"]."`.`".$this->obj->config->default_prefix."game_skybar` gs
                                WHERE 1
                                AND gs.prefixid = '".$this->obj->config->default_prefix_id."'
				AND gs.game_id = '".$game_id."'
				AND gs.switch = 'Y'
				ORDER BY gs.modifyt DESC
                        " ;
                        $result = $this->model->getQueryRecord($query);
                        $this->mc->writeMemCache($key, $result['table']['record'], $this->cachetime) ;
                        $skybar = $result['table']['record'];
                }
                return $skybar;
        }

        public function clear_skybar() {
                $key = 'sns_game_skybar';
                $this->mc->deleteMemcache($key);
        }
}
