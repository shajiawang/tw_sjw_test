<?php

/**
 * BI Api object
 *
 * @author Albert
 * @version $Id:bi.ini.php, v1.0 2012-06-27 Albert $
 * @package wallet
 * @copyright 2012(C)Snsplus
 */
class bi{

	public $api_url;
	
	public function init($params){
		$this->api_url = $params['api_url'].'?'.http_build_query($params['api_params']);
	}	
	
	public function click($params){
	
		$clickAprams = array('api_url'		=>	$params['api_url'],
							'api_params'	=> array('game_id'	=>	$params['game_id'],
													 'event_id'	=>	'1',
													 'uid'		=>	(isset($params['uid']) && !empty($params['uid']))?$params['uid']:'000000',
													 'p1'		=>	date('Ymdhis'),
													 'p2'		=>  (isset($params['p2']) && !empty($params['p2']))?$params['p2']:'0')
							 );
		
		
		self::init($clickAprams);
		//echo $this->api_url;
		self::_curl();
	}
	
	public function install($params){
		$clickAprams = array('api_url'		=>	$params['api_url'],
							'api_params'	=> array('game_id'	=>	$params['game_id'],
													 'event_id'	=>	'2',
													 'uid'		=>	(isset($params['uid']) && !empty($params['uid']))?$params['uid']:'000000',
													 'p1'		=>	date('Ymdhis'),
													 'p2'		=>  (isset($params['p2']) && !empty($params['p2']))?$params['p2']:'0')
							 );
		
		
		self::init($clickAprams);
		//echo $this->api_url;
		self::_curl();		
	}
	
	public function login($params){
		$clickAprams = array('api_url'		=>	$params['api_url'],
							'api_params'	=> array('game_id'	=>	$params['game_id'],
													 'event_id'	=>	'12',
													 'uid'		=>	(isset($params['uid']) && !empty($params['uid']))?$params['uid']:'000000',
													 'p1'		=>	date('Ymdhis'),
													 'p2'		=>  (isset($params['p2']) && !empty($params['p2']))?$params['p2']:'0')
							 );
		
		
		self::init($clickAprams);
		//echo $this->api_url;
		self::_curl();		
	}	
	
	private function _curl(){
		$ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $this->api_url);
        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 15 );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec ( $ch );
        curl_close ( $ch );
	}
}
?>