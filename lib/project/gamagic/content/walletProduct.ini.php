<?php
/**
 * wallet Product object
 *
 * @author Albert
 * @version $Id:walletProduct.php, v1.0 2010-11-21 Albert $
 * @package wallet
 * @copyright 2011(C)Snsplus
 */
class walletProduct{

	public $params;
	public $obj;
	public $dbname;
	public $default_prefix;
	public $model;	
	//private static $depositUserAmountCache;
	
	public function init($obj,$params=''){
		if(empty($this->model)){
			$this->model = new mysql($obj->config->db[1]);
			$this->model->connect();
		}
		$this->params = $params;
		$this->obj = $obj;
		$this->dbname = $this->obj->config->db[11]["dbname"];
		$this->default_prefix = $this->obj->config->default_prefix;
		
	}
	
	
	public function getOrder($obj,$params){
	
		self::init($obj,$params);
		
		$order = array('order' 		=> self::doGetOrder(),
					   'orderList'  => self::doGetOrderList());
						
		return $order;
		
	}
	
	/*
	* albert 20120104
	*/
	public function checkOrder($obj,$params){
		self::init($obj,$params);
	
		$order = self::doGetOrder();
		
		return $order;
		
	}
	
	//取得直購訂單
	public function getDirectOrder($obj,$params){
		self::init($obj,$params);
		
		$order = array('order'		=>	self::doGetOrder(),
					   'orderList'	=>	self::doGetDirectOrderList());
					   
		return $order;
	}
	
	public function setDirectOrder($obj,$params){
	
		self::init($obj,$params);
		
		self::doSetDirectOrder();
		
		$product = self::doGetDirectProduct();
		
		$this->params['pid'] = $product[0]['pid'];
		
		self::doSetDirectOrderList();
		
	}
	
	public function getProductList(){
		
	}
	
	public function getProductByGame($obj,$params){
		self::init($obj,$params);
		
		return $this->doGetProductByGame();
	}
	
	private function doCheckOrder(){
		
	}
	
	private function doGetProductByGame(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->default_prefix."product` WHERE game_id = '".$this->params['game_id']."'";
		
		//echo $query."<br>";
		$result = $this->model->getQueryRecord($query);
		
		return $result['table']['record'];
	}
	
	private function doGetProductList(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->default_prefix."product`";
	}
	
	//寫入直購訂單
	private function doSetDirectOrder(){
		$order_id = self::createUniqkey();
		
		$query ="INSERT INTO 
				`".$this->dbname."`.`".$this->default_prefix."order`(
					`prefixid`, 
					`order_id`, 
					`userid`, 
					`gsid`, 
					`amount`,
					`insertt`
				)
				VALUES(
					'".$this->obj->config->default_prefix_id."',
					'".$order_id."' , 
					'".$this->params["userid"]."' , 
					'".$this->params['gsid']."',
					'".$this->params["amount"]."' , 
					now()
				)
				";
		$this->model->query($query);
				
		$this->params['order_id'] = $order_id;
		
		return true;
	} 	
	//寫入直購訂單內容
	private function doSetDirectOrderList(){
		$order_list_id = self::createUniqkey();
		
		$query ="INSERT INTO 
				`".$this->dbname."`.`".$this->default_prefix."order_list`(
					`prefixid`, 
					`order_list_id`,
					`order_id`, 
					`pid`, 
					`num`, 
					`amount`,
					`insertt`
				)
				VALUES(
					'".$this->obj->config->default_prefix_id."',
					'".$order_list_id."',
					'".$this->params["order_id"]."' , 
					'".$this->params["pid"]."' , 
					'".$this->params['num']."',
					'".$this->params["amount"]."', 
					now()
				)
				";
		$this->model->query($query);
				
		$this->params['order_list_id'] = $order_list_id;
		
		return true;
	} 		
	
	private function doGetDirectProduct(){
	
		$query = "SELECT * 
					FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."product` p
					WHERE p.pcid = '".$this->params['pcid']."'
					AND p.game_id = '".$this->params['game_id']."'";
					
		$result = $this->model->getQueryRecord($query);
		
		return $result['table']['record'];				
	}
	
	private function doGetOrder(){
		$query = "SELECT * 
					FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."order` o 
					WHERE o.prefixid = '".$this->obj->config->default_prefix_id."' 
					AND o.order_id = '".$this->params['order_id']."'";
		
        $result = $this->model->getQueryRecord($query);
		
		return $result['table']['record'];		
	}
	
	private function doGetDirectOrderList(){
	
		$query = "SELECT ol.order_list_id, p.pid, p.name, ol.num as value , ol.amount ,p.value as num
					FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."order_list` ol , `".$this->dbname."`.`".$this->obj->config->default_prefix."product` p , `".$this->dbname."`.`".$this->obj->config->default_prefix."product_category` pc
					WHERE pc.prefixid = '".$this->obj->config->default_prefix_id."' 
					AND ol.prefixid = '".$this->obj->config->default_prefix_id."' 
					AND p.prefixid = '".$this->obj->config->default_prefix_id."'
					AND ol.order_id = '".$this->params['order_id']."'
					AND ol.pid = p.pid
					AND p.pcid = pc.pcid";
		
        $result = $this->model->getQueryRecord($query);
		
		return $result['table']['record'];	
	}
	
	private function doGetOrderList(){
		
		$query = "SELECT * 
					FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."order_list` ol, `".$this->dbname."`.`".$this->obj->config->default_prefix."product` p  
					WHERE ol.prefixid = '".$this->obj->config->default_prefix_id."' 
					AND  ol.order_id = '".$this->params['order_id']."' 
					AND ol.prefixid = '".$this->obj->config->default_prefix_id."' 
					AND p.prefixid = '".$this->obj->config->default_prefix_id."' 
					AND ol.pid = p.pid";
		
        $result = $this->model->getQueryRecord($query);
		
		return $result['table']['record'];	
	}
	
	private function createUniqkey(){
		//var_dump($this); die();
		$key = md5(uniqid($this->default_prefix));
		return $key;
	}
}

?>	
