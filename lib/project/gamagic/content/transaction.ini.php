<?php
/**
 * transcation object
 *
 * @author Albert
 * @version $Id:transaction.ini.php, v1.0 2012-02-06 Albert $
 * @package wallet
 * @copyright 2011(C)Snsplus
 */
class transaction{

	public $params;
	public $obj;
	public $default_prefix;
	public $default_prefix_id;
	public $modelM;
	public $modelS;
	public $limitStart = 0;
	public $limitRows = 20;
	public $gameServer;
	
	public function init($obj,$params){
		if(empty($this->modelS)){
			$this->modelS = new mysql($obj->config->db[0]);
			$this->modelS->connect();		
		}		
		if(empty($this->modelM)){
			$this->modelM = new mysql($obj->config->db[1]);
			$this->modelM->connect();		
		}
		$this->params = $params;
		$this->obj = $obj;
		$this->default_prefix_id = $this->obj->config->default_prefix_id;
		$this->default_prefix = $this->obj->config->default_prefix; 
	}
	
	public function getTransactionListTest($obj,$params){
		self::init($obj,$params);
		
		$result = self::doGetTransaction();
		
		return $result;
	}
	
	/*
	 * 僅計算筆數,做分頁用
	 */
	public function getTransactionCount($obj,$params){
		$params['count'] = true;
		
		self::init($obj,$params);
			
		$result = self::doGetTransaction();
		//return count($result);//傳回總筆數
		return $result;//joe modifyt 
	}
	
	/*
	* 	params
	*	status 訂單狀態 Y:成功 R:失敗倒回 N:未成功
	*	orderid 訂單編號
	*	userid 用戶ID
	*	game_id 遊戲ID
	*	gsid 服務器ID
	*	payway 加點方式 exchange:轉點 directbuy:直購
	*	getDate 單一日期
	*	startDate 起始日期
	*	endDate 結束日期
	*	limitStart 起始筆數
	*	limitRow 讀取筆數
	*	mem_use 啟用memcached 預設保存 3600sec  false:不讀取
	*/
	
	public function getTransactionList($obj,$params){
		self::init($obj,$params);
		$result = self::doGetTransaction();
		return $result;
	}	
	
	/*
	* 	params
	*	game_id
	*	startDate
	*	endDate
	*/
	public function getTransactionListByGame($obj,$params){
	
		if(empty($params['game_id'])){
			return false;
		}else{
			self::init($obj,$params);
			
			return self::doGetTransaction();
		}
	}
	/*
	* 	params
	*	gsid
	*	startDate
	*	endDate
	*/	
	public function getTransactionListByServer($obj,$params){
		if(empty($params['gsid'])){
			return false;
		}else{
			self::init($obj,$params);
			
			return self::doGetTransaction();
		}
	}
	/*
	* 	params
	*	uid
	*	gsid
	*	startDate
	*	endDate
	*/		
	public function getTransactionListByUser($obj,$params){
		if(empty($params['userid'])){
			return false;
		}else{
			self::init($obj,$params);
			return self::doGetTransaction();
		}
	}
	/*
	* 	params
	*	game_id
	*	gsid 
	*	startDate
	*	endDate
	*/	
	public function getTransactionListByDate($obj,$params){
		self::init($obj,$params);
	}
	
	public function getGameServerName($obj,$params){
		self::init($obj,$params);
		
		return self::doGetGameServerName();
	}
	
	public function testGetTestList($obj,$params){
		self::init($obj,$params);
		
		$result = self::doGetTesterList();
		
		//var_dump($result);
		
		if(!empty($result)){
			foreach($result as $resKey => $resValue){
				$returnObj[] = "'".$resValue['uid']."'";
			}
			$return = implode(',',$returnObj);
		}else{
			$return = '';
		}
		return $return;
		
	}	
	
	private function doGetGameServerName(){
		
	}
	
	private function doGetGameServer(){
		
	}
	
	private function doGetServerListByGame(){
	
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$key = 'gameserver' . '_' . 'listSring' . '_' .$this->params['game_id'];
		$GsString = $this->obj->mc->readMemcache($key);
		
		if(empty($GsString) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0))){
		
			$query = "SELECT gs.gsid FROM ".$this->default_prefix."game.".$this->default_prefix."game_server gs WHERE game_id = '".$this->params['game_id']."'";		
			$result = $this->modelS->getQueryRecord($query);
			$tempGs = array();
			foreach($result['table']['record'] as $recordKey => $recordValue){
					$tempGs[] = "'".$recordValue['gsid']."'";
			}
			$GsString = implode(",",$tempGs);
			
			$this->obj->mc->writeMemCache($key,$GsString,86400);
		}
		
		
		return $GsString;
	}
	
	public function checkRollback($obj,$params){
		self::init($obj,$params);
		
		return self::doCheckRollback();
	}
	
	private function doCheckRollback(){
		$query = "SELECT rhid 
					FROM ".$this->default_prefix."exchange.".$this->default_prefix."rollback_history rh 
					WHERE uid = '".$this->params['uid']."'
					AND thid = '".$this->params['thid']."'
					AND rh.prefixid = '".$this->default_prefix_id."'";
					
		$result = $this->modelM->getQueryRecord($query);
		
		return $result['table']['record'][0];
	}
	
	public function getNoRollbackList($obj,$params){
		self::init($obj,$params);
		
		return self::doGetNoRollbackList();
	}
	/*
	* uid
	* behavior : exchange 加點 / rollback 倒回  / deposit / 儲值
	* getDate 
	* startDate
	* endDate
	*/
	public function getUserWalletList($obj,$params){
		self::init($obj,$params);
		
		return self::doGetUserWalletList();
	}
	
	private function doGetUserWalletList(){
		$mecacheKey  = 'userWallet';
		
		$queryObj = array();
		
		$query = "SELECT waid,behavior,htid,uid,amount,insertt,modifyt FROM ".$this->default_prefix."wallet.".$this->default_prefix."wallet_amount wa ";
		
		$query2 = "SELECT behavior, COUNT(waid) AS CT, SUM(wa.amount) AS AMOUNT FROM ".$this->default_prefix."wallet.".$this->default_prefix."wallet_amount wa ";
		
		if(isset($this->params['uid']) && !empty($this->params['uid'])){
			$mecacheKey .= '_uid_'.$this->params['uid'];
			$queryObj[] = "uid = '".$this->params['uid']."'";
		}
		if(isset($this->params['behavior']) && !empty($this->params['behavior'])){
			$mecacheKey .= '_behavior_'.$this->params['behavior'];
			$queryObj[] = "behavior = '".$this->params['behavior']."'";
		}
		
		if(isset($this->params['getDate']) && !empty($this->params['getDate'])){
			$mecacheKey .= '_getDate_'.$this->params['getDate'];
			
			$queryObj[] = " DATE_FORMAT(wa.insertt,'%Y-%m-%d') = '".$this->params['getDate']."'";
			
		}else{
			if(isset($this->params['startDate']) && isset($this->params['endDate']) && !empty($this->params['startDate']) && !empty($this->params['endDate'])){

				$mecacheKey .= '_startDate_'.$this->params['startDate'];

				$mecacheKey .= '_endDate_'.$this->params['endDate'];
				$queryObj[] = " wa.insertt BETWEEN '".$this->params['startDate']." 00:00:00' AND '".$this->params['endDate']." 23:59:59'";
			}else{
				if(isset($this->params['startDate']) && !empty($this->params['startDate'])){
					$mecacheKey .= '_startDate_'.$this->params['startDate'];
					$queryObj[] = " wa.insertt >= '".$this->params['startDate']." 00:00:00'";
				}elseif(isset($this->params['endDate']) && !empty($this->params['endDate'])){
					$mecacheKey .= '_endDate_'.$this->params['endDate'];
					$queryObj[] = " wa.insertt <= '".$this->params['endDate']." 23:59:59'";
				}
			}			
		}		
		
		$queryObj[] = "wa.prefixid = 'sns'";	
		
		$queryWhere = implode(' AND ',$queryObj);
		
		$query .= ($queryWhere == '')?'':' WHERE '.$queryWhere;
		$query2 .= ($queryWhere == '')?'':' WHERE '.$queryWhere;
		
		$query .= ' ORDER BY wa.insertt DESC';
		$query2 .= " GROUP BY wa.behavior";
		
		$userWalletAmountKey = $mecacheKey."_sum";;
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$userWalletAmount = $this->obj->mc->readMemcache($userWalletAmountKey);
	
		if(empty($userWalletAmount) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0)) || (isset($this->params['reMemcache']) && ($this->params['reMemcache'] == 'Y'))){
		
			$userWalletAmount = $this->modelM->getQueryRecord($query2);
			$this->obj->mc->writeMemCache($userWalletAmountKey,$userWalletAmount,3600) ;
		}		
		
		$this->obj->config->max_page = $this->limitRows;

		foreach($userWalletAmount['table']['record'] as $uwaKey => $uwaValue){
			$userWalletAmountObj[$uwaValue['behavior']] = array('times' => $uwaValue['CT'],
																'amount' => $uwaValue['AMOUNT']);
																
			$userWalletAmountObj['CT']					+= $uwaValue['CT'];
		}		

		$page = $this->modelM->recordPage($userWalletAmountObj['CT'], $this->obj);//打入了max_page和	

		if (isset($this->params['p'])) {
			$this->limitStart = $page["rec_start"]-1;	
			$mecacheKey .= '_limitStart_'.$this->limitStart.'_limitRows_'.$this->limitRows;
			$query .= " LIMIT ".$this->limitStart.",".$this->limitRows;				
		}
		
		
		
		
		$userWalletList = $this->obj->mc->readMemcache($mecacheKey);	
	
		if(empty($userWalletList) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0)) || (isset($this->params['reMemcache']) && ($this->params['reMemcache'] == 'Y'))){
		
			$userWalletList = $this->modelM->getQueryRecord($query);
			$this->obj->mc->writeMemCache($mecacheKey,$userWalletList,3600) ;
		}			
		
		foreach($userWalletAmount['table']['record'] as $uwaKey => $uwaValue){
			$userWalletAmountObj[$uwaValue['behavior']] = array('times' => $uwaValue['CT'],
																'amount' => $uwaValue['AMOUNT']);
		}
		
		$result = array('List'	=> $userWalletList['table']['record'],
						'Page'	=> $page,
						'Sum'	=> $userWalletAmountObj);
		
		return $result;
	}
	
	private function doGetNoRollbackList(){
		$query = "SELECT o.gsid, eh.uid, th.thid, eh.order_id, eh.insertt, ea.behavior, o.amount, IF(p.pcid != '','directbuy','exchange') AS PAYWAY
					FROM ".$this->default_prefix."exchange.".$this->default_prefix."exchange_history eh , 
					".$this->default_prefix."exchange.".$this->default_prefix."transaction_history th , 
					".$this->default_prefix."wallet.".$this->default_prefix."exchange_amount ea ,
					".$this->default_prefix."product.".$this->default_prefix."order o, 
					".$this->default_prefix."product.".$this->default_prefix."order_list ol, 
					".$this->default_prefix."product.".$this->default_prefix."product p
					WHERE th.status = 'N' 
					AND o.order_id = '".$this->params['order_id']."'
					AND ea.behavior != 'rollback'
					AND ea.prefixid = '".$this->default_prefix_id."'
					AND eh.prefixid = '".$this->default_prefix_id."'
					AND th.prefixid = '".$this->default_prefix_id."'
					AND o.prefixid = '".$this->default_prefix_id."'
					AND o.order_id = ol.order_id
					AND ol.pid = p.pid
					AND o.order_id = eh.order_id
					AND ea.htid = eh.ehid
					AND eh.order_id = th.order_id";
					
		
		//return $query;//exit();		
		$result = $this->modelM->getQueryRecord($query);
		
		return $result['table']['record'];
	}
	
	private function doGetTransaction(){
	
	//var_dump($this->params);
	
		$mecacheKey  = 'transaction';
		$mecacheKey .= '_count_'.$this->params['count'];
		$queryObj = array();
		$havingObj = array();
		$groupObj = array();
	
		$query = "SELECT o.order_id AS ORDERID, o.gsid AS GSID, o.amount AS COINS, (ol.num * p.value) AS GAME_MONEY , o.userid AS USERID, th.status AS STATUS, DATE_FORMAT(th.insertt,\"%Y-%m-%d\") AS PAYDATE, DATE_FORMAT(th.insertt,\"%H:%i:%s\") AS PAYTIME , IF(p.pcid != '','directbuy','exchange') AS PAYWAY
				  FROM ".$this->default_prefix."exchange.".$this->default_prefix."transaction_history th , ".$this->default_prefix."product.sns_order o ,".$this->default_prefix."product.".$this->default_prefix."order_list ol , ".$this->default_prefix."product.".$this->default_prefix."product p";
		$query2	= "SELECT COUNT(o.order_id) AS CT, SUM( o.amount ) AS SUM_COINS, SUM( ol.num * p.value ) AS SUM_GAME_MONEY ";
		
		if(isset($this->params['payway']) && !empty($this->params['payway'])){
			$query2 .= " , IF(p.pcid != '','directbuy','exchange') AS PAYWAY";
		}
		
		$query2 .= " FROM ".$this->default_prefix."exchange.".$this->default_prefix."transaction_history th , ".$this->default_prefix."product.sns_order o ,".$this->default_prefix."product.".$this->default_prefix."order_list ol , ".$this->default_prefix."product.".$this->default_prefix."product p";
		if(isset($this->params['status']) && !empty($this->params['status'])){
			$mecacheKey .= '_status_'.$this->params['status'];
			$queryObj[] = "th.status = '".$this->params['status']."'";
		}
		
		if(isset($this->params['order_id']) && !empty($this->params['order_id'])){
			$mecacheKey .= '_order_id_'.$this->params['order_id'];
			$queryObj[] = "o.order_id = '".$this->params['order_id']."'";
		}

		if(isset($this->params['tester']) && !empty($this->params['tester'])){
		

			$this->params['tester_type'] = '1';
			$result = self::doGetTesterList();

			
			if(!empty($result)){
				foreach($result as $resKey => $resValue){
					$returnObj[] = "'".$resValue['uid']."'";
				}
				$testerStr = implode(',',$returnObj);
				
				$mecacheKey .= '_tester_'.$this->params['tester'].'_';
				switch($this->params['tester']){
					case 'Y':
						$queryObj[] = "o.userid NOT IN (".$testerStr.")";
					break;
					case 'G':
						$queryObj[] = "o.userid IN (".$testerStr.")";
					break;
				}
			}	
		}
		
		if(isset($this->params['userid']) && !empty($this->params['userid'])){
			$mecacheKey .= '_userid_'.$this->params['userid'];
			$queryObj[] = "o.userid = '".$this->params['userid']."'";
		}
		
		if(isset($this->params['game_id']) && !empty($this->params['game_id'])){
			$serverList = self::doGetServerListByGame();
			
			$mecacheKey .= '_game_id_'.$this->params['game_id'];
			
			$queryObj[] = "o.gsid IN (".$serverList.")";
		}
		
		if(isset($this->params['gsid']) && !empty($this->params['gsid'])){
			$mecacheKey .= '_gsid_'.$this->params['gsid'];
			$queryObj[] = "o.gsid = '".$this->params['gsid']."'";
		}
					
		if(isset($this->params['getDate']) && !empty($this->params['getDate'])){
			$mecacheKey .= '_getDate_'.$this->params['getDate'];
			
			$queryObj[] = " DATE_FORMAT(th.insertt,'%Y-%m-%d') = '".$this->params['getDate']."'";
			
		}else{
			if(isset($this->params['startDate']) && isset($this->params['endDate']) && !empty($this->params['startDate']) && !empty($this->params['endDate'])){

				$mecacheKey .= '_startDate_'.$this->params['startDate'];

				$mecacheKey .= '_endDate_'.$this->params['endDate'];
				$queryObj[] = " th.insertt BETWEEN '".$this->params['startDate']." 00:00:00' AND '".$this->params['endDate']." 23:59:59'";
			}else{
				if(isset($this->params['startDate']) && !empty($this->params['startDate'])){
					$mecacheKey .= '_startDate_'.$this->params['startDate'];
					$queryObj[] = " th.insertt >= '".$this->params['startDate']." 00:00:00'";
				}elseif(isset($this->params['endDate']) && !empty($this->params['endDate'])){
					$mecacheKey .= '_endDate_'.$this->params['endDate'];
					$queryObj[] = " th.insertt <= '".$this->params['endDate']."' 23:59:59";
				}
			}			
		}
		
		if(isset($this->params['payway']) && !empty($this->params['payway'])){
			$mecacheKey .= '_payway_'.$this->params['payway'];
			$havingObj[] = " PAYWAY = '".$this->params['payway']."'";
			$groupObj[] = 'PAYWAY';
		}
		
		$queryObj[] = "o.prefixid = 'sns'";
		$queryObj[] = "th.prefixid = 'sns'";
		$queryObj[] = "ol.prefixid = 'sns'";
		$queryObj[] = "p.prefixid = 'sns'";		
		$queryObj[] = "th.order_id = o.order_id";
		$queryObj[] = "o.order_id = ol.order_id";
		$queryObj[] = "ol.pid = p.pid";				
		
		$queryWhere = implode(' AND ',$queryObj);
		$queryHaving = implode(' AND ',$havingObj);
		$queryGroup = implode(' , ',$groupObj);
		
		$query .= ($queryWhere == '')?'':' WHERE '.$queryWhere;
		$query2 .= ($queryWhere == '')?'':' WHERE '.$queryWhere;				
		
		$query2 .= ($queryGroup == '')?'':' GROUP BY '.$queryGroup;		
		
		$query .= ($queryHaving == '')?'':' HAVING '.$queryHaving;
		$query2 .= ($queryHaving == '')?'':' HAVING '.$queryHaving;
		
		
		$sumMecacheKey = $mecacheKey."_sum";
		
		$query .= " ORDER BY th.insertt DESC ";
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$transactionSumList = $this->obj->mc->readMemcache($sumMecacheKey);	
	
		if(empty($transactionSumList) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0)) || (isset($this->params['reMemcache']) && ($this->params['reMemcache'] == 'Y'))){
		
			$result2 = $this->modelM->getQueryRecord($query2);
			$transactionSumList = $result2['table']['record'];
			
			$this->obj->mc->writeMemCache($sumMecacheKey,$transactionSumList,3600) ;
		}
		
		$this->obj->config->max_page = $this->limitRows;


		$page = $this->modelM->recordPage($transactionSumList[0]['CT'], $this->obj);//打入了max_page和	

		if (isset($this->params['p'])) {
			$this->limitStart = $page["rec_start"]-1;		
			$mecacheKey .= '_limitStart_'.$this->limitStart.'_limitRows_'.$this->limitRows;
			$query .= " LIMIT ".$this->limitStart.",".$this->limitRows;	
		}
		
		
		$transactionList = $this->obj->mc->readMemcache($mecacheKey);	
		if(empty($transactionList) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0)) || (isset($this->params['reMemcache']) && ($this->params['reMemcache'] == 'Y'))){
		
			$result = $this->modelM->getQueryRecord($query);
			$transactionList = $result['table']['record'];
			
			$this->obj->mc->writeMemCache($mecacheKey,$transactionList,3600) ;
		}

		//echo $mecacheKey." | ". $sumMecacheKey."<br>";
		
		//if($this->params['show_me_the_var'] == 'Y'){
				//echo "<PRE>";
				/*
				print_r($this->obj);
				echo "<br>";
				
				print_r($this->modelS);
				echo "<br>";
				*/
				//print_r($query2);
				//echo "<br>";
				/*
				print_r($this->modelM);
				echo "<br>";
				*/
				//print_r($query);
				//echo "<br>";
		//}		
		
		$transaction = array('List'		=>	$transactionList,
							 'Page'		=>	$page,
							 'Sum'		=>	$transactionSumList);
						  
		return $transaction;
	}
	
	
	private function doGetTesterList(){
		$mecacheKey  = 'admin_tester';
		$query = "SELECT uid FROM ".$this->default_prefix."site.".$this->default_prefix."tester";
		
		if(isset($this->params['tester_type'])){
			$mecacheKey  .= '_'.$this->params['tester_type'];
			$query .= " WHERE status = '".$this->params['tester_type']."'";
		}

		//$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		//$tester = $this->obj->mc->readMemcache($mecacheKey);	
		//if(empty($tester) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0)) || (isset($this->params['reMemcache']) && ($this->params['reMemcache'] == 'Y'))){
			$result = $this->modelS->getQueryRecord($query);
			$tester = $result['table']['record'];
			
			//$this->obj->mc->writeMemCache($mecacheKey,$tester,3600) ;
		//}		
		
		return $tester;
	}
}
