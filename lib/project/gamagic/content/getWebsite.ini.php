<?php
$this->cachetime = 86400;
$this->mc->connectionMemcache($this->config->memcache["server_list"]["ap1"]["ip"],$this->config->memcache["server_list"]["ap1"]["port"]);
function getWeb($obj){
        $key = 'sns_web';
	$obj->mc->deleteMemcache($key);
        $web = $obj->mc->readMemcache($key);
        if(empty($web)){
        $query ="
                SELECT * 
                FROM `".$obj->config->db[14]["dbname"]."`.`".$obj->config->default_prefix."web`                 
                ";

                $result = $obj->model->getQueryRecord($query);
                $obj->mc->writeMemCache($key, $result['table']['record'], $obj->cachetime) ;
                $web = $result['table']['record'];
        }
        return $web;
}
function getWebCategory($web_id ,$obj){
        $key = 'sns_web_category_bywid_'.$web_id;
	$obj->mc->deleteMemcache($key);
        $web_category = $obj->mc->readMemcache($key);
        if(empty($web_category)){
        $query ="
                SELECT * 
                FROM `".$obj->config->db[14]["dbname"]."`.`".$obj->config->default_prefix."web_category`                        
                WHERE
                web_id = '".$web_id."'
                ";

                $result = $obj->model->getQueryRecord($query);
                $obj->mc->writeMemCache($key, $result['table']['record'], $obj->cachetime) ;
                $web_category = $result['table']['record'];
        }
        return $web_category;
}
function getWebContent($web_category_id ,$obj){
        $key = 'sns_web_content_bywcid_'.$web_category_id;
	$obj->mc->deleteMemcache($key);
        $web_content = $obj->mc->readMemcache($key);
        if(empty($web_content)){
        $query ="
                SELECT * 
                FROM `".$obj->config->db[14]["dbname"]."`.`".$obj->config->default_prefix."web_content`                 
                WHERE 
                web_category_id = '".$web_category_id."'
                ORDER BY seq DESC
                ";

                $result = $obj->model->getQueryRecord($query);
                $obj->mc->writeMemCache($key, $result['table']['record'], $obj->cachetime) ;
                $web_content = $result['table']['record'];
        }
        $count = count($web_content);
        /* query content exchange to the new array start*/
        $exchagne_array = array();
        for($i = 0 ; $i < $count ; $i++){
        $exchagne_array[$i]['web_content_id'] = $web_content[$i]['web_content_id'];
        $exchagne_array[$i]['web_category_id'] = $web_content[$i]['web_category_id'];
        $exchagne_array[$i]['name'] = $web_content[$i]['name'];
        $exchagne_array[$i]['ctype'] = $web_content[$i]['ctype'];
        /* content select start */
        if($web_content[$i]['ctype'] == 'pic'){
                $pic_content = unserialize($web_content[$i]['content']);
                if(is_array($pic_content)){
                        $exchagne_array[$i]['small_pic_content'] = $pic_content['small'];
                        $exchagne_array[$i]['large_pic_content'] = $pic_content['large'];
                }
                else{
                        $exchagne_array[$i]['small_pic_content'] = '';
                        $exchagne_array[$i]['large_pic_content'] = '';
                }
				/* option select start */
				$exchagne_array[$i]['op1'] = $pic_content['op1'];
				$exchagne_array[$i]['op2'] = $pic_content['op2'];
				$exchagne_array[$i]['op3'] = $pic_content['op3'];
				/* option select end */				
        }
        else if($web_content[$i]['ctype'] == 'other'){
                        $exchagne_array[$i]['other_content'] = $web_content[$i]['content'];
        }
        else if($web_content[$i]['ctype'] == 'flash'){
                        $exchagne_array[$i]['flash_content'] = $web_content[$i]['content'];
        }
        /* content select end */

		
                $exchagne_array[$i]['seq'] = $web_content[$i]['seq'];
                $exchagne_array[$i]['online'] = $web_content[$i]['online'];
                $exchagne_array[$i]['offline'] = $web_content[$i]['offline'];
                $exchagne_array[$i]['display'] = $web_content[$i]['display'];
                $exchagne_array[$i]['insertt'] = $web_content[$i]['insertt'];
                $exchagne_array[$i]['modifyt'] = $web_content[$i]['modifyt'];

        }

        /* query content exchange to the new array end*/
        return $exchagne_array;
}
?>
