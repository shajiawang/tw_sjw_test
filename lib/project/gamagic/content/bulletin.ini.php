<?php
class Bulletin {
	public $mc;
	public $obj;
	public $cachetime;

	function __construct(&$obj) {
		if (empty ($obj)) {
			die();
		}
		$this->obj = $obj;
		if(empty($obj->model)){
			$this->model = new mysql($obj->config->db[0]);
		} else {
			$this->model = $obj->model;
		}
		if(empty($obj->mc)){
			$this->mv = mc();
		} else {
			$this->mc = $obj->mc;
		}

		$this->model->connect();
		$this->cachetime = 86400;
		$this->mc->connectionMemcache($obj->config->memcache["server_list"]["ap1"]["ip"],$obj->config->memcache["server_list"]["ap1"]["port"]);
	}
	
	public function getAllNotice($limit = 0) {
		$key = 'sns_bulletin';
		$notices = $this->mc->readMemcache($key);
		if (empty ($notices)) {
			$query_limit = empty ($limit) ? '' : 'limit ' . $limit;
			$query = "
				SELECT * from `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."game_notice_rt` as gnr
				LEFT OUTER JOIN `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."notice` as n on n.prefixid = '".$this->obj->config->default_prefix_id."' AND (gnr.notice_id = n.notice_id)
				WHERE 1
				AND n.notice_id IS NOT NULL
				AND gnr.prefixid = '".$this->obj->config->default_prefix_id."'
				AND n.display = 'Y'
				order by n.sort
				$query_limit
			" ;
			$result = $this->model->getQueryRecord($query);
			$this->mc->writeMemCache($key, $result['table']['record'], $this->cachetime) ;
			$notices = $result['table']['record'];
		}
		return $notices;	
	}
	
	public function getNoticeByGameId($game_id) {
		if (!empty ($game_id)) {
			$key = 'sns_gamenotice_' . $game_id;
			$notices = $this->mc->readMemcache($key);
			if (empty ($notices)) {
				$query = "
					SELECT * from `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."game_notice_rt` as gnr
					LEFT OUTER JOIN `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."notice` as n on n.prefixid = '".$this->obj->config->default_prefix_id."' AND (gnr.notice_id = n.notice_id)
					WHERE 1
					AND n.notice_id IS NOT NULL
					AND gnr.prefixid = '".$this->obj->config->default_prefix_id."'
					AND game_id = '$game_id'
					AND n.display = 'Y' 
					order by n.sort
					$query_limit
				" ;
				$result = $this->model->getQueryRecord($query);
				$this->mc->writeMemCache($key, $result['table']['record'], $this->cachetime) ;
				$notices = $result['table']['record'];
			}
			return $notices;	
		}
	}
	
	public function getNoticeType() {
		$key = 'sns_notice_type';
		$type = $this->mc->readMemcache($key);
		if (empty ($type)) {
			$query = "
				SELECT * from `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."notice_type` nt
				WHERE 1
				AND nt.prefixid = '".$this->obj->config->default_prefix_id."'
			" ;
			$result = $this->model->getQueryRecord($query);
			$this->mc->writeMemCache($key, $result['table']['record'], $this->cachetime) ;
			$type = $result['table']['record'];
		}
		return $type;
	}

        public function getslider() {
                $key = 'sns_slider';
                $slider = $this->mc->readMemcache($key);
                if (empty ($slider)) {
                        $query = "
                                SELECT slider_id , large_pic_url , title , large_pic_link , small_pic_url , modifyt  from `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."slider` s
                                WHERE 1
                                AND s.prefixid = '".$this->obj->config->default_prefix_id."'
				AND s.display = 'Y'
				ORDER BY s.modifyt DESC
                        " ;
                        $result = $this->model->getQueryRecord($query);
                        $this->mc->writeMemCache($key, $result['table']['record'], $this->cachetime) ;
                        $slider = $result['table']['record'];
                }
                return $slider;
        }
        public function clear_slider() {
                $key = 'sns_slider';
                $this->mc->deleteMemcache($key);
        }
	public function clearAllNoticeCache() {
		$key = 'sns_bulletin';
		$this->mc->deleteMemcache($key);
	}

	public function clearGameNoticeCache($game_id) {
		if (!empty ($game_id)) {
			$key = 'sns_gamenotice_' . $game_id;
			$this->mc->deleteMemcache($key);
		}
	}
}
