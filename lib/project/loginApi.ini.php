<?php
class loginapi{
    public $mc;

    function __construct($obj = '') {
		if (!empty ($obj)) {
		    if (class_exists('mc')) {
				$this->mc = new mc();
				$this->mc->connectionMemcache($obj->config->memcache["server_list"]["session"]["ip"],$obj->config->memcache["server_list"]["session"]["port"]);
		    }
		}
    }
    
    function facebookAppsLogin($obj){
        //Facebook Login start
        $info = $obj->userinfo;

        $api_info = array();
        $api_info["openid"] = $info["id"] ; 
        $api_info["nickname"] =  $info->name ;
        
        if (preg_match("/^([_.0-9a-z-]+)@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,4}$/i",$info["email"])) {
            if ($obj->user->checkMemberId($info["email"], $obj)) {
                $api_info["memberid"] = '';
            } else {
                $api_info["memberid"] = $info["email"] ;
            }
            
            $api_info["email"] =  $info["email"] ;
        }
        
		$api_info['birthday'] = date('Y-m-d', strtotime($info->birthday));
		$api_info['sex'] = $info["gender"] == 'female' ? 'g' : 'b';
		
		if (class_exists('mc')) {
		    $this->mc = new mc();
		    $this->mc->connectionMemcache($obj->config->memcache["server_list"]["session"]["ip"],$obj->config->memcache["server_list"]["session"]["port"]);
		}

		if (isset($this->mc)) {
			$key = 'gamagic_fbuser_' . $info['id'];
			$user = $this->mc->readMemcache($key);
			
			if (empty ($user)) {
			    $user = $this->getOpenIdUser($info["id"],'facebook',$obj) ;
			    $this->mc->writeMemCache($key,$user,86400) ;
			}
		} else {
		    $user = $this->getOpenIdUser($info["id"],'facebook',$obj) ;
		}
		
		if (empty($user['openid'])) {
            $this->registerOpenId($api_info,'facebook',$obj);
            $user = $this->getOpenIdUser($info["id"],'facebook',$obj) ;
        }
        
        return $this->openIdUserLogin($user,$obj) ;
    }

    function facebookLogin($obj){
        //Facebook Login start
        if (isset($obj->io->input["get"]["code"])) {
            $callback = isset($obj->io->input["get"]["redirect_param"]) ? 'redirect_param=' . $obj->io->input["get"]["redirect_param"] : "";

            $param = http_build_query(
                                        array(
                                        'client_id' => $obj->config->fb["appid"],
                                        "client_secret" => $obj->config->fb["secret"],
                                        'redirect_uri' => $obj->openid->realm.'/'.$obj->io->input["get"]["fun"].'/'.$obj->io->input["get"]["act"].'/'.$callback,
                                        'code' => $obj->io->input["get"]["code"],
                                        'req_perms' => $obj->config->fb["req_perms"]
                                        )
                             );
                
            $token = http_parse_message(http_get('https://graph.facebook.com/oauth/access_token?'.$param))->body;
                
            if (is_object($error = json_decode($token))) {
                echo "<pre>Facebook login error message\n"; 
                echo $error->error->type.":".$error->error->message;
            	exit;
            } else {
                $info = json_decode(http_parse_message(http_get('https://graph.facebook.com/me?'.$token))->body);
                
                $api_info = array();
            	$api_info["openid"] = $info->id;
            	$api_info["nickname"] =  $info->name;
                	
            	if (preg_match("/^([_.0-9a-z-]+)@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,4}$/i",$info->email)) {
                	if ($obj->user->checkMemberId($info->email, $obj)) {
                	    $api_info["memberid"] = '';
                	} else {
                	    $api_info["memberid"] = $info->email ;
                	}
                    	
                	$api_info["email"] =  $info->email ;
            	}
                	
                $api_info['birthday'] = date('Y-m-d', strtotime($info->birthday));
                $api_info['sex'] = $info->gender == 'female' ? 'g' : 'b';
 
                $user = array();
            	$user = $this->getOpenIdUser($info->id,'facebook',$obj) ;
                
            	if (empty($user['openid'])) {
                	$this->registerOpenId($api_info,'facebook',$obj);
                	$user = $this->getOpenIdUser($info->id,'facebook',$obj) ;
            	}
                
            	return $this->openIdUserLogin($user,$obj) ;
            }
            //$obj->http->redirectUrl($obj); 
        }
        //Facebook Login end
    }

    function openIdLogin($obj){
        //OPEN-ID Login start
        if (isset($obj->io->input["get"]["openid"]))
        {
            if(!isset($obj->io->input["get"]["openid_mode"]))
            {
                switch ($obj->io->input["get"]["openid"])
                {
                    case "yahoo":
                        $otid = 'yahoo';
                        header('Location:'.$obj->openid->getYahooAuthUrl());
                    break;
                    case "google":
                        $otid = 'google';
                        header('Location:'.$obj->openid->getGoogleAuthUrl());
                    break;
                    default:
                        die("Undefined openid provider!");
                }
            }
            elseif ($obj->io->input["get"]["openid_mode"] == "cancel")
            {
                die("User has canceled authentication!"); exit ;
            }
            else
            {
                if ($obj->openid->validate())
                {
                    $api_info["openid"] = md5($obj->openid->getIdentity()); 
                    $api_info["nickname"] =  $obj->openid->getName() ; 
                    if ($email = $obj->openid->getEmail()) {
                        if(preg_match("/^([_.0-9a-z-]+)@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,4}$/i",$email)) {
                            $api_info["email"] =  $email ;
                            if ($obj->user->checkMemberId($email, $obj)) {
                                $api_info["memberid"] = '';
                            } else {
                                $api_info["memberid"] = $email ;
                            }
                        }
                    }
                    $otid = $obj->openid->getOtid();
                    $user = $this->getOpenIdUser($api_info["openid"],$otid,$obj) ;
                    if(!$user["openid"]){
                        $this->registerOpenId($api_info,$otid,$obj);
                        $user = $this->getOpenIdUser($api_info["openid"],$otid,$obj) ;
                    }
                    return $this->openIdUserLogin($user,$obj) ;
                }
            }
        }
        //OPEN-ID Login end
    }

    function openIdUserLogin($user,$obj){
        //echo "<pre>";print_r($obj->io->input); exit ; 
        if(is_array($user)){
                    $_SESSION["user"] = $user ;
        }

        //$syncLogin=ucClient::login($user['email'],$user['email']);
        if(
            $user["email"] == '' ||
            $user["countryid"] == ''
        ){
            return "login_first".$syncLogin; break ;
            
        }
        return "login_success".$syncLogin; break ;
    }

    function registerOpenId($info,$otid,$obj){
        $query = "INSERT INTO `".$obj->config->default_prefix."user`(
        `prefixid`,
        `openid`,
        `otid`,
        `email`,
        `nickname`,
        `sex`,
        `birthday`,
        `insertt`,
        `domain`
        ) values (
        '".$obj->config->default_prefix_id."',
        '".trim($info['openid'])."',
        '".$otid."' ,
        '".trim($info['email'])."',
        '".trim($info['nickname'])."',
        '".trim($info['sex'])."',
        '".trim($info['birthday'])."',
        now(),
        '".(!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST']."'
        )";
        
        $obj->model->query($query,$obj);
        
        $userid = $obj->model->_con->insert_id;

        $query = "UPDATE `".$obj->config->default_prefix."user` SET memberid = userid WHERE userid = '$userid'";
        $obj->model->query($query,$obj);

        $query = "INSERT INTO `".$obj->config->default_prefix."role_user_rt`(
                            `prefixid`,
                            `roleid`,
                            `userid`,
                            `openid`,
                            `insertt`
                        )
                        values(
                            '".$obj->config->default_prefix_id."',
                            'general',
                            '".$userid."',
                            '".trim($info["openid"])."',
                            now()
                        )
                ";
            
        $obj->model->query($query,$obj);
        return trim($info["openid"]); 
    }

    function getOpenIdUser($openid,$otid,$obj){
        $query = "SELECT * FROM `".$obj->config->default_prefix."user`
                where
                prefixid = '".$obj->config->default_prefix_id."' and
                openid = '".$openid."'";
                
        $user_arr = array();
        $user_arr = $obj->model->getQueryRecord($query,$obj);
                
        //lion:注意 getQueryRecord 回傳的是 $array['table']['record']  2012-04-24
        if (empty($user_arr['table']['record'])) {
        	return false;
        }
        
        $query = "SELECT * FROM `".$obj->config->default_prefix."role_user_rt`
                where
                prefixid = '".$obj->config->default_prefix_id."' and
                openid = '".$user_arr["table"]["record"][0]["openid"]."'";
        
        $role_arr = array();
        $role_arr = $obj->model->getQueryRecord($query,$obj);

        //lion:注意 getQueryRecord 回傳的是 $array['table']['record']  2012-04-25
        if (!empty($role_arr['table']['record'])) {
        	foreach ($role_arr['table']['record'] as $rk => $rv) {
            	$user['roleid'][] = $rv['roleid'] ; 
            }
        }

        $user["userid"] = $user_arr["table"]["record"][0]["userid"] ; 
        $user["openid"] = $user_arr["table"]["record"][0]["openid"] ; 
        $user["otid"] = $user_arr["table"]["record"][0]["otid"] ; 
        $user["nickname"] = $user_arr["table"]["record"][0]["nickname"] ; 
        $user["email"] = $user_arr["table"]["record"][0]["email"] ; 
        $user["memberid"] = $user_arr["table"]["record"][0]["memberid"] ; 
        //$user["s_email"] = $user_arr["table"]["record"][0]["s_email"] ; 
        $user["lanid"] = $user_arr["table"]["record"][0]["lanid"] ; 
        $user["tplid"] = $user_arr["table"]["record"][0]["tplid"] ; 
        $user["countryid"] = $user_arr["table"]["record"][0]["countryid"] ; 
        $user["owner_currencyid"] = $user_arr["table"]["record"][0]["owner_currencyid"] ; 
        $user["currencyid"] = $user_arr["table"]["record"][0]["currencyid"] ;
         
        return $user ; 
    }

    function openIdCallBack($obj){
                if (isset($obj->io->input["get"]["callback"]) && $obj->io->input["get"]["callback"]) {
                        $script = '$(function(){walletApi.callback('.urldecode(base64_decode($obj->io->input["get"]["callback"])).');});';
                } else {
                        $script = '$(function(){walletApi.selfGoToUrl({"url":"http://'.$obj->config->domain_name.'"});});';
                }
                echo '
                        <html>
                                <head>
                                        <script language="javascript" type="text/javascript" src="/javascript/jquery.js"></script>
                                        <script language="javascript" type="text/javascript" src="/javascript/jquery.maxlength.js"></script>
                                        <script language="javascript" type="text/javascript" src="/javascript/jquery.json.min.js"></script>
                                        <script language="javascript" type="text/javascript" src="/javascript/jquery.base64.min.js"></script>
                                        <script language="javascript" type="text/javascript" src="/javascript/jquery.popupWindow.js"></script>
                                        <script language="javascript" type="text/javascript" src="/javascript/jquery.pngFix.js"></script>
                                        <script language="javascript" type="text/javascript" src="/javascript/walletJsApiLib.js"></script>
                                        <script language="javascript" type="text/javascript" src="/javascript/default.js"></script>
                                        <meta charset="utf-8" />
                                        <script type="text/javascript">
                                        '.$script.'
                                        </script>
                                </head>
                        </html>
                ';
    }
}
?>