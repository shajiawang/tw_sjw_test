<?
/**
 * 调用原厂API (虚宝 and 商城币)
 *
 * @param string $type 类型（item=虚宝，money=商城币）
 * @param string $api_url 原厂API url
 * @param array $params 参数（虚宝需要以下参数：transaction_id、item_id、item_num、reason、uid；商城币需要：transaction_id、game_money、reason、uid）
 * @param string $secret 私钥
 * @return string json格式数据
 */
function callGameApi($type, $api_url, $params, $secret){
	try{
		if(!in_array($type,array('item','money'))){
			throw new Exception("type error",-1);
		}

		if(!isset($secret)){
			throw new Exception("secret error",-2);
		}

		switch ($type){
			case 'item':

				if(!isset($params['transaction_id']) || !isset($params['item_id']) || !isset($params['item_num']) || !isset($params['reason']) || !isset($params['uid']) ){
					throw new Exception("params error",-3);
				}
				$p['transaction_id'] = $params['transaction_id'];
				$p['item_id'] = $params['item_id'];
				$p['item_num'] = $params['item_num'];
				$p['reason'] = $params['reason'];
				$p['time'] = time();
				$p['uid'] = $params['uid'];
				$p['sig'] = MD5("item_id=".$p['item_id']."item_num=".$p['item_num']."reason=".$p['reason']."time=".$p['time']."uid=".$p['uid'].$secret);
				break;

			case 'money':
				if(!isset($params['transaction_id']) || !isset($params['game_money']) || !isset($params['reason']) || !isset($params['uid']) ){
					throw new Exception("params error",-3);
				}
				$p['transaction_id'] = $params['transaction_id'];
				$p['game_money'] = $params['game_money'];
				$p['reason'] = $params['reason'];
				$p['time'] = time();
				$p['uid'] = $params['uid'];
				$p['sig'] = MD5("game_money=".$p['game_money']."reason=".$p['reason']."time=".$p['time']."uid=".$p['uid'].$secret);
				break;
		}

		try{
			$response = httpRequest($api_url,$p);
		}catch (Exception $e){
			throw new Exception('can not connect game server. ',-4);
		}
		$content = trim($response);
		$iResult = json_decode($content);
		if(isset($iResult->result) && is_numeric($iResult->result)){
			return $content;
		}else{
			throw new Exception($content, -5);
		}

	}catch (Exception $e){
		$code = $e->getCode();
		$msg = $e->getMessage();
		return  '{"result":2,"message":"'.$msg.'"}';
	}
}


//httpRequest
function httpRequest($url, $params = null, $decode = 0) {
	$ch = curl_init ( $url );
	if (! empty ( $params )) {
		curl_setopt ( $ch, CURLOPT_POST, true );
		if (is_array ( $params )) {
			$params = http_build_query ( $params );
		}
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $params );
	}
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $ch, CURLOPT_TIMEOUT, 10 );
	$response = curl_exec ( $ch );
	$response = trim ( $response );
	$error = curl_error($ch);
	curl_close ( $ch );

	if(!empty($error)){
		throw new Exception($error);
	}

	switch ($decode) {
		case 0 :
			return $response;
			break;
		case 1 :
			return json_decode ( $response );
			break;
		case 2 :
			return json_decode ( $response, true );
			break;
		default :
			return $response;
			break;
	}

}