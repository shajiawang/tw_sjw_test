<?php
class G2GExchange {
    public $prefix = 'go_';
    public $debug = false;

    function __construct($obj) {
        if (!empty ($obj)) {
            $this->obj = $obj;
            $this->prefix = $this->obj->config->default_prefix;
        }
    }
    
    function get_currency_exchange_rate($src_currency = '', $dst_currency = '') {
        $limit = '';

        if (!empty ($src_currency) && !empty ($dst_currency)) {
            if ($src_currency == $dst_currency) {
                return array($src_currency => array($dst_currency => 1));
            } else {
                $limit = 'LIMIT 1';
            }
        }
        
        if (!empty ($src_currency)) {
            $src_currency = " AND `src_currency` = '$src_currency' ";
        }
        
        if (!empty ($dst_currency)) {
            $dst_currency = " AND `dst_currency` = '$dst_currency' ";
        }

        $query = "
            SELECT * FROM `" . $this->prefix . "currency_exchange_rate`
            WHERE 1 
                $src_currency
                $dst_currency
            $limit
        ";
        echo $query;
        $data = $query;
        return $data;
    }
    
    function has_exchange_history($uid, $platform, $src_game_id) {
        if (!empty ($uid) && !empty ($platform) && !empty ($src_game_id)) {
            
            $query = "
                SELECT * FROM `".$this->prefix."deposit_history_summary`
                WHERE 1 
                    AND `uid` = '$uid' 
                    AND `platform` = '$platform' 
                    AND `game_id` = '$src_game_id'
                    AND `amount` < 0
                LIMIT 1
            ";
            echo $this->debug ? $query : '';

            return $this->obj->model->getQueryRecord($query);
        }
    }
    
    function get_exchange_rate($src_game_id, $dst_game_id = '') {
        if (!empty ($src_game_id)) {
            if (!empty ($dst_game_id)) {
                $dst_game_id = "AND `dst_game_id` = '$dst_game_id'";
            }

            $query = "
                SELECT * FROM `" . $this->prefix . "g2g_exchange_rate` ger
                LEFT OUTER JOIN `" . $this->prefix . "game` g ON ger.dst_game_id = g.game_id
                WHERE 1 
                    AND g.game_id IS NOT NULL
                    AND `src_game_id` = '$src_game_id' 
                    $dst_game_id
            ";
            echo $this->debug ? $query : '';

            return $this->obj->model->getQueryRecord($query);
        }
    }
    
    function get_rebate_rate($src_game_id, $dst_game_id = '') {
        if (!empty ($src_game_id)) {
            if (!empty ($dst_game_id)) {
                $dst_game_id = "AND `dst_game_id` = '$dst_game_id'";
            }

            $query = "
                SELECT * FROM `" . $this->prefix . "g2g_rebate_rate` ger
                LEFT OUTER JOIN `" . $this->prefix . "game` g ON ger.dst_game_id = g.game_id
                WHERE 1 
                    AND g.game_id IS NOT NULL
                    AND `src_game_id` = '$src_game_id' 
                    $dst_game_id
            ";
            echo $this->debug ? $query : '';

            return $this->obj->model->getQueryRecord($query);
        }
    }
    
    function calculate_exchange_amount($amount, $src_currency, $dst_currency, $exchange_rate, $rebate_rate) {
        $exchange_amount = 0;

        $currency_rate = $this->get_currency_exchange_rate($src_currency, $dst_currency);
        $amount = round($amount * $currency_rate[$src_currency][$dst_currency], 2);
        $exchange_amount += intval(round($amount * $exchange_rate));
        $exchange_amount += intval(round($amount * $rebate_rate));

        return $exchange_amount;
    }  
    
    function get_deposit_history_summary($uid, $platform, $game_id) {
        if (!empty ($uid) && !empty ($platform)) {
            $query = "
                SELECT * FROM `".$this->prefix."deposit_history_summary`
                WHERE 1 
                    AND `uid` = '$uid' 
                    AND `platform` = '$platform' 
                    AND `game_id` = '$game_id'
                    AND `amount` > 0
                LIMIT 1
            ";
            echo $this->debug ? $query : '';

            $this->table['deposit_history_summary'] = $this->obj->model->getQueryRecord($query);
            return  $this->table['deposit_history_summary'];
        }
    }
    
    function create_exchange_history($uid, $platform, $src_game_id, $dst_game_id, $transaction_id, $exchange_amount) {
        if (!empty ($uid) && !empty ($platform) 
                && !empty ($src_game_id) && !empty ($dst_game_id)
                && !empty ($exchange_amount)) {
            
            $query = "
                INSERT INTO `".$this->prefix."g2g_exchange_history` 
                (`platform`, `uid`, `src_game_id`, `dst_game_id`, `transaction_id`, `exchange_amount`, `insertt`, `modifyt`)
                VALUES 
                ('$platform', '$uid', '$src_game_id', '$dst_game_id', '$transaction_id', '$exchange_amount', NOW(), NOW())
            ";
            echo $this->debug ? $query : '';
            
            if (!$this->obj->model->query($query)) {
                echo 'db error'; exit;
            }

            $dhs = $this->table['deposit_history_summary']['table']['record'][0];

            $query = "
                INSERT INTO `".$this->prefix."deposit_history_summary`
                (`game_id`, `uid`, `type`, `amount`, `money_unit`, `platform`, `summary_type`,  `insertt`, `modifyt`) 
                VALUES 
                ('$src_game_id', '$uid', '".$dhs['type']."', '".($dhs['amount']*-1)."', '".$dhs['money_unit']."', '$platform', 'exchange', NOW(), NOW())
            ";
            echo $this->debug ? $query : '';
            
            return $this->obj->model->query($query);
        }
    }
}