<?php
class FreeTrial {
    public $prefix = 'go_';
    public $debug = false;

    function __construct($obj) {
        if (!empty ($obj)) {
            $this->obj = $obj;
            $this->prefix = $this->obj->config->default_prefix;
        }
    } 
    
    function get_trial_id($src_game_id, $dst_game_id) {
        if (!empty ($src_game_id) && is_numeric($src_game_id) 
                && !empty ($dst_game_id) && is_numeric($dst_game_id)) {
            $query = "
                SELECT * FROM `".$this->prefix."free_trial_rate`
                WHERE 1 
                    AND `src_game_id` = '$src_game_id' 
                    AND `dst_game_id` = '$dst_game_id'
                LIMIT 1
            ";
            echo $this->debug ? $query : '';

            return $this->obj->model->getQueryRecord($query);
        }
    }
    
    function get_free_trial_rate($src_game_id, $dst_game_id = '') {
        if (!empty ($src_game_id)) {
            if (!empty ($dst_game_id)) {
                $dst_game_id = "AND ftr.`dst_game_id` = '$dst_game_id' ";
            }

            $query = "
                SELECT ftr.*, fth.fthid, g.* FROM `".$this->prefix."free_trial_rate` ftr
                LEFT OUTER JOIN `".$this->prefix."free_trial_history` fth ON fth.ftrid = ftr.ftrid
                LEFT OUTER JOIN `".$this->prefix."game` g ON ftr.dst_game_id = g.game_id
                WHERE 1 
                    AND g.game_id IS NOT NULL
                    AND ftr.`src_game_id` = '$src_game_id'
                    $dst_game_id
                GROUP BY ftr.ftrid
                ORDER BY g.game_id
            ";
            echo $this->debug ? $query : '';

            return $this->obj->model->getQueryRecord($query);
        }
    }
    
    function get_deposit_history_summary($uid, $platform, $game_id) {
        if (!empty ($uid) && !empty ($platform)) {
            $query = "
                SELECT * FROM `".$this->prefix."deposit_history_summary`
                WHERE 1 
                    AND `uid` = '$uid' 
                    AND `platform` = '$platform' 
                    AND `game_id` = '$game_id'
                    AND `amount` > 0
                LIMIT 1
            ";
            echo $this->debug ? $query : '';

            return $this->obj->model->getQueryRecord($query);
        }
    }
    
    function get_free_trial_history($uid, $platform, $ftrid) {
        if (!empty ($uid) && !empty ($platform) && !empty ($ftrid)) {
            $query = "
                SELECT * FROM `".$this->prefix."free_trial_history`
                WHERE 1 
                    AND `uid` = '$uid' 
                    AND `platform` = '$platform' 
                    AND `ftrid` = '$ftrid'
                LIMIT 1
            ";
            echo $this->debug ? $query : '';

            return $this->obj->model->getQueryRecord($query);
        }
    }
    
    function deposit_by_original() {
        
    }
    
    function create_trail_history($uid, $platform, $ftrid, $transaction_id) {
        if (!empty ($uid) && !empty ($platform) && !empty ($ftrid)) {
            $query = "
                INSERT INTO `".$this->prefix."free_trial_history`
                (`platform`, `uid`, `ftrid`, `transaction_id`, `insertt`, `modifyt`) 
                VALUES  
                ('$platform', '$uid', '$ftrid', '$transaction_id', NOW(), NOW())
            ";
            echo $this->debug ? $query : '';

            return $this->obj->model->query($query);
        }
    }
}