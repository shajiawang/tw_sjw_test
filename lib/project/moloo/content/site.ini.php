<?php
/**
 * site object
 *
 * @author Albert
 * @version $Id:site.ini.php, v1.0 2012-03-05 Albert $
 * @package wallet
 * @copyright 2012(C)Snsplus
 */
class site{

	public $params;
	public $obj;
	public $default_prefix;
	public $default_prefix_id;
	public $model;
	
	public function init($obj,$params = ''){
		if(empty($this->model)){
			$this->model = new mysql($obj->config->db[0]);
			$this->model->connect();	
		}
		$this->params = $params;
		$this->obj = $obj;
		$this->dbname = $this->obj->config->db[0]["dbname"];
		$this->default_prefix = $this->obj->config->default_prefix;
		$this->testerUserlist = 'site_tester_list';
	}	
	
	public function getTesterList($obj,$params){
		self::init($obj,$params);
		return self::doGetTesterList();
	}
	
	
	/*
	* params 
	*	uid
	*	mode : list
	*/
	public function checkTester($obj,$params){
		
		self::init($obj,$params);
		
		$testerList = self::doGetTesterList();
		
		return in_array($params['uid'],$testerList)?true:false;
	}
	
	private function doGetTesterList(){
	
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		
		$key = $this->testerUserlist.'_'.$this->params['mode'];
		
		$TesterList = $this->obj->mc->readMemcache($key);
		
		if((isset($this->params['reMemcache']) && $this->params['reMemcache'] == 'Y') || empty($TesterList) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0))){
			//$TesterList = array();
			$query = "SELECT * FROM ".$this->dbname.'.'.$this->default_prefix."tester ";
			$result = $this->model->getQueryRecord($query);
			$TesterListObj = $result['table']['record'];
			
			switch($this->params['mode']){
				case 'admin':
					$TesterList = $TesterListObj;
				break;
				case 'list':
					foreach($TesterListObj as $tloKey => $tlovalue){
						$TesterList[] = $tlovalue['uid'];
					}
				break;
			}
			$this->obj->mc->writeMemcache($key,$TesterList,(86400*30));
			
		}
		return $TesterList;
	}
	
	
	
}


?>