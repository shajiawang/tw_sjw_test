<?php
include_once "phpmailer/class.phpmailer.php";
class sendmail {
	public $charset	= "utf8";
	public $encoding = "base64";
	public $from = "service@gamagic.com";
	public $from_name = "Gamagic Service";
	public $host = "smtp-relay.gamagic.com";
	public $secure = "ssl";
	public $port = "25";
	public $smtpauth = true ;
	public $ishtml = false ;
	public $to = "" ; 
	public $cc = "" ;
	public $language = "";
	public $subject = "";
	public $body = "";
	public $content_type =  "text"; // html or text
	public $error_info =  "";
	public $user_name = array(
		'testmail@snsplus.com'
	);
	public $account = "gamagicmail";
	public $password = "xxQub93ogP"; 

	function __construct() {
		// fix issue 308
		//$this->mail =  new PHPMailer();
	}

	function getRandMailAccount(){
		$key = rand('0','29'); 
		return $this->user_name[$key] ; 
	}
	
	function sendSmtp(){
		// fix issue 308
		$this->mail =  new PHPMailer();
		//$our_email = $this->getRandMailAccount();
		$this->mail->IsSMTP(); // set mailer to use SMTP
		$this->mail->CharSet =  $this->charset		;
		$this->mail->Encoding = $this->encoding ;
                //$this->mail->From = $our_email ;
		$this->mail->From = $this->from ;
		$this->mail->FromName = $this->from_name ;
		$this->mail->Host = $this->host ;
		$this->mail->Port = $this->port; //default is 25, gmail is 465 or 587
		$this->mail->SMTPAuth = $this->smtpauth;
		//$this->mail->SMTPSecure = $this->secure;
                //$this->mail->Username = $our_email ;
		$this->mail->Username = $this->account;
		$this->mail->Password = $this->password;
		if(is_Array($this->to)){
			foreach($this->to as $tv){
				if(preg_match("/^([_.0-9a-z-]+)@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,4}$/i",$tv)) {
					$this->mail->AddAddress($tv);
				}
			}
		}
		else{
			if(preg_match("/^([_.0-9a-z-]+)@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,4}$/i",$this->to)) {
				$this->mail->AddAddress($this->to);
			}
		}
		if(is_Array($this->cc)){
			foreach($this->cc as $tv){
				if(preg_match("/^([_.0-9a-z-]+)@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,4}$/i",$tv)) {
					$this->mail->AddCC($tv);
				}
			}
		}
		else{
			if(preg_match("/^([_.0-9a-z-]+)@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,4}$/i",$this->cc)) {
				$this->mail->AddCC($this->cc);
			}
		}


		$this->mail->WordWrap = 50;

		$this->mail->Subject = $this->subject;
		$this->mail->Body = $this->body;

		$this->isHtml($this->ishtml);
		if(!$this->mail->Send())
		{
			echo $this->mail->ErrorInfo ; 
			exit;
		}
	}
	
	function isHtml($flag){
		$this->mail->IsHTML($flag);
	}

	function insertToMailPool($arr,$obj){
		if(is_array($arr["to"])){
			if(count($arr["to"]) > 10){
				$obj->printMsg('parameter_to_accounts_limit_of_ten');
			}
			$arr["to"] = $obj->string->arrayEncode($arr["to"]);
		}
		if(is_array($arr["cc"])){
			if(count($arr["cc"]) > 10){

				$obj->printMsg('parameter_to_accounts_limit_of_ten');
			}
			$arr["cc"] = $obj->string->arrayEncode($arr["cc"]);
		}

	        $query ="
                INSERT INTO 
                `".$obj->config->database_name[9]."`.`".$obj->config->default_prefix."mail_pool`(
			`prefixid`, 
			`to`, 
			`cc`, 
			`subject`, 
			`body`, 
			`table_name`, 
			`pkyid`, 
			`insertt`
		)
		values(
			'".$obj->config->default_prefix_id."',
			'".$arr["to"]."'			,
			'".$arr["cc"]."'			,
			'".$arr["subject"]."'			,
			'".mysql_escape_string($arr["body"])."'			,
			'".$arr["table_name"]."'			,
			'".$arr["pkyid"]."'			,
			now()
		)
                ";

		$obj->model->query($query,$obj);

		

	}

	function getMailPool($obj){
                $query ="
                SELECT  *
                FROM 
		`".$obj->config->database_name[9]."`.`".$obj->config->default_prefix."mail_pool`
                where
                prefixid = '".$obj->config->default_prefix_id."' and
		is_send = 'N'
                ";
                return $obj->model->getQueryRecord($query,$obj);

		
	}

	function modifyIsSendMail($mpid,$obj){
		$query ="
		UPDATE
		`".$obj->config->database_name[9]."`.`".$obj->config->default_prefix."mail_pool`
		SET is_send = 'Y' 
                where
	        prefixid = '".$obj->config->default_prefix_id."' and
		mpid = '".$mpid."'
		";
		//echo $query ; exit ;
		$obj->model->query($query,$obj);
	}

	function sendMailPool($obj){
		$arr = $this->getMailPool($obj) ; 
		if(is_array($arr["table"])){
			foreach($arr["table"]["record"] as $ak => $av){
				if(count(@$obj->string->arrayDecode($av["to"])) > 1){
					$av["to"] = @$obj->string->arrayDecode($av["to"]) ; 
				}
				if(count(@$obj->string->arrayDecode($av["cc"])) > 1){
					$av["cc"] = @$obj->string->arrayDecode($av["cc"]) ; 
				}
				$this->subject = $av["subject"];
				$this->body = $av["body"];
				$this->to = $av["to"];
				$this->cc = $av["cc"];
				$this->ishtml = true ; 
				$this->sendSmtp();
				$this->modifyIsSendMail($av["mpid"],$obj);
				sleep(5);
			}
		}
	
	}
	


	function sendEdm($to,$subject,$body){
		$this->subject = $subject;
		$this->body = $body;
		$this->to = $to;
		$this->ishtml = true ; 
		$this->sendSmtp();
		sleep(5);
	}
	


}
