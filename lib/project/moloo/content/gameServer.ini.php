<?php
/**
 * game server object
 *
 * @author Albert
 * @version $Id:gameServer.php, v1.0 2010-11-23 Albert $
 * @package wallet
 * @copyright 2011(C)Snsplus
 */
class game{

	public $params;
	public $obj;
	public $dbname;
	public $default_prefix;
	public $model;
	
	public function init($obj,$params){
		if(empty($this->model)){
			$this->model = new mysql($obj->config->db[0]);
			$this->model->connect();		
		}
		$this->params = $params;
		$this->obj = $obj;
		$this->dbname = $this->obj->config->db[10]["dbname"];
		$this->default_prefix = $this->obj->config->default_prefix_;
	}

	public function getApikey2GameList($obj,$params){
	
		self::init($obj,$params);
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$key = 'game' . '_' . 'apikey' . '_list';
		$apikey2Game = $this->obj->mc->readMemcache($key);	
		
		if(empty($apikey2Game) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0))){
			$apikey2GameObj = self::doGetGameServerList();
			foreach($apikey2GameObj as $k => $v){
				if($v['api_key']){
					$apikey2Game[$v['api_key']] = $v;
				}
			}
			
			$this->obj->mc->writeMemCache($key,$apikey2Game,86400) ;
		}
		return $apikey2Game;
	}
	
	public function getAllGameList($obj,$params){
	
		self::init($obj,$params);
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$key = 'allgame' . '_'  . '_list';
		$apikey2Game = $this->obj->mc->readMemcache($key);	
		
		
		if(empty($apikey2Game) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0))){
			$apikey2Game = self::doGetGameList();
			
			$this->obj->mc->writeMemCache($key,$apikey2Game,86400) ;
		}
		return $apikey2Game;
	}	
	
	
	public function getGame($obj,$params){
		self::init($obj,$params);
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$key = 'game' . '_' . 'params' . '_' .$this->params['game_id'];
		$gameServerInfo = $this->obj->mc->readMemcache($key);

		if(empty($gameServerInfo) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0))){
		
			$gameInfo = self::doGetGame();
			$gameParams = self::doGetGameParams();
			$gameParamsPairs = self::doPairs($gameParams);
							
			$game 	= array('info'	=> $gameInfo,
							'params'=> $gameParamsPairs);			
		
			$serverInfo = self::doGetgameServer($params);

			$server = array();
			foreach($serverInfo as $k => $v){
				$this->params['gsid'] = $v[gsid];
				$serverParams = self::doGetServerParams();
				$serverParamsPairs = self::doPairs($serverParams);
				
				$server[] = array('info' => $v,
								  'params' => $serverParamsPairs);
			}
			
			$gameServerInfo = array('server' => $server,
									'game' => $game);
									
			$this->obj->mc->writeMemCache($key,$gameServerInfo,86400) ;

		}			
		return $gameServerInfo;		
	}
	
	public function getGameList($obj,$params){
		self::init($obj,$params);
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$key = 'game' . '_' . 'list';	
		$gameList = $this->obj->mc->readMemcache($key);	

		if(empty($gameList) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0))){
			$gameListObj = self::doGetGameList();
			$gameList = array();
			
			foreach($gameListObj as $k => $v){
				
				$this->params['game_id'] = $v['game_id'];
				
				$gameList[$v['game_id']]['info'] =  $v ;
				$gameParams = self::doGetGameParams();
				if(!empty($gamePrams)){
					$gameParamsPairs = self::doPairs($gameParams);	
					$gameList[$v['game_id']]['params'] =  $gameParamsPairs ;
				}
			}
			
			$this->obj->mc->writeMemCache($key,$gameList,86400) ;
		}

		return $gameList;

	}
	
	public function deleteGameList($obj,$params){
		self::init($obj,$params);
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$key = $params['key'];	

		$this->obj->mc->deleteMemcache($key);
	}
	
	public function showGameList($obj,$params){
		self::init($obj,$params);
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$key = $params['key'];

		$gameList = $this->obj->mc->readMemcache($key);	
		
		return array('key' => $key,
					  'game_list' => $gameList);	
	}
	
	public function getGameServer($obj,$params){
	
		self::init($obj,$params);
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		$key = 'gameserver' . '_' . 'params' . '_' .$this->params['gsid'];	

		$gameServerInfo = $this->obj->mc->readMemcache($key);
		
		if(empty($gameServerInfo) || (isset($this->obj->config->memcache['MEM_STATUS']) && ($this->obj->config->memcache['MEM_STATUS'] == 0))){
			$serverInfo = self::doGetServer();
			$serverParams = self::doGetServerParams();
			
			$serverParamsPairs = self::doPairs($serverParams);
			
			$this->params['game_id'] = $serverInfo['game_id'];
			
			$gameInfo = self::doGetGame();
			$gameParams = self::doGetGameParams();
			$gameParamsPairs = self::doPairs($gameParams);
			
			$server = array('info'	=> $serverInfo,
							'params'=> $serverParamsPairs);
							
			$game 	= array('info'	=> $gameInfo,
							'params'=> $gameParamsPairs);
							
			$gameServerInfo = array('server'	=> $server,
									'game'		=> $game);
									
			$this->obj->mc->writeMemCache($key,$gameServerInfo,86400) ;

		}			
		
		return $gameServerInfo;
	}
	
	private function doSetGameParams(){
	
	}
	
	private function doSetGame(){
		$game_id = ( isset( $this->params['game_id'] ) && !empty( $this->params['game_id'] ) )?$this->params['game_id']:self::createUniqkey();
		
		$query ="INSERT INTO 
				`".$this->dbname."`.`".$this->default_prefix."game`(
					`prefixid`, 
					`game_id`,
					`game_type_id`, 
					`name`, 
					`description`, 
					`title`,
					`rating`,
					`title`,
					`platform`,
					`manufactor`,
					`lang_id`,
					`area`,
					`official_site`,
					`game_site`,
					`seq`,
					`switch`,
					`insertt`
					`modifyt`
				)
				VALUES(
					'".$this->obj->config->default_prefix_id."',
					'".$game_id."',
					'".$this->params["game_type_id"]."' , 
					'".$this->params["name"]."' , 
					'".$this->params['description']."',
					'".$this->params["title"]."', 
					'".$this->params["rating"]."' , 
					'".$this->params["title"]."' , 
					'".$this->params['platform']."',
					'".$this->params["manufactor"]."', 
					'".$this->params["lang_id"]."' , 
					'".$this->params["area"]."' , 
					'".$this->params['official_site']."',
					'".$this->params["game_site"]."', 	
					'".$this->params["seq"]."' , 
					'".$this->params["switch"]."' , 
					NOW(),
					NOW()
				)
				";
		$this->model->query($query);
				
		$this->params['game_id'] = $game_id;
		
		return true;	
	}
	
	private function doSetGameServerParams(){
	
	}
	
	private function doSetGameServer(){
	
	}	
	
	private function doPairs($record){
		
		$pairs = array();
		if(!empty($record)){
			foreach($record as $k => $v){
				$pairs[$v['name']] = $v['value'];
			}
		}
		return $pairs;
	}
	
	private function doGetGameList(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."game` g WHERE prefixid = '".$this->obj->config->default_prefix_id."' ORDER BY g.seq DESC";
		
        $result = $this->model->getQueryRecord($query);
		return $result['table']['record'];
	}	
	
	private function doGetGame(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."game` g WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND g.game_id = '".$this->params['game_id']."'";
		
        $result = $this->model->getQueryRecord($query);
		return $result['table']['record'][0];
	}
	
	private function doGetGameParams(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."game_params` gp WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND gp.game_id = '".$this->params['game_id']."'";
		
        $result = $this->model->getQueryRecord($query);
		
		return $result['table']['record'];
	}
	
	private function doGetgameServer($params=array()){
		if ($params['desc'] == 'desc') {
			$pp = "desc";
		} else {
			$pp = "asc";
		}
		
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."game_server` gs WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND gs.game_id = '".$this->params['game_id']."' ORDER BY gs.seq ".$pp;       
		$result = $this->model->getQueryRecord($query);
		return $result['table']['record'];		
	}
	
	
	private function doGetServer(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."game_server` gs WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND gs.gsid = '".$this->params['gsid']."'";
		$result = $this->model->getQueryRecord($query);
		return $result['table']['record'][0];		
	}
	
	private function doGetGameServerList(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."game_server` gs WHERE prefixid = '".$this->obj->config->default_prefix_id."'";
		$result = $this->model->getQueryRecord($query);
		return $result['table']['record'];		
	}
	
	private function doGetServerParams(){
		$query = "SELECT * FROM `".$this->dbname."`.`".$this->obj->config->default_prefix."game_server_params` gsp WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND gsp.gsid = '".$this->params['gsid']."'";
		
        $result = $this->model->getQueryRecord($query);
		
		return $result['table']['record'];
	}	
	private function createUniqkey(){
		//var_dump($this); die();
		$key = md5(uniqid($this->default_prefix));
		return $key;
	}	
}	
?>
