<?php
class NewbiePromote {
	public $mc;
	public $obj;
	public $cache;
	public $cachetime;

	function __construct(&$obj) {
		if (empty ($obj)) {
			die();
		}
		$this->obj = $obj;

		if(empty($obj->model)){
			$this->model = new mysql($obj->config->db[0]);
			$this->model->connect();		
		} else {
			$this->model = $obj->model;
		}

		$this->cachetime = 86400;
		$this->cache = $obj->config->memcache["MEM_STATUS"];
		if ($this->cache && class_exists('mc')) {
			$this->mc = new mc();
			$this->mc->connectionMemcache($obj->config->memcache["server_list"]["ap1"]["ip"],$obj->config->memcache["server_list"]["ap1"]["port"]);
		}
	}

	public function useCache() {
		if ($this->cache && class_exists('mc')) {
			return true;
		}
		return false;
	}
	
	private function getData($query, $key) {
		if (!empty ($query) && !empty ($key)) {
			if ($this->useCache()) {
				$record = $this->mc->readMemcache($key);
				if (empty ($record)) {
					$record = $this->model->getQueryRecord($query);
					$this->mc->writeMemCache($key, $record['table']['record'], $this->cachetime) ;
				} else {
					return $record;
				}
			} else {
				$record = $this->model->getQueryRecord($query);
			}
			return $record['table']['record'];
		}
	}
	
	public function getPromote($game_id, $name, $online = '', $offline = '') {
		if (!empty ($game_id) && !empty ($name)) {
			if (!empty ($online)) {
				$where_online = " AND online >= '$online' ";
			}
			if (!empty ($offline)) {
				$where_offline = " AND offline <= '$offline' ";
			}
		
			$key = 'np_' . $game_id . '_' . $name;
			$query = "
				SELECT * FROM `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."newbie_promote` np
				WHERE 1
				AND np.prefixid = '".$this->obj->config->default_prefix_id."'
				AND np.game_id = '$game_id'
				AND np.name = '$name'
				$where_online
				$where_offline
			";
			return $this->getData($query, $key);
		}
	}
	
	public function getHistory($userid, $gsid, $npid) {
		if (!empty ($userid) && !empty ($gsid) && !empty ($npid)) {
			$key = 'nur_' . $userid . '_' . $gsid . '_' . $npid;
			$query = "
				SELECT * FROM `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."newbie_user_rt` nur
				LEFT OUTER JOIN `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."newbie_promote_list` npl ON npl.prefixid = nur.prefixid AND npl.nplid = nur.nplid AND npl.npid = '$npid'
				WHERE 1
				AND npl.nplid IS NOT NULL
				AND nur.prefixid = 'sns'
				AND nur.userid = '$userid'
				AND nur.gsid = '$gsid'
			";

			$record = $this->getData($query, $key);
			if (empty ($record)) {
				$key = $this->getNewKey($npid);
				if (!empty ($key)) {
					$this->updateKeyHistory($userid, $gsid, $key[0]['nplid']);
				}
				return $this->getHistory($userid, $gsid, $npid);
			} else {
				return $record;
			}
		}
	}
	
	public function getNewKey($npid) {
		if (!empty ($npid)) {
			$query = "
				SELECT npl.* FROM `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."newbie_promote_list` npl
				LEFT OUTER JOIN `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."newbie_user_rt` nur ON nur.prefixid = npl.prefixid AND nur.nplid = npl.nplid
				WHERE 1
				AND nur.nurid IS NULL
				AND npl.prefixid = '".$this->obj->config->default_prefix_id."'
				AND npl.npid = '$npid'
				LIMIT 1
			";

			$result = $this->model->getQueryRecord($query);
			return (empty ($result) ? '' : $result['table']['record']);
		}
	}
	
	public function updateKeyHistory($userid, $gsid, $nplid) {
		if (!empty ($gsid) && !empty ($userid) && !empty ($nplid)) {
			$query = "
				INSERT INTO `".$this->obj->config->db[9]["dbname"]."`.`".$this->obj->config->default_prefix."newbie_user_rt` (`prefixid`, `nurid`, `userid`, `gsid`, `nplid`, `insertt`)
				VALUES ('".$this->obj->config->default_prefix_id."', '" . md5(microtime(true)) . "', '$userid', '$gsid', '$nplid', NOW())
			";
			$this->model->query($query);
		}
	}
}
