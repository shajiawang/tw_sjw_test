<?php
/**
 * wallet object
 *
 * @author Albert
 * @version $Id:wallet.php, v1.0 2010-11-25 Albert $
 * @package wallet
 * @copyright 2011(C)Snsplus
 */
class wallet{

	public $params;
	public $obj;
	public $dbname;
	public $default_prefix;
	public $model;	
	private $depositUserAmountCache;

	
	public function init($obj,$params = ''){
		if(empty($this->model)){
			$this->model = new mysql($obj->config->db[1]);
			$this->model->connect();	
		}
		$this->params = $params;
		$this->obj = $obj;
		$this->dbname = $this->obj->config->db[3]["dbname"];
		$this->default_prefix = $this->obj->config->default_prefix;
		$this->depositUserAmountCache = 'deposit' . '_' . 'user_amount' . '_' . $params['userid'];
	}
	
	public function doCheckWalletAmount($obj,$params){
		self::init($obj,$params);
		
		// 取得錢包餘額
		if(empty($this->params['wallet_amount'])){ 
			$wallet_amount = self::doGetWalletAmount();
			$this->params['wallet_amount'] = $wallet_amount;
		}
		//判斷錢包餘額 是否足夠
		if($this->params['wallet_amount'] >= $this->params['amount'] ){	
			return self::returnParams('1','ok');
			die();
		}else{
			return self::returnParams('0','point_not_enough');
			die();
		}		
		
	}
	

	public function doExchange($obj,$params){
		self::init($obj,$params);
		$this->params['behavior'] = 'exchange';
		self::setWallet();
	}
	
	public function doRollback($obj,$params){
		self::init($obj,$params);
		$this->params['behavior'] = 'rollback';
		self::setWallet();
	}
	
	public function doRefund($obj,$params){
		self::init($obj,$params);
		$this->params['behavior'] = 'refund';
		self::setWalletAmountRecord();
	}
	
	private function setWallet(){
		$returnWAR = self::setWalletAmountRecord();
		$returnEAR = self::setExchangeAmountRecord();
	}
	
	public function eraseWalletAmount($obj,$params){
		self::init($obj,$params);
		
		self::doEraseWalletAmount();
	}
	
	private function doEraseWalletAmount(){
		
		$this->obj->mc->deleteMemcache($this->depositUserAmountCache);
	}
	
	/* 取得當前錢包餘額 */
	public function getWalletAmount($obj,$params){
	
		self::init($obj,$params);
		
		$this->obj->mc->connectionMemcache($this->obj->config->memcache["server_list"]["ap1"]["ip"],$this->obj->config->memcache["server_list"]["ap1"]["port"]);
		
		$wallet_amount = $obj->mc->readMemcache($this->depositUserAmountCache);
		
		if((isset($this->params['reMemcache']) && $this->params['reMemcache'] == 'Y') || empty($wallet_amount)){
			$wallet_amount = self::doGetWalletAmount();
			$obj->mc->writeMemcache($this->depositUserAmountCache,$wallet_amount,(86400*30));
		}
		
		$this->params['wallet_amount'] = $wallet_amount;
	}
	
	/*
	params 
	behavior 
	htid 
	*/
	public function checkWalletAmountRecord($obj,$params){
		self::init($obj,$params);
		
		return self::doGetWalletAmountRecord();
	}
	
	private function returnParams($result,$message){
		$return_params = array("result" => $result,"message"=>$message);
		return json_encode($return_params);
	}	
	
	private function doGetWalletAmount(){
		$wallet_amount = 0;
		
		$query = "SELECT SUM(amount) AS WA FROM `".$this->dbname."`.`".$this->default_prefix."wallet_amount` WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND uid = '".$this->params['userid']."'";
		$result = $this->model->getQueryRecord($query);
		
		/*
		foreach($result['table']['record'] as $k => $v){
			$wallet_amount += $v['amount'];
		}*/
		$wallet_amount = $result['table']['record'][0]['WA'];
		
		return (int)$wallet_amount;
	}	
	
	private function doGetWalletAmountRecord(){
		$query = "SELECT waid FROM `".$this->dbname."`.`".$this->default_prefix."wallet_amount` WHERE behavior = '".$this->params['behavior']."' AND htid = '".$this->params['htid']."'";
		$result = $this->model->getQueryRecord($query);
		
		return empty($result['table']['record'][0]['waid'])?false:$result['table']['record'][0]['waid'];
	}
	
	private function setWalletAmountRecord(){
	
	//print_r($this->params);
		
		$waid = self::createUniqkey();
		
		$query ="INSERT INTO 
				`".$this->dbname."`.`".$this->default_prefix."wallet_amount`(
					`prefixid`, 
					`waid`, 
					`behavior`, 
					`htid`, 
					`uid`, 
					`amount`,
					`insertt`,
					`modifyt`
				)
				VALUES(
					'".$this->obj->config->default_prefix_id."',
					'".$waid."' , 
					'".$this->params['behavior']."' , 
					'".$this->params['ehid']."',
					'".$this->params["userid"]."' , 
					'".$this->params["minus_amount"]."' , 
					now(),
					now()
				)
				";
				
		//print_r($query);
		
		$this->model->query($query);
				
		$this->params['waid'] = $waid;
		
		return true;
	}	
	
	private function setExchangeAmountRecord(){
	
		$eaid = self::createUniqkey();
		
		$query ="INSERT INTO 
				`".$this->dbname."`.`".$this->default_prefix."exchange_amount`(
					`prefixid`, 
					`eaid`, 
					`behavior`, 
					`htid`, 
					`uid`, 
					`amount`, 
					`insertt`,
					`modifyt`
				)
				VALUES(
					'".$this->obj->config->default_prefix_id."',
					'".$eaid."' , 
					'".$this->params['behavior']."' , 
					'".$this->params["ehid"]."' , 
					'".$this->params["userid"]."' , 
					'".$this->params["amount"]."' ,  
					now(),
					now()
				)
				";
				//print_r($query);
		$this->model->query($query);
				
		$this->params['eaid'] = $eaid;
		
		return true;
	}	
	
	private function createUniqkey(){
		$key = md5(uniqid($this->default_prefix));
		return $key;
	}	
}
?>