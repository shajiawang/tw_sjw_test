<?php
class UserGameServer {
	public $mc;
	public $obj;
	public $cache;
	public $cachetime;

	function __construct(&$obj) {
		if (empty ($obj)) {
			die();
		}
		$this->obj = $obj;

		if(empty($obj->model)){
			$this->model = new mysql($obj->config->db[0]);
			$this->model->connect();		
		} else {
			$this->model = $obj->model;
		}

		$this->cachetime = 86400;
		$this->cache = $obj->config->memcache["MEM_STATUS"];
		if ($this->cache && class_exists('mc')) {
			$this->mc = new mc();
			$this->mc->connectionMemcache($obj->config->memcache["server_list"]["ap1"]["ip"],$obj->config->memcache["server_list"]["ap1"]["port"]);
		}
	}

	public function useCache() {
		if ($this->cache && class_exists('mc')) {
			return true;
		}
		return false;
	}
	
	public function getHistory($game_id, $userid) {
		if (!empty ($game_id) && !empty ($userid)) {
			$ugsr_key = 'ugsr_' . $game_id . '_' . $userid;
			$query = "
				SELECT * FROM `".$this->obj->config->db[10]["dbname"]."`.`".$this->obj->config->default_prefix."user_game_server_rt` ugsr
				LEFT OUTER JOIN `".$this->obj->config->db[10]["dbname"]."`.`".$this->obj->config->default_prefix."game_server` gs ON gs.prefixid = '" . $this->obj->config->default_prefix_id . "' AND gs.gsid = ugsr.gsid AND game_id = '$game_id'
				WHERE 1
				AND gs.gsid IS NOT NULL
				AND ugsr.prefixid = '".$this->obj->config->default_prefix_id."'
				AND ugsr.userid = '$userid'
				ORDER BY ugsr.modifyt DESC
			";

			if ($this->useCache()) {
				$usergsr = $this->mc->readMemcache($ugsr_key);
				if (empty ($usergsr)) {
					$result = $this->model->getQueryRecord($query);
					$this->mc->writeMemCache($ugsr_key, $result['table']['record'], $this->cachetime) ;
				}
			} else {
				$result = $this->model->getQueryRecord($query);
			}
			return $result['table']['record'];
		}
	}
	
	public function updateHistory($gsid, $userid, $gsuserid, $game_id = '') {
		if (!empty ($gsid) && !empty ($userid) && !empty ($gsuserid)) {
			$query = "
				INSERT INTO `".$this->obj->config->db[10]["dbname"]."`.`".$this->obj->config->default_prefix."user_game_server_rt` (`prefixid`, `gsid`, `userid`, `gsuserid`, `insertt`)
				VALUES ('".$this->obj->config->default_prefix_id."', '$gsid', '$userid', '$gsuserid', NOW())
				ON DUPLICATE KEY UPDATE
				`modifyt` = NOW()
			";
			$this->model->query($query);

			$query = "
				INSERT INTO `".$this->obj->config->db[10]["dbname"]."`.`".$this->obj->config->default_prefix."user_game_server_history` (`prefixid`, `gsid`, `userid`, `gsuserid`, `ip`, `sources`, `insertt`)
				VALUES ('".$this->obj->config->default_prefix_id."', '$gsid', '$userid', '$gsuserid', '" . $this->obj->io->input['server']['REMOTE_ADDR'] . "', '" . (empty ($this->obj->io->input['get']['src']) ? $this->obj->io->input['server']['HTTP_REFERER'] : $this->obj->io->input['get']['src']) . "', NOW())
			";
			$this->model->query($query);
			$ugsr_key = 'ugsr_' . $game_id . '_' . $userid;
			if ($this->useCache()) {
				$this->mc->deleteMemCache($ugsr_key) ;
			}
		}
	}
}
