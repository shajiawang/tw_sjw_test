<?php
/**
 * deposit object
 *
 * @author Albert
 * @version $Id:deposit.php, v1.0 2012-03-13 Albert $
 * @package deposit
 * @copyright 2012(C)Snsplus
 */
class deposit{

	public $params;
	public $obj;
	public $dbname;
	public $default_prefix;
	public $model;	
	
	public function init($obj,$params = ''){
		if(empty($this->model)){
			$this->model = new mysql($obj->config->db[1]);
			$this->model->connect();	
		}
		$this->params = $params;
		$this->obj = $obj;
		$this->dbname = $this->obj->config->db[2]["dbname"];
		$this->default_prefix = $this->obj->config->default_prefix;
	}
	/*
	params 
	transaction_id
	*/
	public function getDeposit($obj,$params){
		self::init($obj,$params);
		
		$result = self::doGetDeposit();
		
		return $result;		
	}
	/*
	params 
	uid
	transaction_id
	*/
	public function doRufund($obj,$params){
		
		if(empty($params['transaction_id']) || empty($params['uid'])){
			return self::returnParams('0','params error');
		}else{
	
			self::init($obj,$params);
			$result = self::doGetDeposit();
			
			if($result == false){
				return self::returnParams('0','transaction id fall;');
			}
			
			if($result['userid'] != $this->params['uid']){
				return self::returnParams('0','uid not match');
			}else{
				$this->params['amount'] = $result['amount'];
				$check = self::doCheckRefund();
				
				if($check){
					$result = self::setRefund();
					$returnParams = array('rfhid'	=> $this->params['rfhid'],
										  'amount'	=> $this->params['amount'] );
					$return_params = array("result" => 1,"params"=>$returnParams);
					return json_encode($return_params);
				}else{
					return self::returnParams('0','transaction was refunded');
				}
			}
		}
		//return $result;
	}
	
	private function doGetDeposit(){
		$query = "SELECT transaction_id ,amount , userid FROM `".$this->dbname."`.`".$this->default_prefix."deposit_history` WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND transaction_id = '".$this->params['transaction_id']."'";
		
		$result = $this->model->getQueryRecord($query);

		$deposit = $result['table']['record'][0];
		
		return empty($deposit['transaction_id'])?false:$deposit;	
	}
	
	private function doCheckRefund(){
		$query = "SELECT transaction_id FROM `".$this->dbname."`.`".$this->default_prefix."refund_history` WHERE prefixid = '".$this->obj->config->default_prefix_id."' AND transaction_id = '".$this->params['transaction_id']."'";
		
		$result = $this->model->getQueryRecord($query);

		return empty($result['table']['record'][0]['transaction_id'])?true:false;

	}
	
	private function setRefund(){
	
		$rfhid = self::createUniqkey();
		
		$query ="INSERT INTO 
				`".$this->dbname."`.`".$this->default_prefix."refund_history`(
					`prefixid`, 
					`rfhid`, 
					`uid`, 
					`transaction_id`,
					`amount`,
					`insertt`,
					`modifyt`
				)
				VALUES(
					'".$this->obj->config->default_prefix_id."',
					'".$rfhid."' , 
					'".$this->params['uid']."' , 
					'".$this->params['transaction_id']."',
					'".$this->params["amount"]."' , 
					now(),
					now()
				)
				";
				//echo $query; exit();
		
		$this->model->query($query);
				
		$this->params['rfhid'] = $rfhid;
		
		return true;
	}
	
	private function createUniqkey(){
		$key = md5(uniqid($this->default_prefix));
		return $key;
	}		
	
	private function returnParams($result,$message){
		$return_params = array("result" => $result,"message"=>$message);
		return json_encode($return_params);
	}		
}	
	
?>	