<?php
class user{

    public $login_expire = "";
    public $user_info="";

    /*
     * 摩爾投票的api
     * Lion 2012-05-22
     */
    function voteapi($obj, $param=array()){
    	$act = $param['act'];
    	$memberid = $param['memberid'];
    	$email = $param['email'];
    	
    	switch ($act) {
    		//登入
    		case 'login':
    			$url = 'http://test-vote.moloo.in.th/';
    			$obj->curl->postCurl($url, $param);
    			break;
    			
    		//登出
    		case 'logout':
    			$url = 'http://test-vote.moloo.in.th/logout.html';
    			$obj->curl->postCurl($url, $param);
    			break;
    	}
    }
    
    /* 
     * 因應兒童平台處理成簡單帳號(不使用,以table schema解,AUTO_INCREMENT 起始位數縮小)
     * Lion 2012-05-16
     */
    function easyMemberId($memberid){
    	if (empty($memberid)) {
    		return false;
    	} else {
    		$array = array();
    		$array = str_split($memberid);
    		
    		$str = null;
    		$tag = false;
    		
    		foreach ($array as $key => $char) {
    			//0 != $key : 因為第1碼固定不是0,故跳過
    			if (!$tag && 0 != $key && '0' != $char) {
    				$tag = true;
    			}
    			
    			if ($tag) $str .= $char;
    		}
    		
    		if (empty($str)) return false;
    		
    		return $str;
    	}
    }
    
    function getPassword($obj){
        return $obj->string->strEncode($obj->io->input["post"]["password"],$obj->config->encode_key) ;
    }

    function checkMemberId($memberid,$obj){
        $sql = "SELECT memberid FROM `".$obj->config->db[0]["dbname"]."`.`".$obj->config->default_prefix."user` ru
        		WHERE ru.prefixid = '".$obj->config->default_prefix_id."'
        		and ru.memberid = '".$memberid."'";
        
        $result = array();
        $result = $obj->model->getQueryRecord($sql);
        
        if (trim($result["table"]["record"][0]["memberid"]) != '') {
        	return true ;
        } else {
            return false ;
        }
    }
    //2012-5-28 會員訂閱
	function mSubscribe($mail,$obj){
		$sql = "SELECT edm FROM `".$obj->config->db[0]["dbname"]."`.`".$obj->config->default_prefix."user` ru
        		WHERE ru.prefixid = '".$obj->config->default_prefix_id."' and ru.email = '".$mail."' and ru.edm = 'Y'";
		$sql_update="UPDATE`".$obj->config->db[0]["dbname"]."`.`".$obj->config->default_prefix."user` SET `edm`='Y' WHERE `memberid`='".$mail."'";
		
		$result= array();
        $result = $obj->model->getQueryRecord($sql);	
		if (trim($result["table"]["record"][0]["edm"]) != '') {
				return "ได้สมัคร"; //已訂閱
			} else {
		       $obj->model->query($sql_update);
				return "สมัครสมาชิกที่ประสบความสำเร็จ" ;//訂閱成功
        
		}
	}
	//2012-5-28 非會員訂閱
	function nomSubscribe($mail,$obj){
		$sql = "SELECT email FROM `".$obj->config->db[0]["dbname"]."`.`".$obj->config->default_prefix."subscribe` ru
        		WHERE ru.prefixid = '".$obj->config->default_prefix_id."'and ru.email = '".$mail."'";
		$sql_indert = "INSERT INTO `".$obj->config->default_prefix."subscribe`(
                  `prefixid`,
                  `email`,
                  `switch`,
                  `insertt`
                  ) values (
                  '".$obj->config->default_prefix_id."',
                  '".$mail."',
                  'Y',
                  now()
                  )";
		$result= array();
        $result = $obj->model->getQueryRecord($sql);
		//print_r($result);
		//die;
		if (trim($result["table"]["record"][0]["email"]) != '') {
				return "ได้สมัคร"; //已訂閱
			} else {
		       $obj->model->query($sql_indert);
				return "สมัครสมาชิกที่ประสบความสำเร็จ" ;//訂閱成功
		}
	}
	
    //確認使用者
    function checkUser($params=array(),$obj){
    	if (empty($params)) {
    		return false ;
    	} else {
    		$ext = null;
    		
    		foreach ($params as $k => $v) {
    			$ext .= "and ".$k." = '".$v."' ";
    		}
    	}
    	
    	$sql = "SELECT memberid FROM `".$obj->config->db[0]["dbname"]."`.`".$obj->config->default_prefix."user`
    	WHERE prefixid = '".$obj->config->default_prefix_id."'
    	".$ext;
    	
    	$result = array();
    	$result = $obj->model->getQueryRecord($sql);
    
    	if (trim($result["table"]["record"][0]["memberid"]) != '') {
    		return true ;
    	} else {
    		return false ;
    	}
    }
    
    function checkUserId($userid,$obj){
        $query = "
            SELECT userid FROM `".$obj->config->db[0]["dbname"]."`.`".$obj->config->default_prefix."user` ru WHERE ru.userid = '".$userid."'
        ";
                $result["table"] = "";
                $result = $obj->model->getQueryRecord($query);
                if($result["table"]["record"][0]["userid"] != ""){
                        return true ;
                }
                else{
                        return false ;
                }
    }	

    //註冊
    function register($obj){
    	$query = "INSERT INTO `".$obj->config->default_prefix."user`(
                  `prefixid`,
                  `email`,
                  `password`,
                  `birthday`,
                  `countryid`,
                  `insertt`
                  ) values (
                  '".$obj->config->default_prefix_id."',
                  '".trim($obj->io->input["post"]["memberid"])."',
                  '".$this->getPassword($obj)."',
                  '".sprintf('%4d-%02d-%02d',$obj->io->input['post']['year'], $obj->io->input['post']['month'], $obj->io->input['post']['day'])."',
                  '" . $obj->io->input['post']['countryid'] . "',
                  now()
                  )";
    	
        $obj->model->query($query);
        
        $userid = $obj->model->_con->insert_id;
        
        //Lion:為兒童平台提供容易記憶的登入帳號 2012-05-16
        $query = "UPDATE `".$obj->config->default_prefix."user` SET memberid = '".$userid."' WHERE userid = '$userid'";
        $obj->model->query($query);
        
        $query = "INSERT INTO `".$obj->config->default_prefix."role_user_rt`(
                  `prefixid`,
                  `roleid`,
                  `userid`,
                  `insertt`
                  ) values (
                  '".$obj->config->default_prefix_id."',
                  'general',
                  '".$userid."',
                  now()
                  )";
        
        $obj->model->query($query);

        //寫入session
        $user = array();
        $user["userid"] = $userid;
        $user["memberid"] = $userid;
        $user["email"] = trim($obj->io->input["post"]["memberid"]);
        $user["countryid"] = $obj->io->input['post']['countryid']; 

        //摩爾推薦人
        if (isset($obj->io->input['post']['invite_uid']) && '' != trim($obj->io->input['post']['invite_uid'])) $user["invite_uid"] = $obj->io->input['post']['invite_uid'];
        
        $this->createUserSession($user,$obj);
        
        return $userid; 
    }
    
    function setUserVerify($userid,$obj){
        $query = "INSERT INTO
        `".$obj->config->default_prefix."user_verify`(
                                        `prefixid`,
                                        `userid`,
                                        `key`,
                                        `expire`,
                                        `insertt`
                                )
                                values(
                                        '".$obj->config->default_prefix_id."',
                                        '".$userid."',
                                        '".$obj->string->strEncode($userid.",".trim($obj->io->input["post"]["memberid"]).",".time().",".md5(microtime(true)),$obj->config->encode_key)."',
                    '259200' ,
                                        now()
                                )
        ";
        
        $obj->model->query($query);
    }

    //登入確認
    function loginChk($obj){
        if (!isset($_SESSION["wrong_num"])) {
            $_SESSION["wrong_num"] = 0 ;
        }
        
		if (isset($obj->config->login['params'])) {
			switch ($obj->config->login['params']) {
				case 'id':
					$query = "SELECT * FROM `".$obj->config->default_prefix."user`
					where
					prefixid = '".$obj->config->default_prefix_id."' and
					userid = '".trim($obj->io->input["post"]["userid"])."'";
				break;
			}
		} else {
			if (empty($obj->params)) {
				return "user_empty";
			}
			
			$query = "SELECT * FROM `".$obj->config->default_prefix."user`
			where
			prefixid = '".$obj->config->default_prefix_id."' and
			".$obj->params." = '".trim($obj->io->input["post"]["memberid"])."'";
		}
		
		$user_arr = array();
		$user_arr = $obj->model->getQueryRecord($query,$obj);
		
		if (empty($user_arr['table']['record'])) {
			$_SESSION["wrong_num"]++;
			return "user_empty";
		}
		
		if ($user_arr["table"]["record"][0]["password"] != $this->getPassword($obj)) {
			$_SESSION["wrong_num"] = $_SESSION["wrong_num"] + 1;
			return "password_is_wrong";
		}

        if ($_SESSION["wrong_num"] > 3) {
            if (!empty($obj->io->input["post"]["captcha"]) && !$obj->simg->check($obj->io->input["post"]["captcha"])) {
                return "captcha_is_wrong";
            } else {
            	return "Error too many times, please try again later..";
            }
        }
        
        $query = "SELECT *
                FROM `".$obj->config->default_prefix."role_user_rt`
                where
                prefixid = '".$obj->config->default_prefix_id."' and
                userid = '".$user_arr["table"]["record"][0]["userid"]."' 
                ";
        
        $role_arr["table"] = "";
        $role_arr = $obj->model->getQueryRecord($query,$obj);
        
        if ($role_arr["table"] == "") {
            $_SESSION["wrong_num"]++;
            return "role_not_verify";
        } else {
            foreach ($role_arr["table"]["record"] as $rk => $rv) {
                if ($rv["roleid"] == "verify") {
                    $_SESSION["wrong_num"]++;
                    return "role_not_verify" ;
                } else {
                    $user["roleid"][] = $rv["roleid"] ; 
                }
            }			
        }

        $user["userid"] = $user_arr["table"]["record"][0]["userid"] ; 
        $user["nickname"] = $user_arr["table"]["record"][0]["nickname"] ; 
        $user["memberid"] = $user_arr["table"]["record"][0]["memberid"] ; 
        $user["email"] = $user_arr["table"]["record"][0]["email"] ; 
        //$user["s_email"] = $user_arr["table"]["record"][0]["s_email"] ; 
        $user["lanid"] = $user_arr["table"]["record"][0]["lanid"] ; 
        $user["tplid"] = $user_arr["table"]["record"][0]["tplid"] ; 
        $user["countryid"] = $user_arr["table"]["record"][0]["countryid"] ; 
        $user["currencyid"] = $user_arr["table"]["record"][0]["currencyid"] ; 
        $user["owner_currencyid"] = $user_arr["table"]["record"][0]["owner_currencyid"] ; 

        $this->createUserSession($user,$obj);

       	//摩爾投票的api
        $param = array();
        $param['act'] = 'login';
        $param['memberid'] = $user["memberid"];
        $param['email'] = $user["email"];
        
        if ($user_arr["table"]["record"][0]["email"] == '' || $user_arr["table"]["record"][0]["countryid"] == '') {
        	$this->voteapi($obj, $param);
            return "login_first";
        }
        
        $this->voteapi($obj, $param);
        return "login_success";
    }

    function createUserSession($user,$obj){
        $remember = "";
        
        if(isset($obj->io->input["post"]["remember"])){
            $remember = $obj->io->input["post"]["remember"] ; 
        }
        
        if($remember == 'Y'){
            $this->session->session(86400*365);
            //$_SESSION["user"] = $this->string->strEncode($userdata,$this->config->encode_key,$this->config->encode_type) ; 
            $_SESSION["user"] = $user ; 
        }
        else{
            $_SESSION["user"] = $user ; 
            //$_SESSION["user"] = $this->string->strEncode($userdata,$this->config->encode_key,$this->config->encode_type) ; 
        }
        
        unset($_SESSION["wrong_num"]);
    }

    function logOut($obj){
    	//摩爾投票的api
    	$param = array();
    	$param['act'] = 'logout';
    	$param['memberid'] = $obj->io->input["session"]['user']['memberid'];
    	$param['email'] = $obj->io->input["session"]['user']['email'];
    	
    	$this->voteapi($obj, $param);
    	
        session_destroy();
        /*
        if (isset($obj->io->input['get']['PHPSESSID'])) {
            $this->connectionMemcache();
            $this->cleanMemCache($this->config->default_prefix_id,'key',$obj->io->input['get']['PHPSESSID']);
        }
        */
    }

    function userFunPermit($obj){

    }

    function siteMap($obj){

    }

    function funTree($obj){

    }

    function getUserInfo($obj){

    }

    function countRegisterVerify($obj){
                $query ="
                SELECT  count(userid) as num
                FROM `".$obj->config->default_prefix."user_verify`
                where
                prefixid = '".$obj->config->default_prefix_id."' and
        userid = '".$obj->io->input["post"]["userid"]."' and
        insertt like '".strftime('%Y-%m-%d',time())."%';
                ";
                $result["table"] = "";
                $result = $obj->model->getQueryRecord($query,$obj);
        if(is_array($result["table"])){
            return $result["table"]["record"][0]["num"] ; 
        }
        else{
            return 0 ;
        }

    }

    function countForgetPassword($obj){
                $query ="
                SELECT  count(memberid) as num
                FROM `".$obj->config->default_prefix."user_forget_password`
                where
                prefixid = '".$obj->config->default_prefix_id."' and
        memberid = '".trim($obj->io->input["post"]["memberid"])."' and
        insertt like '".strftime('%Y-%m-%d',time())."%';
                ";
                $result["table"] = "";
                $result = $obj->model->getQueryRecord($query,$obj);
        if(is_array($result["table"])){
            return $result["table"]["record"][0]["num"] ; 
        }
        else{
            return 0 ;
        }
    }

    function sendForgetPasswordHistory($obj){
    	$column = (!isset($obj->param['column']))? 'memberid' : $obj->param['column'];
    	
        $query = "SELECT * FROM `".$obj->config->db[0]["dbname"]."`.`".$obj->config->default_prefix."user`
                where prefixid = '".$obj->config->default_prefix_id."'
                and	".$column." = '".trim($obj->io->input["post"]["memberid"])."'";
        
        $result = array();
        $result = $obj->model->getQueryRecord($query,$obj);
        
        $return = array();
        
        if (empty($result['table']['record'])) {
            $return['result'] = 2;
            $return['message'] = 'User is not exist';
        }
        elseif ($result["table"]["record"][0]["email"] == ''){
            $return['result'] = 2;
            $return['message'] = 'E-Mail is not exist';
        }
        elseif ($result["table"]["record"][0]["birthday"] != sprintf('%4d-%02d-%02d', $obj->io->input['post']['year'], $obj->io->input['post']['month'], $obj->io->input['post']['day'])) {
            $return['result'] = 2;
            $return['message'] = 'Wrong birthday';
        }
        elseif ($this->countForgetPassword($obj) >= 3) {
        	$return['result'] = 2;
        	$return['message'] = 'Send forget password limit';
        }
        
        if (!empty($return)) {
        	return $return;
        }
        
        //寫入寄送忘記密碼紀錄
        $query = "INSERT INTO `".$obj->config->default_prefix."user_forget_password`(
        `prefixid`,
        `memberid`,
        `email`,
        `key`,
        `expire`,
        `insertt`
        ) values (
        '".$obj->config->default_prefix_id."',
        '".$result["table"]["record"][0]["memberid"]."' ,
        '".$result["table"]["record"][0]["email"]."'	,
        '".$obj->string->strEncode($result["table"]["record"][0]["memberid"].",".time().",".md5(microtime(true)),$obj->config->encode_key)."',
        '259200',
        now()
        )";
         
        $obj->model->query($query);
        
        $return['result'] = 1;
        $return['message'] = 'success';
        $return['user'] = $result["table"]["record"][0];
        
        return $return;
    }
    
    //寄送忘記密碼信
    function sendForgetPassword($obj){
        $query = "SELECT *
                FROM `".$obj->config->default_prefix."user_forget_password` ufp
                LEFT OUTER JOIN `".$obj->config->default_prefix."user` u ON ufp.prefixid = u.prefixid and ufp.memberid = u.memberid
                where
                ufp.prefixid = '".$obj->config->default_prefix_id."' and
                ufp.issend = 'N' and
                NOW() < TIMESTAMPADD(SECOND, ufp.expire, ufp.insertt) limit 0,10";
        
        $result = array();
        $result = $obj->model->getQueryRecord($query,$obj);
        
        if (!empty ($result['table']['record'])) {
            for ($i = 0 , $n = count($result["table"]['record']); $i < $n; $i++) {
                if ($result["table"]["record"][$i]["nickname"] == '') {
                    $user_name = $result["table"]["record"][$i]["memberid"] ; 
                } else {
                    $user_name = $result["table"]["record"][$i]["nickname"] ; 
                }
                
                $obj->sendmail->subject = "GaMagic --- Forget password";
                $obj->sendmail->body = "
    Hi，".$result["table"]["record"][$i]["email"]."：
    รหัสผ่านของท่านคือ  [" . $obj->string->strDecode($result["table"]["record"][$i]["password"],$obj->config->encode_key) . "]
    นี่คือข่าวสารจากทางระบบ ซึ่งท่านไม่ต้องตอบกลับ ขอบพระคุณในความร่วมมือ！
    GaMagic 
                ";

                $obj->sendmail->to = $result["table"]["record"][$i]["email"];
                $obj->sendmail->sendSmtp();
                
                $query = "UPDATE `".$obj->config->default_prefix."user_forget_password` 
                          SET issend = 'Y'
                          where prefixid = '".$obj->config->default_prefix_id."'
                          and memberid = '".$result["table"]["record"][$i]["memberid"]."'";
                
                $obj->model->query($query);
                sleep(2);
            }
        }
    }

    function modifyForgetPassword($obj){
    }

    function sendRegisterConfirm($obj){
                $query ="
                SELECT  *
                FROM `".$obj->config->default_prefix."user_verify` uv 
        LEFT OUTER JOIN `".$obj->config->default_prefix."user` u ON uv.prefixid = u.prefixid and uv.userid = u.userid 
                where
                uv.prefixid = '".$obj->config->default_prefix_id."' and
        uv.issend = 'N'
                ";
                $result["table"] = "";
                $result = $obj->model->getQueryRecord($query,$obj);
        if(!empty ($result["table"]['record'])){
            $obj->sendmail->subject = "GAMAGIC - คำอธิบายการเปิดใช้บัญชีผู้ใช้";
            $obj->sendmail->body = "
คำอธิบายการเปิดใช้บัญชีผู้ใช้
----------------------------------------------------------------------
Dear " . $result["table"]["record"][0]["email"] . "

ยินดีต้อนรับเข้าเป็นสมาชิก Gamagic 
ทางเรานั้นจำเป็นต้องทำการยืนยันอีเมล์
ของคุณว่าเป็นอีเมล์ที่ใช้ได้จริงเพื่อ
เป็นการลดอีเมล์ขยะและการใช้เว็บไซต์ในทางที่ผิด

เพียงแค่คุณคลิกลิ้งค์เชื่อมโยงข้างล่างนี้
เพื่อยืนยันการเปิดใช้บัญชีของคุณ：

http://".$obj->config->domain_name."/user/confirm_register/key=".base64_encode($result["table"]["record"][0]["key"])."

(ถ้าหากด้านบนนี้ไม่อยู่ในรูปแบบลิ้งค์เชื่อมโยง
กรุณาคัดลอกที่อยู่เวปไซต์ไปเปิดบนเบาร์เซอร์)

ขอขอบคุณที่เข้าร่วมกับเรา
ขอให้ใช้บริการอย่างมีความสุข!



ด้วยความเคารพ

 Gamagic 
อีเมล์ลูกค้าสัมพันธ์: service_th@snsplus.com
";
            $obj->sendmail->to = $result["table"]["record"][0]["email"];
            $obj->sendmail->sendSmtp();
                $query ="
            UPDATE
            `".$obj->config->default_prefix."user_verify` 
            SET issend = 'Y' 
                    where
                    prefixid = '".$obj->config->default_prefix_id."' and
            userid = '".$result["table"]["record"][0]["userid"]."'
            ";
            //echo $query ; exit ;
            $obj->model->query($query);
            sleep(2);
        }
    }
    
    function reSendRegisterKey($obj){
            $query ="
                SELECT  rur.roleid , u.email , u.userid , u.memberid
                FROM `".$obj->config->default_prefix."role_user_rt` rur
        LEFT OUTER JOIN  `".$obj->config->default_prefix."user` u ON rur.prefixid = u.prefixid and rur.userid = u.userid and u.memberid = '".trim($obj->io->input["post"]["memberid"])."'
                where
                rur.prefixid = '".$obj->config->default_prefix_id."' and 
        rur.roleid = 'verify' and 
        u.userid is not null 
                ";
                $result["table"] = "";
                $result = $obj->model->getQueryRecord($query,$obj);
        if(is_array($result["table"])){
            $obj->io->input["post"]["userid"] = $result["table"]["record"][0]["userid"] ; 
            if($this->countRegisterVerify($obj) >= 3 ){
                return "re_send_register_limit"; break;
            }
            $this->setUserVerify($result["table"]["record"][0]["userid"],$obj) ; 
            return 'success' ; break ;
        }
        
        else{
            return 'user_verify_empty'; break ;
        }
        return 'success' ; break;
    }

    function verifyKeyDecode($obj){
        $arr = "";
        if(isset($obj->io->input["get"]["key"])){
            $str = $obj->string->strDecode(base64_decode($obj->io->input["get"]["key"]),$obj->config->encode_key,$obj->config->encode_type) ;
            $arr = explode(',',$str);
            return $arr ; 
        }
        return $arr ; 
        
    }

    function confirmForgetPasswordChange($obj){
        $arr = $this->verifyKeyDecode($obj);
        if(isset($arr[1])){
            if($arr[1]+259200 <= time() ){
                return 'forget_password_timeout' ; break ; 
            }
        }
        else{
            return 'key_formate_is_wrong' ; break ; 
        }
        if(isset($arr[1])){
                        $query ="
                        SELECT  *
                        FROM `".$obj->config->default_prefix."user_forget_password`
                        where
                        prefixid = '".$obj->config->default_prefix_id."' and 
            `key` = '".base64_decode($obj->io->input["get"]["key"])."' and
            `issend` = 'Y' and
            `isverify` = 'Y' and 
            `ischange` = 'N'
                        ";

                        $result["table"] = "";
                        $result = $obj->model->getQueryRecord($query,$obj);
                if(!is_array($result["table"])){
                return 'password_was_changed' ; break ; 
            }
            else{

                    $query ="
                UPDATE
                `".$obj->config->default_prefix."user` 
                SET password = '".$this->getPassword($obj)."'
                            where
                        prefixid = '".$obj->config->default_prefix_id."'  and 
                email = '".$result["table"]["record"][0]["email"]."' 
                ";
                //echo $query ; exit ;
                $obj->model->query($query);

                    $query ="
                UPDATE
                `".$obj->config->default_prefix."user_forget_password` 
                SET ischange = 'Y' 
                            where
                        prefixid = '".$obj->config->default_prefix_id."' and
                email = '".$result["table"]["record"][0]["email"]."'
                ";
                //echo $query ; exit ;
                $obj->model->query($query);
            }
        }
        else{
            return 'key_format_is_wrong'; break ; 
        }
        return 'success';
    }

    function confirmForgetPasswordChk($obj){
        $arr = $this->verifyKeyDecode($obj);
        if(isset($arr[1])){
            if($arr[1]+259200 <= time() ){
                header("location:http://".$obj->config->domain_name."/message/view/msg=forget_password_timeout&key=".$obj->io->input["get"]["key"]);
            }
        }
        else{
            header("location:http://".$obj->config->domain_name."/message/view/msg=key_format_is_wrong");
        }

        if(isset($arr[1])){
                        $query ="
                        SELECT  *
                        FROM `".$obj->config->default_prefix."user_forget_password`
                        where
                        prefixid = '".$obj->config->default_prefix_id."' and 
            `key` = '".base64_decode($obj->io->input["get"]["key"])."' and
            `issend` = 'Y' and
            `isverify` = 'Y' and
            `ischange` = 'N'
                        ";
                        $result["table"] = "";
                        $result = $obj->model->getQueryRecord($query,$obj);
                if(!is_array($result["table"])){
                header("location:http://".$obj->config->domain_name."/message/view/msg=password_was_changed&key=".$obj->io->input["get"]["key"]);
            }
        }
        else{
            header("location:http://".$obj->config->domain_name."/message/view/msg=key_format_is_wrong");
        }
    }

    function confirmForgetPassword($obj){
        //echo $obj->io->input["get"]["key"] ;  exit ;
        $arr = $this->verifyKeyDecode($obj);
        //print_r($arr); exit ;
        //echo "<pre>";		print_r($arr);		exit ;
        if(isset($arr[1])){
            if($arr[1]+259200 <= time() ){
                header("location:http://".$obj->config->domain_name."/message/view/msg=forget_password_timeout&key=".$obj->io->input["get"]["key"]);
            }
        }
        else{
            header("location:http://".$obj->config->domain_name."/message/view/msg=key_format_is_wrong");
        }
        if(isset($arr[1])){
                        $query ="
                        SELECT  *
                        FROM `".$obj->config->default_prefix."user_forget_password`
                        where
                        prefixid = '".$obj->config->default_prefix_id."' and 
            `key` = '".base64_decode($obj->io->input["get"]["key"])."' and
            `issend` = 'Y' and
            `isverify` = 'N'
                        ";
                        $result["table"] = "";
                        $result = $obj->model->getQueryRecord($query,$obj);
                if(is_array($result["table"])){
                    $query ="
                UPDATE
                `".$obj->config->default_prefix."user_forget_password` 
                SET isverify = 'Y' 
                            where
                        prefixid = '".$obj->config->default_prefix_id."' and
                memberid = '".$result["table"]["record"][0]["memberid"]."'
                ";
                //echo $query ; exit ;
                $obj->model->query($query);
                header("location:http://".$obj->config->domain_name."/user/pwd_reset/key=".$obj->io->input["get"]["key"]);
            }
            else{
                header("location:http://".$obj->config->domain_name."/message/view/msg=password_was_changed&key=".$obj->io->input["get"]["key"]);
            }
        }
        else{
            header("location:http://".$obj->config->domain_name."/message/view/msg=key_format_is_wrong");
        }
    }

    function confirmRegister($obj){
        //echo $obj->io->input["get"]["key"] ;  exit ;
        $arr = $this->verifyKeyDecode($obj);
        //echo "<pre>";		print_r($arr);		exit ;
        if(isset($arr[2])){
            if($arr[2]+259200 <= time() ){
                header("location:http://".$obj->config->domain_name."/message/view/msg=signup_activate_invalid_timeout&key=".$obj->io->input["get"]["key"]);
            }
        }
        else{
            header("location:http://".$obj->config->domain_name."/message/view/msg=signup_activate_invalid_key_format_is_wrong");
        }
        if(isset($arr[1])){
                        $query ="
                        SELECT  *
                        FROM `".$obj->config->default_prefix."user_verify`
                        where
                        prefixid = '".$obj->config->default_prefix_id."' and 
            `key` = '".base64_decode($obj->io->input["get"]["key"])."' and
            `issend` = 'Y' and
            `isverify` = 'N'
                        ";
                        $result["table"] = "";
                        $result = $obj->model->getQueryRecord($query,$obj);
                if(is_array($result["table"])){
                    $query ="
                UPDATE
                `".$obj->config->default_prefix."user_verify` 
                SET isverify = 'Y' 
                            where
                        prefixid = '".$obj->config->default_prefix_id."' and
                userid = '".$result["table"]["record"][0]["userid"]."'
                ";
                //echo $query ; exit ;
                $obj->model->query($query);

                    $query ="
                UPDATE
                `".$obj->config->default_prefix."role_user_rt` 
                SET roleid = 'general' 
                            where
                        prefixid = '".$obj->config->default_prefix_id."' and
                roleid = 'verify' and 
                userid = '".$result["table"]["record"][0]["userid"]."' 
                ";
                //echo $query ; exit ;
                $obj->model->query($query);
                header("location:http://".$obj->config->domain_name."/message/view/msg=signup_activate");
            }
            else{
                header("location:http://".$obj->config->domain_name."/message/view/msg=signup_activate_invalid_timeout&key=".$obj->io->input["get"]["key"]);
            }
        }
        else{
            header("location:http://".$obj->config->domain_name."/message/view/msg=signup_activate_invalid_key_format_is_wrong".$obj->io->input["get"]["key"]);
        }
    }

    function checkEmail($email,$obj){
            $query ="
                SELECT  *
                FROM `".$obj->config->default_prefix."user`
                where
                prefixid = '".$obj->config->default_prefix_id."' and
                email = '".$email."'
                limit 1
                ";

            $result["table"] = "";
            $result = $obj->model->getQueryRecord($query,$obj);
            if($result["table"] != ""){
                    return true ;
            }
            else{
                    return false ;
            }
    }

    function changePassword($password, $new_pwd, $retype_pwd, $obj) {
        if (is_object($obj)) {
            if (isset($obj->io->input["session"]["user"]["userid"])) {
                $query ="
                SELECT  *
                FROM `".$obj->config->default_prefix."user`
                where
                prefixid = '".$obj->config->default_prefix_id."' and
                userid = '".$obj->io->input["session"]["user"]["userid"]."' 
                ";

                $user_arr["table"] = "";
                $user_arr = $obj->model->getQueryRecord($query,$obj);

                if($user_arr["table"] == ""){
                    $obj->printMsg("user_empty") ; exit;
                }
                if($user_arr["table"]["record"][0]["password"] != $obj->string->strEncode($password,$obj->config->encode_key)){
                    $obj->printMsg("password_is_wrong") ; exit;
                }

                if ($obj->string->strEncode($new_pwd,$obj->config->encode_key) === $obj->string->strEncode($retype_pwd,$obj->config->encode_key)) {
                    $query ="
                        UPDATE `".$obj->config->default_prefix."user`
                        SET password = '".$obj->string->strEncode($new_pwd,$obj->config->encode_key)."'
                        WHERE
                        prefixid = '".$obj->config->default_prefix_id."' and
                        userid = '".$obj->io->input["session"]["user"]["userid"]."' 
                    ";
                    $obj->model->query($query);
                    
                    $obj->printMsg("success"); exit;
                } else {
                    $obj->printMsg("password_different"); exit;
                }
            }
        }
        $obj->printMsg("not_login"); exit;
    }

    //登入邏輯
    function isLogin($type = 'web',$obj){ // type : web - HTTP Environment , api - Other environment ex : mobile ..
		if (isset($obj->config->proxy['mode']) && ($obj->config->proxy['mode'] == 1)) {
			switch ($obj->config->proxy['platform']) {
				case 'Pubgame':
					if(isset($obj->io->input['cookie']['gm_account']) && !empty($obj->io->input['cookie']['gm_account'])){
						$uid = $this->tripleDesDecrypt($obj->io->input['cookie']['gm_account'],$obj->config->pubgame['cookie_key']);
						$obj->io->input["session"]["user"]['userid'] = (int)trim($uid);
					}
					//print_r($obj->io->input["session"]["user"]);
				break;
			}
		} else {
			if ($type == 'api') {
				if (!isset($obj->io->input["session"]["user"]) && !isset($obj->io->input["session"]["user"]["memberid"])) {
					echo "/user/login/location_url=,"."not_login"; exit ;
				}
			} else {
				$arr_url_param = array();

				if (isset($obj->io->input["get"]["tpl"]) && $obj->io->input["get"]["tpl"]) {
					$arr_url_param["tpl"] = $obj->io->input["get"]["tpl"];
				}

				if (!empty ($obj->io->input['get']['location_url'])) {
					$arr_url_param["location_url"] = $obj->io->input['get']['location_url'];
				} elseif (!empty ($obj->io->input["get"]["redirect"])) {
					$arr_url_param["redirect"] = $obj->io->input['get']['redirect'];
				} elseif (!empty ($obj->io->input["server"]["REQUEST_URI"])) {
					$arr_url_param["redirect"] = base64_encode($obj->io->input["server"]["REQUEST_URI"]);
				}
				
				if(!isset($obj->io->input["session"]["user"]['roleid'])){
					if (isset($arr_url_param["tpl"])) {
						header("location:/user/login_choose/".http_build_query($arr_url_param));
						exit;
					} else {
						header("location:/user/login/".http_build_query($arr_url_param));
						exit;
					}
				} else {
					if(
						$obj->io->input["session"]["user"]["email"] == '' or
						$obj->io->input["session"]["user"]["countryid"] == ''
					){
						//print_r($obj->io->input);exit;
						if ($obj->io->input['get']['fun'] != 'user' && $obj->io->input['get']['act'] != 'login_first') {
							header("location:/user/login_first/".http_build_query($arr_url_param));
							exit;
						}
					}
				}
			}
		}
    }

    function getUserProfile($obj) {
        if (is_object($obj)) {
            if (isset($obj->io->input["session"]["user"]["userid"])) {
                $userid = $obj->io->input["session"]["user"]["userid"];
            } else {
                return ;
            }
        } elseif (is_string($obj)) {
            $userid = intval($obj);
        }

        $obj->configure = 
        array(
            "dev"		=> "N" ,
            "prefix"	=> "N" ,
            "pky" 		=> array('userid') , 
            "select" 	=> 
            array(
                'database_name'	=> $obj->config->database_name[0] , 
                'table_name' 	=> $obj->config->default_prefix.'user' , 
                'table_as_name' => 'user' , 
                'field'		=> array(
                            array('id'=>'userid'		,'sort' => 'Y','width' => '3%','colspan' => '1' , 'switch' => 'N' , 'seq' => '10' ,'edit'=>'N' ),
                            array('id'=>'first_name'	,'sort' => 'Y','width' => '3%','colspan' => '1' , 'switch' => 'N' , 'seq' => '10' ,'edit'=>'N' ),
                            array('id'=>'last_name'		,'sort' => 'N','width' => '5%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '20','edit'=>'N', ),
                            array('id'=>'nickname'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '40','edit'=>'N' ),
                            array('id'=>'email'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '50','edit'=>'N' ),
                            //array('id'=>'s_email'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'sex'		,'sort' => 'N','width' => '5%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '70','edit'=>'N'  ),
                            array('id'=>'birthday'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'countryid'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'memberid'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'lanid'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'tipsid'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'currencyid'	,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '65','edit'=>'N' ),
                            array('id'=>'owner_currencyid'	,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'insertt'		,'sort' => 'N','width' => '5%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '90','edit'=>'N'  )
                        ) ,
                'search'	=>
                        array(
                            'and' => 
                                array( 
                                ),
                            '' => " and user.prefixid = '".$obj->config->default_prefix_id."' and user.userid = '".$userid."'" ,
                        ) ,
                "sort"	=> array( 'insertt'=>'desc' ) ,
                //"sort"	=> null ,
                "max_page"	=> "10" ,
                "max_range"	=> "10" ,
                "group_by"	=> null ,
            ),
            "relation" 	=> 
                    array(
                        array(
                            'database_name'	=> $obj->config->database_name[1] , 
                            "prefix"	=> "Y" ,
                            'table_name' 	=> 'language' , 
                            'name'		=> 'name'	,
                            'pky'		=> array('lanid')
                        ),
                        array(
                            'database_name'	=> $obj->config->database_name[0] , 
                            "prefix"	=> "Y" ,
                            'table_name' 	=> 'country' , 
                            'name'		=> 'name'	,
                            'pky'		=> array('countryid')
                        ),
                        array(
                            'database_name'	=> $obj->config->database_name[0] , 
                            "prefix"	=> "Y" ,
                            'table_name' 	=> 'tips' , 
                            'name'		=> 'name'	,
                            'pky'		=> array('tipsid')
                        ),
                        array(
                            'database_name'	=> $obj->config->database_name[3] , 
                            "prefix"	=> "Y" ,
                            'table_name' 	=> 'currency' , 
                            'name'		=> 'name'	,
                            'pky'		=> array('currencyid')
                        )
                            )

        );


        return  $obj->getOneRecord($obj);
    }

    function getOpenidProfile($obj) {
        if (is_object($obj)) {
            if (isset($obj->io->input["session"]["user"]["openid"])) {
                $openid = $obj->io->input["session"]["user"]["openid"];
            } else {
                return ;
            }
        } elseif (is_string($obj)) {
            $openid = $obj;
        }

        $obj->configure = 
        array(
            "dev"		=> "N" ,
            "prefix"	=> "N" ,
            "pky" 		=> array('openid') , 
            "select" 	=> 
            array(
                'database_name'	=> $obj->config->database_name[0] , 
                'table_name' 	=> $obj->config->default_prefix.'openid' , 
                'table_as_name' => 'openid' , 
                'field'		=> array(
                            array('id'=>'openid'		,'sort' => 'Y','width' => '3%','colspan' => '1' , 'switch' => 'N' , 'seq' => '10' ,'edit'=>'N' ),
                            array('id'=>'first_name'	,'sort' => 'Y','width' => '3%','colspan' => '1' , 'switch' => 'N' , 'seq' => '10' ,'edit'=>'N' ),
                            array('id'=>'last_name'		,'sort' => 'N','width' => '5%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '20','edit'=>'N', ),
                            array('id'=>'nickname'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '40','edit'=>'N' ),
                            array('id'=>'email'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '50','edit'=>'N' ),
                            //array('id'=>'s_email'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'sex'		,'sort' => 'N','width' => '5%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '70','edit'=>'N'  ),
                            array('id'=>'birthday'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'countryid'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'memberid'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'lanid'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'tipsid'		,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'currencyid'	,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '65','edit'=>'N' ),
                            array('id'=>'owner_currencyid'	,'sort' => 'Y','width' => '15%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '60','edit'=>'N' ),
                            array('id'=>'insertt'		,'sort' => 'N','width' => '5%','colspan' => '1' , 'switch' => 'Y' , 'seq' => '90','edit'=>'N'  )
                        ) ,
                'search'	=>
                        array(
                            'and' => 
                                array( 
                                ),
                            '' => " and openid.prefixid = '".$obj->config->default_prefix_id."' and openid.openid = '".$openid."'" ,
                        ) ,
                "sort"	=> array( 'insertt'=>'desc' ) ,
                //"sort"	=> null ,
                "max_page"	=> "10" ,
                "max_range"	=> "10" ,
                "group_by"	=> null ,
            ),
            "relation" 	=> 
                    array(
                        array(
                            'database_name'	=> $obj->config->database_name[1] , 
                            "prefix"	=> "Y" ,
                            'table_name' 	=> 'language' , 
                            'name'		=> 'name'	,
                            'pky'		=> array('lanid')
                        ),
                        array(
                            'database_name'	=> $obj->config->database_name[0] , 
                            "prefix"	=> "Y" ,
                            'table_name' 	=> 'country' , 
                            'name'		=> 'name'	,
                            'pky'		=> array('countryid')
                        ),
                        array(
                            'database_name'	=> $obj->config->database_name[0] , 
                            "prefix"	=> "Y" ,
                            'table_name' 	=> 'tips' , 
                            'name'		=> 'name'	,
                            'pky'		=> array('tipsid')
                        ),
                        array(
                            'database_name'	=> $obj->config->database_name[3] , 
                            "prefix"	=> "Y" ,
                            'table_name' 	=> 'currency' , 
                            'name'		=> 'name'	,
                            'pky'		=> array('currencyid')
                        )
                            )

        );


        return  $obj->getOneRecord($obj);
    }
	/*
	* Albert 2012.04.26  3des for Pubgame
	*/
	function tripleDesEncrypt($string,$key) {  
	  
		$cipher_alg = MCRYPT_TRIPLEDES;   
		$iv = mcrypt_create_iv(mcrypt_get_iv_size($cipher_alg,MCRYPT_MODE_ECB), MCRYPT_RAND);   
		$encrypted_string = mcrypt_encrypt($cipher_alg, $key, $string, MCRYPT_MODE_ECB, $iv);   
		return urlencode(base64_encode($encrypted_string));
	}  
	
	function tripleDesDecrypt($string,$key) {  
	
		 $string = base64_decode(urldecode($string));  
		 $cipher_alg = MCRYPT_TRIPLEDES;  
		 $iv = mcrypt_create_iv(mcrypt_get_iv_size($cipher_alg,MCRYPT_MODE_ECB), MCRYPT_RAND);   
		 $decrypted_string = mcrypt_decrypt($cipher_alg, $key, $string, MCRYPT_MODE_ECB, $iv);   
		 return trim($decrypted_string);  
	}


}
?>
