<?php

#require_once '/var/www/test.gamagic.com/phpmodule/include/uc_config.php';

class ucClient{

    function register($username,$password,$email,$type='g'){
        if(empty($username) || empty($password) || !validateEmail($email)){
            return false;
        }
        $rt=uc_user_register($username.'#'.$type,$password,$email);
        if($rt>0){
            return array('result'=>$rt,'message'=>'register success!!');
        }
        switch($rt){
            case -1:
                $return = array('result'=>-1,'message'=>'invalid username!!');
                break;
            case -2:
                $return = array('result'=>-2,'message'=>'illegal words has beed found!!');
                break;
            case -3:
                $return = array('result'=>-3,'message'=>'usename exist!!');
                break;
            case -4:
                $return = array('result'=>-4,'message'=>'invalid email address!!');
                break;
            case -5:
                $return = array('return'=>-5,'message'=>'email not allow!!');
                break;
            case -6:
                $return = array('return'=>-6,'message'=>'email has been used!!');
                break;	
        }
        return $return;
    }

    function login($username,$password,$type='g'){
        if(empty($username) || empty($password)){
            return false;
        }

        $rt=uc_user_login($username.'#'.$type,$password);
        $uid = $rt[0];
        if($uid>0){
            return uc_user_synlogin($uid);
        }
        switch($uid){
            case -1:
                $return = array('result'=>$uid,'message'=>'user is not exist!!');
                break;
            case -2:
                $return = array('result'=>$uid,'message'=>'wrong password!!');
                break;
        }
        
        return $return;
    }

    function logout(){
        return uc_user_synlogout();
    }
    
    function checkUsername($username,$type='g'){
        return uc_user_checkname($username.'#'.$type);
    }
    
    function editUser($username,$oldpassword,$newpassword,$email,$type='g'){
        return uc_user_edit($username.'#'.$type,$oldpassword,$newpassword,$email);
    }
}


function validateEmail($email)
{
   $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex)
   {
      $isValid = false;
   }
   else
   {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if
(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
                 str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
      {
         // domain not found in DNS
         $isValid = false;
      }
   }
   return $isValid;
}
