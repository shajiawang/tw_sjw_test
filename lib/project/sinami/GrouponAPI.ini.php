<?php
class GrouponAPI {
    private $publicKey = '4235658afbea2c82b3d3d2e738bd17de';
    private $privateKey = 'bcae5f9569d900a3';
    private $url = array(
	'product'=>'http://api.groupon.com.tw/getproducts.php?',
	'status'=>'http://api.groupon.com.tw/getproductstatus.php?'
	);
    private $city = array(
        1 => 1,
        2 => 7,
        3 => 5,
        4 => 9,
        5 => 16,
        6 => 18,
	7 => 99
    );
    private $columns = array(
        'id', 
        'title',
        'description',
        'photos',
        'start_time',
        'end_time',
        //'exchange_starttime',
        //'exchange_endtime',
        'original_price',
        'discount_price',
        //'num_items',
        //'cities',
        'store_name',
        'store_description',
        //'branches',
        //'link',
        //'food',
        'permalink'
    );
    
    function __construct($publicKey = '', $privateKey = '') {
        if (!empty ($publicKey)) {
            $this->publicKey = $publicKey;
        }
        if (!empty ($privateKey)) {
            $this->privateKey = $privateKey;
        }
    }

    private function getUrl($args, $type = 'product') {
        $ts = time();
        $query = array(
            'key' => $this->publicKey,
            'ts' => $ts,
            'sig' => md5($this->publicKey.$ts.$this->privateKey),
            'f' => 'json'
        );

        if (is_array($args) && !empty($args)) {
            $query = array_merge($query, $args);
        }

        return $this->url[$type].http_build_query($query);
    }

    public function getData($dataType = 'json', $city_id = 0) {
        $arg = array('f'=>$dataType);

        if (isset($this->city[$city_id])) {
            $this->city_id = $this->city[$city_id];
            $arg['c'] = $this->city_id;
        }

        $function_name = 'fetch' . strtoupper($dataType) . 'Data';
        if (method_exists($this, $function_name)) {
            return $this->$function_name($arg);
        } else {
            return null;
        }
    }

    private function fetchJSONData($args) {
        $data = json_decode($this->getRawData($args))->products;
        $result = array();

        foreach ($data as $index => $obj) {
            if (time() > strtotime($obj->end_time)) {
                continue;
            }
            foreach ($obj as $key => $value) {
                if ($key == 'photos') {
                    $obj->$key = $value[0]->orig_img;
                } elseif ($key == 'start_time') {
                    $obj->$key = strtotime($value);
                } elseif ($key == 'end_time') {
                    $obj->$key = strtotime($value);
                } elseif (!in_array($key, $this->columns)) {
                    unset($obj->$key);
                }
            }

            $sarg = array('p' => $obj->id);
            $status = json_decode($this->getRawData($sarg, 'status'))->product;
            $obj->sales_num = $status->sale_num;

            $result[] = $obj;
        }

        return $result;
    }
    
    private function fetchXMLData($args) {
        
    }

    public function getRawData($argsi, $type = 'product') {
        $url = $this->getUrl($argsi, $type);
        $body = http_parse_message(http_get($url));
        return $body->body;
    }

    public function getCache() {

    }

    public function setCache($data) {

    }
}
