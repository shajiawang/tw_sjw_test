<?php
class NuomiAPI {
    private $apiurl = 'http://tw.nuomi.com/api/dailydeal?';
    private $apiparam = array('version'=>'v1');
    private $city = array(
        1 => 'Taipei',
        2 => 'Taoyuan',
        3 => 'Hsinchu',
        4 => 'Taichung',
        5 => 'Tainan',
        6 => 'Kaohsiung',
        7 => array('city=Taipei','city=Taoyuan','city=Hsinchu','city=Taichung','cuty=Tainan','city=Kaohsiung')
    );
    private $columns = array(
        'title' => 'title',
        'description' => 'description',
        'photos' => 'image',
        'start_time' => 'startTime',
        'end_time' => 'endTime',
        'original_price' => 'value',
        'discount_price' => 'price',
        'store_name' => 'dpshopid',
        'store_description' => 'address',
        'permalink' => 'loc',
        'sales_num' => 'bought'
    );

    function __construct() {
        
    }
    function getApiUrl($args) {
        $param = '';
        if (isset($args['city_id']) && isset($this->city[$args['city_id']])) {
            if (is_array($this->city[$args['city_id']])) {
                $param = '&' . implode('&', $this->city[$args['city_id']]);
            } else {
                $this->apiparam['city'] = $this->city[$args['city_id']];
            }
        }
        return $this->apiurl.http_build_query($this->apiparam).$param;
    }
    function getXMLData($args) {
        return simplexml_load_file($this->getApiUrl($args));
    }
    function transformData($xml) {
        $result = array();

        foreach ($xml as $index => $item) {
            $data->id = count($result)+1;
            foreach ($this->columns as $dst => $src) {
                if (array_key_exists($src, $item)) {
                    $data->$dst = (string) $item->$src;
                } else {
                    $data->$dst = (string) $item->data->display->$src;
                }
            }
            if (time() > $data->end_time) {
                unset($data);
                continue;
            }
            
            $result[] = $data;
            unset($data);
        }
        return $result;
    }
    function getData($args) {
        $data = $this->getXMLData($args);
        return $this->transformData($data);
    }
}