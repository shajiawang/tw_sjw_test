<?php
include 'GrouponAPI.ini.php';
include 'NuomiAPI.ini.php';
include 'OnetwothreeAPI.ini.php';

class GroupAPI {
    public $table = array();
    private $city = array(
        1 => 'taipei',
        2 => 'taoyuan',
        3 => 'hsinchu',
        4 => 'taichung',
        5 => 'tainan',
        6 => 'kaohsiung',
        7 => 'taiwan'
    );

    function __construct($filename = 'sinami_group.cache') {
        $this->groupon = new GrouponAPI();
        $this->nuomi = new NuomiAPI();
        $this->onetwothree = new OnetwothreeAPI();
        $this->filename = $filename;
    }
    
    function getData($city_id = 1) {
        $arg = array('city_id' => $city_id);

        $this->data['groupon']['table']['record'][$city_id] = $this->groupon->getData('json', $city_id);
        $this->data['nuomi']['table']['record'][$city_id] = $this->nuomi->getData($arg);
        $this->data['onetwothree']['table']['record'][$city_id] = $this->onetwothree->getData($arg);

        return $data;
    }
    
    function getAllData() {
        for ($i = 1 ; $i <= 7 ; $i++) {
            $arg = array('city_id' => $i);
            $this->data['groupon']['table']['record'][$i] = $this->groupon->getData('json', $i);
            $this->data['nuomi']['table']['record'][$i] = $this->nuomi->getData($arg);
            $this->data['onetwothree']['table']['record'][$i] = $this->onetwothree->getData($arg);
        }
    }
    
    function getCache() {
        if (file_exists($this->filename)) {
            return unserialize(file_get_contents($this->filename));
        }
        return null;
    }
    
    function updateCache($force = false) {
        if (    $force == true 
                || !file_exists($this->filename) 
                || date('Y-m-d', filemtime($this->filename)) != date('Y-m-d')) {
            $this->getAllData();
            file_put_contents($this->filename, serialize($this->data));
        }
    }
    
    function getCityName($city_id) {
        if (isset($this->city[$city_id])) {
            return $this->city[$city_id];
        }
    }
}