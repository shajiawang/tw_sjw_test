<?php
class OnetwothreeAPI {
    private $apiurl = 'http://www.123.com.tw/rss.xml?';
    private $city = array(
        1 => array('台北','全國'),
        2 => array('桃園','全國'),
        3 => array('新竹','全國'),
        4 => array('台中','全國'),
        5 => array('台南','全國'),
        6 => array('高雄','全國'),
        7 => array('全國')
    );
    private $columns = array(
        'id' => 'guid',
        'title' => 'title',
        'description' => 'description',
        'photos' => 'image',
        'start_time' => 'startTime',
        'end_time' => 'endTime',
        'original_price' => 'original',
        'discount_price' => 'price',
        'store_name' => 'shop',
        'store_description' => 'address',
        'permalink' => 'link',
        'sales_num' => 'bought'
    );
    private $result = array();

    function __construct() {
        
    }
    function getApiUrl($args) {
        $this->apiurl;
    }
    function getXMLData($args) {
        if (!empty ($this->apiurl)) {
            $this->xml = simplexml_load_file($this->apiurl);
        }
    }
    function transformData($args) {
        $arr_city = $this->city[$args['city_id']];
        if (is_array($this->city[$args['city_id']])) {
            foreach ($this->xml->channel->item as $item) {
                if (in_array((string) $item->city, $this->city[$args['city_id']])) {
                    foreach ($this->columns as $dst => $src) {
                        $data->$dst = (string) $item->$src;
                    }
                    if (time() > $data->end_time) {
                        unset($data);
                        continue;
                    }

                    $this->result[] = $data;
                    unset($data);
                }
            }
        }
    }
    function getData($args) {
        $this->getXMLData($args);
        $this->transformData($args);
        return $this->result;
    }
}