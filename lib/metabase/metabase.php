<?php
/*
 * metabase_interface.php
 *
 * @(#) $Header: /home/mlemos/cvsroot/metabase/metabase_interface.php,v 1.74 2006/07/08 21:30:07 mlemos Exp $
 *
 */

Function MetabaseSetupDatabase($arguments,&$database)
{
	global $metabase_databases;

	$database=count($metabase_databases)+1;
	if(strcmp($error=MetabaseSetupInterface($arguments,$metabase_databases[$database]),""))
	{
		Unset($metabase_databases[$database]);
		$database=0;
	}
	else
		$metabase_databases[$database]->database=$database;
	return($error);
}

Function MetabaseQuery($database,$query)
{
	global $metabase_databases;
	return($metabase_databases[$database]->Query($query));
}

Function MetabaseQueryField($database,$query,&$field,$type="text")
{
	global $metabase_databases;

	return($metabase_databases[$database]->QueryField($query,$field,$type));
}

Function MetabaseQueryRow($database,$query,&$row,$types="")
{
	global $metabase_databases;

	return($metabase_databases[$database]->QueryRow($query,$row,$types));
}

Function MetabaseQueryColumn($database,$query,&$column,$type="text")
{
	global $metabase_databases;

	return($metabase_databases[$database]->QueryColumn($query,$column,$type));
}

Function MetabaseQueryAll($database,$query,&$all,$types="")
{
	global $metabase_databases;

	return($metabase_databases[$database]->QueryAll($query,$all,$types));
}

Function MetabaseReplace($database,$table,&$fields)
{
	global $metabase_databases;

	return($metabase_databases[$database]->Replace($table,$fields));
}

Function MetabasePrepareQuery($database,$query)
{
	global $metabase_databases;

	return($metabase_databases[$database]->PrepareQuery($query));
}

Function MetabaseFreePreparedQuery($database,$prepared_query)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FreePreparedQuery($prepared_query));
}

Function MetabaseExecuteQuery($database,$prepared_query)
{
	global $metabase_databases;

	return($metabase_databases[$database]->ExecuteQuery($prepared_query));
}

Function MetabaseQuerySet($database,$prepared_query,$parameter,$type,$value,$is_null=0,$field="")
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySet($prepared_query,$parameter,$type,$value,$is_null,$field));
}

Function MetabaseQuerySetNull($database,$prepared_query,$parameter,$type)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetNull($prepared_query,$parameter,$type));
}

Function MetabaseQuerySetText($database,$prepared_query,$parameter,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetText($prepared_query,$parameter,$value));
}

Function MetabaseQuerySetCLOB($database,$prepared_query,$parameter,$value,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetCLOB($prepared_query,$parameter,$value,$field));
}

Function MetabaseQuerySetBLOB($database,$prepared_query,$parameter,$value,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetBLOB($prepared_query,$parameter,$value,$field));
}

Function MetabaseQuerySetInteger($database,$prepared_query,$parameter,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetInteger($prepared_query,$parameter,$value));
}

Function MetabaseQuerySetBoolean($database,$prepared_query,$parameter,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetBoolean($prepared_query,$parameter,$value));
}

Function MetabaseQuerySetDate($database,$prepared_query,$parameter,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetDate($prepared_query,$parameter,$value));
}

Function MetabaseQuerySetTimestamp($database,$prepared_query,$parameter,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetTimestamp($prepared_query,$parameter,$value));
}

Function MetabaseQuerySetTime($database,$prepared_query,$parameter,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetTime($prepared_query,$parameter,$value));
}

Function MetabaseQuerySetFloat($database,$prepared_query,$parameter,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetFloat($prepared_query,$parameter,$value));
}

Function MetabaseQuerySetDecimal($database,$prepared_query,$parameter,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetDecimal($prepared_query,$parameter,$value));
}

Function MetabaseQuerySetKey($database,$prepared_query,$parameter,$table)
{
	global $metabase_databases;

	return($metabase_databases[$database]->QuerySetKey($prepared_query,$parameter,$table));
}

Function MetabaseAffectedRows($database,&$affected_rows)
{
	global $metabase_databases;

	return($metabase_databases[$database]->AffectedRows($affected_rows));
}

Function MetabaseFetchResult($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchResult($result,$row,$field));
}

Function MetabaseFetchCLOBResult($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchCLOBResult($result,$row,$field));
}

Function MetabaseFetchBLOBResult($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchBLOBResult($result,$row,$field));
}

Function MetabaseDestroyResultLOB($database,$lob)
{
	global $metabase_databases;

	return($metabase_databases[$database]->DestroyResultLOB($lob));
}

Function MetabaseEndOfResultLOB($database,$lob)
{
	global $metabase_databases;

	return($metabase_databases[$database]->EndOfResultLOB($lob));
}

Function MetabaseReadResultLOB($database,$lob,&$data,$length)
{
	global $metabase_databases;

	return($metabase_databases[$database]->ReadResultLOB($lob,$data,$length));
}

Function MetabaseResultIsNull($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->ResultIsNull($result,$row,$field));
}

Function MetabaseFetchDateResult($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchDateResult($result,$row,$field));
}

Function MetabaseFetchTimestampResult($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchTimestampResult($result,$row,$field));
}

Function MetabaseFetchTimeResult($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchTimeResult($result,$row,$field));
}

Function MetabaseFetchBooleanResult($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchBooleanResult($result,$row,$field));
}

Function MetabaseFetchFloatResult($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchFloatResult($result,$row,$field));
}

Function MetabaseFetchDecimalResult($database,$result,$row,$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchDecimalResult($result,$row,$field));
}

Function MetabaseFetchResultField($database,$result,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchResultField($result,$field));
}

Function MetabaseFetchResultArray($database,$result,&$array,$row)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchResultArray($result,$array,$row));
}

Function MetabaseFetchResultRow($database,$result,&$row)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchResultRow($result,$row));
}

Function MetabaseFetchResultColumn($database,$result,&$column)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchResultColumn($result,$column));
}

Function MetabaseFetchResultAll($database,$result,&$all)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FetchResultAll($result,$all));
}

Function MetabaseNumberOfRows($database,$result)
{
	global $metabase_databases;

	return($metabase_databases[$database]->NumberOfRows($result));
}

Function MetabaseNumberOfColumns($database,$result)
{
	global $metabase_databases;

	return($metabase_databases[$database]->NumberOfColumns($result));
}

Function MetabaseGetColumnNames($database,$result,&$column_names)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetColumnNames($result,$column_names));
}

Function MetabaseSetResultTypes($database,$result,&$types)
{
	global $metabase_databases;

	return($metabase_databases[$database]->SetResultTypes($result,$types));
}

Function MetabaseFreeResult($database,$result)
{
	global $metabase_databases;

	return($metabase_databases[$database]->FreeResult($result));
}

Function MetabaseError($database)
{
	global $metabase_databases;

	return($metabase_databases[$database]->Error());
}

Function MetabaseSetErrorHandler($database,$function)
{
	global $metabase_databases;

	return($metabase_databases[$database]->SetErrorHandler($function));
}

Function MetabaseCreateDatabase($database,$name)
{
	global $metabase_databases;

	return($metabase_databases[$database]->CreateDatabase($name));
}

Function MetabaseDropDatabase($database,$name)
{
	global $metabase_databases;

	return($metabase_databases[$database]->DropDatabase($name));
}

Function MetabaseSetDatabase($database,$name)
{
	global $metabase_databases;

	return($metabase_databases[$database]->SetDatabase($name));
}

Function MetabaseGetIntegerFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetIntegerFieldTypeDeclaration($name,$field));
}

Function MetabaseGetTextFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetTextFieldTypeDeclaration($name,$field));
}

Function MetabaseGetCLOBFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetCLOBFieldTypeDeclaration($name,$field));
}

Function MetabaseGetBLOBFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetBLOBFieldTypeDeclaration($name,$field));
}

Function MetabaseGetBooleanFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetBooleanFieldTypeDeclaration($name,$field));
}

Function MetabaseGetDateFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetDateFieldTypeDeclaration($name,$field));
}

Function MetabaseGetTimestampFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetTimestampFieldTypeDeclaration($name,$field));
}

Function MetabaseGetTimeFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetTimeFieldTypeDeclaration($name,$field));
}

Function MetabaseGetFloatFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetFloatFieldTypeDeclaration($name,$field));
}

Function MetabaseGetDecimalFieldTypeDeclaration($database,$name,&$field)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetDecimalFieldTypeDeclaration($name,$field));
}

Function MetabaseGetTextFieldValue($database,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetTextFieldValue($value));
}

Function MetabaseGetBooleanFieldValue($database,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetBooleanFieldValue($value));
}

Function MetabaseGetDateFieldValue($database,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetDateFieldValue($value));
}

Function MetabaseGetTimestampFieldValue($database,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetTimestampFieldValue($value));
}

Function MetabaseGetTimeFieldValue($database,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetTimeFieldValue($value));
}

Function MetabaseGetFloatFieldValue($database,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetFloatFieldValue($value));
}

Function MetabaseGetDecimalFieldValue($database,$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetDecimalFieldValue($value));
}

Function MetabaseSupport($database,$feature)
{
	global $metabase_databases;

	return($metabase_databases[$database]->Support($feature));
}

Function MetabaseCreateTable($database,$name,&$fields)
{
	global $metabase_databases;

	return($metabase_databases[$database]->CreateTable($name,$fields));
}

Function MetabaseCreateDetailedTable($database,&$table,$check)
{
	global $metabase_databases;

	return($metabase_databases[$database]->CreateDetailedTable($table,$check));
}

Function MetabaseDropTable($database,$name)
{
	global $metabase_databases;

	return($metabase_databases[$database]->DropTable($name));
}

Function MetabaseDropDetailedTable($database,&$table,$check)
{
	global $metabase_databases;

	return($metabase_databases[$database]->DropDetailedTable($table,$check));
}

Function MetabaseAlterTable($database,$name,&$changes,$check=0)
{
	global $metabase_databases;

	return($metabase_databases[$database]->AlterTable($name,$changes,$check));
}

Function MetabaseListTables($database,&$tables)
{
	global $metabase_databases;

	return($metabase_databases[$database]->ListTables($tables));
}

Function MetabaseListTableFields($database,$table,&$fields)
{
	global $metabase_databases;

	return($metabase_databases[$database]->ListTableFields($table,$fields));
}

Function MetabaseGetTableFieldDefinition($database,$table,$field,&$definition)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetTableFieldDefinition($table,$field,$definition));
}

Function MetabaseListTableKeys($database, $table, $primary, &$keys)
{
	global $metabase_databases;

	return($metabase_databases[$database]->ListTableKeys($table, $primary, $keys));
}

Function MetabaseGetTableKeyDefinition($database, $table, $key, $primary, &$definition)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetTableKeyDefinition($table, $key, $primary, $definition));
}

Function MetabaseListTableIndexes($database,$table,&$indexes)
{
	global $metabase_databases;

	return($metabase_databases[$database]->ListTableIndexes($table,$indexes));
}

Function MetabaseGetTableIndexDefinition($database,$table,$index,&$definition)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetTableIndexDefinition($table,$index,$definition));
}

Function MetabaseListSequences($database,&$sequences)
{
	global $metabase_databases;

	return($metabase_databases[$database]->ListSequences($sequences));
}

Function MetabaseGetSequenceDefinition($database,$sequence,&$definition)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetSequenceDefinition($sequence,$definition));
}

Function MetabaseCreateSequence($database,$name,$start)
{
	global $metabase_databases;

	return($metabase_databases[$database]->CreateSequence($name,$start));
}

Function MetabaseDropSequence($database,$name)
{
	global $metabase_databases;

	return($metabase_databases[$database]->DropSequence($name));
}

Function MetabaseGetSequenceNextValue($database,$name,&$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetSequenceNextValue($name,$value));
}

Function MetabaseGetSequenceCurrentValue($database,$name,&$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetSequenceCurrentValue($name,$value));
}

Function MetabaseGetNextKey($database,$table,&$key)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetNextKey($table,$key));
}

Function MetabaseGetInsertedKey($database,$table,&$value)
{
	global $metabase_databases;

	return($metabase_databases[$database]->GetInsertedKey($table,$value));
}

Function MetabaseAutoCommitTransactions($database,$auto_commit)
{
	global $metabase_databases;

	return($metabase_databases[$database]->AutoCommitTransactions($auto_commit));
}

Function MetabaseCommitTransaction($database)
{
	global $metabase_databases;

	return($metabase_databases[$database]->CommitTransaction());
}

Function MetabaseRollbackTransaction($database)
{
	global $metabase_databases;

	return($metabase_databases[$database]->RollbackTransaction());
}

Function MetabaseCreateIndex($database,$table,$name,$definition)
{
	global $metabase_databases;

	return($metabase_databases[$database]->CreateIndex($table,$name,$definition));
}

Function MetabaseDropIndex($database,$table,$name)
{
	global $metabase_databases;

	return($metabase_databases[$database]->DropIndex($table,$name));
}

Function MetabaseSetSelectedRowRange($database,$first,$limit)
{
	global $metabase_databases;

	return($metabase_databases[$database]->SetSelectedRowRange($first,$limit));
}

Function MetabaseEndOfResult($database,$result)
{
	global $metabase_databases;

	return($metabase_databases[$database]->EndOfResult($result));
}

Function MetabaseCaptureDebugOutput($database,$capture)
{
	global $metabase_databases;

	$metabase_databases[$database]->CaptureDebugOutput($capture);
}

Function MetabaseDebugOutput($database)
{
	global $metabase_databases;

	return($metabase_databases[$database]->DebugOutput());
}

Function MetabaseDebug($database,$message)
{
	global $metabase_databases;

	return($metabase_databases[$database]->Debug($message));
}

Function MetabaseBeginsWith($database, $text)
{
	global $metabase_databases;

	return($metabase_databases[$database]->BeginsWith($text));
}

Function MetabaseContains($database, $text)
{
	global $metabase_databases;

	return($metabase_databases[$database]->Contains($text));
}

Function MetabaseEndsWith($database, $text)
{
	global $metabase_databases;

	return($metabase_databases[$database]->EndsWith($text));
}

Function MetabaseMatchPattern($database, $pattern)
{
	global $metabase_databases;

	return($metabase_databases[$database]->MatchPattern($pattern));
}

/*
 * metabase_database.php
 *
 * @(#) $Header: /home/mlemos/cvsroot/metabase/metabase_database.php,v 1.107 2008/01/19 05:12:32 mlemos Exp $
 *
 */

define("METABASE_TYPE_TEXT",0);
define("METABASE_TYPE_BOOLEAN",1);
define("METABASE_TYPE_INTEGER",2);
define("METABASE_TYPE_DECIMAL",3);
define("METABASE_TYPE_FLOAT",4);
define("METABASE_TYPE_DATE",5);
define("METABASE_TYPE_TIME",6);
define("METABASE_TYPE_TIMESTAMP",7);
define("METABASE_TYPE_CLOB",8);
define("METABASE_TYPE_BLOB",9);

$metabase_registered_transactions_shutdown=0;

$metabase_databases=array();

Function MetabaseParseConnectionArguments($connection,&$arguments)
{
	$parameters=parse_url($connection);
	if(!IsSet($parameters["scheme"]))
		return("it was not specified the connection type argument");
	$arguments["Type"]=$parameters["scheme"];
	if(IsSet($parameters["host"]))
		$arguments["Host"]=UrlDecode($parameters["host"]);
	if(IsSet($parameters["user"]))
		$arguments["User"]=UrlDecode($parameters["user"]);
	if(IsSet($parameters["pass"]))
		$arguments["Password"]=UrlDecode($parameters["pass"]);
	if(IsSet($parameters["port"]))
		$arguments["Options"]["Port"]=$parameters["port"];
	if(IsSet($parameters["path"]))
		$arguments["Database"]=UrlDecode(substr($parameters["path"],1));
	if(IsSet($parameters["query"]))
	{
		$options=explode("&",$parameters["query"]);
		for($option=0;$option<count($options);$option++)
		{
			if(GetType($equal=strpos($options[$option],"="))!="integer")
				return($options[$option]." connection option argument does not specify a value");
			$argument=UrlDecode(substr($options[$option],0,$equal));
			$value=UrlDecode(substr($options[$option],$equal+1));
			if(GetType($slash=strpos($argument,"/"))=="integer")
			{
				if(substr($argument,0,$slash)!="Options")
					return("it was not specified a valid conection option argument");
				$arguments["Options"][substr($argument,$slash+1)]=$value;
			}
			else
				$arguments[$argument]=$value;
		}
	}
	return("");
}

Function MetabaseLoadClass($include,$include_path,$type)
{
	$separator="";
	$directory_separator=(defined("DIRECTORY_SEPARATOR") ? DIRECTORY_SEPARATOR : "/");
	$length=strlen($include_path);
	if($length)
	{
		if($include_path[$length-1]!=$directory_separator)
			$separator=$directory_separator;
	}
	if(file_exists($include_path.$separator.$include))
	{
		include($include_path.$separator.$include);
		return("");
	}
	if(function_exists("ini_get")
	&& strlen($php_include_paths=ini_get("include_path")))
	{
		$paths=explode((defined("PHP_OS") && !strcmp(substr(PHP_OS,0,3),"WIN")) ? ";" : ":",$php_include_paths);
		for($path=0;$path<count($paths);$path++)
		{
			$php_include_path=$paths[$path];
			$length=strlen($php_include_path);
			if($length)
			{
				if($php_include_path[$length-1]!=$directory_separator)
					$separator=$directory_separator;
			}
			/* Shaun modify 
			if(file_exists($php_include_path.$separator.$include))
			{
				include($php_include_path.$separator.$include);
				return("");
			}
			*/
			if(file_exists($php_include_path.$include_path.$separator.$include))
			{
				include($php_include_path.$include_path.$separator.$include);
				return("");
			}
		
		}
	}
	$directory=0;
	if(strlen($include_path)==0
	|| ($directory=@opendir($include_path)))
	{
		if($directory)
			closedir($directory);
		return("it was not specified an existing $type file ($include)".(strlen($include_path)==0 ? " and no Metabase IncludePath option was specified in the setup call" : ""));
	}
	return("it was not specified a valid $type include path");
}

Function MetabaseSetupInterface(&$arguments,&$db)
{
	if(IsSet($arguments["Connection"])
	&& strlen($error=MetabaseParseConnectionArguments($arguments["Connection"],$arguments)))
		return($error);
	if(IsSet($arguments["Type"]))
	{
		if(GetType($dash=strpos($arguments["Type"],"-"))=="integer")
		{
			$type=substr($arguments["Type"],0,$dash);
			$sub_type=substr($arguments["Type"],$dash+1);
		}
		else
		{
			$type=$arguments["Type"];
			$sub_type="";
		}
	}
	else
		$type=$sub_type="";
	$sub_include=$sub_included="";
	switch($type)
	{
		case "ibase";
			$include="metabase_ibase.php";
			$class_name="metabase_ibase_class";
			$included="METABASE_IBASE_INCLUDED";
			break;
		case "ifx";
			$include="metabase_ifx.php";
			$class_name="metabase_ifx_class";
			$included="METABASE_IFX_INCLUDED";
			break;
		case "msql";
			$include="metabase_msql.php";
			$class_name="metabase_msql_class";
			$included="METABASE_MSQL_INCLUDED";
			break;
		case "mssql";
			$include="metabase_mssql.php";
			$class_name="metabase_mssql_class";
			$included="METABASE_MSSQL_INCLUDED";
			break;
		case "mysql";
			$include="metabase_mysql.php";
			$class_name="metabase_mysql_class";
			$included="METABASE_MYSQL_INCLUDED";
			break;
		case "pgsql";
			$include="metabase_pgsql.php";
			$class_name="metabase_pgsql_class";
			$included="METABASE_PGSQL_INCLUDED";
			break;
		case "odbc";
			$include="metabase_odbc.php";
			$class_name="metabase_odbc_class";
			$included="METABASE_ODBC_INCLUDED";
			switch($sub_type)
			{
				case "":
					break;
				case "msaccess":
					$sub_include="metabase_odbc_msaccess.php";
					$class_name="metabase_odbc_msaccess_class";
					$sub_included="METABASE_ODBC_MSACCESS_INCLUDED";
					break;
				default:
					return("\"$sub_type\" is not a supported ODBC database sub type");
			}
			break;
		case "oci";
			$include="metabase_oci.php";
			$class_name="metabase_oci_class";
			$included="METABASE_OCI_INCLUDED";
			break;
		case "sqlite";
			$include="metabase_sqlite.php";
			$class_name="metabase_sqlite_class";
			$included="METABASE_SQLITE_INCLUDED";
			break;
		case "":
			$included=(IsSet($arguments["IncludedConstant"]) ? $arguments["IncludedConstant"] : "");
			if(!IsSet($arguments["Include"])
			|| !strcmp($include=$arguments["Include"],""))
				return(IsSet($arguments["Include"]) ? "it was not specified a valid database include file" : "it was not specified a valid DBMS driver type");

			$sub_included=(IsSet($arguments["SubIncludedConstant"]) ? $arguments["SubIncludedConstant"] : "");
			if(!IsSet($arguments["SubInclude"])
			|| !strcmp($sub_include=$arguments["SubInclude"],""))
				return(IsSet($arguments["SubInclude"]) ? "it was not specified a valid database sub-include file" : "it was not specified a valid DBMS sub-driver type");

			if(!IsSet($arguments["ClassName"])
			|| !strcmp($class_name=$arguments["ClassName"],""))
				return("it was not specified a valid database class name");
			break;
		default:
			return("\"$type\" is not a supported driver type");
	}
	$include_path=(IsSet($arguments["IncludePath"]) ? $arguments["IncludePath"] : "");
	$length=strlen($include_path);
	$directory_separator=(defined("DIRECTORY_SEPARATOR") ? DIRECTORY_SEPARATOR : "/");
	$separator="";
	if($length)
	{
		if($include_path[$length-1]!=$directory_separator)
			$separator=$directory_separator;
	}
	if(strlen($included)
	&& !defined($included))
	{
		$error=MetabaseLoadClass($include,$include_path,"DBMS driver");
		if(strlen($error))
			return($error);
	}
	if(strlen($sub_included)
	&& !defined($sub_included))
	{
		$error=MetabaseLoadClass($sub_include,$include_path,"DBMS sub driver");
		if(strlen($error))
			return($error);
	}
	$db=new $class_name;
	$db->include_path=$include_path;
	if(IsSet($arguments["Host"]))
		$db->host=$arguments["Host"];
	if(IsSet($arguments["User"]))
		$db->user=$arguments["User"];
	if(IsSet($arguments["Password"]))
		$db->password=$arguments["Password"];
	if(IsSet($arguments["Persistent"]))
		$db->persistent=$arguments["Persistent"];
	if(IsSet($arguments["Debug"]))
		$db->debug=$arguments["Debug"];
	$db->decimal_places=(IsSet($arguments["DecimalPlaces"]) ? $arguments["DecimalPlaces"] : 2);
	$db->lob_buffer_length=(IsSet($arguments["LOBBufferLength"]) ? $arguments["LOBBufferLength"] : 8000);
	if(IsSet($arguments["LogLineBreak"]))
		$db->log_line_break=$arguments["LogLineBreak"];
	if(IsSet($arguments["Options"]))
		$db->options=$arguments["Options"];
	if(IsSet($arguments["AllowNestedTransactions"]))
		$db->allow_nested_transactions = !!$arguments["AllowNestedTransactions"];
	if(strlen($error=$db->Setup()))
		return($error);
	if(IsSet($arguments["Database"]))
		$db->SetDatabase($arguments["Database"]);
	return("");
}

Function MetabaseSetupDatabaseObject($arguments,&$db)
{
	global $metabase_databases;

	$database=count($metabase_databases)+1;
	if(strcmp($error=MetabaseSetupInterface($arguments,$db),""))
		Unset($metabase_databases[$database]);
	else
	{
		eval("\$metabase_databases[\$database]= &\$db;");
		$db->database=$database;
	}
	return($error);
}

Function MetabaseCloseSetup($database)
{
	global $metabase_databases;

	$metabase_databases[$database]->CloseSetup();
	$metabase_databases[$database]="";
}

Function MetabaseNow()
{
	return(strftime("%Y-%m-%d %H:%M:%S"));
}

Function MetabaseToday()
{
	return(strftime("%Y-%m-%d"));
}

Function MetabaseTime()
{
	return(strftime("%H:%M:%S"));
}

Function MetabaseShutdownTransactions()
{
	global $metabase_databases;

	for(Reset($metabase_databases),$database=0;$database<count($metabase_databases);Next($metabase_databases),$database++)
	{
		$metabase_database=Key($metabase_databases);
		if(GetType($metabase_databases[$metabase_database])=='object')
			$metabase_databases[$metabase_database]->CloseSetup();
	}
}

Function MetabaseDefaultDebugOutput($database,$message)
{
	global $metabase_databases;

	$metabase_databases[$database]->debug_output.="$database $message".$metabase_databases[$database]->log_line_break;
}

class metabase_database_class
{
	/* PUBLIC DATA */

	var $database=0;
	var $host="";
	var $user="";
	var $password="";
	var $options=array();
	var $supported=array();
	var $persistent=1;
	var $database_name="";
	var $warning="";
	var $affected_rows=-1;
	var $auto_commit=1;
	var $prepared_queries=array();
	var $decimal_places=2;
	var $first_selected_row=0;
	var $selected_row_limit=0;
	var $lob_buffer_length=8000;
	var $escape_quotes="";
	var $escape_pattern="";
	var $log_line_break="\n";

	/* PRIVATE DATA */

	var $lobs=array();
	var $clobs=array();
	var $blobs=array();
	var $last_error="";
	var $in_transaction=0;
	var $allow_nested_transactions = 1;
	var $commit_nested_transactions = 0;
	var $rollback_nested_transactions = 0;
	var $debug="";
	var $debug_output="";
	var $pass_debug_handle=0;
	var $result_types=array();
	var $error_handler="";
	var $manager;
	var $include_path="";
	var $manager_included_constant="";
	var $manager_include="";
	var $manager_sub_included_constant="";
	var $manager_sub_include="";
	var $manager_class_name="";

	/* PRIVATE METHODS */

	Function EscapeText(&$text)
	{
		if(strcmp($this->escape_quotes,"'"))
			$text=str_replace($this->escape_quotes,$this->escape_quotes.$this->escape_quotes,$text);
		$text=str_replace("'",$this->escape_quotes."'",$text);
	}

	Function EscapePatternText($text)
	{
		$text="pattern escaping is not implemented";
		$this->Debug($text);
		return($text);
	}

	/* PUBLIC METHODS */

	Function Close()
	{
	}

	Function CloseSetup()
	{
		for($t = $this->in_transaction; $t && $this->RollbackTransaction() && $this->AutoCommitTransactions(1); --$t);
		$this->Close();
	}

	Function Debug($message)
	{
		if(strcmp($function=$this->debug,""))
		{
			if($this->pass_debug_handle)
				$function($this->database,$message);
			else
				$function($message);
		}
	}

	Function DebugOutput()
	{
		$output=$this->debug_output;
		$this->debug_output="";
		return($output);
	}

	Function SetDatabase($name)
	{
		$previous_database_name=$this->database_name;
		$this->database_name=$name;
		return($previous_database_name);
	}

	Function RegisterTransactionShutdown($auto_commit)
	{
		global $metabase_registered_transactions_shutdown;

		$this->in_transaction = ($auto_commit ? 0 : 1);
		if(!$auto_commit
		&& !$metabase_registered_transactions_shutdown)
		{
			register_shutdown_function("MetabaseShutdownTransactions");
			$metabase_registered_transactions_shutdown=1;
		}
		return(1);
	}

	Function CaptureDebugOutput($capture)
	{
		$this->pass_debug_handle=$capture;
		$this->debug=($capture ? "MetabaseDefaultDebugOutput" : "");
	}

	Function SetError($scope,$message)
	{
		$this->last_error=$message;
		$this->Debug($scope." error: ".$message);
		if(strcmp($function=$this->error_handler,""))
		{
			$error=array(
				"Scope"=>$scope,
				"Message"=>$message
			);
			$function($this,$error);
		}
		return(0);
	}

	Function LoadExtension($scope,$extension,$included_constant,$include)
	{
		if(strlen($included_constant)==0
		|| !defined($included_constant))
		{
			$error=MetabaseLoadClass($include,$this->include_path,$extension);
			if(strlen($error))
				return($this->SetError($scope,$error));
			if(strlen($included_constant)
			&& !defined($included_constant))
				return($this->SetError($scope,"it was not possible to load ".$extension." extension file ".$include." using path \"".$this->include_path."\""));
		}
		return(1);
	}

	Function LoadManager($scope)
	{
		if(IsSet($this->manager))
			return(1);
		if(!$this->LoadExtension($scope,"database manager","METABASE_MANAGER_DATABASE_INCLUDED","manager_database.php"))
			return(0);
		if(strlen($this->manager_class_name))
		{
			if(strlen($this->manager_include)==0)
				return($this->SetError($scope,"it was not configured a valid database manager include file"));
			if(!$this->LoadExtension($scope,"database manager",$this->manager_included_constant,$this->manager_include))
				return(0);
			if(strlen($this->manager_sub_include)
			&& !$this->LoadExtension($scope,"database manager",$this->manager_sub_included_constant,$this->manager_sub_include))
				return(0);
			$class_name=$this->manager_class_name;
		}
		else
			$class_name="metabase_manager_database_class";
		$this->manager=new $class_name;
		return(1);
	}

	Function CreateDatabase($database)
	{
		if(!$this->LoadManager("Create database"))
			return(0);
		return($this->manager->CreateDatabase($this,$database));
	}

	Function DropDatabase($database)
	{
		if(!$this->LoadManager("Drop database"))
			return(0);
		return($this->manager->DropDatabase($this,$database));
	}

	Function CreateTable($name,&$fields)
	{
		if(!$this->LoadManager("Create table"))
			return(0);
		return($this->manager->CreateTable($this,$name,$fields));
	}

	Function CreateDetailedTable(&$table, $check)
	{
		if(!$this->LoadManager("Create detailed table"))
			return(0);
		return($this->manager->CreateDetailedTable($this,$table, $check));
	}

	Function DropTable($name)
	{
		if(!$this->LoadManager("Drop table"))
			return(0);
		return($this->manager->DropTable($this,$name));
	}

	Function DropDetailedTable(&$table, $check)
	{
		if(!$this->LoadManager("Create detailed table"))
			return(0);
		return($this->manager->DropDetailedTable($this,$table, $check));
	}

	Function AlterTable($name,&$changes,$check)
	{
		if(!$this->LoadManager("Alter table"))
			return(0);
		return($this->manager->AlterTable($this,$name,$changes,$check));
	}

	Function ListTables(&$tables)
	{
		if(!$this->LoadManager("List tables"))
			return(0);
		return($this->manager->ListTables($this,$tables));
	}

	Function ListTableFields($table,&$fields)
	{
		if(!$this->LoadManager("List table fields"))
			return(0);
		return($this->manager->ListTableFields($this,$table,$fields));
	}

	Function GetTableFieldDefinition($table,$field,&$definition)
	{
		if(!$this->LoadManager("Get table field definition"))
			return(0);
		return($this->manager->GetTableFieldDefinition($this,$table,$field,$definition));
	}

	Function ListTableKeys($table, $primary, &$keys)
	{
		if(!$this->LoadManager("List table keys"))
			return(0);
		return($this->manager->ListTableKeys($this, $table, $primary, $keys));
	}

	Function GetTableKeyDefinition($table, $key, $primary, &$definition)
	{
		if(!$this->LoadManager("Get table key definition"))
			return(0);
		return($this->manager->GetTableKeyDefinition($this, $table, $key, $primary, $definition));
	}

	Function ListTableIndexes($table,&$indexes)
	{
		if(!$this->LoadManager("List table indexes"))
			return(0);
		return($this->manager->ListTableIndexes($this,$table,$indexes));
	}

	Function GetTableIndexDefinition($table,$index,&$definition)
	{
		if(!$this->LoadManager("Get table index definition"))
			return(0);
		return($this->manager->GetTableIndexDefinition($this,$table,$index,$definition));
	}

	Function ListSequences(&$sequences)
	{
		if(!$this->LoadManager("List sequences"))
			return(0);
		return($this->manager->ListSequences($this,$sequences));
	}

	Function GetSequenceDefinition($sequence,&$definition)
	{
		if(!$this->LoadManager("Get sequence definition"))
			return(0);
		return($this->manager->GetSequenceDefinition($this,$sequence,$definition));
	}

	Function CreateIndex($table,$name,&$definition)
	{
		if(!$this->LoadManager("Create index"))
			return(0);
		return($this->manager->CreateIndex($this,$table,$name,$definition));
	}

	Function DropIndex($table,$name)
	{
		if(!$this->LoadManager("Drop index"))
			return(0);
		return($this->manager->DropIndex($this,$table,$name));
	}

	Function CreateSequence($name,$start)
	{
		if(!$this->LoadManager("Create sequence"))
			return(0);
		return($this->manager->CreateSequence($this,$name,$start));
	}

	Function DropSequence($name)
	{
		if(!$this->LoadManager("Drop sequence"))
			return(0);
		return($this->manager->DropSequence($this,$name));
	}

	Function GetSequenceNextValue($name,&$value)
	{
		return($this->SetError("Get sequence next value","getting sequence next value is not supported"));
	}

	Function GetSequenceCurrentValue($name,&$value)
	{
		if(!$this->LoadManager("Get sequence current value"))
			return(0);
		return($this->manager->GetSequenceCurrentValue($this,$name,$value));
	}

	Function GetNextKey($table,&$key)
	{
		return($this->SetError("Get next key","getting next table key is not supported"));
	}

	Function GetInsertedKey($table,&$value)
	{
		return($this->SetError("Get inserted key","getting last inserted table key is not supported"));
	}

	Function Query($query)
	{
		$this->Debug("Query: $query");
		return($this->SetError("Query","database queries are not implemented"));
	}

	Function GetTypedFieldValue($type,$value, &$sql, $scope)
	{
		switch($type)
		{
			case "text":
				$sql=$this->GetTextFieldValue($value);
				break;
			case "boolean":
				$sql=$this->GetBooleanFieldValue($value);
				break;
			case "integer":
				$sql=strval($value);
				break;
			case "decimal":
				$sql=$this->GetDecimalFieldValue($value);
				break;
			case "float":
				$sql=$this->GetFloatFieldValue($value);
				break;
			case "date":
				$sql=$this->GetDateFieldValue($value);
				break;
			case "time":
				$sql=$this->GetTimeFieldValue($value);
				break;
			case "timestamp":
				$sql=$this->GetTimestampFieldValue($value);
				break;
			default:
				return($this->SetError($scope,$type." is not a supported field type"));
		}
		return(1);
	}
	
	Function Replace($table,&$fields)
	{
		if(!$this->supported["Replace"])
			return($this->SetError("Replace","replace query is not supported"));
		$count=count($fields);
		for($keys=0,$condition=$update=$insert=$values="",Reset($fields),$field=0;$field<$count;Next($fields),$field++)
		{
			$name=Key($fields);
			if($field>0)
			{
				$update.=",";
				$insert.=",";
				$values.=",";
			}
			$update.=$name;
			$insert.=$name;
			if(IsSet($fields[$name]["Null"])
			&& $fields[$name]["Null"])
				$value="NULL";
			else
			{
				if(!IsSet($fields[$name]["Value"]))
					return($this->SetError("Replace","it was not specified a value for the $name field"));
				if(!$this->GetTypedFieldValue(IsSet($fields[$name]["Type"]) ? $fields[$name]["Type"] : "text", $fields[$name]["Value"], $value, "Replace"))
					return(0);
			}
			$update.="=".$value;
			$values.=$value;
			if(IsSet($fields[$name]["Key"])
			&& $fields[$name]["Key"])
			{
				if($value=="NULL")
					return($this->SetError("Replace","key values may not be NULL"));
				$condition.=($keys ? " AND " : " WHERE ").$name."=".$value;
				$keys++;
			}
		}
		if($keys==0)
			return($this->SetError("Replace","it were not specified which fields are keys"));
		if(!($in_transaction=$this->in_transaction)
		&& !$this->AutoCommitTransactions(0))
			return(0);
		if(($success=$this->QueryField("SELECT COUNT(*) FROM $table$condition",$affected_rows,"integer")))
		{
			switch($affected_rows)
			{
				case 0:
					$success=$this->Query("INSERT INTO $table ($insert) VALUES($values)");
					$affected_rows=1;
					break;
				case 1:
					$success=$this->Query("UPDATE $table SET $update$condition");
					$affected_rows=$this->affected_rows*2;
					break;
				default:
					$success=$this->SetError("Replace","replace keys are not unique");
					break;
			}
		}
		if(!$in_transaction)
		{
			if($success)
			{
				if(($success=($this->CommitTransaction() && $this->AutoCommitTransactions(1)))
				&& IsSet($this->supported["AffectedRows"]))
					$this->affected_rows=$affected_rows;
			}
			else
			{
				$this->RollbackTransaction();
				$this->AutoCommitTransactions(1);
			}
		}
		return($success);
	}

	Function PrepareQuery($query)
	{
		$this->Debug("PrepareQuery: $query");
		$positions=array();
		for($position=0;$position<strlen($query) && GetType($question=strpos($query,"?",$position))=="integer";)
		{
			if(GetType($quote=strpos($query,"'",$position))=="integer"
			&& $quote<$question)
			{
				if(GetType($end_quote=strpos($query,"'",$quote+1))!="integer")
					return($this->SetError("Prepare query","it was specified a query with an unterminated text string"));
				switch($this->escape_quotes)
				{
					case "":
					case "'":
						$position=$end_quote+1;
						break;
					default:
						if($end_quote==$quote+1)
							$position=$end_quote+1;
						else
						{
							if($query[$end_quote-1]==$this->escape_quotes)
								$position=$end_quote;
							else
								$position=$end_quote+1;
						}
						break;
				}
			}
			else
			{
				$positions[]=$question;
				$position=$question+1;
			}
		}
		$this->prepared_queries[]=array(
			"Query"=>$query,
			"Positions"=>$positions,
			"Values"=>array(),
			"Types"=>array()
		);
		$prepared_query=count($this->prepared_queries);
		if($this->selected_row_limit>0)
		{
			$this->prepared_queries[$prepared_query-1]["First"]=$this->first_selected_row;
			$this->prepared_queries[$prepared_query-1]["Limit"]=$this->selected_row_limit;
		}
		return($prepared_query);
	}

	Function ValidatePreparedQuery($prepared_query)
	{
		if($prepared_query<1
		|| $prepared_query>count($this->prepared_queries))
			return($this->SetError("Validate prepared query","invalid prepared query"));
		if(GetType($this->prepared_queries[$prepared_query-1])!="array")
			return($this->SetError("Validate prepared query","prepared query was already freed"));
		return(1);
	}

	Function FreePreparedQuery($prepared_query)
	{
		if(!$this->ValidatePreparedQuery($prepared_query))
			return(0);
		$this->prepared_queries[$prepared_query-1]="";
		return(1);
	}

	Function ExecutePreparedQuery($prepared_query,$query)
	{
		return($this->Query($query));
	}

	Function ExecuteQuery($prepared_query)
	{
		if(!$this->ValidatePreparedQuery($prepared_query))
			return(0);
		$index=$prepared_query-1;
		for($this->clobs[$prepared_query]=$this->blobs[$prepared_query]=array(),$success=1,$query="",$last_position=$position=0;$position<count($this->prepared_queries[$index]["Positions"]);$position++)
		{
			if(!IsSet($this->prepared_queries[$index]["Values"][$position]))
				return($this->SetError("Execute query","it was not defined query argument ".($position+1)));
			$current_position=$this->prepared_queries[$index]["Positions"][$position];
			$query.=substr($this->prepared_queries[$index]["Query"],$last_position,$current_position-$last_position);
			$value=$this->prepared_queries[$index]["Values"][$position];
			if($this->prepared_queries[$index]["IsNULL"][$position])
				$query.=$value;
			else
			{
				switch($this->prepared_queries[$index]["Types"][$position])
				{
					case "clob":
						if(!($success=$this->GetCLOBFieldValue($prepared_query,$position+1,$value,$this->clobs[$prepared_query][$position+1])))
						{
							Unset($this->clobs[$prepared_query][$position+1]);
							break;
						}
						$query.=$this->clobs[$prepared_query][$position+1];
						break;
					case "blob":
						if(!($success=$this->GetBLOBFieldValue($prepared_query,$position+1,$value,$this->blobs[$prepared_query][$position+1])))
						{
							Unset($this->blobs[$prepared_query][$position+1]);
							break;
						}
						$query.=$this->blobs[$prepared_query][$position+1];
						break;
					case "key":
						if(!($success=$this->GetNextKey($value, $key)))
							break;
						$query.=$key;
						break;
					default:
						$query.=$value;
						break;
				}
			}
			$last_position=$current_position+1;
		}
		if($success)
		{
			$query.=substr($this->prepared_queries[$index]["Query"],$last_position);
			if($this->selected_row_limit>0)
			{
				$this->prepared_queries[$index]["First"]=$this->first_selected_row;
				$this->prepared_queries[$index]["Limit"]=$this->selected_row_limit;
			}
			if(IsSet($this->prepared_queries[$index]["Limit"])
			&& $this->prepared_queries[$index]["Limit"]>0)
			{
				$this->first_selected_row=$this->prepared_queries[$index]["First"];
				$this->selected_row_limit=$this->prepared_queries[$index]["Limit"];
			}
			else
				$this->first_selected_row=$this->selected_row_limit=0;
			$success=$this->ExecutePreparedQuery($prepared_query,$query);
		}
		for(Reset($this->clobs[$prepared_query]),$clob=0;$clob<count($this->clobs[$prepared_query]);$clob++,Next($this->clobs[$prepared_query]))
			$this->FreeCLOBValue($prepared_query,Key($this->clobs[$prepared_query]),$this->clobs[$prepared_query][Key($this->clobs[$prepared_query])],$success);
		UnSet($this->clobs[$prepared_query]);
		for(Reset($this->blobs[$prepared_query]),$blob=0;$blob<count($this->blobs[$prepared_query]);$blob++,Next($this->blobs[$prepared_query]))
			$this->FreeBLOBValue($prepared_query,Key($this->blobs[$prepared_query]),$this->blobs[$prepared_query][Key($this->blobs[$prepared_query])],$success);
		UnSet($this->blobs[$prepared_query]);
		return($success);
	}

	Function QuerySet($prepared_query,$parameter,$type,$value,$is_null=0,$field="")
	{
		if(!$this->ValidatePreparedQuery($prepared_query))
			return(0);
		$index=$prepared_query-1;
		if($parameter<1
		|| $parameter>count($this->prepared_queries[$index]["Positions"]))
			return($this->SetError("Query set","it was not specified a valid argument number"));
		$this->prepared_queries[$index]["Values"][$parameter-1]=$value;
		$this->prepared_queries[$index]["Types"][$parameter-1]=$type;
		$this->prepared_queries[$index]["Fields"][$parameter-1]=$field;
		$this->prepared_queries[$index]["IsNULL"][$parameter-1]=$is_null;
		return(1);
	}

	Function QuerySetNull($prepared_query,$parameter,$type)
	{
		return($this->QuerySet($prepared_query,$parameter,$type,"NULL",1,""));
	}

	Function QuerySetText($prepared_query,$parameter,$value)
	{
		return($this->QuerySet($prepared_query,$parameter,"text",$this->GetTextFieldValue($value)));
	}

	Function QuerySetCLOB($prepared_query,$parameter,$value,$field)
	{
		return($this->QuerySet($prepared_query,$parameter,"clob",$value,0,$field));
	}

	Function QuerySetBLOB($prepared_query,$parameter,$value,$field)
	{
		return($this->QuerySet($prepared_query,$parameter,"blob",$value,0,$field));
	}

	Function QuerySetInteger($prepared_query,$parameter,$value)
	{
		return($this->QuerySet($prepared_query,$parameter,"integer",$this->GetIntegerFieldValue($value)));
	}

	Function QuerySetBoolean($prepared_query,$parameter,$value)
	{
		return($this->QuerySet($prepared_query,$parameter,"boolean",$this->GetBooleanFieldValue($value)));
	}

	Function QuerySetDate($prepared_query,$parameter,$value)
	{
		return($this->QuerySet($prepared_query,$parameter,"date",$this->GetDateFieldValue($value)));
	}

	Function QuerySetTimestamp($prepared_query,$parameter,$value)
	{
		return($this->QuerySet($prepared_query,$parameter,"timestamp",$this->GetTimestampFieldValue($value)));
	}

	Function QuerySetTime($prepared_query,$parameter,$value)
	{
		return($this->QuerySet($prepared_query,$parameter,"time",$this->GetTimeFieldValue($value)));
	}

	Function QuerySetFloat($prepared_query,$parameter,$value)
	{
		return($this->QuerySet($prepared_query,$parameter,"float",$this->GetFloatFieldValue($value)));
	}

	Function QuerySetDecimal($prepared_query,$parameter,$value)
	{
		return($this->QuerySet($prepared_query,$parameter,"decimal",$this->GetDecimalFieldValue($value)));
	}

	Function QuerySetKey($prepared_query,$parameter,$table)
	{
		return($this->QuerySet($prepared_query,$parameter,"key",$table));
	}

	Function AffectedRows(&$affected_rows)
	{
		if($this->affected_rows==-1)
			return($this->SetError("Affected rows","there was no previous valid query to determine the number of affected rows"));
		$affected_rows=$this->affected_rows;
		return(1);
	}

	Function EndOfResult($result)
	{
		$this->SetError("End of result","end of result method not implemented");
		return(-1);
	}

	Function FetchResult($result,$row,$field)
	{
		$this->warning="fetch result method not implemented";
		return("");
	}

	Function FetchLOBResult($result,$row,$field)
	{
		$lob=count($this->lobs)+1;
		$this->lobs[$lob]=array(
			"Result"=>$result,
			"Row"=>$row,
			"Field"=>$field,
			"Position"=>0
		);
		$character_lob=array(
			"Database"=>$this->database,
			"Error"=>"",
			"Type"=>"resultlob",
			"ResultLOB"=>$lob
		);
		if(!MetabaseCreateLOB($character_lob,$clob))
			return($this->SetError("Fetch LOB result",$character_lob["Error"]));
		return($clob);
	}

	Function RetrieveLOB($lob)
	{
		if(!IsSet($this->lobs[$lob]))
			return($this->SetError("Fetch LOB result","it was not specified a valid lob"));
		if(!IsSet($this->lobs[$lob]["Value"]))
			$this->lobs[$lob]["Value"]=$this->FetchResult($this->lobs[$lob]["Result"],$this->lobs[$lob]["Row"],$this->lobs[$lob]["Field"]);
		return(1);
	}

	Function EndOfResultLOB($lob)
	{
		if(!$this->RetrieveLOB($lob))
			return(0);
		return($this->lobs[$lob]["Position"]>=strlen($this->lobs[$lob]["Value"]));
	}

	Function ReadResultLOB($lob,&$data,$length)
	{
		if(!$this->RetrieveLOB($lob))
			return(-1);
		$length=min($length,strlen($this->lobs[$lob]["Value"])-$this->lobs[$lob]["Position"]);
		$data=substr($this->lobs[$lob]["Value"],$this->lobs[$lob]["Position"],$length);
		$this->lobs[$lob]["Position"]+=$length;
		return($length);
	}

	Function DestroyResultLOB($lob)
	{
		if(IsSet($this->lobs[$lob]))
			$this->lobs[$lob]="";
	}

	Function FetchCLOBResult($result,$row,$field)
	{
		return($this->SetError("Fetch CLOB result","fetch clob result method is not implemented"));
	}

	Function FetchBLOBResult($result,$row,$field)
	{
		return($this->SetError("Fetch BLOB result","fetch blob result method is not implemented"));
	}

	Function ResultIsNull($result,$row,$field)
	{
		$value=$this->FetchResult($result,$row,$field);
		return(!IsSet($value));
	}

	Function BaseConvertResult(&$value,$type)
	{
		switch($type)
		{
			case METABASE_TYPE_TEXT:
				return(1);
			case METABASE_TYPE_INTEGER:
				$value=intval($value);
				return(1);
			case METABASE_TYPE_BOOLEAN:
				$value=(strcmp($value,"Y") ? 0 : 1);
				return(1);
			case METABASE_TYPE_DECIMAL:
				return(1);
			case METABASE_TYPE_FLOAT:
				$value=doubleval($value);
				return(1);
			case METABASE_TYPE_DATE:
			case METABASE_TYPE_TIME:
			case METABASE_TYPE_TIMESTAMP:
				return(1);
			case METABASE_TYPE_CLOB:
			case METABASE_TYPE_BLOB:
				$value="";
				return($this->SetError("BaseConvertResult","attempt to convert result value to an unsupported type $type"));
			default:
				$value="";
				return($this->SetError("BaseConvertResult","attempt to convert result value to an unknown type $type"));
		}
	}

	Function ConvertResult(&$value,$type)
	{
		return($this->BaseConvertResult($value,$type));
	}

	Function ConvertResultRow($result,&$row)
	{
		if(IsSet($this->result_types[$result]))
		{
			if(($columns=$this->NumberOfColumns($result))==-1)
				return(0);
			for($column=0;$column<$columns;$column++)
			{
				if(!IsSet($row[$column]))
					continue;
				switch($type=$this->result_types[$result][$column])
				{
					case METABASE_TYPE_TEXT:
						break;
					case METABASE_TYPE_INTEGER:
						$row[$column]=intval($row[$column]);
						break;
					default:
						if(!$this->ConvertResult($row[$column],$type))
							return(0);
				}
			}
		}
		return(1);
	}

	Function FetchDateResult($result,$row,$field)
	{
		$value=$this->FetchResult($result,$row,$field);
		$this->ConvertResult($value,METABASE_TYPE_DATE);
		return($value);
	}

	Function FetchTimestampResult($result,$row,$field)
	{
		$value=$this->FetchResult($result,$row,$field);
		$this->ConvertResult($value,METABASE_TYPE_TIMESTAMP);
		return($value);
	}

	Function FetchTimeResult($result,$row,$field)
	{
		$value=$this->FetchResult($result,$row,$field);
		$this->ConvertResult($value,METABASE_TYPE_TIME);
		return($value);
	}

	Function FetchBooleanResult($result,$row,$field)
	{
		$value=$this->FetchResult($result,$row,$field);
		$this->ConvertResult($value,METABASE_TYPE_BOOLEAN);
		return($value);
	}

	Function FetchFloatResult($result,$row,$field)
	{
		$value=$this->FetchResult($result,$row,$field);
		$this->ConvertResult($value,METABASE_TYPE_FLOAT);
		return($value);
	}

	Function FetchDecimalResult($result,$row,$field)
	{
		$value=$this->FetchResult($result,$row,$field);
		$this->ConvertResult($value,METABASE_TYPE_DECIMAL);
		return($value);
	}

	Function NumberOfRows($result)
	{
		$this->warning="number of rows method not implemented";
		return(0);
	}

	Function FreeResult($result)
	{
		$this->warning="free result method not implemented";
		return(0);
	}

	Function Error()
	{
		return($this->last_error);
	}

	Function SetErrorHandler($function)
	{
		$last_function=$this->error_handler;
		$this->error_handler=$function;
		return($last_function);
	}

	Function GetIntegerFieldTypeDeclaration($name,&$field)
	{
		if(IsSet($field["unsigned"]))
			$this->warning="unsigned integer field \"$name\" is being declared as signed integer";
		return("$name INT".(IsSet($field["default"]) ? " DEFAULT ".$field["default"] : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetTextFieldTypeDeclaration($name,&$field)
	{
		return((IsSet($field["length"]) ? "$name CHAR (".$field["length"].")" : "$name TEXT").(IsSet($field["default"]) ? " DEFAULT ".$this->GetTextFieldValue($field["default"]) : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetCLOBFieldTypeDeclaration($name,&$field)
	{
		return((IsSet($field["length"]) ? "$name CHAR (".$field["length"].")" : "$name TEXT").(IsSet($field["default"]) ? " DEFAULT ".$this->GetTextFieldValue($field["default"]) : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetBLOBFieldTypeDeclaration($name,&$field)
	{
		return((IsSet($field["length"]) ? "$name CHAR (".$field["length"].")" : "$name TEXT").(IsSet($field["default"]) ? " DEFAULT ".$this->GetTextFieldValue($field["default"]) : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetBooleanFieldTypeDeclaration($name,&$field)
	{
		return("$name CHAR (1)".(IsSet($field["default"]) ? " DEFAULT ".$this->GetBooleanFieldValue($field["default"]) : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetDateFieldTypeDeclaration($name,&$field)
	{
		return("$name CHAR (".strlen("YYYY-MM-DD").")".(IsSet($field["default"]) ? " DEFAULT ".$this->GetDateFieldValue($field["default"]) : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetTimestampFieldTypeDeclaration($name,&$field)
	{
		return("$name CHAR (".strlen("YYYY-MM-DD HH:MM:SS").")".(IsSet($field["default"]) ? " DEFAULT ".$this->GetTimestampFieldValue($field["default"]) : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetTimeFieldTypeDeclaration($name,&$field)
	{
		return("$name CHAR (".strlen("HH:MM:SS").")".(IsSet($field["default"]) ? " DEFAULT ".$this->GetTimeFieldValue($field["default"]) : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetFloatFieldTypeDeclaration($name,&$field)
	{
		return("$name TEXT ".(IsSet($field["default"]) ? " DEFAULT ".$this->GetFloatFieldValue($field["default"]) : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetDecimalFieldTypeDeclaration($name,&$field)
	{
		return("$name TEXT ".(IsSet($field["default"]) ? " DEFAULT ".$this->GetDecimalFieldValue($field["default"]) : "").(IsSet($field["notnull"]) ? " NOT NULL" : ""));
	}

	Function GetIntegerFieldValue($value)
	{
		return(!strcmp($value,"NULL") ? "NULL" : "$value");
	}

	Function GetTextFieldValue($value)
	{
		$this->EscapeText($value);
		return("'$value'");
	}
	
	Function GetCLOBFieldValue($prepared_query,$parameter,$clob,&$value)
	{
		return($this->SetError("Get CLOB field value","prepared queries with values of type \"clob\" are not yet supported"));
	}

	Function FreeCLOBValue($prepared_query,$clob,&$value,$success)
	{
	}

	Function GetBLOBFieldValue($prepared_query,$parameter,$blob,&$value)
	{
		return($this->SetError("Get BLOB field value","prepared queries with values of type \"blob\" are not yet supported"));
	}

	Function FreeBLOBValue($prepared_query,$blob,&$value,$success)
	{
	}

	Function GetBooleanFieldValue($value)
	{
		return(!strcmp($value,"NULL") ? "NULL" : ($value ? "'Y'" : "'N'"));
	}

	Function GetDateFieldValue($value)
	{
		return(!strcmp($value,"NULL") ? "NULL" : "'$value'");
	}

	Function GetTimestampFieldValue($value)
	{
		return(!strcmp($value,"NULL") ? "NULL" : "'$value'");
	}

	Function GetTimeFieldValue($value)
	{
		return(!strcmp($value,"NULL") ? "NULL" : "'$value'");
	}

	Function GetFloatFieldValue($value)
	{
		return(!strcmp($value,"NULL") ? "NULL" : "'$value'");
	}

	Function GetDecimalFieldValue($value)
	{
		return(!strcmp($value,"NULL") ? "NULL" : "'$value'");
	}

	Function GetFieldValue($type,$value)
	{
		switch($type)
		{
			case "integer":
				return($this->GetIntegerFieldValue($value));
			case "text":
				return($this->GetTextFieldValue($value));
			case "boolean":
				return($this->GetBooleanFieldValue($value));
			case "date":
				return($this->GetDateFieldValue($value));
			case "timestamp":
				return($this->GetTimestampFieldValue($value));
			case "time":
				return($this->GetTimeFieldValue($value));
			case "float":
				return($this->GetFloatFieldValue($value));
			case "decimal":
				return($this->GetDecimalFieldValue($value));
		}
		return("");
	}

	Function Support($feature)
	{
		return(IsSet($this->supported[$feature]));
	}

	Function CheckNestedTransaction($update, $commit, &$allow)
	{
		if($update)
		{
			$allow = $this->allow_nested_transactions;
			if($commit)
			{
				$nested = ($this->in_transaction>1);
				if($nested)
				{
					if($allow)
					{
						$this->Debug("EndNestedTransaction: level ".$this->in_transaction);
						--$this->in_transaction;
					}
				}
				else
				{
					if($this->rollback_nested_transactions)
					{
						$this->SetError('Auto-commit transactions', 'attempted to auto-commit a previously rolled back nested transaction');
						return(1);
					}
				}
			}
			else
			{
				$nested = ($this->in_transaction>0);
				if($nested)
				{
					if($allow)
					{
						++$this->in_transaction;
						$this->Debug("StartNestedTransaction: level ".$this->in_transaction);
					}
					else
					{
						$this->SetError("StartNestedTransaction", "nested transactions are not allowed");
						return(1);
					}
				}
				else
					$this->commit_nested_transactions = $this->rollback_nested_transactions = 0;
			}
		}
		else
		{
			$allow = ($this->in_transaction>0);
			if($allow)
			{
				if($commit
				&& $this->rollback_nested_transactions)
				{
					$this->SetError('CommitNestedTransaction', 'attempted to commit a previously rolled back nested transaction');
					return(1);
				}
				elseif(!$commit
				&& $this->commit_nested_transactions)
				{
					$this->SetError('RollbackNestedTransaction', 'attempted to rollback a previously committed nested transaction');
					return(1);
				}
				$nested = ($this->in_transaction!=1);
				if($nested)
				{
					if($commit)
						$this->commit_nested_transactions = 1;
					else
						$this->rollback_nested_transactions = 1;
					$this->Debug(($commit ? "Commit" : "Rollback")."NestedTransaction: level ".$this->in_transaction);
				}
				else
					$this->commit_nested_transactions = $this->rollback_nested_transactions = 0;
			}
			else
			{
				$nested = 1;
				$this->SetError($commit ? "Commit transaction" : "Rollback transaction", "transaction was not started");
			}
		}
		return($nested);
	}

	Function AutoCommitTransactions($auto_commit)
	{
		$this->Debug("AutoCommit: ".($auto_commit ? "On" : "Off"));
		return($this->SetError("Auto-commit transactions","transactions are not supported"));
	}

	Function CommitTransaction()
	{
 		$this->Debug("Commit Transaction");
		return($this->SetError("Commit transaction","commiting transactions are not supported"));
	}

	Function RollbackTransaction()
	{
 		$this->Debug("Rollback Transaction");
		return($this->SetError("Rollback transaction","rolling back transactions are not supported"));
	}

	Function Setup()
	{
		return("");
	}

	Function SetSelectedRowRange($first,$limit)
	{
		if(!IsSet($this->supported["SelectRowRanges"]))
			return($this->SetError("Set selected row range","selecting row ranges is not supported by this driver"));
		if(GetType($first)!="integer"
		|| $first<0)
			return($this->SetError("Set selected row range","it was not specified a valid first selected range row"));
		if(GetType($limit)!="integer"
		|| $limit<1)
			return($this->SetError("Set selected row range","it was not specified a valid selected range row limit"));
		$this->first_selected_row=$first;
		$this->selected_row_limit=$limit;
		return(1);
	}

	Function GetColumnNames($result,&$columns)
	{
		$columns=array();
		return($this->SetError("Get column names","obtaining result column names is not implemented"));
	}

	Function NumberOfColumns($result)
	{
		$this->SetError("Number of columns","obtaining the number of result columns is not implemented");
		return(-1);
	}

	Function SetResultTypes($result,&$types)
	{
		if(IsSet($this->result_types[$result]))
			return($this->SetError("Set result types","attempted to redefine the types of the columns of a result set"));
		if(($columns=$this->NumberOfColumns($result))==-1)
			return(0);
		if($columns<count($types))
			return($this->SetError("Set result types","it were specified more result types (".count($types).") than result columns ($columns)"));
		$valid_types=array(
			"text" =>      METABASE_TYPE_TEXT,
			"boolean" =>   METABASE_TYPE_BOOLEAN,
			"integer" =>   METABASE_TYPE_INTEGER,
			"decimal" =>   METABASE_TYPE_DECIMAL,
			"float" =>     METABASE_TYPE_FLOAT,
			"date" =>      METABASE_TYPE_DATE,
			"time" =>      METABASE_TYPE_TIME,
			"timestamp" => METABASE_TYPE_TIMESTAMP,
			"clob" =>      METABASE_TYPE_CLOB,
			"blob" =>      METABASE_TYPE_BLOB
		);
		for($column=0;$column<count($types);$column++)
		{
			if(!IsSet($valid_types[$types[$column]]))
				return($this->SetError("Set result types",$types[$column]." is not a supported column type"));
			$this->result_types[$result][$column]=$valid_types[$types[$column]];
		}
		for(;$column<$columns;$column++)
			$this->result_types[$result][$column]=METABASE_TYPE_TEXT;
		return(1);
	}

	Function FetchResultField($result,&$value)
	{
		if(!$result)
			return($this->SetError("Fetch field","it was not specified a valid result set"));
		if($this->EndOfResult($result))
			$success=$this->SetError("Fetch field","result set is empty");
		else
		{
			if($this->ResultIsNull($result,0,0))
				Unset($value);
			else
				$value=$this->FetchResult($result,0,0);
			$success=1;
		}
		if($success
		&& IsSet($this->result_types[$result])
		&& IsSet($value))
		{
			switch($type=$this->result_types[$result][0])
			{
				case METABASE_TYPE_TEXT:
					break;
				case METABASE_TYPE_INTEGER:
					$value=intval($value);
					break;
				default:
					$success=$this->ConvertResult($value,$type);
					break;
			}
		}
		$this->FreeResult($result);
		return($success);
	}

	Function BaseFetchResultArray($result,&$array,$row)
	{
		if(($columns=$this->NumberOfColumns($result))==-1)
			return(0);
		for($array=array(),$column=0;$column<$columns;$column++)
		{
			if(!$this->ResultIsNull($result,$row,$column))
				$array[$column]=$this->FetchResult($result,$row,$column);
		}
		return($this->ConvertResultRow($result,$array));
	}

	Function FetchResultArray($result,&$array,$row)
	{
		return($this->BaseFetchResultArray($result,$array,$row));
	}

	Function FetchResultRow($result,&$row)
	{
		if(!$result)
			return($this->SetError("Fetch field","it was not specified a valid result set"));
		if($this->EndOfResult($result))
			$success=$this->SetError("Fetch field","result set is empty");
		else
			$success=$this->FetchResultArray($result,$row,0);
		$this->FreeResult($result);
		return($success);
	}

	Function FetchResultColumn($result,&$column)
	{
		if(!$result)
			return($this->SetError("Fetch field","it was not specified a valid result set"));
		for($success=1,$column=array(),$row=0;!$this->EndOfResult($result);$row++)
		{
			if($this->ResultIsNull($result,0,0))
				continue;
			$column[$row]=$this->FetchResult($result,$row,0);
			if(IsSet($this->result_types[$result]))
			{
				switch($type=$this->result_types[$result][0])
				{
					case METABASE_TYPE_TEXT:
						break;
					case METABASE_TYPE_INTEGER:
						$column[$row]=intval($column[$row]);
						break;
					default:
						if(!($success=$this->ConvertResult($column[$row],$type)))
							break 2;
						break;
				}
			}
		}
		$this->FreeResult($result);
		return($success);
	}

	Function FetchResultAll($result,&$all)
	{
		if(!$result)
			return($this->SetError("Fetch field","it was not specified a valid result set"));
		for($success=1,$all=array(),$row=0;!$this->EndOfResult($result);$row++)
		{
			if(!($success=$this->FetchResultArray($result,$all[$row],$row)))
				break;
		}
		$this->FreeResult($result);
		return($success);
	}

	Function QueryField($query,&$field,$type="text")
	{
		if(!($result=$this->Query($query)))
			return(0);
		if(strcmp($type,"text"))
		{
			$types=array($type);
			if(!($success=$this->SetResultTypes($result,$types)))
			{
				$this->FreeResult($result);
				return(0);
			}
		}
		return($this->FetchResultField($result,$field));
	}

	Function QueryRow($query,&$row,$types="")
	{
		if(!($result=$this->Query($query)))
			return(0);
		if(GetType($types)=="array")
		{
			if(!($success=$this->SetResultTypes($result,$types)))
			{
				$this->FreeResult($result);
				return(0);
			}
		}
		return($this->FetchResultRow($result,$row));
	}

	Function QueryColumn($query,&$column,$type="text")
	{
		if(!($result=$this->Query($query)))
			return(0);
		if(strcmp($type,"text"))
		{
			$types=array($type);
			if(!($success=$this->SetResultTypes($result,$types)))
			{
				$this->FreeResult($result);
				return(0);
			}
		}
		return($this->FetchResultColumn($result,$column));
	}

	Function QueryAll($query,&$all,$types="")
	{
		if(!($result=$this->Query($query)))
			return(0);
		if(GetType($types)=="array")
		{
			if(!($success=$this->SetResultTypes($result,$types)))
			{
				$this->FreeResult($result);
				return(0);
			}
		}
		return($this->FetchResultAll($result,$all));
	}

	Function BeginsWith($value)
	{
		$this->EscapeText($value);
		$match="LIKE '".$this->EscapePatternText($value)."%'";
		if(strlen($this->escape_pattern))
		{
			$text=$this->escape_pattern;
			$this->EscapeText($text).
			$match.=" ESCAPE '".$text."'";
		}
		return($match);
	}

	Function Contains($value)
	{
		$this->EscapeText($value);
		$match="LIKE '%".$this->EscapePatternText($value)."%'";
		if(strlen($this->escape_pattern))
		{
			$text=$this->escape_pattern;
			$this->EscapeText($text).
			$match.=" ESCAPE '".$text."'";
		}
		return($match);
	}

	Function EndsWith($value)
	{
		$this->EscapeText($value);
		$match="LIKE '%".$this->EscapePatternText($value)."'";
		if(strlen($this->escape_pattern))
		{
			$text=$this->escape_pattern;
			$this->EscapeText($text).
			$match.=" ESCAPE '".$text."'";
		}
		return($match);
	}

	Function MatchPattern($pattern)
	{
		for($c=count($pattern), $match="LIKE '", $p=0; $p<$c; $p++)
		{
			$value=$pattern[$p];
			if(strlen($value))
			{
				$this->EscapeText($value);
				$match.=$this->EscapePatternText($value);
			}
			$p++;
			if($p<$c)
				$match.=$pattern[$p];
		}
		$match.="'";
		if(strlen($this->escape_pattern))
		{
			$text=$this->escape_pattern;
			$this->EscapeText($text).
			$match.=" ESCAPE '".$text."'";
		}
		return($match);
	}
};


/*
 * metabase_manager.php
 *
 * @(#) $Header: /home/mlemos/cvsroot/metabase/metabase_manager.php,v 1.90 2007/05/04 06:50:40 mlemos Exp $
 *
 */

class metabase_manager_class
{
	var $fail_on_invalid_names=1;
	var $error="";
	var $warnings=array();
	var $database=0;
	var $database_definition=array(
		"name"=>"",
		"create"=>0,
		"TABLES"=>array()
	);

	Function SetupDatabase(&$arguments)
	{
		if(IsSet($arguments["Connection"])
		&& strlen($error=MetabaseParseConnectionArguments($arguments["Connection"],$arguments)))
			return($error);
		if(IsSet($arguments["Debug"]))
			$this->debug=$arguments["Debug"];
		if(strlen($error=MetabaseSetupDatabase($arguments,$this->database)))
			return($error);
		if(!IsSet($arguments["Debug"]))
			MetabaseCaptureDebugOutput($this->database,1);
		return("");
	}

	Function CloseSetup()
	{
		if($this->database!=0)
		{
			MetabaseCloseSetup($this->database);
			$this->database=0;
		}
	}

	Function GetField(&$field,$field_name,$declaration,&$query)
	{
		if(!strcmp($field_name,""))
			return("it was not specified a valid field name (\"$field_name\")");
		switch($field["type"])
		{
			case "integer":
				if($declaration)
					$query=MetabaseGetIntegerFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			case "text":
				if($declaration)
					$query=MetabaseGetTextFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			case "clob":
				if($declaration)
					$query=MetabaseGetCLOBFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			case "blob":
				if($declaration)
					$query=MetabaseGetBLOBFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			case "boolean":
				if($declaration)
					$query=MetabaseGetBooleanFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			case "date":
				if($declaration)
					$query=MetabaseGetDateFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			case "timestamp":
				if($declaration)
					$query=MetabaseGetTimestampFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			case "time":
				if($declaration)
					$query=MetabaseGetTimeFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			case "float":
				if($declaration)
					$query=MetabaseGetFloatFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			case "decimal":
				if($declaration)
					$query=MetabaseGetDecimalFieldTypeDeclaration($this->database,$field_name,$field);
				else
					$query=$field_name;
				break;
			default:
				return("type \"".$field["type"]."\" is not yet supported");
		}
		return("");
	}

	Function GetFieldList($fields,$declaration,&$query_fields)
	{
		for($query_fields="",Reset($fields),$field_number=0;$field_number<count($fields);$field_number++,Next($fields))
		{
			if($field_number>0)
				$query_fields.=",";
			$field_name=Key($fields);
			if(strcmp($error=$this->GetField($fields[$field_name],$field_name,$declaration,$query),""))
				return($error);
			$query_fields.=$query;
		}
		return("");
	}

	Function GetFields($table,&$fields)
	{
		return($this->GetFieldList($this->database_definition["TABLES"][$table]["FIELDS"],0,$fields));
	}

	Function CreateTable($table_name,$table,$check)
	{
		if(!$check)
			MetabaseDebug($this->database,"Create table: ".$table_name);
		if(!MetabaseCreateDetailedTable($this->database,$table,$check))
			return(MetabaseError($this->database));
		$success=1;
		$error="";
		if(!$check
		&& IsSet($table["initialization"]))
		{
			$instructions=$table["initialization"];
			for(Reset($instructions),$instruction=0;$success && $instruction<count($instructions);$instruction++,Next($instructions))
			{
				switch($instructions[$instruction]["type"])
				{
					case "insert":
						$fields=$instructions[$instruction]["FIELDS"];
						for($query_fields=$query_values="",Reset($fields),$field_number=0;$field_number<count($fields);$field_number++,Next($fields))
						{
							if($field_number>0)
							{
								$query_fields.=",";
								$query_values.=",";
							}
							$field_name=Key($fields);
							$field=$table["FIELDS"][$field_name];
							if(strcmp($error=$this->GetField($field,$field_name,0,$query),""))
								return($error);
							$query_fields.=$query;
							$query_values.="?";
						}
						if(($success=($prepared_query=MetabasePrepareQuery($this->database,"INSERT INTO $table_name ($query_fields) VALUES ($query_values)"))))
						{
							for($lobs=array(),Reset($fields),$field_number=0;$field_number<count($fields);$field_number++,Next($fields))
							{
								$field_name=Key($fields);
								$field=$table["FIELDS"][$field_name];
								if(strcmp($error=$this->GetField($field,$field_name,0,$query),""))
									return($error);
								switch($field["type"])
								{
									case "integer":
										$success=MetabaseQuerySetInteger($this->database,$prepared_query,$field_number+1,intval($fields[$field_name]));
										break;
									case "text":
										$success=MetabaseQuerySetText($this->database,$prepared_query,$field_number+1,$fields[$field_name]);
										break;
									case "clob":
										$lob_definition=array(
											"Database"=>$this->database,
											"Error"=>"",
											"Data"=>$fields[$field_name]
										);
										$lob=count($lobs);
										if(!($success=MetabaseCreateLOB($lob_definition,$lobs[$lob])))
										{
											$error=$lob_definition["Error"];
											break;
										}
										$success=MetabaseQuerySetCLOB($this->database,$prepared_query,$field_number+1,$lobs[$lob],$field_name);
										break;
									case "blob":
										$lob_definition=array(
											"Database"=>$this->database,
											"Error"=>"",
											"Data"=>$fields[$field_name]
										);
										$lob=count($lobs);
										if(!($success=MetabaseCreateLOB($lob_definition,$lobs[$lob])))
										{
											$error=$lob_definition["Error"];
											break;
										}
										$success=MetabaseQuerySetBLOB($this->database,$prepared_query,$field_number+1,$lobs[$lob],$field_name);
										break;
									case "boolean":
										$success=MetabaseQuerySetBoolean($this->database,$prepared_query,$field_number+1,intval($fields[$field_name]));
										break;
									case "date":
										$success=MetabaseQuerySetDate($this->database,$prepared_query,$field_number+1,$fields[$field_name]);
										break;
									case "timestamp":
										$success=MetabaseQuerySetTimestamp($this->database,$prepared_query,$field_number+1,$fields[$field_name]);
										break;
									case "time":
										$success=MetabaseQuerySetTime($this->database,$prepared_query,$field_number+1,$fields[$field_name]);
										break;
									case "float":
										$success=MetabaseQuerySetFloat($this->database,$prepared_query,$field_number+1,doubleval($fields[$field_name]));
										break;
									case "decimal":
										$success=MetabaseQuerySetDecimal($this->database,$prepared_query,$field_number+1,$fields[$field_name]);
										break;
									default:
										$error="type \"".$field["type"]."\" is not yet supported";
										$success=0;
										break;
								}
								if(!$success
								&& $error=="")
								{
									$error=MetabaseError($this->database);
									break;
								}
							}
							if($success
							&& !($success=MetabaseExecuteQuery($this->database,$prepared_query)))
								$error=MetabaseError($this->database);
							for($lob=0;$lob<count($lobs);$lob++)
								MetabaseDestroyLOB($lobs[$lob]);
							MetabaseFreePreparedQuery($this->database,$prepared_query);
						}
						else
							$error=MetabaseError($this->database);
						break;
				}
			}
		}
		if($success
		&& IsSet($table["INDEXES"]))
		{
			if(!MetabaseSupport($this->database,"Indexes"))
				return("indexes are not supported");
			if(!$check)
			{
				$indexes=$table["INDEXES"];
				for($index=0,Reset($indexes);$index<count($indexes);Next($indexes),$index++)
				{
					if(!MetabaseCreateIndex($this->database,$table_name,Key($indexes),$indexes[Key($indexes)]))
					{
						$error=MetabaseError($this->database);
						$success=0;
						break;
					}
				}
			}
		}
		if(!$success
		&& !$check)
		{
			if(strlen($drop_error=$this->DropTable($table,0)))
				$error="could not initialize the table \"".$table_name."\" (".$error.") and then could not drop the table (".$drop_error.")";
		}
		return($error);
	}

	Function DropTable($table,$check)
	{
		return(MetabaseDropDetailedTable($this->database,$table,$check) ? "" : MetabaseError($this->database));
	}

	Function CreateSequence($sequence_name,$sequence,$created_on_table)
	{
		if(!MetabaseSupport($this->database,"Sequences"))
			return("sequences are not supported");
		MetabaseDebug($this->database,"Create sequence: ".$sequence_name);
		if(!IsSet($sequence_name)
		|| !strcmp($sequence_name,""))
			return("it was not specified a valid sequence name");
		$start=$sequence["start"];
		if(IsSet($sequence["on"])
		&& !$created_on_table)
		{
			$table=$sequence["on"]["table"];
			$field=$sequence["on"]["field"];
			if(MetabaseSupport($this->database,"SummaryFunctions"))
				$field="MAX($field)";
			if(!($result=MetabaseQuery($this->database,"SELECT $field FROM $table")))
				return(MetabaseError($this->database));
			if(($rows=MetabaseNumberOfRows($this->database,$result)))
			{
				for($row=0;$row<$rows;$row++)
				{
					if(!MetabaseResultIsNull($this->database,$result,$row,0)
					&& ($value=MetabaseFetchResult($this->database,$result,$row,0)+1)>$start)
						$start=$value;
				}
			}
			MetabaseFreeResult($this->database,$result);
		}
		if(!MetabaseCreateSequence($this->database,$sequence_name,$start))
			return(MetabaseError($this->database));
		return("");
	}

	Function DropSequence($sequence_name)
	{
		return(MetabaseDropSequence($this->database,$sequence_name) ? "" : MetabaseError($this->database));
	}

	Function CreateDatabase()
	{
		if(!IsSet($this->database_definition["name"])
		|| !strcmp($this->database_definition["name"],""))
			return("it was not specified a valid database name");
		for(Reset($this->database_definition["TABLES"]),$table=0;$table<count($this->database_definition["TABLES"]);Next($this->database_definition["TABLES"]),$table++)
		{
			$table_name=Key($this->database_definition["TABLES"]);
			if(strcmp($error=$this->CreateTable($table_name,$this->database_definition["TABLES"][$table_name],1),""))
				return("database driver is not able to perform the database instalation: ".$error);
		}
		if(IsSet($this->database_definition["SEQUENCES"])
		&& !MetabaseSupport($this->database,"Sequences"))
			return("database driver is not able to perform the database instalation: sequences are not supported");
		$create=(IsSet($this->database_definition["create"]) && $this->database_definition["create"]);
		if($create)
		{
			MetabaseDebug($this->database,"Create database: ".$this->database_definition["name"]);
			if(!MetabaseCreateDatabase($this->database,$this->database_definition["name"]))
			{
				$error=MetabaseError($this->database);
				MetabaseDebug($this->database,"Create database error: ".$error);
				return($error);
			}
		}
		$previous_database_name=MetabaseSetDatabase($this->database,$this->database_definition["name"]);
		if(($support_transactions=MetabaseSupport($this->database,"Transactions"))
		&& !MetabaseAutoCommitTransactions($this->database,0))
			return(MetabaseError($this->database));
		$created_objects=0;
		for($error="",Reset($this->database_definition["TABLES"]),$table=0;$table<count($this->database_definition["TABLES"]);Next($this->database_definition["TABLES"]),$table++)
		{
			$table_name=Key($this->database_definition["TABLES"]);
			if(strcmp($error=$this->CreateTable($table_name,$this->database_definition["TABLES"][$table_name],0),""))
				break;
			$created_objects++;
		}
		if(!strcmp($error,"")
		&& IsSet($this->database_definition["SEQUENCES"]))
		{
			for($error="",Reset($this->database_definition["SEQUENCES"]),$sequence=0;$sequence<count($this->database_definition["SEQUENCES"]);Next($this->database_definition["SEQUENCES"]),$sequence++)
			{
				$sequence_name=Key($this->database_definition["SEQUENCES"]);
				if(strcmp($error=$this->CreateSequence($sequence_name,$this->database_definition["SEQUENCES"][$sequence_name],1),""))
					break;
				$created_objects++;
			}
		}
		if(strcmp($error,""))
		{
			if($created_objects)
			{
				if($support_transactions)
				{
					if(!MetabaseRollbackTransaction($this->database))
						$error="Could not rollback the partially created database alterations: Rollback error: ".MetabaseError($this->database)." Creation error: $error";
				}
				else
					$error="the database was only partially created: $error";
			}
		}
		else
		{
			if($support_transactions)
			{
				if(!MetabaseAutoCommitTransactions($this->database,1))
					$error="Could not end transaction after successfully created the database: ".MetabaseError($this->database);
			}
		}
		MetabaseSetDatabase($this->database,$previous_database_name);
		if(strcmp($error,"")
		&& $create
		&& !MetabaseDropDatabase($this->database,$this->database_definition["name"]))
			$error="Could not drop the created database after unsuccessful creation attempt: ".MetabaseError($this->database)." Creation error: ".$error;
		return($error);
	}

	Function AddDefinitionChange(&$changes,$definition,$item,$change)
	{
		if(!IsSet($changes[$definition][$item]))
			$changes[$definition][$item]=array();
		for($change_number=0,Reset($change);$change_number<count($change);Next($change),$change_number++)
		{
			$name=Key($change);
			if(!strcmp(GetType($change[$name]),"array"))
			{
				if(!IsSet($changes[$definition][$item][$name]))
					$changes[$definition][$item][$name]=array();
				$change_parts=$change[$name];
				for($change_part=0,Reset($change_parts);$change_part<count($change_parts);Next($change_parts),$change_part++)
					$changes[$definition][$item][$name][Key($change_parts)]=$change_parts[Key($change_parts)];
			} 
			else
				$changes[$definition][$item][Key($change)]=$change[Key($change)];
		}
	}

	Function CompareDefinitions(&$previous_definition,&$changes)
	{
		$changes=array();
		for($defined_tables=array(),Reset($this->database_definition["TABLES"]),$table=0;$table<count($this->database_definition["TABLES"]);Next($this->database_definition["TABLES"]),$table++)
		{
			$table_name=Key($this->database_definition["TABLES"]);
			$was_table_name=$this->database_definition["TABLES"][$table_name]["was"];
			if(IsSet($previous_definition["TABLES"][$table_name])
			&& IsSet($previous_definition["TABLES"][$table_name]["was"])
			&& !strcmp($previous_definition["TABLES"][$table_name]["was"],$was_table_name))
				$was_table_name=$table_name;
			if(IsSet($previous_definition["TABLES"][$was_table_name]))
			{
				$information=array();
				if(strcmp($was_table_name,$table_name))
				{
					$this->AddDefinitionChange($changes,"TABLES",$was_table_name,array("name"=>$table_name));
					MetabaseDebug($this->database,"Renamed table '$was_table_name' to '$table_name'");
				}
				if(IsSet($defined_tables[$was_table_name]))
					return("the table '$was_table_name' was specified as base of more than of table of the database");
				$defined_tables[$was_table_name]=1;

				$fields=$this->database_definition["TABLES"][$table_name]["FIELDS"];
				$previous_fields=$previous_definition["TABLES"][$was_table_name]["FIELDS"];
				for($defined_fields=array(),Reset($fields),$field=0;$field<count($fields);Next($fields),$field++)
				{
					$field_name=Key($fields);
					$was_field_name=$fields[$field_name]["was"];
					if(IsSet($previous_fields[$field_name])
					&& IsSet($previous_fields[$field_name]["was"])
					&& !strcmp($previous_fields[$field_name]["was"],$was_field_name))
						$was_field_name=$field_name;
					if(IsSet($previous_fields[$was_field_name]))
					{
						if(strcmp($was_field_name,$field_name))
						{
							$field_declaration=$fields[$field_name];
							if(strcmp($error=$this->GetField($field_declaration,$field_name,1,$query),""))
								return($error);
							$this->AddDefinitionChange($changes,"TABLES",$was_table_name,array("RenamedFields"=>array($was_field_name=>array(
								"name"=>$field_name,
								"Declaration"=>$query
							))));
							MetabaseDebug($this->database,"Renamed field '$was_field_name' to '$field_name' in table '$table_name'");
						}
						if(IsSet($defined_fields[$was_field_name]))
							return("the field '$was_field_name' was specified as base of more than one field of table '$table_name'");
						$defined_fields[$was_field_name]=1;
						$change=array();
						if(!strcmp($fields[$field_name]["type"],$previous_fields[$was_field_name]["type"]))
						{
							switch($fields[$field_name]["type"])
							{
								case "integer":
									$previous_unsigned=IsSet($previous_fields[$was_field_name]["unsigned"]);
									$unsigned=IsSet($fields[$field_name]["unsigned"]);
									if(strcmp($previous_unsigned,$unsigned))
									{
										$change["unsigned"]=$unsigned;
										MetabaseDebug($this->database,"Changed field '$field_name' type from '".($previous_unsigned ? "unsigned " : "").$previous_fields[$was_field_name]["type"]."' to '".($unsigned ? "unsigned " : "").$fields[$field_name]["type"]."' in table '$table_name'");
									}
									if(($previous_auto_increment=IsSet($previous_fields[$was_field_name]["autoincrement"])))
										$this->AddDefinitionChange($information,"TABLES",$was_table_name,array("AutoIncrement"=>array("was"=>$was_field_name)));
									if(($auto_increment=IsSet($fields[$field_name]["autoincrement"])))
										$this->AddDefinitionChange($information,"TABLES",$was_table_name,array("AutoIncrement"=>array("field"=>$field_name)));
									if(strcmp($previous_auto_increment,$auto_increment))
									{
										$change["autoincrement"]=$auto_increment;
										MetabaseDebug($this->database,"Changed field '$field_name' from '".($previous_auto_increment ? "" : "no ")."autoincrement' to '".($auto_increment ? "" : "no ")."autoincrement' in table '$table_name'");
									}
									break;
								case "text":
								case "clob":
								case "blob":
									$previous_length=(IsSet($previous_fields[$was_field_name]["length"]) ? $previous_fields[$was_field_name]["length"] : 0);
									$length=(IsSet($fields[$field_name]["length"]) ? $fields[$field_name]["length"] : 0);
									if(strcmp($previous_length,$length))
									{
										$change["length"]=$length;
										MetabaseDebug($this->database,"Changed field '$field_name' length from '".$previous_fields[$was_field_name]["type"].($previous_length==0 ? " no length" : "($previous_length)")."' to '".$fields[$field_name]["type"].($length==0 ? " no length" : "($length)")."' in table '$table_name'");
									}
									break;
								case "date":
								case "timestamp":
								case "time":
								case "boolean":
								case "float":
								case "decimal":
									break;
								default:
									return("type \"".$fields[$field_name]["type"]."\" is not yet supported");
							}

							$previous_notnull=(IsSet($previous_fields[$was_field_name]["notnull"]) ? 1 : 0);
							$notnull=(IsSet($fields[$field_name]["notnull"]) ? 1 : 0);
							if($previous_notnull!=$notnull)
							{
								$change["ChangedNotNull"]=1;
								if($notnull)
									$change["notnull"]=IsSet($fields[$field_name]["notnull"]);
								MetabaseDebug($this->database,"Changed field '$field_name' notnull from $previous_notnull to $notnull in table '$table_name'");
							}

							$previous_default=IsSet($previous_fields[$was_field_name]["default"]);
							$default=IsSet($fields[$field_name]["default"]);
							if(strcmp($previous_default,$default))
							{
								$change["ChangedDefault"]=1;
								if($default)
									$change["default"]=$fields[$field_name]["default"];
								MetabaseDebug($this->database,"Changed field '$field_name' default from ".($previous_default ? "'".$previous_fields[$was_field_name]["default"]."'" : "NULL")." TO ".($default ? "'".$fields[$field_name]["default"]."'" : "NULL")." IN TABLE '$table_name'");
							}
							else
							{
								if($default
								&& strcmp($previous_fields[$was_field_name]["default"],$fields[$field_name]["default"]))
								{
									$change["ChangedDefault"]=1;
									$change["default"]=$fields[$field_name]["default"];
									MetabaseDebug($this->database,"Changed field '$field_name' default from '".$previous_fields[$was_field_name]["default"]."' to '".$fields[$field_name]["default"]."' in table '$table_name'");
								}
							}
						}
						else
						{
							$change["type"]=$fields[$field_name]["type"];
							MetabaseDebug($this->database,"Changed field '$field_name' type from '".$previous_fields[$was_field_name]["type"]."' to '".$fields[$field_name]["type"]."' in table '$table_name'");
						}
						if(count($change))
						{
							$field_declaration=$fields[$field_name];
							if(strcmp($error=$this->GetField($field_declaration,$field_name,1,$query),""))
								return($error);
							$change["Declaration"]=$query;
							$change["Definition"]=$field_declaration;
							$this->AddDefinitionChange($changes,"TABLES",$was_table_name,array("ChangedFields"=>array($field_name=>$change)));
						}
					}
					else
					{
						if(strcmp($field_name,$was_field_name))
							return("it was specified a previous field name ('$was_field_name') for field '$field_name' of table '$table_name' that does not exist");
						$field_declaration=$fields[$field_name];
						if(strcmp($error=$this->GetField($field_declaration,$field_name,1,$query),""))
							return($error);
						$field_declaration["Declaration"]=$query;
						$this->AddDefinitionChange($changes,"TABLES",$was_table_name,array("AddedFields"=>array($field_name=>$field_declaration)));
						MetabaseDebug($this->database,"Added field '$field_name' to table '$table_name'");
					}
				}
				for(Reset($previous_fields),$field=0;$field<count($previous_fields);Next($previous_fields),$field++)
				{
					$field_name=Key($previous_fields);
					if(!IsSet($defined_fields[$field_name]))
					{
						$this->AddDefinitionChange($changes,"TABLES",$was_table_name,array("RemovedFields"=>array($field_name=>array())));
						MetabaseDebug($this->database,"Removed field '$field_name' from table '$table_name'");
					}
				}
				$has_primary_key=IsSet($this->database_definition["TABLES"][$table_name]["PRIMARYKEY"]);
				if($has_primary_key)
					$this->AddDefinitionChange($information,"TABLES",$was_table_name,array("PrimaryKey"=>array("key"=>$this->database_definition["TABLES"][$table_name]["PRIMARYKEY"])));
				$had_primary_key=IsSet($previous_definition["TABLES"][$was_table_name]["PRIMARYKEY"]);
				if($had_primary_key)
					$this->AddDefinitionChange($information,"TABLES",$was_table_name,array("PrimaryKey"=>array("was"=>$previous_definition["TABLES"][$was_table_name]["PRIMARYKEY"])));
				if($has_primary_key)
				{
					if($had_primary_key)
					{
						$changed=0;
						$fields=$this->database_definition["TABLES"][$table_name]["PRIMARYKEY"]["FIELDS"];
						if(count($fields)!=count($previous_definition["TABLES"][$was_table_name]["PRIMARYKEY"]["FIELDS"]))
							$changed=1;
						else
						{
							for($field=0, Reset($fields); $field<count($fields); Next($fields), $field++)
							{
								$name=Key($fields);
								if(!IsSet($previous_definition["TABLES"][$was_table_name]["PRIMARYKEY"]["FIELDS"][$name])
								|| count($fields[$name])!=count($previous_definition["TABLES"][$was_table_name]["PRIMARYKEY"]["FIELDS"][$name])
								|| IsSet($fields[$name]["sorting"])!=IsSet($previous_definition["TABLES"][$was_table_name]["PRIMARYKEY"]["FIELDS"][$name]["sorting"])
								|| (IsSet($fields[$name]["sorting"])
								&& strcmp($fields[$name]["sorting"],$previous_definition["TABLES"][$was_table_name]["PRIMARYKEY"]["FIELDS"][$name]["sorting"])))
								{
									$changed=1;
									break;
								}
							}
						}
						if($changed)
						{
							$this->AddDefinitionChange($changes,"TABLES",$was_table_name,array("ChangedPrimaryKey"=>$this->database_definition["TABLES"][$table_name]["PRIMARYKEY"]));
							MetabaseDebug($this->database,"Changed primary key of table '$table_name'");
						}
					}
					else
					{
						$this->AddDefinitionChange($changes,"TABLES",$was_table_name,array("AddedPrimaryKey"=>$this->database_definition["TABLES"][$table_name]["PRIMARYKEY"]));
						MetabaseDebug($this->database,"Added primary key to table '$table_name'");
					}
				}
				elseif(IsSet($previous_definition["TABLES"][$was_table_name]["PRIMARYKEY"]))
				{
					$this->AddDefinitionChange($changes,"TABLES",$was_table_name,array("RemovedPrimaryKey"=>$previous_definition["TABLES"][$was_table_name]["PRIMARYKEY"]));
					MetabaseDebug($this->database,"Removed primary key from table '$table_name'");
				}
				if(IsSet($changes["TABLES"][$was_table_name]))
				{
					if(IsSet($information["TABLES"][$was_table_name]["AutoIncrement"]))
						$changes["TABLES"][$was_table_name]["AutoIncrement"]=$information["TABLES"][$was_table_name]["AutoIncrement"];
					if(IsSet($information["TABLES"][$was_table_name]["PrimaryKey"]))
						$changes["TABLES"][$was_table_name]["PrimaryKey"]=$information["TABLES"][$was_table_name]["PrimaryKey"];
				}
				$indexes=(IsSet($this->database_definition["TABLES"][$table_name]["INDEXES"]) ? $this->database_definition["TABLES"][$table_name]["INDEXES"] : array());
				$previous_indexes=(IsSet($previous_definition["TABLES"][$was_table_name]["INDEXES"]) ? $previous_definition["TABLES"][$was_table_name]["INDEXES"] : array());
				for($defined_indexes=array(),Reset($indexes),$index=0;$index<count($indexes);Next($indexes),$index++)
				{
					$index_name=Key($indexes);
					$was_index_name=$indexes[$index_name]["was"];
					if(IsSet($previous_indexes[$index_name])
					&& IsSet($previous_indexes[$index_name]["was"])
					&& !strcmp($previous_indexes[$index_name]["was"],$was_index_name))
						$was_index_name=$index_name;
					if(IsSet($previous_indexes[$was_index_name]))
					{
						$change=array();

						if(strcmp($was_index_name,$index_name))
						{
							$change["name"]=$was_index_name;
							MetabaseDebug($this->database,"Changed index '$was_index_name' name to '$index_name' in table '$table_name'");
						}
						if(IsSet($defined_indexes[$was_index_name]))
							return("the index '$was_index_name' was specified as base of more than one index of table '$table_name'");
						$defined_indexes[$was_index_name]=1;

						$previous_unique=IsSet($previous_indexes[$was_index_name]["unique"]);
						$unique=IsSet($indexes[$index_name]["unique"]);
						if($previous_unique!=$unique)
						{
							$change["ChangedUnique"]=1;
							if($unique)
								$change["unique"]=$unique;
							MetabaseDebug($this->database,"Changed index '$index_name' unique from $previous_unique to $unique in table '$table_name'");
						}

						$fields=$indexes[$index_name]["FIELDS"];
						$previous_fields=$previous_indexes[$was_index_name]["FIELDS"];
						for($defined_fields=array(),Reset($fields),$field=0;$field<count($fields);Next($fields),$field++)
						{
							$field_name=Key($fields);
							if(IsSet($previous_fields[$field_name]))
							{
								$defined_fields[$field_name]=1;
								$sorting=(IsSet($fields[$field_name]["sorting"]) ? $fields[$field_name]["sorting"] : "");
								$previous_sorting=(IsSet($previous_fields[$field_name]["sorting"]) ? $previous_fields[$field_name]["sorting"] : "");
								if(strcmp($sorting,$previous_sorting))
								{
									MetabaseDebug($this->database,"Changed index field '$field_name' sorting default from '$previous_sorting' to '$sorting' in table '$table_name'");
									$change["ChangedFields"]=1;
								}
							}
							else
							{
								$change["ChangedFields"]=1;
								MetabaseDebug($this->database,"Added field '$field_name' to index '$index_name' of table '$table_name'");
							}
						}
						for(Reset($previous_fields),$field=0;$field<count($previous_fields);Next($previous_fields),$field++)
						{
							$field_name=Key($previous_fields);
							if(!IsSet($defined_fields[$field_name]))
							{
								$change["ChangedFields"]=1;
								MetabaseDebug($this->database,"Removed field '$field_name' from index '$index_name' of table '$table_name'");
							}
						}

						if(count($change))
							$this->AddDefinitionChange($changes,"INDEXES",$table_name,array("ChangedIndexes"=>array($index_name=>$change)));

					}
					else
					{
						if(strcmp($index_name,$was_index_name))
							return("it was specified a previous index name ('$was_index_name') for index '$index_name' of table '$table_name' that does not exist");
						$this->AddDefinitionChange($changes,"INDEXES",$table_name,array("AddedIndexes"=>array($index_name=>$indexes[$index_name])));
						MetabaseDebug($this->database,"Added index '$index_name' to table '$table_name'");
					}
				}
				for(Reset($previous_indexes),$index=0;$index<count($previous_indexes);Next($previous_indexes),$index++)
				{
					$index_name=Key($previous_indexes);
					if(!IsSet($defined_indexes[$index_name]))
					{
						$this->AddDefinitionChange($changes,"INDEXES",$table_name,array("RemovedIndexes"=>array($index_name=>$was_table_name)));
						MetabaseDebug($this->database,"Removed index '$index_name' from table '$was_table_name'");
					}
				}
			}
			else
			{
				if(strcmp($table_name,$was_table_name))
					return("it was specified a previous table name ('$was_table_name') for table '$table_name' that does not exist");
				$this->AddDefinitionChange($changes,"TABLES",$table_name,array("Add"=>1));
				MetabaseDebug($this->database,"Added table '$table_name'");
			}
		}
		for(Reset($previous_definition["TABLES"]),$table=0;$table<count($previous_definition["TABLES"]);Next($previous_definition["TABLES"]),$table++)
		{
			$table_name=Key($previous_definition["TABLES"]);
			if(!IsSet($defined_tables[$table_name]))
			{
				$this->AddDefinitionChange($changes,"TABLES",$table_name,array("Remove"=>$previous_definition["TABLES"][$table_name]));
				MetabaseDebug($this->database,"Removed table '$table_name'");
			}
		}
		if(IsSet($this->database_definition["SEQUENCES"]))
		{
			for($defined_sequences=array(),Reset($this->database_definition["SEQUENCES"]),$sequence=0;$sequence<count($this->database_definition["SEQUENCES"]);Next($this->database_definition["SEQUENCES"]),$sequence++)
			{
				$sequence_name=Key($this->database_definition["SEQUENCES"]);
				$was_sequence_name=$this->database_definition["SEQUENCES"][$sequence_name]["was"];
				if(IsSet($previous_definition["SEQUENCES"][$sequence_name])
				&& IsSet($previous_definition["SEQUENCES"][$sequence_name]["was"])
				&& !strcmp($previous_definition["SEQUENCES"][$sequence_name]["was"],$was_sequence_name))
					$was_sequence_name=$sequence_name;
				if(IsSet($previous_definition["SEQUENCES"][$was_sequence_name]))
				{
					if(strcmp($was_sequence_name,$sequence_name))
					{
						$this->AddDefinitionChange($changes,"SEQUENCES",$was_sequence_name,array("name"=>$sequence_name));
						MetabaseDebug($this->database,"Renamed sequence '$was_sequence_name' to '$sequence_name'");
					}
					if(IsSet($defined_sequences[$was_sequence_name]))
						return("the sequence '$was_sequence_name' was specified as base of more than of sequence of the database");
					$defined_sequences[$was_sequence_name]=1;
					$change=array();
					if(strcmp($this->database_definition["SEQUENCES"][$sequence_name]["start"],$previous_definition["SEQUENCES"][$was_sequence_name]["start"]))
					{
						$change["start"]=$this->database_definition["SEQUENCES"][$sequence_name]["start"];
						MetabaseDebug($this->database,"Changed sequence '$sequence_name' start from '".$previous_definition["SEQUENCES"][$was_sequence_name]["start"]."' to '".$this->database_definition["SEQUENCES"][$sequence_name]["start"]."'");
					}
					if(IsSet($this->database_definition["SEQUENCES"][$sequence_name]["on"])
					&& (strcmp($this->database_definition["SEQUENCES"][$sequence_name]["on"]["table"],$previous_definition["SEQUENCES"][$was_sequence_name]["on"]["table"])
					|| strcmp($this->database_definition["SEQUENCES"][$sequence_name]["on"]["field"],$previous_definition["SEQUENCES"][$was_sequence_name]["on"]["field"])))
					{
						$change["on"]=$this->database_definition["SEQUENCES"][$sequence_name]["on"];
						MetabaseDebug($this->database,"Changed sequence '$sequence_name' on table field from '".$previous_definition["SEQUENCES"][$was_sequence_name]["on"]["table"].".".$previous_definition["SEQUENCES"][$was_sequence_name]["on"]["field"]."' to '".$this->database_definition["SEQUENCES"][$sequence_name]["on"]["table"].".".$this->database_definition["SEQUENCES"][$sequence_name]["on"]["field"]."'");
					}
					if(count($change))
						$this->AddDefinitionChange($changes,"SEQUENCES",$was_sequence_name,array("Change"=>array($sequence_name=>array($change))));
				}
				else
				{
					if(strcmp($sequence_name,$was_sequence_name))
						return("it was specified a previous sequence name ('$was_sequence_name') for sequence '$sequence_name' that does not exist");
					$this->AddDefinitionChange($changes,"SEQUENCES",$sequence_name,array("Add"=>1));
					MetabaseDebug($this->database,"Added sequence '$sequence_name'");
				}
			}
		}
		if(IsSet($previous_definition["SEQUENCES"]))
		{
			for(Reset($previous_definition["SEQUENCES"]),$sequence=0;$sequence<count($previous_definition["SEQUENCES"]);Next($previous_definition["SEQUENCES"]),$sequence++)
			{
				$sequence_name=Key($previous_definition["SEQUENCES"]);
				if(!IsSet($defined_sequences[$sequence_name]))
				{
					$this->AddDefinitionChange($changes,"SEQUENCES",$sequence_name,array("Remove"=>1));
					MetabaseDebug($this->database,"Removed sequence '$sequence_name'");
				}
			}
		}
		return("");
	}

	Function CheckTableAutoIncrement(&$table_definition)
	{
		$fields=$table_definition["FIELDS"];
		for($field=0, Reset($fields); $field<count($fields); Next($fields), $field++)
		{
			$name=Key($fields);
			if(IsSet($fields[$name]["autoincrement"]))
				return(1);
		}
		return(0);
	}

	Function AlterDatabase(&$previous_definition,&$changes)
	{
		if(IsSet($changes["TABLES"]))
		{
			for($change=0,Reset($changes["TABLES"]);$change<count($changes["TABLES"]);Next($changes["TABLES"]),$change++)
			{
				$table_name=Key($changes["TABLES"]);
				$name=(IsSet($changes["TABLES"][$table_name]["name"]) ? $changes["TABLES"][$table_name]["name"] : $table_name);
				if(IsSet($changes["TABLES"][$table_name]["Remove"]))
				{
					if(strcmp($error=$this->DropTable($changes["TABLES"][$table_name]["Remove"],1),""))
						return("database driver is not able to perform the requested alterations: ".$error);
					continue;
				}
				if($this->CheckTableAutoIncrement($this->database_definition["TABLES"][$name])
				&& !MetabaseSupport($this->database,"AutoIncrement"))
					return("database driver does not support table autoincrement fields");
				if(IsSet($changes["TABLES"][$table_name]["Add"]))
				{
					if(strcmp($error=$this->CreateTable($table_name,$this->database_definition["TABLES"][$name],1),""))
						return("database driver is not able to perform the requested alterations: ".$error);
				}
				elseif(!MetabaseAlterTable($this->database,$table_name,$changes["TABLES"][$table_name],1))
					return("database driver is not able to perform the requested alterations: ".MetabaseError($this->database));
			}
		}
		if(IsSet($changes["SEQUENCES"]))
		{
			if(!MetabaseSupport($this->database,"Sequences"))
				return("sequences are not supported");
			for($change=0,Reset($changes["SEQUENCES"]);$change<count($changes["SEQUENCES"]);Next($changes["SEQUENCES"]),$change++)
			{
				$sequence_name=Key($changes["SEQUENCES"]);
				if(IsSet($changes["SEQUENCES"][$sequence_name]["Add"])
				|| IsSet($changes["SEQUENCES"][$sequence_name]["Remove"])
				|| IsSet($changes["SEQUENCES"][$sequence_name]["Change"]))
					continue;
				return("some sequences changes are not yet supported");
			}
		}
		if(IsSet($changes["INDEXES"]))
		{
			if(!MetabaseSupport($this->database,"Indexes"))
				return("indexes are not supported");
			for($change=0,Reset($changes["INDEXES"]);$change<count($changes["INDEXES"]);Next($changes["INDEXES"]),$change++)
			{
				$table_name=Key($changes["INDEXES"]);
				$table_changes=count($changes["INDEXES"][$table_name]);
				if(IsSet($changes["INDEXES"][$table_name]["AddedIndexes"]))
					$table_changes--;
				if(IsSet($changes["INDEXES"][$table_name]["RemovedIndexes"]))
					$table_changes--;
				if(IsSet($changes["INDEXES"][$table_name]["ChangedIndexes"]))
					$table_changes--;
				if($table_changes)
					return("index alteration not yet supported");
			}
		}
		$previous_database_name=MetabaseSetDatabase($this->database,$this->database_definition["name"]);
		if(($support_transactions=MetabaseSupport($this->database,"Transactions"))
		&& !MetabaseAutoCommitTransactions($this->database,0))
			return(MetabaseError($this->database));
		$error="";
		$alterations=0;
		if(IsSet($changes["INDEXES"]))
		{
			for($change=0,Reset($changes["INDEXES"]);$change<count($changes["INDEXES"]);Next($changes["INDEXES"]),$change++)
			{
				$table_name=Key($changes["INDEXES"]);
				if(IsSet($changes["INDEXES"][$table_name]["RemovedIndexes"]))
				{
					$indexes=$changes["INDEXES"][$table_name]["RemovedIndexes"];
					for($index=0,Reset($indexes);$index<count($indexes);Next($indexes),$index++)
					{
						if(!MetabaseDropIndex($this->database,$indexes[Key($indexes)],Key($indexes)))
						{
							$error=MetabaseError($this->database);
							break;
						}
						$alterations++;
					}
				}
				if(!strcmp($error,"")
				&& IsSet($changes["INDEXES"][$table_name]["ChangedIndexes"]))
				{
					$indexes=$changes["INDEXES"][$table_name]["ChangedIndexes"];
					for($index=0,Reset($indexes);$index<count($indexes);Next($indexes),$index++)
					{
						$name=Key($indexes);
						$was_name=(IsSet($indexes[$name]["name"]) ? $indexes[$name]["name"] : $name);
						if(!MetabaseDropIndex($this->database,$table_name,$was_name))
						{
							$error=MetabaseError($this->database);
							break;
						}
						$alterations++;
					}
				}
				if(strcmp($error,""))
					break;
			}
		}
		if(!strcmp($error,"")
		&& IsSet($changes["TABLES"]))
		{
			for($change=0,Reset($changes["TABLES"]);$change<count($changes["TABLES"]);Next($changes["TABLES"]),$change++)
			{
				$table_name=Key($changes["TABLES"]);
				if(IsSet($changes["TABLES"][$table_name]["Remove"]))
				{
					if(!strcmp($error=$this->DropTable($changes["TABLES"][$table_name]["Remove"],0),""))
						$alterations++;
				}
				else
				{
					if(!IsSet($changes["TABLES"][$table_name]["Add"]))
					{
						if(!MetabaseAlterTable($this->database,$table_name,$changes["TABLES"][$table_name],0))
							$error=MetabaseError($this->database);
						else
							$alterations++;
					}
				}
				if(strcmp($error,""))
					break;
			}
			for($change=0,Reset($changes["TABLES"]);$change<count($changes["TABLES"]);Next($changes["TABLES"]),$change++)
			{
				$table_name=Key($changes["TABLES"]);
				if(IsSet($changes["TABLES"][$table_name]["Add"]))
				{
					if(!strcmp($error=$this->CreateTable($table_name,$this->database_definition["TABLES"][$table_name],0),""))
						$alterations++;
				}
				if(strcmp($error,""))
					break;
			}
		}
		if(!strcmp($error,"")
		&& IsSet($changes["SEQUENCES"]))
		{
			for($change=0,Reset($changes["SEQUENCES"]);$change<count($changes["SEQUENCES"]);Next($changes["SEQUENCES"]),$change++)
			{
				$sequence_name=Key($changes["SEQUENCES"]);
				if(IsSet($changes["SEQUENCES"][$sequence_name]["Add"]))
				{
					$created_on_table=0;
					if(IsSet($this->database_definition["SEQUENCES"][$sequence_name]["on"]))
					{
						$table=$this->database_definition["SEQUENCES"][$sequence_name]["on"]["table"];
						if(IsSet($changes["TABLES"])
						&& IsSet($changes["TABLES"][$table_name])
						&& IsSet($changes["TABLES"][$table_name]["Add"]))
							$created_on_table=1;
					}
					if(!strcmp($error=$this->CreateSequence($sequence_name,$this->database_definition["SEQUENCES"][$sequence_name],$created_on_table),""))
						$alterations++;
				}
				else
				{
					if(IsSet($changes["SEQUENCES"][$sequence_name]["Remove"]))
					{
						if(!strcmp($error=$this->DropSequence($sequence_name),""))
							$alterations++;
					}
					else
					{
						if(IsSet($changes["SEQUENCES"][$sequence_name]["Change"]))
						{
							$created_on_table=0;
							if(IsSet($this->database_definition["SEQUENCES"][$sequence_name]["on"]))
							{
								$table=$this->database_definition["SEQUENCES"][$sequence_name]["on"]["table"];
								if(IsSet($changes["TABLES"])
								&& IsSet($changes["TABLES"][$table_name])
								&& IsSet($changes["TABLES"][$table_name]["Add"]))
									$created_on_table=1;
							}
							if(!strcmp($error=$this->DropSequence($this->database_definition["SEQUENCES"][$sequence_name]["was"]),"")
							&& !strcmp($error=$this->CreateSequence($sequence_name,$this->database_definition["SEQUENCES"][$sequence_name],$created_on_table),""))
								$alterations++;
						}
						else
							$error="changing sequences is not yet supported";
					}
				}
				if(strcmp($error,""))
					break;
			}
		}
		if(!strcmp($error,"")
		&& IsSet($changes["INDEXES"]))
		{
			for($change=0,Reset($changes["INDEXES"]);$change<count($changes["INDEXES"]);Next($changes["INDEXES"]),$change++)
			{
				$table_name=Key($changes["INDEXES"]);
				if(IsSet($changes["INDEXES"][$table_name]["ChangedIndexes"]))
				{
					$indexes=$changes["INDEXES"][$table_name]["ChangedIndexes"];
					for($index=0,Reset($indexes);$index<count($indexes);Next($indexes),$index++)
					{
						if(!MetabaseCreateIndex($this->database,$table_name,Key($indexes),$this->database_definition["TABLES"][$table_name]["INDEXES"][Key($indexes)]))
						{
							$error=MetabaseError($this->database);
							break;
						}
						$alterations++;
					}
				}
				if(!strcmp($error,"")
				&& IsSet($changes["INDEXES"][$table_name]["AddedIndexes"]))
				{
					$indexes=$changes["INDEXES"][$table_name]["AddedIndexes"];
					for($index=0,Reset($indexes);$index<count($indexes);Next($indexes),$index++)
					{
						if(!MetabaseCreateIndex($this->database,$table_name,Key($indexes),$this->database_definition["TABLES"][$table_name]["INDEXES"][Key($indexes)]))
						{
							$error=MetabaseError($this->database);
							break;
						}
						$alterations++;
					}
				}
				if(strcmp($error,""))
					break;
			}
		}
		if($alterations
		&& strcmp($error,""))
		{
			if($support_transactions)
			{
				if(!MetabaseRollbackTransaction($this->database))
					$error="Could not rollback the partially implemented the requested database alterations: Rollback error: ".MetabaseError($this->database)." Alterations error: $error";
			}
			else
				$error="the requested database alterations were only partially implemented: $error";
		}
		if($support_transactions)
		{
			if(!MetabaseAutoCommitTransactions($this->database,1))
				$this->warnings[]="Could not end transaction after successfully implemented the requested database alterations: ".MetabaseError($this->database);
		}
		MetabaseSetDatabase($this->database,$previous_database_name);
		return($error);
	}

	Function EscapeSpecialCharacters($string)
	{
		if(GetType($string)!="string")
			$string=strval($string);
		for($escaped="",$character=0;$character<strlen($string);$character++)
		{
			switch($string[$character])
			{
				case "\"":
				case ">":
				case "<":
				case "&":
					$escaped.=HtmlEntities($string[$character]);
					break;
				default:
					$code=Ord($string[$character]);
					if($code<32
					|| $code>127)
					{
						$escaped.="&#$code;";
						break;
					}
					$escaped.=$string[$character];
					break;
			}
		}
		return($escaped);
	}

	Function DumpSequence($sequence_name,$output,$eol,$dump_definition)
	{
		$sequence_definition=$this->database_definition["SEQUENCES"][$sequence_name];
		if($dump_definition)
			$start=$sequence_definition["start"];
		else
		{
			if(MetabaseSupport($this->database,"GetSequenceCurrentValue"))
			{
				if(!MetabaseGetSequenceCurrentValue($this->database,$sequence_name,$start))
					return(0);
				$start++;
			}
			else
			{
				if(!MetabaseGetSequenceNextValue($this->database,$sequence_name,$start))
					return(0);
				$this->warnings[]="database does not support getting current sequence value, the sequence value was incremented";
			}
		}
		$output("$eol <sequence>$eol  <name>$sequence_name</name>$eol  <start>$start</start>$eol");
		if(IsSet($sequence_definition["on"]))
			$output("  <on>$eol   <table>".$sequence_definition["on"]["table"]."</table>$eol   <field>".$sequence_definition["on"]["field"]."</field>$eol  </on>$eol");
		$output(" </sequence>$eol");
		return(1);
	}

	Function DumpDatabase($arguments)
	{
		if(!IsSet($arguments["Output"]))
			return("it was not specified a valid output function");
		$output=$arguments["Output"];
		$eol=(IsSet($arguments["EndOfLine"]) ? $arguments["EndOfLine"] : "\n");
		$dump_definition=IsSet($arguments["Definition"]);
		$sequences=array();
		if(IsSet($this->database_definition["SEQUENCES"]))
		{
			for($error="",Reset($this->database_definition["SEQUENCES"]),$sequence=0;$sequence<count($this->database_definition["SEQUENCES"]);Next($this->database_definition["SEQUENCES"]),$sequence++)
			{
				$sequence_name=Key($this->database_definition["SEQUENCES"]);
				if(IsSet($this->database_definition["SEQUENCES"][$sequence_name]["on"]))
					$table=$this->database_definition["SEQUENCES"][$sequence_name]["on"]["table"];
				else
					$table="";
				$sequences[$table][]=$sequence_name;
			}
		}
		$previous_database_name=(strcmp($this->database_definition["name"],"") ? MetabaseSetDatabase($this->database,$this->database_definition["name"]) : "");
		$output("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>$eol");
		$output("<database>$eol$eol <name>".$this->database_definition["name"]."</name>$eol <create>".$this->database_definition["create"]."</create>$eol");
		for($error="",Reset($this->database_definition["TABLES"]),$table=0;$table<count($this->database_definition["TABLES"]);Next($this->database_definition["TABLES"]),$table++)
		{
			$table_name=Key($this->database_definition["TABLES"]);
			$output("$eol <table>$eol$eol  <name>$table_name</name>$eol");
			$output("$eol  <declaration>$eol");
			$fields=$this->database_definition["TABLES"][$table_name]["FIELDS"];
			for(Reset($fields),$field_number=0;$field_number<count($fields);$field_number++,Next($fields))
			{
				$field_name=Key($fields);
				$field=$fields[$field_name];
				if(!IsSet($field["type"]))
					return("it was not specified the type of the field \"$field_name\" of the table \"$table_name\"");
				$output("$eol   <field>$eol    <name>$field_name</name>$eol    <type>".$field["type"]."</type>$eol");
				switch($field["type"])
				{
					case "integer":
						if(IsSet($field["unsigned"]))
							$output("    <unsigned>1</unsigned>$eol");
						if(IsSet($field["autoincrement"]))
							$output("    <autoincrement>1</autoincrement>$eol");
						break;
					case "text":
					case "clob":
					case "blob":
						if(IsSet($field["length"]))
							$output("    <length>".$field["length"]."</length>$eol");
						break;
					case "boolean":
					case "date":
					case "timestamp":
					case "time":
					case "float":
					case "decimal":
						break;
					default:
						return("type \"".$field["type"]."\" is not yet supported");
				}
				if(IsSet($field["notnull"]))
					$output("    <notnull>1</notnull>$eol");
				if(IsSet($field["default"]))
					$output("    <default>".$this->EscapeSpecialCharacters($field["default"])."</default>$eol");
				$output("   </field>$eol");
			}

			if(IsSet($this->database_definition["TABLES"][$table_name]["PRIMARYKEY"]))
			{
				$output("$eol   <primarykey>$eol");
				$fields=$this->database_definition["TABLES"][$table_name]["PRIMARYKEY"]["FIELDS"];
				for(Reset($fields),$field_number=0;$field_number<count($fields);$field_number++,Next($fields))
				{
					$field_name=Key($fields);
					$field=$fields[$field_name];
					$output("    <field>$eol     <name>$field_name</name>$eol");
					if(IsSet($field["sorting"]))
						$output("     <sorting>".$field["sorting"]."</sorting>$eol");
					$output("    </field>$eol");
				}
				$output("   </primarykey>$eol");
			}

			if(IsSet($this->database_definition["TABLES"][$table_name]["INDEXES"]))
			{
				$indexes=$this->database_definition["TABLES"][$table_name]["INDEXES"];
				for(Reset($indexes),$index_number=0;$index_number<count($indexes);$index_number++,Next($indexes))
				{
					$index_name=Key($indexes);
					$index=$indexes[$index_name];
					$output("$eol   <index>$eol    <name>$index_name</name>$eol");
					if(IsSet($indexes[$index_name]["unique"]))
						$output("    <unique>1</unique>$eol");
					for(Reset($index["FIELDS"]),$field_number=0;$field_number<count($index["FIELDS"]);$field_number++,Next($index["FIELDS"]))
					{
						$field_name=Key($index["FIELDS"]);
						$field=$index["FIELDS"][$field_name];
						$output("    <field>$eol     <name>$field_name</name>$eol");
						if(IsSet($field["sorting"]))
							$output("     <sorting>".$field["sorting"]."</sorting>$eol");
						$output("    </field>$eol");
					}
					$output("   </index>$eol");
				}
			}

			$output("$eol  </declaration>$eol");
			if($dump_definition)
			{
				if(IsSet($this->database_definition["TABLES"][$table_name]["initialization"]))
				{
					$output("$eol  <initialization>$eol");
					$instructions=$this->database_definition["TABLES"][$table_name]["initialization"];
					for(Reset($instructions),$instruction=0;$instruction<count($instructions);$instruction++,Next($instructions))
					{
						switch($instructions[$instruction]["type"])
						{
							case "insert":
								$output("$eol   <insert>$eol");
								$fields=$instructions[$instruction]["FIELDS"];
								for(Reset($fields),$field_number=0;$field_number<count($fields);$field_number++,Next($fields))
								{
									$field_name=Key($fields);
									$output("$eol    <field>$eol     <name>$field_name</name>$eol     <value>".$this->EscapeSpecialCharacters($fields[$field_name])."</value>$eol    </field>$eol");
								}
								$output("$eol   </insert>$eol");
								break;
						}
					}
					$output("$eol  </initialization>$eol");
				}
			}
			else
			{
				if(count($this->database_definition["TABLES"][$table_name]["FIELDS"])==0)
					return("the definition of the table \"$table_name\" does not contain any fields");
				if(strcmp($error=$this->GetFields($table_name,$query_fields),""))
					return($error);
				if(($support_summary_functions=MetabaseSupport($this->database,"SummaryFunctions")))
				{
					if(($result=MetabaseQuery($this->database,"SELECT COUNT(*) FROM $table_name"))==0)
						return(MetabaseError($this->database));
					$rows=MetabaseFetchResult($this->database,$result,0,0);
					MetabaseFreeResult($this->database,$result);
				}
				if(($result=MetabaseQuery($this->database,"SELECT $query_fields FROM $table_name"))==0)
					return(MetabaseError($this->database));
				if(!$support_summary_functions)
					$rows=MetabaseNumberOfRows($this->database,$result);
				if($rows>0)
				{
					$fields=$this->database_definition["TABLES"][$table_name]["FIELDS"];
					$output("$eol  <initialization>$eol");
					for($row=0;$row<$rows;$row++)
					{
						$output("$eol   <insert>$eol");
						for(Reset($fields),$field_number=0;$field_number<count($fields);$field_number++,Next($fields))
						{
							$field_name=Key($fields);
							if(!MetabaseResultIsNull($this->database,$result,$row,$field_name))
							{
								$field=$fields[$field_name];
								$output("$eol    <field>$eol     <name>$field_name</name>$eol     <value>");
								switch($field["type"])
								{
									case "integer":
									case "text":
										$output($this->EscapeSpecialCharacters(MetabaseFetchResult($this->database,$result,$row,$field_name)));
										break;
									case "clob":
										if(!($lob=MetabaseFetchCLOBResult($this->database,$result,$row,$field_name)))
											return(MetabaseError($this->database));
										while(!MetabaseEndOfLOB($lob))
										{
											if(MetabaseReadLOB($lob,$data,8000)<0)
												return(MetabaseLOBError($lob));
											$output($this->EscapeSpecialCharacters($data));
										}
										MetabaseDestroyLOB($lob);
										break;
									case "blob":
										if(!($lob=MetabaseFetchBLOBResult($this->database,$result,$row,$field_name)))
											return(MetabaseError($this->database));
										while(!MetabaseEndOfLOB($lob))
										{
											if(MetabaseReadLOB($lob,$data,8000)<0)
												return(MetabaseLOBError($lob));
											$output(bin2hex($data));
										}
										MetabaseDestroyLOB($lob);
										break;
									case "float":
										$output($this->EscapeSpecialCharacters(MetabaseFetchFloatResult($this->database,$result,$row,$field_name)));
										break;
									case "decimal":
										$output($this->EscapeSpecialCharacters(MetabaseFetchDecimalResult($this->database,$result,$row,$field_name)));
										break;
									case "boolean":
										$output($this->EscapeSpecialCharacters(MetabaseFetchBooleanResult($this->database,$result,$row,$field_name)));
										break;
									case "date":
										$output($this->EscapeSpecialCharacters(MetabaseFetchDateResult($this->database,$result,$row,$field_name)));
										break;
									case "timestamp":
										$output($this->EscapeSpecialCharacters(MetabaseFetchTimestampResult($this->database,$result,$row,$field_name)));
										break;
									case "time":
										$output($this->EscapeSpecialCharacters(MetabaseFetchTimeResult($this->database,$result,$row,$field_name)));
										break;
									default:
										return("type \"".$field["type"]."\" is not yet supported");
								}
								$output("</value>$eol    </field>$eol");
							}
						}
						$output("$eol   </insert>$eol");
					}
					$output("$eol  </initialization>$eol");
				}
				MetabaseFreeResult($this->database,$result);
			}
			$output("$eol </table>$eol");
			if(IsSet($sequences[$table_name]))
			{
				for($sequence=0;$sequence<count($sequences[$table_name]);$sequence++)
				{
					if(!$this->DumpSequence($sequences[$table_name][$sequence],$output,$eol,$dump_definition))
						return(MetabaseError($this->database));
				}
			}
		}
		if(IsSet($sequences[""]))
		{
			for($sequence=0;$sequence<count($sequences[""]);$sequence++)
			{
				if(!$this->DumpSequence($sequences[""][$sequence],$output,$eol,$dump_definition))
					return(MetabaseError($this->database));
			}
		}
		$output("$eol</database>$eol");
		if(strcmp($previous_database_name,""))
			MetabaseSetDatabase($this->database,$previous_database_name);
		return($error);
	}

	Function ParseDatabaseDefinitionFile($input_file,&$database_definition,&$variables,$fail_on_invalid_names=1)
	{
		if(!($file=@fopen($input_file,"r")))
		{
			$error="Could not open input file \"$input_file\"";
			if(IsSet($php_errormsg))
				$error.=" (".$php_errormsg.")";
			return($error);
		}
		$parser=new metabase_parser_class;
		$parser->variables=$variables;
		$parser->fail_on_invalid_names=$fail_on_invalid_names;
		if(strcmp($error=$parser->ParseStream($file),""))
			$error.=" Line ".$parser->error_line." Column ".$parser->error_column." Byte index ".$parser->error_byte_index;
		else
			$database_definition=$parser->database;
		fclose($file);
		return($error);
	}

	Function DumpDatabaseChanges(&$changes)
	{
		if(IsSet($changes["TABLES"]))
		{
			for($change=0,Reset($changes["TABLES"]);$change<count($changes["TABLES"]);Next($changes["TABLES"]),$change++)
			{
				$table_name=Key($changes["TABLES"]);
				MetabaseDebug($this->database,"$table_name:");
				if(IsSet($changes["tables"][$table_name]["Add"]))
					MetabaseDebug($this->database,"\tAdded table '$table_name'");
				else
				{
					if(IsSet($changes["TABLES"][$table_name]["Remove"]))
						MetabaseDebug($this->database,"\tRemoved table '$table_name'");
					else
					{
						if(IsSet($changes["TABLES"][$table_name]["name"]))
							MetabaseDebug($this->database,"\tRenamed table '$table_name' to '".$changes["TABLES"][$table_name]["name"]."'");
						if(IsSet($changes["TABLES"][$table_name]["AddedFields"]))
						{
							$fields=$changes["TABLES"][$table_name]["AddedFields"];
							for($field=0,Reset($fields);$field<count($fields);$field++,Next($fields))
								MetabaseDebug($this->database,"\tAdded field '".Key($fields)."'");
						}
						if(IsSet($changes["TABLES"][$table_name]["RemovedFields"]))
						{
							$fields=$changes["TABLES"][$table_name]["RemovedFields"];
							for($field=0,Reset($fields);$field<count($fields);$field++,Next($fields))
								MetabaseDebug($this->database,"\tRemoved field '".Key($fields)."'");
						}
						if(IsSet($changes["TABLES"][$table_name]["RenamedFields"]))
						{
							$fields=$changes["TABLES"][$table_name]["RenamedFields"];
							for($field=0,Reset($fields);$field<count($fields);$field++,Next($fields))
								MetabaseDebug($this->database,"\tRenamed field '".Key($fields)."' to '".$fields[Key($fields)]["name"]."'");
						}
						if(IsSet($changes["TABLES"][$table_name]["ChangedFields"]))
						{
							$fields=$changes["TABLES"][$table_name]["ChangedFields"];
							for($field=0,Reset($fields);$field<count($fields);$field++,Next($fields))
							{
								$field_name=Key($fields);
								if(IsSet($fields[$field_name]["type"]))
									MetabaseDebug($this->database,"\tChanged field '$field_name' type to '".$fields[$field_name]["type"]."'");
								if(IsSet($fields[$field_name]["unsigned"]))
									MetabaseDebug($this->database,"\tChanged field '$field_name' type to '".($fields[$field_name]["unsigned"] ? "" : "not ")."unsigned'");
								if(IsSet($fields[$field_name]["autoincrement"]))
									MetabaseDebug($this->database,"\tChanged field '$field_name' type to '".($fields[$field_name]["autoincrement"] ? "" : "not ")."autoincrement'");
								if(IsSet($fields[$field_name]["length"]))
									MetabaseDebug($this->database,"\tChanged field '$field_name' length to '".($fields[$field_name]["length"]==0 ? "no length" : $fields[$field_name]["length"])."'");
								if(IsSet($fields[$field_name]["ChangedDefault"]))
									MetabaseDebug($this->database,"\tChanged field '$field_name' default to ".(IsSet($fields[$field_name]["default"]) ? "'".$fields[$field_name]["default"]."'" : "NULL"));
								if(IsSet($fields[$field_name]["ChangedNotNull"]))
									MetabaseDebug($this->database,"\tChanged field '$field_name' notnull to ".(IsSet($fields[$field_name]["notnull"]) ? "'1'" : "0"));
							}
						}
						if(IsSet($changes["TABLES"][$table_name]["RemovedPrimaryKey"]))
							MetabaseDebug($this->database,"\tRemoved primary key");
						if(IsSet($changes["TABLES"][$table_name]["AddedPrimaryKey"]))
							MetabaseDebug($this->database,"\tAdded primary key");
						if(IsSet($changes["TABLES"][$table_name]["ChangedPrimaryKey"]))
							MetabaseDebug($this->database,"\tChanged primary key");
					}
				}
			}
		}
		if(IsSet($changes["SEQUENCES"]))
		{
			for($change=0,Reset($changes["SEQUENCES"]);$change<count($changes["SEQUENCES"]);Next($changes["SEQUENCES"]),$change++)
			{
				$sequence_name=Key($changes["SEQUENCES"]);
				MetabaseDebug($this->database,"$sequence_name:");
				if(IsSet($changes["SEQUENCES"][$sequence_name]["Add"]))
					MetabaseDebug($this->database,"\tAdded sequence '$sequence_name'");
				else
				{
					if(IsSet($changes["SEQUENCES"][$sequence_name]["Remove"]))
						MetabaseDebug($this->database,"\tRemoved sequence '$sequence_name'");
					else
					{
						if(IsSet($changes["SEQUENCES"][$sequence_name]["name"]))
							MetabaseDebug($this->database,"\tRenamed sequence '$sequence_name' to '".$changes["SEQUENCES"][$sequence_name]["name"]."'");
						if(IsSet($changes["SEQUENCES"][$sequence_name]["Change"]))
						{
							$sequences=$changes["SEQUENCES"][$sequence_name]["Change"];
							for($s=0,Reset($sequences);$s<count($sequences);$s++,Next($sequences))
							{
								$name=Key($sequences);
								for($sequence=0;$sequence<count($sequences[$name]);$sequence++)
								{
									if(IsSet($sequences[$name][$sequence]["start"]))
										MetabaseDebug($this->database,"\tChanged sequence '$name' start to '".$sequences[$name][$sequence]["start"]."'");
									if(IsSet($sequences[$name][$sequence]["on"]))
										MetabaseDebug($this->database,"\tChanged sequence '$name' to on field '".$sequences[$name][$sequence]["on"]["field"]."' of table '".$sequences[$name][$sequence]["on"]["table"]."'");
								}
							}
						}
					}
				}
			}
		}
		if(IsSet($changes["INDEXES"]))
		{
			for($change=0,Reset($changes["INDEXES"]);$change<count($changes["INDEXES"]);Next($changes["INDEXES"]),$change++)
			{
				$table_name=Key($changes["INDEXES"]);
				MetabaseDebug($this->database,"$table_name:");
				if(IsSet($changes["INDEXES"][$table_name]["AddedIndexes"]))
				{
					$indexes=$changes["INDEXES"][$table_name]["AddedIndexes"];
					for($index=0,Reset($indexes);$index<count($indexes);Next($indexes),$index++)
						MetabaseDebug($this->database,"\tAdded index '".Key($indexes)."' of table '$table_name'");
				}
				if(IsSet($changes["INDEXES"][$table_name]["RemovedIndexes"]))
				{
					$indexes=$changes["INDEXES"][$table_name]["RemovedIndexes"];
					for($index=0,Reset($indexes);$index<count($indexes);Next($indexes),$index++)
						MetabaseDebug($this->database,"\tRemoved index '".Key($indexes)."' of table '".$indexes[Key($indexes)]."'");
				}
				if(IsSet($changes["INDEXES"][$table_name]["ChangedIndexes"]))
				{
					$indexes=$changes["INDEXES"][$table_name]["ChangedIndexes"];
					for($index=0,Reset($indexes);$index<count($indexes);Next($indexes),$index++)
					{
						if(IsSet($indexes[Key($indexes)]["name"]))
							MetabaseDebug($this->database,"\tRenamed index '".Key($indexes)."' to '".$indexes[Key($indexes)]["name"]."' on table '$table_name'");
						if(IsSet($indexes[Key($indexes)]["ChangedUnique"]))
							MetabaseDebug($this->database,"\tChanged index '".Key($indexes)."' unique to '".IsSet($indexes[Key($indexes)]["unique"])."' on table '$table_name'");
						if(IsSet($indexes[Key($indexes)]["ChangedFields"]))
							MetabaseDebug($this->database,"\tChanged index '".Key($indexes)."' on table '$table_name'");
					}
				}
			}
		}
	}

	Function UpdateDatabase($current_schema_file,$previous_schema_file,&$arguments,&$variables, $check=0)
	{
		if(strcmp($error=$this->ParseDatabaseDefinitionFile($current_schema_file,$this->database_definition,$variables,$this->fail_on_invalid_names),""))
		{
			$this->error="Could not parse database schema file: $error";
			return(0);
		}
		if(strcmp($error=$this->SetupDatabase($arguments),""))
		{
			$this->error="Could not setup database: $error";
			return(0);
		}
		$copy=0;
		if(file_exists($previous_schema_file))
		{
			if(!strcmp($error=$this->ParseDatabaseDefinitionFile($previous_schema_file,$database_definition,$variables,0),"")
			&& !strcmp($error=$this->CompareDefinitions($database_definition,$changes),"")
			&& count($changes))
			{
				if($check
				|| !strcmp($error=$this->AlterDatabase($database_definition,$changes),""))
				{
					
					$copy=!$check;
					$this->DumpDatabaseChanges($changes);
				}
			}
		}
		else
		{
			if(!$check
			&& !strcmp($error=$this->CreateDatabase(),""))
				$copy=1;
		}
		if(strcmp($error,""))
		{
			$this->error="Could not install database: $error";
			return(0);
		}
		if($copy
		&& !copy($current_schema_file,$previous_schema_file))
		{
			$this->error="could not copy the new database definition file to the current file";
			return(0);
		}
		return(1);
	}

	Function DumpDatabaseContents($schema_file,&$setup_arguments,&$dump_arguments,&$variables)
	{
		if(strcmp($error=$this->ParseDatabaseDefinitionFile($schema_file,$database_definition,$variables,$this->fail_on_invalid_names),""))
			return("Could not parse database schema file: $error");
		$this->database_definition=$database_definition;
		if(strcmp($error=$this->SetupDatabase($setup_arguments),""))
			return("Could not setup database: $error");
		return($this->DumpDatabase($dump_arguments));
	}

	Function GetDefinitionFromDatabase(&$arguments)
	{
		if(strcmp($error=$this->SetupDatabase($arguments),""))
			return($this->error="Could not setup database: $error");
		MetabaseSetDatabase($this->database,$database=MetabaseSetDatabase($this->database,""));
		if(strlen($database)==0)
			return("it was not specified a valid database name");
		$this->database_definition=array(
			"name"=>$database,
			"create"=>1,
			"TABLES"=>array()
		);
		if(!MetabaseListTables($this->database,$tables))
			return(MetabaseError($this->database));
		for($table=0;$table<count($tables);$table++)
		{
			$table_name=$tables[$table];
			if(!MetabaseListTableFields($this->database,$table_name,$fields))
				return(MetabaseError($this->database));
			$this->database_definition["TABLES"][$table_name]=array(
				"FIELDS"=>array()
			);
			for($field=0;$field<count($fields);$field++)
			{
				$field_name=$fields[$field];
				if(!MetabaseGetTableFieldDefinition($this->database,$table_name,$field_name,$definition))
					return(MetabaseError($this->database));
				$this->database_definition["TABLES"][$table_name]["FIELDS"][$field_name]=$definition[0];
			}
			if(!MetabaseListTableKeys($this->database,$table_name,1,$keys))
				return(MetabaseError($this->database));
			if(count($keys)
			&& !MetabaseGetTableKeyDefinition($this->database,$table_name,$keys[0],1,$this->database_definition["TABLES"][$table_name]["PRIMARYKEY"]))
				return(MetabaseError($this->database));
			if(!MetabaseListTableIndexes($this->database,$table_name,$indexes))
				return(MetabaseError($this->database));
			if(count($indexes))
			{
				$this->database_definition["TABLES"][$table_name]["INDEXES"]=array();
				for($index=0;$index<count($indexes);$index++)
				{
					$index_name=$indexes[$index];
					if(!MetabaseGetTableIndexDefinition($this->database,$table_name,$index_name,$definition))
						return(MetabaseError($this->database));
					$this->database_definition["TABLES"][$table_name]["INDEXES"][$index_name]=$definition;
				}
			}
		}
		if(!MetabaseListSequences($this->database,$sequences))
			return(MetabaseError($this->database));
		for($sequence=0;$sequence<count($sequences);$sequence++)
		{
			$sequence_name=$sequences[$sequence];
			if(!MetabaseGetSequenceDefinition($this->database,$sequence_name,$definition))
				return(MetabaseError($this->database));
			$this->database_definition["SEQUENCES"][$sequence_name]=$definition;
		}
		return("");
	}
};



?>
